﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeaQuizController : MonoBehaviour
{
    // Start is called before the first frame update
    List<int> questionQueueList = new List<int>();
    List<QuestionList> questionLists = new List<QuestionList>();
    public int limitTime = 60;
    private float time = 1;
    private int currentQuestionNum = 1;
    private int correctCount = 0;
    public float buttontimer;
    public bool buttontime = false;
    private bool isSelectA = false, isSelectB = false, isSelectC = false, isSelectD = false, isStart = false;
    private bool isResumebutton = false, isPausebutton = false, isStartbutton = false, isConfirmbutton = false , isQuitbutton; // button click handle
    private int animalsCount = 11;
    private bool canAnswer = true;


    // object 
    public GameObject obj_Octopus;//	Octopus
    public GameObject obj_Crab;//	Crab
    public GameObject obj_Starfish;//	Starfish
    public GameObject obj_Dolphin;//	Dolphin
    public GameObject obj_Sea_Urchin;//	Sea ​​urchin
    public GameObject obj_Moray_Eel;//	Moray eel
    public GameObject obj_Long_Finned_Pilot_Whale;//	Long finned pilot whale
    public GameObject obj_Killer_Whale;//	Killer whale
    public GameObject obj_Basking_Shark;//	Basking shark
    public GameObject obj_Great_White_Shark;//	Great white shark
    public GameObject obj_Squid;//	Squid
    public Text gui_time;    //time
    public Text gui_round;    //round
    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair
    public GameObject canvas_question;    //Question
    public GameObject canvas_pause;    //pause
    public GameObject obj_start_menu;    //start menu
    public Text gui_mark;    //start button
    public GameObject canvas_finish;    //finish canvas
    public Text gui_choiceA;
    public Text gui_choiceB;
    public Text gui_choiceC;
    public Text gui_choiceD;

    public GameObject btn_choiceA, btn_choiceB, btn_choiceC, btn_choiceD; // MC    



    void Start()
    {
        addQuestions(); // add on question on the List
    }

    // Update is called once per frame
    void Update()
    {
        //timer
        if (isStart)
        {
            time -= Time.deltaTime;
            if (time < 0)
            {
                limitTime--;
                time = 1;
                //Debug.Log(limitTime);
            }
            gui_time.text = "Time : " + limitTime;

            if (limitTime == 0)
            {
                currentQuestionNum++;
                if (questionLists.Count > currentQuestionNum)
                {
                    genQuestion();
                }
                limitTime = 60;
                gui_round.text = "Round : " + currentQuestionNum + "/" + questionQueueList.Count;
            }
        }

        buttonResponse();

        if (isStart && currentQuestionNum > questionQueueList.Count)
        {
            canvas_finish.SetActive(true);
            canvas_question.SetActive(false);
            isStart = false;
            gui_mark.text = "Mark : " + correctCount + "/" + questionQueueList.Count;
        }
    }

    private void buttonResponse()
    {
        // select button 
        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isSelectA) // A
        {
            canAnswer = false;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            isSelectA = false;
            checkAns("A");
            StartCoroutine(waiter());
        }
        else if (buttontimer >= 2 && isSelectB)
        {
            canAnswer = false;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isSelectB = false;
            obj_shoot.SetActive(true);
            checkAns("B");
            StartCoroutine(waiter());
        }
        else if (buttontimer >= 2 && isSelectC)
        {
            canAnswer = false;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isSelectC = false;
            obj_shoot.SetActive(true);
            checkAns("C");
            StartCoroutine(waiter());
        }
        else if (buttontimer >= 2 && isSelectD)
        {
            canAnswer = false;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isSelectD = false;
            obj_shoot.SetActive(true);
            checkAns("D");
            StartCoroutine(waiter());
        }
        else if (buttontimer >= 2 && isResumebutton)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            canvas_question.SetActive(true);
            canvas_pause.SetActive(false);
            canvas_question.SetActive(true);
            isStart = true;
            isResumebutton = false;
        }
        else if (buttontimer >= 2 && isPausebutton)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            canvas_pause.SetActive(true);
            canvas_question.SetActive(false);
            isStart = false;
            isPausebutton = false;
        }
        else if (buttontimer >= 2 && isStartbutton)
        {
            questionQueueList.Clear();
            currentQuestionNum = 1;
            correctCount = 0;
            limitTime = 60;
            genRandomQueue(); // gen the queue(number) of question
            for (int i = 0; i < questionQueueList.Count; i++)
            {
                Debug.Log("questionLists " + questionQueueList[i] + " question " + questionLists[questionQueueList[i]].getQuestion() + "  A  " + questionLists[questionQueueList[i]].getchoiceA() + "  B  " + questionLists[questionQueueList[i]].getchoiceB() + "  C  " + questionLists[questionQueueList[i]].getchoiceC() + "  D  " + questionLists[questionQueueList[i]].getchoiceD() + "  ans  " + questionLists[questionQueueList[i]].getans());
            }
            genQuestion();
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            canvas_question.SetActive(true);
            canvas_pause.SetActive(false);
            obj_start_menu.SetActive(false);
            gui_round.text = "Round : " + currentQuestionNum + "/" + questionQueueList.Count;
            isStart = true;
            isResumebutton = false;
        }
        else if (buttontimer >= 2 && isConfirmbutton)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            canvas_question.SetActive(false);
            canvas_pause.SetActive(false);
            obj_start_menu.SetActive(true);
            canvas_finish.SetActive(false);
            isStart = false;
            isConfirmbutton = false;
        }
        else if (buttontimer >= 2 && isQuitbutton)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            canvas_question.SetActive(false);
            canvas_pause.SetActive(false);
            obj_start_menu.SetActive(true);
            canvas_finish.SetActive(false);
            isStart = false;
            isQuitbutton = false;
        }
    }

    public void genRandomQueue()
    {
        // gen Question Queue

        List<int> list = new List<int>();
        for (int i = 0; i < animalsCount; i++)
        {
            list.Add(i);
        }
        for (int j = 0; j < animalsCount; j++)
        {
            int n = Random.Range(1, list.Count);
            questionQueueList.Add(list[n - 1]);
            list.RemoveAt(n - 1);
        }
        int random = Random.Range(1, questionQueueList.Count - 1);
        int temp = questionQueueList[questionQueueList.Count - 1];
        questionQueueList[random] = temp;
        questionQueueList.RemoveAt(questionQueueList.Count - 1);

    }

    public void addQuestions()
    {
        List<string> animalsList = new List<string>();
        animalsList.Add("Octopus");
        animalsList.Add("Crab");
        animalsList.Add("Sea ​​urchin");
        animalsList.Add("Starfish");
        animalsList.Add("Dolphin");
        animalsList.Add("Moray eel");
        animalsList.Add("Long finned pilot whale");
        animalsList.Add("Killer whale");
        animalsList.Add("Basking shark");
        animalsList.Add("Great white shark");
        animalsList.Add("Squid");
        for (int i = 0; i < animalsList.Count; i++)
        {
            List<int> list = new List<int>();
            for (int y = 0; y < animalsList.Count; y++)
            {
                list.Add(y);
            }
            QuestionList question = new QuestionList();
            question.setQuestion("Which name of this animal?");
            question.setans(animalsList[i]);
            list.RemoveAt(i);
            int random1 = Random.Range(0, list.Count);
            int choice1 = list[random1];
            list.RemoveAt(random1);
            int random2 = Random.Range(0, list.Count);
            int choice2 = list[random2];
            list.RemoveAt(random2);
            int random3 = Random.Range(0, list.Count);
            int choice3 = list[random3];
            list.RemoveAt(random3);
            int ansNum = Random.Range(1, 4);
            Debug.Log("list " + i + " " + " " + choice1 + " " + choice2 + "  " + choice3);
            Debug.Log("list " + animalsList[i] + " " + " " + animalsList[choice1] + " " + animalsList[choice2] + "  " + animalsList[choice3]);
            switch (ansNum)
            {
                case 1:
                    question.setchoiceA(animalsList[i]);
                    question.setchoiceB(animalsList[choice1]);
                    question.setchoiceC(animalsList[choice2]);
                    question.setchoiceD(animalsList[choice3]);
                    Debug.Log("list " + ansNum + "  " + animalsList[i] + " " + " " + animalsList[choice1] + " " + animalsList[choice2] + "  " + animalsList[choice3]);
                    break;
                case 2:
                    question.setchoiceA(animalsList[choice1]);
                    question.setchoiceB(animalsList[i]);
                    question.setchoiceC(animalsList[choice2]);
                    question.setchoiceD(animalsList[choice3]);
                    Debug.Log("list " + ansNum + "  " + animalsList[i] + " " + " " + animalsList[choice1] + " " + animalsList[choice2] + "  " + animalsList[choice3]);
                    break;
                case 3:
                    question.setchoiceA(animalsList[choice1]);
                    question.setchoiceB(animalsList[choice2]);
                    question.setchoiceC(animalsList[i]);
                    question.setchoiceD(animalsList[choice3]);
                    Debug.Log("list " + ansNum + "  " + animalsList[i] + " " + " " + animalsList[choice1] + " " + animalsList[choice2] + "  " + animalsList[choice3]);
                    break;
                case 4:
                    question.setchoiceA(animalsList[choice1]);
                    question.setchoiceB(animalsList[choice2]);
                    question.setchoiceC(animalsList[choice3]);
                    question.setchoiceD(animalsList[i]);
                    Debug.Log("list " + ansNum + "  " + animalsList[i] + " " + " " + animalsList[choice1] + " " + animalsList[choice2] + "  " + animalsList[choice3]);
                    break;

            }
            questionLists.Add(question);
        }
        /*for (int i = 0; i < questionLists.Count; i++)
        {
            Debug.Log("questionLists " + i + " question " + questionLists[i].getQuestion() + "  A  " + questionLists[i].getchoiceA() + "  B  " + questionLists[i].getchoiceB() + "  C  " + questionLists[i].getchoiceC() + "  D  " + questionLists[i].getchoiceD() + "  ans  " + questionLists[i].getans());
        }*/
    }

    public void genQuestion()
    {
        Debug.Log("current " + currentQuestionNum);
        obj_Octopus.SetActive(false);
        obj_Crab.SetActive(false);
        obj_Starfish.SetActive(false);
        obj_Dolphin.SetActive(false);
        obj_Sea_Urchin.SetActive(false);
        obj_Moray_Eel.SetActive(false);
        obj_Long_Finned_Pilot_Whale.SetActive(false);
        obj_Killer_Whale.SetActive(false);
        obj_Basking_Shark.SetActive(false);
        obj_Great_White_Shark.SetActive(false);
        obj_Squid.SetActive(false);

        gui_choiceA.text = questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceA();
        gui_choiceB.text = questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceB();
        gui_choiceC.text = questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceC();
        gui_choiceD.text = questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceD();
        Debug.Log("ans " + questionLists[questionQueueList[currentQuestionNum - 1]].getans());
        switch (questionLists[questionQueueList[currentQuestionNum - 1]].getans())
        {
            case "Octopus":
                obj_Octopus.SetActive(true);
                break;
            case "Crab":
                obj_Crab.SetActive(true);
                break;
            case "Sea ​​urchin":
                obj_Sea_Urchin.SetActive(true);
                break;
            case "Starfish":
                obj_Starfish.SetActive(true);
                break;
            case "Dolphin":
                obj_Dolphin.SetActive(true);
                break;
            case "Moray eel":
                obj_Moray_Eel.SetActive(true);
                break;
            case "Long finned pilot whale":
                obj_Long_Finned_Pilot_Whale.SetActive(true);
                break;
            case "Killer whale":
                obj_Killer_Whale.SetActive(true);
                break;
            case "Basking shark":
                obj_Basking_Shark.SetActive(true);
                break;
            case "Great white shark":
                obj_Great_White_Shark.SetActive(true);
                break;
            case "Squid":
                obj_Squid.SetActive(true);
                break;
        }
    }

    public void checkAns(string selection)
    {
        switch (selection)
        {
            case "A":
                if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceA().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
                {
                    btn_choiceA.GetComponent<Image>().color = Color.green;
                    Debug.Log("correct");
                    correctCount++;
                    currentQuestionNum++;
                }
                else
                {
                    btn_choiceA.GetComponent<Image>().color = Color.red;
                    Debug.Log("wrong");
                    showCorrectAns();
                    currentQuestionNum++;
                }
                break;
            case "B":
                if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceB().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
                {
                    btn_choiceB.GetComponent<Image>().color = Color.green;
                    Debug.Log("correct");
                    correctCount++;
                    currentQuestionNum++;
                }
                else
                {
                    btn_choiceB.GetComponent<Image>().color = Color.red;
                    Debug.Log("wrong");
                    showCorrectAns();
                    currentQuestionNum++;
                }
                break;
            case "C":
                if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceC().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
                {
                    btn_choiceC.GetComponent<Image>().color = Color.green;
                    Debug.Log("correct");
                    correctCount++;
                    currentQuestionNum++;
                }
                else
                {
                    btn_choiceC.GetComponent<Image>().color = Color.red;
                    Debug.Log("wrong");
                    showCorrectAns();
                    currentQuestionNum++;
                }
                break;
            case "D":
                if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceD().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
                {
                    btn_choiceD.GetComponent<Image>().color = Color.green;
                    Debug.Log("correct");
                    correctCount++;
                    currentQuestionNum++;
                }
                else
                {
                    btn_choiceD.GetComponent<Image>().color = Color.red;
                    Debug.Log("wrong");
                    showCorrectAns();
                    currentQuestionNum++;
                }
                break;
        }

    }

    public void showCorrectAns()
    {
        if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceA().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
        {
            btn_choiceA.GetComponent<Image>().color = Color.green;
        }
        else if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceB().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
        {
            btn_choiceB.GetComponent<Image>().color = Color.green;
        }
        else if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceC().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
        {
            btn_choiceC.GetComponent<Image>().color = Color.green;
        }
        else if (questionLists[questionQueueList[currentQuestionNum - 1]].getchoiceD().Equals(questionLists[questionQueueList[currentQuestionNum - 1]].getans()))
        {
            btn_choiceD.GetComponent<Image>().color = Color.green;
        }
    }

    public void selectA()
    {
        if (canAnswer)
        {
            isSelectA = true;
            isSelectB = false;
            isSelectC = false;
            isSelectD = false;
            obj_shoot.SetActive(false);
            buttontime = true;
        }

    }

    public void selectB()
    {
        if (canAnswer)
        {
            isSelectA = false;
            isSelectB = true;
            isSelectC = false;
            isSelectD = false;
            obj_shoot.SetActive(false);
            buttontime = true;
        }
    }

    public void selectC()
    {
        if (canAnswer)
        {
            isSelectA = false;
            isSelectB = false;
            isSelectC = true;
            isSelectD = false;
            obj_shoot.SetActive(false);
            buttontime = true;
        }
    }

    public void selectD()
    {
        if (canAnswer)
        {
            isSelectA = false;
            isSelectB = false;
            isSelectC = false;
            isSelectD = true;
            obj_shoot.SetActive(false);
            buttontime = true;
        }
    }

    public void btn_resume()
    {
        isResumebutton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_pause()
    {
        isPausebutton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_start()
    {
        isStartbutton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_confirm()
    {
        isConfirmbutton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_quit()
    {
        isQuitbutton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }


    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        isSelectA = false;
        isSelectB = false;
        isSelectC = false;
        isSelectD = false;
        isPausebutton = false;
        isResumebutton = false;
        isStartbutton = false;
        isQuitbutton = false;
        buttontime = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        obj_shoot.SetActive(true);
        //取消select個button
    }

    private IEnumerator waiter()
    {
        yield return new WaitForSeconds(2.0F);
        btn_choiceA.GetComponent<Image>().color = Color.white;
        btn_choiceB.GetComponent<Image>().color = Color.white;
        btn_choiceC.GetComponent<Image>().color = Color.white;
        btn_choiceD.GetComponent<Image>().color = Color.white;
        if (questionLists.Count > currentQuestionNum)
        {
            genQuestion();
        }
        limitTime = 60;
        gui_round.text = "Round : " + currentQuestionNum + "/" + questionQueueList.Count;
        canAnswer = true;

    }

}
