﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public float buttontimer;
    public bool buttontime = false;
    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair

    private bool isLow = false, isMid = false, isHigh = false;


    // Use this for initialization
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;  //the monitor never close
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true; //the screen rotation
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    // Update is called once per frame
    void Update()
    {
    

        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isMid)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("SeaScenes");
            isMid = false;
        }
        else if (buttontimer >= 2 && isHigh)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("AridAreaScenes");
            isHigh = false;
        }
        else if (buttontimer >= 2 && isLow)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("ForestScenes");
            isHigh = false;
        }
    }



    public void goToMid()
    {
        isMid = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void goToLow()
    {
        isLow = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void goToHigh()
    {
        isHigh = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        buttontime = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        isMid = false;
        isLow = false;
        isHigh = false;
        obj_shoot.SetActive(true);
        //取消select個button
    }
}
