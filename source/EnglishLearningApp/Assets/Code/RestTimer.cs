﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestTimer : MonoBehaviour
{
    public static float timer;
    public static bool isStop = false;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (isStop == false)
        {
            Debug.Log("RestTimer" + timer);
            timer += Time.deltaTime;
        }

    }

}
