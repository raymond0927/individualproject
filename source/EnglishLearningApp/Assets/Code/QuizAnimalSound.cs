﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizAnimalSound : MonoBehaviour
{

    public AudioClip sound_animal;
    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void forcus()
    {
            if (sound_animal != null)
            {
                source.PlayOneShot(sound_animal);
            }
    }
}
