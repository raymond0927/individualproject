﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeaAnimalController : MonoBehaviour
{
    private Transform player;
    private Transform originalLocation;
    public float buttontimer;
    public bool buttontime = false;
    public static bool isEnable = true;
    public double arrivedPosition = 4;
    private double Runtimer = 0;   // run time
    private Boolean walk = false;
    private double backtimer = 0;   // back time
    private bool isConfirmButton = false;
    private bool isSoundButton = false;
    private bool isTranslateButton = false;
    private bool isBack = false;
    private bool isEng = true;

    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair
    public GameObject obj_animals;    //animal
    public GameObject obj_original;    //originalLocation
    public GameObject obj_infocanvas;    //infocanvas
    public Text gui_title;   //show the title and info
    public Text gui_message;
    public String str_title;
    public String str_title_chi;
    public String str_message;
    public String str_message_chi;
    public AudioClip sound_pronunciation;
    private AudioSource source { get { return GetComponent<AudioSource>(); } }




    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();        // 拎player既方位
        originalLocation = GameObject.Find(obj_original.name).GetComponent<Transform>();
        gui_title.text = str_title + " (" + str_title_chi + ") ";
        gui_message.text = str_message;
        gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
        source.volume = 1;

    }

    // Update is called once per frame
    void Update()
    {

        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isConfirmButton) // back
        {
            //AnimalsController.isEnable = false;
            isBack = true;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            obj_infocanvas.SetActive(false);
            Debug.Log("done");
        }
        else if (buttontimer >= 2 && isSoundButton)
        {
            source.PlayOneShot(sound_pronunciation);
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isSoundButton = false;
            obj_shoot.SetActive(true);
        }
        else if (buttontimer >= 2 && isTranslateButton)
        {
            if (isEng)
            {
                gui_message.text = str_message_chi;
                isEng = false;
            }
            else
            {
                gui_message.text = str_message;
                isEng = true;
            }
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isTranslateButton = false;
            obj_shoot.SetActive(true);
        }
        else if (buttontimer >= 2)
        {
            AnimalsController.isEnable = false;
            walk = true;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            Debug.Log("done");
        }

        // go to player
        if (walk)
        {
            Runtimer -= Time.deltaTime;
            if (Runtimer < 0)
            {
                AnimalsController.isEnable = false;
                transform.LookAt(player);
                transform.position = Vector3.MoveTowards(transform.position, player.position, 1 * Time.deltaTime);
                Runtimer = 0.01;
            }

            if ((transform.position.x > -arrivedPosition) && (transform.position.x < arrivedPosition) && (transform.position.z < arrivedPosition) && (transform.position.z > -arrivedPosition)) // arrived 
            {
                //AnimalsAnimation.isWalk = false;

                walk = false;
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0); // lock x and z axis to zero
                obj_infocanvas.SetActive(true);
                //AnimalsController.isEnable = true;
                SeaHideAnimalController.hideAnimals(str_title);
            }

        }


        // Back
        if (isBack)
        {
            Runtimer -= Time.deltaTime;
            backtimer += Time.deltaTime;
            if (Runtimer < 0)
            {
                //AnimalsAnimation.isWalk = true;

                transform.LookAt(originalLocation);
                transform.position = Vector3.MoveTowards(transform.position, originalLocation.position, 1 * Time.deltaTime);
                Runtimer = 0.01;
            }

            if (backtimer >= 2)// arrived 
            {
                transform.localPosition = new Vector3(0, 0, 0);
                transform.localRotation = Quaternion.Euler(0, 0, 0); 
                //transform.LookAt(player);
                //AnimalsAnimation.isWalk = false;

                isBack = false;
                isConfirmButton = false;
                backtimer = 0;
                AnimalsController.isEnable = true;
                SeaHideAnimalController.hideAnimals("");
            }
        }

    }



    public void forcus()
    {
        if (AnimalsController.isEnable)
        {

            Debug.Log("forcus");
            obj_shoot.SetActive(false);
            buttontime = true;
        }

    }

    public void confirmButton()
    {

        Debug.Log("confirmButton");
        isConfirmButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;

    }



    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        isConfirmButton = false;
        buttontime = false;
        isTranslateButton = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        obj_shoot.SetActive(true);
        //取消select個button
    }

    public void playsound()
    {
        isSoundButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_translate()
    {
        isTranslateButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }
}
