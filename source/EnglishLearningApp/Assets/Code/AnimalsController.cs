﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class AnimalsController : MonoBehaviour
{
    private Transform player;
    private Transform originalLocation;
    public float buttontimer;
    public bool buttontime = false;
    public static bool isEnable = true;
    public double arrivedPosition = 4;
    private double Runtimer = 0;   // run time
    private Boolean walk = false;
    private double backtimer = 0;   // back time
    private bool isConfirmButton = false;
    private bool isSoundButton = false;
    private bool isTranslateButton = false;
    private bool isBack = false;
    private bool isEng = true;

    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair
    public GameObject obj_animals;    //animal
    public GameObject obj_original;    //originalLocation
    public GameObject obj_infocanvas;    //infocanvas
    public Text gui_title;   //show the title and info
    public Text gui_message;
    public String str_title;
    public String str_title_chi;
    public String str_message;
    public String str_message_chi;
    public AudioClip sound_pronunciation;
    public AudioClip sound_animal;
    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    public Animator Anim;
    public AnimatorStateInfo BS;
    public Animation anima_tion;

    public String str_animation_walk = "walk";
    public String str_animation_idle = "idle";

    String sceneName;

    // Use this for initialization
    void Start()
    {
        Scene  m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        player = GameObject.Find("Player").GetComponent<Transform>();        // 拎player既方位
        originalLocation = GameObject.Find(obj_original.name).GetComponent<Transform>();
        gui_title.text = str_title + " (" + str_title_chi + ") ";
        gui_message.text = str_message;
        gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
    
    }

    // Update is called once per frame
    void Update()
    {

        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isConfirmButton) // back
        {
            //AnimalsController.isEnable = false;
            isBack = true;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            obj_infocanvas.SetActive(false);
            Debug.Log("done");
        }
        else if (buttontimer >= 2 && isSoundButton)
        {
            source.PlayOneShot(sound_pronunciation);
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isSoundButton = false;
            obj_shoot.SetActive(true);
        }
        else if (buttontimer >= 2 && isTranslateButton)
        {
            if (isEng)
            {
                gui_message.text = str_message_chi;
                isEng = false;
            }
            else
            {
                gui_message.text = str_message;
                isEng = true;
            }
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            isTranslateButton = false;
            obj_shoot.SetActive(true);
        }
        else if (buttontimer >= 2)
        {
            AnimalsController.isEnable = false;
            walk = true;
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            Debug.Log("done");
        }

        // go to player
        if (walk)
        {
            Runtimer -= Time.deltaTime;
            if (Runtimer < 0)
            {
                //AnimalsAnimation.isWalk = true;
                if (anima_tion != null)
                {
                    anima_tion.Play(str_animation_walk, PlayMode.StopAll);
                }
                else
                {
                    Anim.SetBool("isWalk", true);
                }


                transform.LookAt(player);
                transform.position = Vector3.MoveTowards(transform.position, player.position, 1 * Time.deltaTime);
                Runtimer = 0.01;
            }

            if ((transform.position.x > -arrivedPosition) && (transform.position.x < arrivedPosition) && (transform.position.z < arrivedPosition) && (transform.position.z > -arrivedPosition)) // arrived 
            {
                //AnimalsAnimation.isWalk = false;

                if (anima_tion != null)
                {
                    anima_tion.Play(str_animation_idle, PlayMode.StopAll);
                }
                else
                {
                    Anim.SetBool("isWalk", false);
                }

                walk = false;
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0); // lock x and z axis to zero
                obj_infocanvas.SetActive(true);
                //AnimalsController.isEnable = true;
                if (sceneName.Equals("AridAreaScenes"))
                {
                    AridHideAnimalController.hideAnimals(str_title);
                }
                else if (sceneName.Equals("ForestScenes"))
                {
                    ForestHideAnimalController.hideAnimals(str_title);
                }
            }

        }


        // Back
        if (isBack)
        {
            Runtimer -= Time.deltaTime;
            backtimer += Time.deltaTime;
            if (Runtimer < 0)
            {
                //AnimalsAnimation.isWalk = true;
                if (anima_tion != null)
                {
                    anima_tion.Play(str_animation_walk, PlayMode.StopAll);
                }
                else
                {
                    Anim.SetBool("isWalk", true);
                }

                transform.LookAt(originalLocation);
                transform.position = Vector3.MoveTowards(transform.position, originalLocation.position, 1 * Time.deltaTime);
                Runtimer = 0.01;
            }

            if (backtimer >= 1.5)// arrived 
            {
                transform.localPosition = new Vector3(0, 1, 0);
                transform.LookAt(player);
                //AnimalsAnimation.isWalk = false;
                if (anima_tion != null)
                {
                    anima_tion.Play(str_animation_idle, PlayMode.StopAll);
                }
                else
                {
                    Anim.SetBool("isWalk", false);
                }
                isBack = false;
                isConfirmButton = false;
                backtimer = 0;
                AnimalsController.isEnable = true;
                if (sceneName.Equals("AridAreaScenes"))
                {
                    AridHideAnimalController.hideAnimals("");
                }
                else if (sceneName.Equals("ForestScenes"))
                {
                    ForestHideAnimalController.hideAnimals("");
                }
            }
        }

    }



    public void forcus()
    {
        if (isEnable)
        {
            if (sound_animal!=null && !source.isPlaying)
            {
                source.PlayOneShot(sound_animal);
            }
            Debug.Log("forcus");
            obj_shoot.SetActive(false);
            buttontime = true;
        }

    }

    public void confirmButton()
    {

        Debug.Log("confirmButton");
        isConfirmButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;

    }



    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        isConfirmButton = false;
        isTranslateButton = false;
        isSoundButton = false;
        buttontime = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        obj_shoot.SetActive(true);
        //取消select個button
    }

    public void playsound()
    {
        isSoundButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void btn_translate()
    {
        isTranslateButton = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }
}