﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionList 
{
    private String question;
    private String choiceA;
    private String choiceB;
    private String choiceC;
    private String choiceD;
    private String ans;

    public QuestionList()
    {
        this.question = "";
        this.choiceA = "";
        this.choiceB = "";
        this.choiceC = "";
        this.choiceD = "";
        this.ans = "";
    }

    public QuestionList (String question, String choiceA, String choiceB, String choiceC, String choiceD, String ans)
    {
        this.question = question;
        this.choiceA = choiceA;
        this.choiceB = choiceB;
        this.choiceC = choiceC;
        this.choiceD = choiceD;
        this.ans = ans;
    }

    public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public String getchoiceA()
    {
        return choiceA;
    }

    public void setchoiceA(String choiceA)
    {
        this.choiceA = choiceA;
    }

    public String getchoiceB()
    {
        return choiceB;
    }

    public void setchoiceB(String choiceB)
    {
        this.choiceB = choiceB;
    }

    public String getchoiceC()
    {
        return choiceC;
    }

    public void setchoiceC(String choiceC)
    {
        this.choiceC = choiceC;
    }

    public String getchoiceD()
    {
        return choiceD;
    }

    public void setchoiceD(String choiceD)
    {
        this.choiceD = choiceD;
    }

    public String getans()
    {
        return ans;
    }

    public void setans(String ans)
    {
        this.ans = ans;
    }
}
