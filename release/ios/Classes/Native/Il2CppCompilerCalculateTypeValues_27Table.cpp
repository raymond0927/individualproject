﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct PopsicleList_1_t95715200FE3EBBDE1714079383707539CB8BC801;
// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct PopsicleList_1_t14390BBA7993164E3E4B3BAEE383DBA40E2C8120;
// Gvr.Internal.ControllerState
struct ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451;
// Gvr.Internal.IHeadsetProvider
struct IHeadsetProvider_tA4539CD65E957C0594B61DC42979FB952ECAD03A;
// Gvr.Internal.IKeyboardProvider
struct IKeyboardProvider_t73111A37B5BAE0F3363AE280A4FD70315EA16EFC;
// GvrControllerHand[]
struct GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157;
// GvrHeadset
struct GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799;
// GvrHeadset/OnRecenterEvent
struct OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A;
// GvrHeadset/OnSafetyRegionEvent
struct OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6;
// GvrKeyboard
struct GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22;
// GvrKeyboard/EditTextCallback
struct EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A;
// GvrKeyboard/ErrorCallback
struct ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1;
// GvrKeyboard/StandardCallback
struct StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9;
// GvrKeyboardDelegateBase
struct GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231;
// KeyboardState
struct KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`2<System.String,System.String>
struct Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<Gvr.Internal.EmulatorTouchEvent/Pointer>
struct List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D;
// System.Collections.Generic.List`1<GvrKeyboardEvent>
struct List_1_t18E8F7C1D3B24CBB1ACFC0EEE5F30E81390C8ED3;
// System.Collections.Generic.List`1<System.Action`1<System.Int32>>
struct List_1_tE6DF805E6081D53922B71B07436272A8E4D07156;
// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>>
struct List_1_t9977425BDAA7A83F2247096EC315496A7FFEA1B7;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t9CE24C9765CEA576BA5850425955BF1016C0B607;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// proto.PhoneEvent
struct PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66;
// proto.PhoneEvent/Types/AccelerometerEvent
struct AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F;
// proto.PhoneEvent/Types/DepthMapEvent
struct DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571;
// proto.PhoneEvent/Types/GyroscopeEvent
struct GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764;
// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C;
// proto.PhoneEvent/Types/MotionEvent
struct MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1;
// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTBUILDERLITE_2_TC0AF8A3076FA603E0862880D0914C27154155846_H
#define ABSTRACTBUILDERLITE_2_TC0AF8A3076FA603E0862880D0914C27154155846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent_Builder>
struct  AbstractBuilderLite_2_tC0AF8A3076FA603E0862880D0914C27154155846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_TC0AF8A3076FA603E0862880D0914C27154155846_H
#ifndef ABSTRACTBUILDERLITE_2_T58AD69F86F731BCD90D64984E23BB16A359629B1_H
#define ABSTRACTBUILDERLITE_2_T58AD69F86F731BCD90D64984E23BB16A359629B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_AccelerometerEvent,proto.PhoneEvent_Types_AccelerometerEvent_Builder>
struct  AbstractBuilderLite_2_t58AD69F86F731BCD90D64984E23BB16A359629B1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T58AD69F86F731BCD90D64984E23BB16A359629B1_H
#ifndef ABSTRACTBUILDERLITE_2_TF8C2D6945B6CF9F72EB0A520850111D04C84B4BC_H
#define ABSTRACTBUILDERLITE_2_TF8C2D6945B6CF9F72EB0A520850111D04C84B4BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_DepthMapEvent,proto.PhoneEvent_Types_DepthMapEvent_Builder>
struct  AbstractBuilderLite_2_tF8C2D6945B6CF9F72EB0A520850111D04C84B4BC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_TF8C2D6945B6CF9F72EB0A520850111D04C84B4BC_H
#ifndef ABSTRACTBUILDERLITE_2_T2E4BAD04727C967A540B2C9121E925B0C53EEC4A_H
#define ABSTRACTBUILDERLITE_2_T2E4BAD04727C967A540B2C9121E925B0C53EEC4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_GyroscopeEvent,proto.PhoneEvent_Types_GyroscopeEvent_Builder>
struct  AbstractBuilderLite_2_t2E4BAD04727C967A540B2C9121E925B0C53EEC4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T2E4BAD04727C967A540B2C9121E925B0C53EEC4A_H
#ifndef ABSTRACTBUILDERLITE_2_TF105F06DFBD2A8C5EEC871A099B64376C233F58B_H
#define ABSTRACTBUILDERLITE_2_TF105F06DFBD2A8C5EEC871A099B64376C233F58B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_KeyEvent,proto.PhoneEvent_Types_KeyEvent_Builder>
struct  AbstractBuilderLite_2_tF105F06DFBD2A8C5EEC871A099B64376C233F58B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_TF105F06DFBD2A8C5EEC871A099B64376C233F58B_H
#ifndef ABSTRACTBUILDERLITE_2_TBF2EB7BA20C8A693EA653D2A54A13AA862C64435_H
#define ABSTRACTBUILDERLITE_2_TBF2EB7BA20C8A693EA653D2A54A13AA862C64435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_MotionEvent,proto.PhoneEvent_Types_MotionEvent_Builder>
struct  AbstractBuilderLite_2_tBF2EB7BA20C8A693EA653D2A54A13AA862C64435  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_TBF2EB7BA20C8A693EA653D2A54A13AA862C64435_H
#ifndef ABSTRACTBUILDERLITE_2_TDBD74D9279624EFD9A39F28C5129BC0C6998E7F5_H
#define ABSTRACTBUILDERLITE_2_TDBD74D9279624EFD9A39F28C5129BC0C6998E7F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_MotionEvent_Types_Pointer,proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder>
struct  AbstractBuilderLite_2_tDBD74D9279624EFD9A39F28C5129BC0C6998E7F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_TDBD74D9279624EFD9A39F28C5129BC0C6998E7F5_H
#ifndef ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#define ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent_Types_OrientationEvent,proto.PhoneEvent_Types_OrientationEvent_Builder>
struct  AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4_H
#ifndef ABSTRACTMESSAGELITE_2_TA2A6F4EF9DBD4BEBE25AB6F5FFF3BDCB4DD6AC71_H
#define ABSTRACTMESSAGELITE_2_TA2A6F4EF9DBD4BEBE25AB6F5FFF3BDCB4DD6AC71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent,proto.PhoneEvent_Builder>
struct  AbstractMessageLite_2_tA2A6F4EF9DBD4BEBE25AB6F5FFF3BDCB4DD6AC71  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_TA2A6F4EF9DBD4BEBE25AB6F5FFF3BDCB4DD6AC71_H
#ifndef ABSTRACTMESSAGELITE_2_TE1CEBB64F476BFCE43CAC268A3CD4FAF6D10CEEF_H
#define ABSTRACTMESSAGELITE_2_TE1CEBB64F476BFCE43CAC268A3CD4FAF6D10CEEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_AccelerometerEvent,proto.PhoneEvent_Types_AccelerometerEvent_Builder>
struct  AbstractMessageLite_2_tE1CEBB64F476BFCE43CAC268A3CD4FAF6D10CEEF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_TE1CEBB64F476BFCE43CAC268A3CD4FAF6D10CEEF_H
#ifndef ABSTRACTMESSAGELITE_2_T7FD0936E7FE55DE56B27D5A6563B235D0487F237_H
#define ABSTRACTMESSAGELITE_2_T7FD0936E7FE55DE56B27D5A6563B235D0487F237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_DepthMapEvent,proto.PhoneEvent_Types_DepthMapEvent_Builder>
struct  AbstractMessageLite_2_t7FD0936E7FE55DE56B27D5A6563B235D0487F237  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T7FD0936E7FE55DE56B27D5A6563B235D0487F237_H
#ifndef ABSTRACTMESSAGELITE_2_T87F65052CB44B66B10DEDC5B1FCDBC67B508A063_H
#define ABSTRACTMESSAGELITE_2_T87F65052CB44B66B10DEDC5B1FCDBC67B508A063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_GyroscopeEvent,proto.PhoneEvent_Types_GyroscopeEvent_Builder>
struct  AbstractMessageLite_2_t87F65052CB44B66B10DEDC5B1FCDBC67B508A063  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T87F65052CB44B66B10DEDC5B1FCDBC67B508A063_H
#ifndef ABSTRACTMESSAGELITE_2_T76E687B38A46E096AF0714E4FA9DB028124B7F92_H
#define ABSTRACTMESSAGELITE_2_T76E687B38A46E096AF0714E4FA9DB028124B7F92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_KeyEvent,proto.PhoneEvent_Types_KeyEvent_Builder>
struct  AbstractMessageLite_2_t76E687B38A46E096AF0714E4FA9DB028124B7F92  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T76E687B38A46E096AF0714E4FA9DB028124B7F92_H
#ifndef ABSTRACTMESSAGELITE_2_T9F8CA44C99AEA5F23F9B733F83E11E6D04790CF0_H
#define ABSTRACTMESSAGELITE_2_T9F8CA44C99AEA5F23F9B733F83E11E6D04790CF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_MotionEvent,proto.PhoneEvent_Types_MotionEvent_Builder>
struct  AbstractMessageLite_2_t9F8CA44C99AEA5F23F9B733F83E11E6D04790CF0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T9F8CA44C99AEA5F23F9B733F83E11E6D04790CF0_H
#ifndef ABSTRACTMESSAGELITE_2_TDE94650651E9A1A787536406F3C4E3B0D05C5FBB_H
#define ABSTRACTMESSAGELITE_2_TDE94650651E9A1A787536406F3C4E3B0D05C5FBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_MotionEvent_Types_Pointer,proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder>
struct  AbstractMessageLite_2_tDE94650651E9A1A787536406F3C4E3B0D05C5FBB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_TDE94650651E9A1A787536406F3C4E3B0D05C5FBB_H
#ifndef ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#define ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent_Types_OrientationEvent,proto.PhoneEvent_Types_OrientationEvent_Builder>
struct  AbstractMessageLite_2_tA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_TA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5_H
#ifndef CONTROLLERPROVIDERFACTORY_T1109E019479E73C5F60CC087DFD5A7C17D0E276A_H
#define CONTROLLERPROVIDERFACTORY_T1109E019479E73C5F60CC087DFD5A7C17D0E276A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerProviderFactory
struct  ControllerProviderFactory_t1109E019479E73C5F60CC087DFD5A7C17D0E276A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERPROVIDERFACTORY_T1109E019479E73C5F60CC087DFD5A7C17D0E276A_H
#ifndef CONTROLLERUTILS_T5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_H
#define CONTROLLERUTILS_T5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerUtils
struct  ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D  : public RuntimeObject
{
public:

public:
};

struct ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_StaticFields
{
public:
	// GvrControllerHand[] Gvr.Internal.ControllerUtils::AllHands
	GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* ___AllHands_0;

public:
	inline static int32_t get_offset_of_AllHands_0() { return static_cast<int32_t>(offsetof(ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_StaticFields, ___AllHands_0)); }
	inline GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* get_AllHands_0() const { return ___AllHands_0; }
	inline GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157** get_address_of_AllHands_0() { return &___AllHands_0; }
	inline void set_AllHands_0(GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* value)
	{
		___AllHands_0 = value;
		Il2CppCodeGenWriteBarrier((&___AllHands_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERUTILS_T5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_H
#ifndef DUMMYCONTROLLERPROVIDER_T0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96_H
#define DUMMYCONTROLLERPROVIDER_T0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyControllerProvider
struct  DummyControllerProvider_t0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96  : public RuntimeObject
{
public:
	// Gvr.Internal.ControllerState Gvr.Internal.DummyControllerProvider::dummyState
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyControllerProvider_t0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96, ___dummyState_0)); }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * get_dummyState_0() const { return ___dummyState_0; }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 ** get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * value)
	{
		___dummyState_0 = value;
		Il2CppCodeGenWriteBarrier((&___dummyState_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYCONTROLLERPROVIDER_T0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96_H
#ifndef HEADSETPROVIDERFACTORY_TA8CF38CBC95F9E1D7003E63264FEEB89E85FF65F_H
#define HEADSETPROVIDERFACTORY_TA8CF38CBC95F9E1D7003E63264FEEB89E85FF65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetProviderFactory
struct  HeadsetProviderFactory_tA8CF38CBC95F9E1D7003E63264FEEB89E85FF65F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETPROVIDERFACTORY_TA8CF38CBC95F9E1D7003E63264FEEB89E85FF65F_H
#ifndef GVRACTIVITYHELPER_TFC98059228A65C623CB507DD6D6AC9B9CDB1D1FB_H
#define GVRACTIVITYHELPER_TFC98059228A65C623CB507DD6D6AC9B9CDB1D1FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrActivityHelper
struct  GvrActivityHelper_tFC98059228A65C623CB507DD6D6AC9B9CDB1D1FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRACTIVITYHELPER_TFC98059228A65C623CB507DD6D6AC9B9CDB1D1FB_H
#ifndef GVRDAYDREAMAPI_TEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_H
#define GVRDAYDREAMAPI_TEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi
struct  GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2  : public RuntimeObject
{
public:

public:
};

struct GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_StaticFields
{
public:
	// GvrDaydreamApi GvrDaydreamApi::m_instance
	GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2 * ___m_instance_4;

public:
	inline static int32_t get_offset_of_m_instance_4() { return static_cast<int32_t>(offsetof(GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_StaticFields, ___m_instance_4)); }
	inline GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2 * get_m_instance_4() const { return ___m_instance_4; }
	inline GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2 ** get_address_of_m_instance_4() { return &___m_instance_4; }
	inline void set_m_instance_4(GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2 * value)
	{
		___m_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDAYDREAMAPI_TEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T2B0191D0E254B1E5F8CC7BBC30C6138F86859C84_H
#define U3CU3EC__DISPLAYCLASS12_0_T2B0191D0E254B1E5F8CC7BBC30C6138F86859C84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi_<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t2B0191D0E254B1E5F8CC7BBC30C6138F86859C84  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> GvrDaydreamApi_<>c__DisplayClass12_0::callback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t2B0191D0E254B1E5F8CC7BBC30C6138F86859C84, ___callback_0)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_callback_0() const { return ___callback_0; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T2B0191D0E254B1E5F8CC7BBC30C6138F86859C84_H
#ifndef U3CENDOFFRAMEU3ED__28_TEDA04539C2348C215D96216CB150618E561DB92E_H
#define U3CENDOFFRAMEU3ED__28_TEDA04539C2348C215D96216CB150618E561DB92E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset_<EndOfFrame>d__28
struct  U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E  : public RuntimeObject
{
public:
	// System.Int32 GvrHeadset_<EndOfFrame>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GvrHeadset_<EndOfFrame>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GvrHeadset GvrHeadset_<EndOfFrame>d__28::<>4__this
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E, ___U3CU3E4__this_2)); }
	inline GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDOFFRAMEU3ED__28_TEDA04539C2348C215D96216CB150618E561DB92E_H
#ifndef GVRINTENT_T94EC573B8DC2FF531EFEE3F2A901AC977B0CA568_H
#define GVRINTENT_T94EC573B8DC2FF531EFEE3F2A901AC977B0CA568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrIntent
struct  GvrIntent_t94EC573B8DC2FF531EFEE3F2A901AC977B0CA568  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINTENT_T94EC573B8DC2FF531EFEE3F2A901AC977B0CA568_H
#ifndef U3CEXECUTERU3ED__44_T9E8C3F83B8FC4335640C612A804854691D2B9D7B_H
#define U3CEXECUTERU3ED__44_T9E8C3F83B8FC4335640C612A804854691D2B9D7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard_<Executer>d__44
struct  U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B  : public RuntimeObject
{
public:
	// System.Int32 GvrKeyboard_<Executer>d__44::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GvrKeyboard_<Executer>d__44::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GvrKeyboard GvrKeyboard_<Executer>d__44::<>4__this
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B, ___U3CU3E4__this_2)); }
	inline GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTERU3ED__44_T9E8C3F83B8FC4335640C612A804854691D2B9D7B_H
#ifndef GVRKEYBOARDINTENT_T97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_H
#define GVRKEYBOARDINTENT_T97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent
struct  GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516  : public RuntimeObject
{
public:

public:
};

struct GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_StaticFields
{
public:
	// GvrKeyboardIntent GvrKeyboardIntent::theInstance
	GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516 * ___theInstance_2;

public:
	inline static int32_t get_offset_of_theInstance_2() { return static_cast<int32_t>(offsetof(GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_StaticFields, ___theInstance_2)); }
	inline GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516 * get_theInstance_2() const { return ___theInstance_2; }
	inline GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516 ** get_address_of_theInstance_2() { return &___theInstance_2; }
	inline void set_theInstance_2(GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516 * value)
	{
		___theInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___theInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINTENT_T97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_H
#ifndef GVRMATHHELPERS_T59635576885D632A9739B3333BD5FE56418A19AF_H
#define GVRMATHHELPERS_T59635576885D632A9739B3333BD5FE56418A19AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrMathHelpers
struct  GvrMathHelpers_t59635576885D632A9739B3333BD5FE56418A19AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRMATHHELPERS_T59635576885D632A9739B3333BD5FE56418A19AF_H
#ifndef GVRSETTINGS_TDD84E5C6B4BAE16BC00E9EB6BBEF7D6F84B1906A_H
#define GVRSETTINGS_TDD84E5C6B4BAE16BC00E9EB6BBEF7D6F84B1906A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings
struct  GvrSettings_tDD84E5C6B4BAE16BC00E9EB6BBEF7D6F84B1906A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSETTINGS_TDD84E5C6B4BAE16BC00E9EB6BBEF7D6F84B1906A_H
#ifndef GVRUIHELPERS_T6EAF84EB92DA6F8D9D4F9A05541D2CE62833A629_H
#define GVRUIHELPERS_T6EAF84EB92DA6F8D9D4F9A05541D2CE62833A629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUIHelpers
struct  GvrUIHelpers_t6EAF84EB92DA6F8D9D4F9A05541D2CE62833A629  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUIHELPERS_T6EAF84EB92DA6F8D9D4F9A05541D2CE62833A629_H
#ifndef GVRUNITYSDKVERSION_T3EA8C55A1AA07CE5F11623B831B2125B5A159D54_H
#define GVRUNITYSDKVERSION_T3EA8C55A1AA07CE5F11623B831B2125B5A159D54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUnitySdkVersion
struct  GvrUnitySdkVersion_t3EA8C55A1AA07CE5F11623B831B2125B5A159D54  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUNITYSDKVERSION_T3EA8C55A1AA07CE5F11623B831B2125B5A159D54_H
#ifndef GVRVRHELPERS_T18660FD90F25F1BA1E804F476A6921633E1BBF9C_H
#define GVRVRHELPERS_T18660FD90F25F1BA1E804F476A6921633E1BBF9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVRHelpers
struct  GvrVRHelpers_t18660FD90F25F1BA1E804F476A6921633E1BBF9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRVRHELPERS_T18660FD90F25F1BA1E804F476A6921633E1BBF9C_H
#ifndef U3CU3EC_TD9F253A483F30D6C2F08A8994A7AFFB292E8D649_H
#define U3CU3EC_TD9F253A483F30D6C2F08A8994A7AFFB292E8D649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_<>c
struct  U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields
{
public:
	// GvrVideoPlayerTexture_<>c GvrVideoPlayerTexture_<>c::<>9
	U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649 * ___U3CU3E9_0;
	// System.Action`2<System.String,System.String> GvrVideoPlayerTexture_<>c::<>9__69_1
	Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * ___U3CU3E9__69_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__69_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields, ___U3CU3E9__69_1_1)); }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * get_U3CU3E9__69_1_1() const { return ___U3CU3E9__69_1_1; }
	inline Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 ** get_address_of_U3CU3E9__69_1_1() { return &___U3CU3E9__69_1_1; }
	inline void set_U3CU3E9__69_1_1(Action_2_t1CB04E22693098ADDC0738D6ED994D8F298698C3 * value)
	{
		___U3CU3E9__69_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__69_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD9F253A483F30D6C2F08A8994A7AFFB292E8D649_H
#ifndef U3CU3EC__DISPLAYCLASS90_0_TCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334_H
#define U3CU3EC__DISPLAYCLASS90_0_TCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_<>c__DisplayClass90_0
struct  U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture_<>c__DisplayClass90_0::player
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * ___player_0;
	// System.Int32 GvrVideoPlayerTexture_<>c__DisplayClass90_0::eventId
	int32_t ___eventId_1;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334, ___player_0)); }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_eventId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334, ___eventId_1)); }
	inline int32_t get_eventId_1() const { return ___eventId_1; }
	inline int32_t* get_address_of_eventId_1() { return &___eventId_1; }
	inline void set_eventId_1(int32_t value)
	{
		___eventId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS90_0_TCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334_H
#ifndef U3CU3EC__DISPLAYCLASS92_0_T154921651FC9A68B8DC7FC27C1E8522A363E801E_H
#define U3CU3EC__DISPLAYCLASS92_0_T154921651FC9A68B8DC7FC27C1E8522A363E801E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_<>c__DisplayClass92_0
struct  U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture_<>c__DisplayClass92_0::player
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * ___player_0;
	// System.String GvrVideoPlayerTexture_<>c__DisplayClass92_0::type
	String_t* ___type_1;
	// System.String GvrVideoPlayerTexture_<>c__DisplayClass92_0::msg
	String_t* ___msg_2;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E, ___player_0)); }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_msg_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E, ___msg_2)); }
	inline String_t* get_msg_2() const { return ___msg_2; }
	inline String_t** get_address_of_msg_2() { return &___msg_2; }
	inline void set_msg_2(String_t* value)
	{
		___msg_2 = value;
		Il2CppCodeGenWriteBarrier((&___msg_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS92_0_T154921651FC9A68B8DC7FC27C1E8522A363E801E_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#define ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifndef TYPES_T4E1129DB2DA1976763A5E1940C85EBA8EBB73210_H
#define TYPES_T4E1129DB2DA1976763A5E1940C85EBA8EBB73210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types
struct  Types_t4E1129DB2DA1976763A5E1940C85EBA8EBB73210  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_T4E1129DB2DA1976763A5E1940C85EBA8EBB73210_H
#ifndef TYPES_TB6BA8DE659576B2D1767BA1503423D241112EE98_H
#define TYPES_TB6BA8DE659576B2D1767BA1503423D241112EE98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_MotionEvent_Types
struct  Types_tB6BA8DE659576B2D1767BA1503423D241112EE98  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_TB6BA8DE659576B2D1767BA1503423D241112EE98_H
#ifndef PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#define PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072  : public RuntimeObject
{
public:

public:
};

struct PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::Descriptor
	RuntimeObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields, ___Descriptor_0)); }
	inline RuntimeObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline RuntimeObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(RuntimeObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___Descriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifndef GENERATEDBUILDERLITE_2_T1874DEA62BEC7D523C565D33E405082BD01607C1_H
#define GENERATEDBUILDERLITE_2_T1874DEA62BEC7D523C565D33E405082BD01607C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent_Builder>
struct  GeneratedBuilderLite_2_t1874DEA62BEC7D523C565D33E405082BD01607C1  : public AbstractBuilderLite_2_tC0AF8A3076FA603E0862880D0914C27154155846
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T1874DEA62BEC7D523C565D33E405082BD01607C1_H
#ifndef GENERATEDBUILDERLITE_2_T4662386564B924425F9CF3CB3829A84D391136C6_H
#define GENERATEDBUILDERLITE_2_T4662386564B924425F9CF3CB3829A84D391136C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_AccelerometerEvent,proto.PhoneEvent_Types_AccelerometerEvent_Builder>
struct  GeneratedBuilderLite_2_t4662386564B924425F9CF3CB3829A84D391136C6  : public AbstractBuilderLite_2_t58AD69F86F731BCD90D64984E23BB16A359629B1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T4662386564B924425F9CF3CB3829A84D391136C6_H
#ifndef GENERATEDBUILDERLITE_2_T718FB2342CB3BC4A823EFA51BE7B0ABF31845656_H
#define GENERATEDBUILDERLITE_2_T718FB2342CB3BC4A823EFA51BE7B0ABF31845656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_DepthMapEvent,proto.PhoneEvent_Types_DepthMapEvent_Builder>
struct  GeneratedBuilderLite_2_t718FB2342CB3BC4A823EFA51BE7B0ABF31845656  : public AbstractBuilderLite_2_tF8C2D6945B6CF9F72EB0A520850111D04C84B4BC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T718FB2342CB3BC4A823EFA51BE7B0ABF31845656_H
#ifndef GENERATEDBUILDERLITE_2_T4D48615E09F7819C6505B73B82FAECCE7650526D_H
#define GENERATEDBUILDERLITE_2_T4D48615E09F7819C6505B73B82FAECCE7650526D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_GyroscopeEvent,proto.PhoneEvent_Types_GyroscopeEvent_Builder>
struct  GeneratedBuilderLite_2_t4D48615E09F7819C6505B73B82FAECCE7650526D  : public AbstractBuilderLite_2_t2E4BAD04727C967A540B2C9121E925B0C53EEC4A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T4D48615E09F7819C6505B73B82FAECCE7650526D_H
#ifndef GENERATEDBUILDERLITE_2_T7F66FD94BEE7EB747665A42D112B1D1D2B3BF222_H
#define GENERATEDBUILDERLITE_2_T7F66FD94BEE7EB747665A42D112B1D1D2B3BF222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_KeyEvent,proto.PhoneEvent_Types_KeyEvent_Builder>
struct  GeneratedBuilderLite_2_t7F66FD94BEE7EB747665A42D112B1D1D2B3BF222  : public AbstractBuilderLite_2_tF105F06DFBD2A8C5EEC871A099B64376C233F58B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T7F66FD94BEE7EB747665A42D112B1D1D2B3BF222_H
#ifndef GENERATEDBUILDERLITE_2_T7476B897B3679878A8A39DD9C4F09DD99BE4F183_H
#define GENERATEDBUILDERLITE_2_T7476B897B3679878A8A39DD9C4F09DD99BE4F183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_MotionEvent,proto.PhoneEvent_Types_MotionEvent_Builder>
struct  GeneratedBuilderLite_2_t7476B897B3679878A8A39DD9C4F09DD99BE4F183  : public AbstractBuilderLite_2_tBF2EB7BA20C8A693EA653D2A54A13AA862C64435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T7476B897B3679878A8A39DD9C4F09DD99BE4F183_H
#ifndef GENERATEDBUILDERLITE_2_T9620BCC5A37FA1FDDB9943F022CC552E33D177D1_H
#define GENERATEDBUILDERLITE_2_T9620BCC5A37FA1FDDB9943F022CC552E33D177D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_MotionEvent_Types_Pointer,proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder>
struct  GeneratedBuilderLite_2_t9620BCC5A37FA1FDDB9943F022CC552E33D177D1  : public AbstractBuilderLite_2_tDBD74D9279624EFD9A39F28C5129BC0C6998E7F5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T9620BCC5A37FA1FDDB9943F022CC552E33D177D1_H
#ifndef GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#define GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent_Types_OrientationEvent,proto.PhoneEvent_Types_OrientationEvent_Builder>
struct  GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A  : public AbstractBuilderLite_2_t634C58AA017D2A5B1AC2C4DC4C57641B3BDE20F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T5FE12C42B399AD2099894359F40D69FC579B1B9A_H
#ifndef GENERATEDMESSAGELITE_2_TB81B19DA2DBB49691CB4C7EF70E3486F006F4805_H
#define GENERATEDMESSAGELITE_2_TB81B19DA2DBB49691CB4C7EF70E3486F006F4805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent,proto.PhoneEvent_Builder>
struct  GeneratedMessageLite_2_tB81B19DA2DBB49691CB4C7EF70E3486F006F4805  : public AbstractMessageLite_2_tA2A6F4EF9DBD4BEBE25AB6F5FFF3BDCB4DD6AC71
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_TB81B19DA2DBB49691CB4C7EF70E3486F006F4805_H
#ifndef GENERATEDMESSAGELITE_2_TDE80B1D20C4C49805F90AF5F1E9A2441D27E1881_H
#define GENERATEDMESSAGELITE_2_TDE80B1D20C4C49805F90AF5F1E9A2441D27E1881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_AccelerometerEvent,proto.PhoneEvent_Types_AccelerometerEvent_Builder>
struct  GeneratedMessageLite_2_tDE80B1D20C4C49805F90AF5F1E9A2441D27E1881  : public AbstractMessageLite_2_tE1CEBB64F476BFCE43CAC268A3CD4FAF6D10CEEF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_TDE80B1D20C4C49805F90AF5F1E9A2441D27E1881_H
#ifndef GENERATEDMESSAGELITE_2_T6FCB86E904843057A67C5E4EA401994F784489FA_H
#define GENERATEDMESSAGELITE_2_T6FCB86E904843057A67C5E4EA401994F784489FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_DepthMapEvent,proto.PhoneEvent_Types_DepthMapEvent_Builder>
struct  GeneratedMessageLite_2_t6FCB86E904843057A67C5E4EA401994F784489FA  : public AbstractMessageLite_2_t7FD0936E7FE55DE56B27D5A6563B235D0487F237
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T6FCB86E904843057A67C5E4EA401994F784489FA_H
#ifndef GENERATEDMESSAGELITE_2_T91D39F4FF47A74575D85083883EDDAC443F96FB3_H
#define GENERATEDMESSAGELITE_2_T91D39F4FF47A74575D85083883EDDAC443F96FB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_GyroscopeEvent,proto.PhoneEvent_Types_GyroscopeEvent_Builder>
struct  GeneratedMessageLite_2_t91D39F4FF47A74575D85083883EDDAC443F96FB3  : public AbstractMessageLite_2_t87F65052CB44B66B10DEDC5B1FCDBC67B508A063
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T91D39F4FF47A74575D85083883EDDAC443F96FB3_H
#ifndef GENERATEDMESSAGELITE_2_T6EEEF2CFF7BEC92FEA73FAB4528C25ABB7C34E95_H
#define GENERATEDMESSAGELITE_2_T6EEEF2CFF7BEC92FEA73FAB4528C25ABB7C34E95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_KeyEvent,proto.PhoneEvent_Types_KeyEvent_Builder>
struct  GeneratedMessageLite_2_t6EEEF2CFF7BEC92FEA73FAB4528C25ABB7C34E95  : public AbstractMessageLite_2_t76E687B38A46E096AF0714E4FA9DB028124B7F92
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T6EEEF2CFF7BEC92FEA73FAB4528C25ABB7C34E95_H
#ifndef GENERATEDMESSAGELITE_2_T4059E40E4014A616AB6F919B2B40705CBA5BF168_H
#define GENERATEDMESSAGELITE_2_T4059E40E4014A616AB6F919B2B40705CBA5BF168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_MotionEvent,proto.PhoneEvent_Types_MotionEvent_Builder>
struct  GeneratedMessageLite_2_t4059E40E4014A616AB6F919B2B40705CBA5BF168  : public AbstractMessageLite_2_t9F8CA44C99AEA5F23F9B733F83E11E6D04790CF0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T4059E40E4014A616AB6F919B2B40705CBA5BF168_H
#ifndef GENERATEDMESSAGELITE_2_T828F0AA7CD042862A1C0224BDFE22C528C63FB93_H
#define GENERATEDMESSAGELITE_2_T828F0AA7CD042862A1C0224BDFE22C528C63FB93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_MotionEvent_Types_Pointer,proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder>
struct  GeneratedMessageLite_2_t828F0AA7CD042862A1C0224BDFE22C528C63FB93  : public AbstractMessageLite_2_tDE94650651E9A1A787536406F3C4E3B0D05C5FBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T828F0AA7CD042862A1C0224BDFE22C528C63FB93_H
#ifndef GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#define GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent_Types_OrientationEvent,proto.PhoneEvent_Types_OrientationEvent_Builder>
struct  GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219  : public AbstractMessageLite_2_tA74FA190BBEBA6158E3DCCB1EEAEDE89D682A6B5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_TF07B6BF883A996942E93FD145584FDDE91AC5219_H
#ifndef EMULATORTOUCHEVENT_T621AB662684F3BA6D159C02C9AFB6020BA76C18E_H
#define EMULATORTOUCHEVENT_T621AB662684F3BA6D159C02C9AFB6020BA76C18E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent
struct  EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::action
	int32_t ___action_0;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::relativeTimestamp
	int32_t ___relativeTimestamp_1;
	// System.Collections.Generic.List`1<Gvr.Internal.EmulatorTouchEvent_Pointer> Gvr.Internal.EmulatorTouchEvent::pointers
	List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D * ___pointers_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E, ___action_0)); }
	inline int32_t get_action_0() const { return ___action_0; }
	inline int32_t* get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(int32_t value)
	{
		___action_0 = value;
	}

	inline static int32_t get_offset_of_relativeTimestamp_1() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E, ___relativeTimestamp_1)); }
	inline int32_t get_relativeTimestamp_1() const { return ___relativeTimestamp_1; }
	inline int32_t* get_address_of_relativeTimestamp_1() { return &___relativeTimestamp_1; }
	inline void set_relativeTimestamp_1(int32_t value)
	{
		___relativeTimestamp_1 = value;
	}

	inline static int32_t get_offset_of_pointers_2() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E, ___pointers_2)); }
	inline List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D * get_pointers_2() const { return ___pointers_2; }
	inline List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D ** get_address_of_pointers_2() { return &___pointers_2; }
	inline void set_pointers_2(List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D * value)
	{
		___pointers_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointers_2), value);
	}
};

struct EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_POINTER_INDEX_SHIFT
	int32_t ___ACTION_POINTER_INDEX_SHIFT_3;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_POINTER_INDEX_MASK
	int32_t ___ACTION_POINTER_INDEX_MASK_4;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_MASK
	int32_t ___ACTION_MASK_5;

public:
	inline static int32_t get_offset_of_ACTION_POINTER_INDEX_SHIFT_3() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields, ___ACTION_POINTER_INDEX_SHIFT_3)); }
	inline int32_t get_ACTION_POINTER_INDEX_SHIFT_3() const { return ___ACTION_POINTER_INDEX_SHIFT_3; }
	inline int32_t* get_address_of_ACTION_POINTER_INDEX_SHIFT_3() { return &___ACTION_POINTER_INDEX_SHIFT_3; }
	inline void set_ACTION_POINTER_INDEX_SHIFT_3(int32_t value)
	{
		___ACTION_POINTER_INDEX_SHIFT_3 = value;
	}

	inline static int32_t get_offset_of_ACTION_POINTER_INDEX_MASK_4() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields, ___ACTION_POINTER_INDEX_MASK_4)); }
	inline int32_t get_ACTION_POINTER_INDEX_MASK_4() const { return ___ACTION_POINTER_INDEX_MASK_4; }
	inline int32_t* get_address_of_ACTION_POINTER_INDEX_MASK_4() { return &___ACTION_POINTER_INDEX_MASK_4; }
	inline void set_ACTION_POINTER_INDEX_MASK_4(int32_t value)
	{
		___ACTION_POINTER_INDEX_MASK_4 = value;
	}

	inline static int32_t get_offset_of_ACTION_MASK_5() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields, ___ACTION_MASK_5)); }
	inline int32_t get_ACTION_MASK_5() const { return ___ACTION_MASK_5; }
	inline int32_t* get_address_of_ACTION_MASK_5() { return &___ACTION_MASK_5; }
	inline void set_ACTION_MASK_5(int32_t value)
	{
		___ACTION_MASK_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.EmulatorTouchEvent
struct EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_marshaled_pinvoke
{
	int32_t ___action_0;
	int32_t ___relativeTimestamp_1;
	List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D * ___pointers_2;
};
// Native definition for COM marshalling of Gvr.Internal.EmulatorTouchEvent
struct EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_marshaled_com
{
	int32_t ___action_0;
	int32_t ___relativeTimestamp_1;
	List_1_tAFAFB0C16E54DC5769EBE19A5445B9515571AE5D * ___pointers_2;
};
#endif // EMULATORTOUCHEVENT_T621AB662684F3BA6D159C02C9AFB6020BA76C18E_H
#ifndef POINTER_TE4CEDEA82E3FC63C990C824277BE5F151E19907F_H
#define POINTER_TE4CEDEA82E3FC63C990C824277BE5F151E19907F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent_Pointer
struct  Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent_Pointer::fingerId
	int32_t ___fingerId_0;
	// System.Single Gvr.Internal.EmulatorTouchEvent_Pointer::normalizedX
	float ___normalizedX_1;
	// System.Single Gvr.Internal.EmulatorTouchEvent_Pointer::normalizedY
	float ___normalizedY_2;

public:
	inline static int32_t get_offset_of_fingerId_0() { return static_cast<int32_t>(offsetof(Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F, ___fingerId_0)); }
	inline int32_t get_fingerId_0() const { return ___fingerId_0; }
	inline int32_t* get_address_of_fingerId_0() { return &___fingerId_0; }
	inline void set_fingerId_0(int32_t value)
	{
		___fingerId_0 = value;
	}

	inline static int32_t get_offset_of_normalizedX_1() { return static_cast<int32_t>(offsetof(Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F, ___normalizedX_1)); }
	inline float get_normalizedX_1() const { return ___normalizedX_1; }
	inline float* get_address_of_normalizedX_1() { return &___normalizedX_1; }
	inline void set_normalizedX_1(float value)
	{
		___normalizedX_1 = value;
	}

	inline static int32_t get_offset_of_normalizedY_2() { return static_cast<int32_t>(offsetof(Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F, ___normalizedY_2)); }
	inline float get_normalizedY_2() const { return ___normalizedY_2; }
	inline float* get_address_of_normalizedY_2() { return &___normalizedY_2; }
	inline void set_normalizedY_2(float value)
	{
		___normalizedY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_TE4CEDEA82E3FC63C990C824277BE5F151E19907F_H
#ifndef RESOLUTIONSIZE_T6CDDCD67E1BE1D80E34B593670F582DEAF116EC4_H
#define RESOLUTIONSIZE_T6CDDCD67E1BE1D80E34B593670F582DEAF116EC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_ResolutionSize
struct  ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_ResolutionSize::width
	int32_t ___width_0;
	// System.Int32 Gvr.Internal.InstantPreview_ResolutionSize::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONSIZE_T6CDDCD67E1BE1D80E34B593670F582DEAF116EC4_H
#ifndef KEYBOARDCALLBACK_T08BF5F9BC9534D07B8D85B578585310A598E6CAF_H
#define KEYBOARDCALLBACK_T08BF5F9BC9534D07B8D85B578585310A598E6CAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent_KeyboardCallback
struct  KeyboardCallback_t08BF5F9BC9534D07B8D85B578585310A598E6CAF  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T08BF5F9BC9534D07B8D85B578585310A598E6CAF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EMULATORACCELEVENT_TDCD33679FD8E98A3B307D52F6148569846B047D1_H
#define EMULATORACCELEVENT_TDCD33679FD8E98A3B307D52F6148569846B047D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorAccelEvent
struct  EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1 
{
public:
	// System.Int64 Gvr.Internal.EmulatorAccelEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Vector3 Gvr.Internal.EmulatorAccelEvent::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1, ___value_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_1() const { return ___value_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORACCELEVENT_TDCD33679FD8E98A3B307D52F6148569846B047D1_H
#ifndef BUTTONCODE_T4A94C0D8F14CD6BB4655AB140F26E629D03748B5_H
#define BUTTONCODE_T4A94C0D8F14CD6BB4655AB140F26E629D03748B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorButtonEvent_ButtonCode
struct  ButtonCode_t4A94C0D8F14CD6BB4655AB140F26E629D03748B5 
{
public:
	// System.Int32 Gvr.Internal.EmulatorButtonEvent_ButtonCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonCode_t4A94C0D8F14CD6BB4655AB140F26E629D03748B5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCODE_T4A94C0D8F14CD6BB4655AB140F26E629D03748B5_H
#ifndef MODE_T075FFF717D0EFAE94D1B7E5F4B400603D6238B48_H
#define MODE_T075FFF717D0EFAE94D1B7E5F4B400603D6238B48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorConfig_Mode
struct  Mode_t075FFF717D0EFAE94D1B7E5F4B400603D6238B48 
{
public:
	// System.Int32 Gvr.Internal.EmulatorConfig_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t075FFF717D0EFAE94D1B7E5F4B400603D6238B48, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T075FFF717D0EFAE94D1B7E5F4B400603D6238B48_H
#ifndef EMULATORGYROEVENT_T01030B7841C1F666BBFF45B9CC7BF70AA14ACC93_H
#define EMULATORGYROEVENT_T01030B7841C1F666BBFF45B9CC7BF70AA14ACC93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorGyroEvent
struct  EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93 
{
public:
	// System.Int64 Gvr.Internal.EmulatorGyroEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Vector3 Gvr.Internal.EmulatorGyroEvent::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93, ___value_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_1() const { return ___value_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORGYROEVENT_T01030B7841C1F666BBFF45B9CC7BF70AA14ACC93_H
#ifndef EMULATORORIENTATIONEVENT_T13D24B09418528D66FCA783094801F43F2EAE0CA_H
#define EMULATORORIENTATIONEVENT_T13D24B09418528D66FCA783094801F43F2EAE0CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorOrientationEvent
struct  EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA 
{
public:
	// System.Int64 Gvr.Internal.EmulatorOrientationEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Quaternion Gvr.Internal.EmulatorOrientationEvent::orientation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___orientation_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_orientation_1() { return static_cast<int32_t>(offsetof(EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA, ___orientation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_orientation_1() const { return ___orientation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_orientation_1() { return &___orientation_1; }
	inline void set_orientation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___orientation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORORIENTATIONEVENT_T13D24B09418528D66FCA783094801F43F2EAE0CA_H
#ifndef ACTION_T1152BDDBD0A80C8D7203F5FE66E73A1B8651805D_H
#define ACTION_T1152BDDBD0A80C8D7203F5FE66E73A1B8651805D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent_Action
struct  Action_t1152BDDBD0A80C8D7203F5FE66E73A1B8651805D 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent_Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_t1152BDDBD0A80C8D7203F5FE66E73A1B8651805D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1152BDDBD0A80C8D7203F5FE66E73A1B8651805D_H
#ifndef BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#define BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_BitRates
struct  BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_BitRates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#ifndef MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#define MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_MultisampleCounts
struct  MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_MultisampleCounts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#ifndef RESOLUTIONS_TA438755150C18D8A2A0C3C87A34C7E741E4BC424_H
#define RESOLUTIONS_TA438755150C18D8A2A0C3C87A34C7E741E4BC424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_Resolutions
struct  Resolutions_tA438755150C18D8A2A0C3C87A34C7E741E4BC424 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_Resolutions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Resolutions_tA438755150C18D8A2A0C3C87A34C7E741E4BC424, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONS_TA438755150C18D8A2A0C3C87A34C7E741E4BC424_H
#ifndef GVR_FEATURE_TF0F811D9C8BF77586B015C5FA76A34EC608187DE_H
#define GVR_FEATURE_TF0F811D9C8BF77586B015C5FA76A34EC608187DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_feature
struct  gvr_feature_tF0F811D9C8BF77586B015C5FA76A34EC608187DE 
{
public:
	// System.Int32 Gvr.Internal.gvr_feature::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(gvr_feature_tF0F811D9C8BF77586B015C5FA76A34EC608187DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_FEATURE_TF0F811D9C8BF77586B015C5FA76A34EC608187DE_H
#ifndef GVR_PROPERTY_TYPE_TA9521BDBC33D00B4491D09EABF7DAFA201200553_H
#define GVR_PROPERTY_TYPE_TA9521BDBC33D00B4491D09EABF7DAFA201200553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_property_type
struct  gvr_property_type_tA9521BDBC33D00B4491D09EABF7DAFA201200553 
{
public:
	// System.Int32 Gvr.Internal.gvr_property_type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(gvr_property_type_tA9521BDBC33D00B4491D09EABF7DAFA201200553, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_PROPERTY_TYPE_TA9521BDBC33D00B4491D09EABF7DAFA201200553_H
#ifndef GVR_RECENTER_FLAGS_TA234601F30F88671225972AD827D96CD306F1F56_H
#define GVR_RECENTER_FLAGS_TA234601F30F88671225972AD827D96CD306F1F56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_recenter_flags
struct  gvr_recenter_flags_tA234601F30F88671225972AD827D96CD306F1F56 
{
public:
	// System.Int32 Gvr.Internal.gvr_recenter_flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(gvr_recenter_flags_tA234601F30F88671225972AD827D96CD306F1F56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECENTER_FLAGS_TA234601F30F88671225972AD827D96CD306F1F56_H
#ifndef GVR_VALUE_TYPE_T9506B044915CA4F1202BCC5976BCB808E9107900_H
#define GVR_VALUE_TYPE_T9506B044915CA4F1202BCC5976BCB808E9107900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_value_type
struct  gvr_value_type_t9506B044915CA4F1202BCC5976BCB808E9107900 
{
public:
	// System.Int32 Gvr.Internal.gvr_value_type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(gvr_value_type_t9506B044915CA4F1202BCC5976BCB808E9107900, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_VALUE_TYPE_T9506B044915CA4F1202BCC5976BCB808E9107900_H
#ifndef GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#define GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrConnectionState
struct  GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982 
{
public:
	// System.Int32 GvrConnectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#ifndef GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#define GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerApiStatus
struct  GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85 
{
public:
	// System.Int32 GvrControllerApiStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#ifndef GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#define GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerBatteryLevel
struct  GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904 
{
public:
	// System.Int32 GvrControllerBatteryLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#ifndef GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#define GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerButton
struct  GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645 
{
public:
	// System.Int32 GvrControllerButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#ifndef GVRERRORTYPE_TEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA_H
#define GVRERRORTYPE_TEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrErrorType
struct  GvrErrorType_tEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA 
{
public:
	// System.Int32 GvrErrorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrErrorType_tEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRERRORTYPE_TEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA_H
#ifndef GVREVENTTYPE_TC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14_H
#define GVREVENTTYPE_TC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventType
struct  GvrEventType_tC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14 
{
public:
	// System.Int32 GvrEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrEventType_tC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTTYPE_TC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14_H
#ifndef GVRINFO_T8439C7B5D9A17F3900584550E2DC83531645E0FF_H
#define GVRINFO_T8439C7B5D9A17F3900584550E2DC83531645E0FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrInfo
struct  GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.String GvrInfo::text
	String_t* ___text_0;
	// System.Int32 GvrInfo::numLines
	int32_t ___numLines_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_numLines_1() { return static_cast<int32_t>(offsetof(GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF, ___numLines_1)); }
	inline int32_t get_numLines_1() const { return ___numLines_1; }
	inline int32_t* get_address_of_numLines_1() { return &___numLines_1; }
	inline void set_numLines_1(int32_t value)
	{
		___numLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINFO_T8439C7B5D9A17F3900584550E2DC83531645E0FF_H
#ifndef GVRKEYBOARDERROR_TDC237522A4F6C62FE108DEEF4FE3882DD60E42D4_H
#define GVRKEYBOARDERROR_TDC237522A4F6C62FE108DEEF4FE3882DD60E42D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardError
struct  GvrKeyboardError_tDC237522A4F6C62FE108DEEF4FE3882DD60E42D4 
{
public:
	// System.Int32 GvrKeyboardError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrKeyboardError_tDC237522A4F6C62FE108DEEF4FE3882DD60E42D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDERROR_TDC237522A4F6C62FE108DEEF4FE3882DD60E42D4_H
#ifndef GVRKEYBOARDEVENT_TE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9_H
#define GVRKEYBOARDEVENT_TE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardEvent
struct  GvrKeyboardEvent_tE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9 
{
public:
	// System.Int32 GvrKeyboardEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrKeyboardEvent_tE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDEVENT_TE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9_H
#ifndef GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#define GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardInputMode
struct  GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED 
{
public:
	// System.Int32 GvrKeyboardInputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#ifndef GVRRECENTEREVENTTYPE_T4A5C521A229315E6BF682CCDB9DA79069389E2E2_H
#define GVRRECENTEREVENTTYPE_T4A5C521A229315E6BF682CCDB9DA79069389E2E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterEventType
struct  GvrRecenterEventType_t4A5C521A229315E6BF682CCDB9DA79069389E2E2 
{
public:
	// System.Int32 GvrRecenterEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrRecenterEventType_t4A5C521A229315E6BF682CCDB9DA79069389E2E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTEREVENTTYPE_T4A5C521A229315E6BF682CCDB9DA79069389E2E2_H
#ifndef GVRRECENTERFLAGS_TD91F69BA36CF2697A175C1DE357032ABEA0E194D_H
#define GVRRECENTERFLAGS_TD91F69BA36CF2697A175C1DE357032ABEA0E194D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterFlags
struct  GvrRecenterFlags_tD91F69BA36CF2697A175C1DE357032ABEA0E194D 
{
public:
	// System.Int32 GvrRecenterFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrRecenterFlags_tD91F69BA36CF2697A175C1DE357032ABEA0E194D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERFLAGS_TD91F69BA36CF2697A175C1DE357032ABEA0E194D_H
#ifndef GVRSAFETYREGIONTYPE_TB79667D62FE7D043A07C5562A527A0F35BC731B6_H
#define GVRSAFETYREGIONTYPE_TB79667D62FE7D043A07C5562A527A0F35BC731B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSafetyRegionType
struct  GvrSafetyRegionType_tB79667D62FE7D043A07C5562A527A0F35BC731B6 
{
public:
	// System.Int32 GvrSafetyRegionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrSafetyRegionType_tB79667D62FE7D043A07C5562A527A0F35BC731B6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSAFETYREGIONTYPE_TB79667D62FE7D043A07C5562A527A0F35BC731B6_H
#ifndef USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#define USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings_UserPrefsHandedness
struct  UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE 
{
public:
	// System.Int32 GvrSettings_UserPrefsHandedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#ifndef VIEWERPLATFORMTYPE_T972B7912D65EEB94F6A1BF223C97D35B5D11EA6F_H
#define VIEWERPLATFORMTYPE_T972B7912D65EEB94F6A1BF223C97D35B5D11EA6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings_ViewerPlatformType
struct  ViewerPlatformType_t972B7912D65EEB94F6A1BF223C97D35B5D11EA6F 
{
public:
	// System.Int32 GvrSettings_ViewerPlatformType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ViewerPlatformType_t972B7912D65EEB94F6A1BF223C97D35B5D11EA6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPLATFORMTYPE_T972B7912D65EEB94F6A1BF223C97D35B5D11EA6F_H
#ifndef RENDERCOMMAND_T41B8C75B0D75BAB6F2E700B30B7351CD18DACE43_H
#define RENDERCOMMAND_T41B8C75B0D75BAB6F2E700B30B7351CD18DACE43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_RenderCommand
struct  RenderCommand_t41B8C75B0D75BAB6F2E700B30B7351CD18DACE43 
{
public:
	// System.Int32 GvrVideoPlayerTexture_RenderCommand::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderCommand_t41B8C75B0D75BAB6F2E700B30B7351CD18DACE43, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERCOMMAND_T41B8C75B0D75BAB6F2E700B30B7351CD18DACE43_H
#ifndef STEREOMODE_TF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7_H
#define STEREOMODE_TF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_StereoMode
struct  StereoMode_tF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7 
{
public:
	// System.Int32 GvrVideoPlayerTexture_StereoMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoMode_tF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOMODE_TF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7_H
#ifndef VIDEOEVENTS_TCF660099ABF0BA02C6A29F1F205D17B13668E349_H
#define VIDEOEVENTS_TCF660099ABF0BA02C6A29F1F205D17B13668E349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_VideoEvents
struct  VideoEvents_tCF660099ABF0BA02C6A29F1F205D17B13668E349 
{
public:
	// System.Int32 GvrVideoPlayerTexture_VideoEvents::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoEvents_tCF660099ABF0BA02C6A29F1F205D17B13668E349, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOEVENTS_TCF660099ABF0BA02C6A29F1F205D17B13668E349_H
#ifndef VIDEOPLAYERSTATE_TF88330EB1305982C0484EE2EAFA554BE509395DB_H
#define VIDEOPLAYERSTATE_TF88330EB1305982C0484EE2EAFA554BE509395DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_VideoPlayerState
struct  VideoPlayerState_tF88330EB1305982C0484EE2EAFA554BE509395DB 
{
public:
	// System.Int32 GvrVideoPlayerTexture_VideoPlayerState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoPlayerState_tF88330EB1305982C0484EE2EAFA554BE509395DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYERSTATE_TF88330EB1305982C0484EE2EAFA554BE509395DB_H
#ifndef VIDEORESOLUTION_T80D666F578BADB8489D0894CB58BC868564B9D7F_H
#define VIDEORESOLUTION_T80D666F578BADB8489D0894CB58BC868564B9D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_VideoResolution
struct  VideoResolution_t80D666F578BADB8489D0894CB58BC868564B9D7F 
{
public:
	// System.Int32 GvrVideoPlayerTexture_VideoResolution::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoResolution_t80D666F578BADB8489D0894CB58BC868564B9D7F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORESOLUTION_T80D666F578BADB8489D0894CB58BC868564B9D7F_H
#ifndef VIDEOTYPE_TC41DCEDD05448446169FAD1C41F1ED3964792D10_H
#define VIDEOTYPE_TC41DCEDD05448446169FAD1C41F1ED3964792D10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_VideoType
struct  VideoType_tC41DCEDD05448446169FAD1C41F1ED3964792D10 
{
public:
	// System.Int32 GvrVideoPlayerTexture_VideoType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoType_tC41DCEDD05448446169FAD1C41F1ED3964792D10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTYPE_TC41DCEDD05448446169FAD1C41F1ED3964792D10_H
#ifndef POSE3D_T5AA21E36568E430CFD8D466575DB75CB62E1FB54_H
#define POSE3D_T5AA21E36568E430CFD8D466575DB75CB62E1FB54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pose3D
struct  Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Pose3D::<Position>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionU3Ek__BackingField_1;
	// UnityEngine.Quaternion Pose3D::<Orientation>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3COrientationU3Ek__BackingField_2;
	// UnityEngine.Matrix4x4 Pose3D::<Matrix>k__BackingField
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___U3CMatrixU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54, ___U3CPositionU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionU3Ek__BackingField_1() const { return ___U3CPositionU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionU3Ek__BackingField_1() { return &___U3CPositionU3Ek__BackingField_1; }
	inline void set_U3CPositionU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COrientationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54, ___U3COrientationU3Ek__BackingField_2)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3COrientationU3Ek__BackingField_2() const { return ___U3COrientationU3Ek__BackingField_2; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3COrientationU3Ek__BackingField_2() { return &___U3COrientationU3Ek__BackingField_2; }
	inline void set_U3COrientationU3Ek__BackingField_2(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3COrientationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54, ___U3CMatrixU3Ek__BackingField_3)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_U3CMatrixU3Ek__BackingField_3() const { return ___U3CMatrixU3Ek__BackingField_3; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_U3CMatrixU3Ek__BackingField_3() { return &___U3CMatrixU3Ek__BackingField_3; }
	inline void set_U3CMatrixU3Ek__BackingField_3(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___U3CMatrixU3Ek__BackingField_3 = value;
	}
};

struct Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Pose3D::FLIP_Z
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___FLIP_Z_0;

public:
	inline static int32_t get_offset_of_FLIP_Z_0() { return static_cast<int32_t>(offsetof(Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54_StaticFields, ___FLIP_Z_0)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_FLIP_Z_0() const { return ___FLIP_Z_0; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_FLIP_Z_0() { return &___FLIP_Z_0; }
	inline void set_FLIP_Z_0(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___FLIP_Z_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE3D_T5AA21E36568E430CFD8D466575DB75CB62E1FB54_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef BUILDER_T9A3B43148D02AB6C7D345FE90B54F2288BC522CB_H
#define BUILDER_T9A3B43148D02AB6C7D345FE90B54F2288BC522CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Builder
struct  Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB  : public GeneratedBuilderLite_2_t1874DEA62BEC7D523C565D33E405082BD01607C1
{
public:
	// System.Boolean proto.PhoneEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent proto.PhoneEvent_Builder::result
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB, ___result_1)); }
	inline PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * get_result_1() const { return ___result_1; }
	inline PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T9A3B43148D02AB6C7D345FE90B54F2288BC522CB_H
#ifndef ACCELEROMETEREVENT_TB158AB76F365C24CE204B273BC3FFD429C12F01F_H
#define ACCELEROMETEREVENT_TB158AB76F365C24CE204B273BC3FFD429C12F01F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_AccelerometerEvent
struct  AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F  : public GeneratedMessageLite_2_tDE80B1D20C4C49805F90AF5F1E9A2441D27E1881
{
public:
	// System.Boolean proto.PhoneEvent_Types_AccelerometerEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent_Types_AccelerometerEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent_Types_AccelerometerEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent_Types_AccelerometerEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent_Types_AccelerometerEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent_Types_AccelerometerEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent_Types_AccelerometerEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent_Types_AccelerometerEvent::z_
	float ___z__14;
	// System.Int32 proto.PhoneEvent_Types_AccelerometerEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields
{
public:
	// proto.PhoneEvent_Types_AccelerometerEvent proto.PhoneEvent_Types_AccelerometerEvent::defaultInstance
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_AccelerometerEvent::_accelerometerEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____accelerometerEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_AccelerometerEvent::_accelerometerEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____accelerometerEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields, ___defaultInstance_0)); }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__accelerometerEventFieldNames_1() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields, ____accelerometerEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__accelerometerEventFieldNames_1() const { return ____accelerometerEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__accelerometerEventFieldNames_1() { return &____accelerometerEventFieldNames_1; }
	inline void set__accelerometerEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____accelerometerEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____accelerometerEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__accelerometerEventFieldTags_2() { return static_cast<int32_t>(offsetof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields, ____accelerometerEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__accelerometerEventFieldTags_2() const { return ____accelerometerEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__accelerometerEventFieldTags_2() { return &____accelerometerEventFieldTags_2; }
	inline void set__accelerometerEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____accelerometerEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____accelerometerEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCELEROMETEREVENT_TB158AB76F365C24CE204B273BC3FFD429C12F01F_H
#ifndef BUILDER_T17E7B4A37A494267F90C1C0E60A2854A44757055_H
#define BUILDER_T17E7B4A37A494267F90C1C0E60A2854A44757055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_AccelerometerEvent_Builder
struct  Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055  : public GeneratedBuilderLite_2_t4662386564B924425F9CF3CB3829A84D391136C6
{
public:
	// System.Boolean proto.PhoneEvent_Types_AccelerometerEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_AccelerometerEvent proto.PhoneEvent_Types_AccelerometerEvent_Builder::result
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055, ___result_1)); }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * get_result_1() const { return ___result_1; }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T17E7B4A37A494267F90C1C0E60A2854A44757055_H
#ifndef DEPTHMAPEVENT_TE655CB2D2B764E6BEDE058F70764D529BEA00571_H
#define DEPTHMAPEVENT_TE655CB2D2B764E6BEDE058F70764D529BEA00571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_DepthMapEvent
struct  DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571  : public GeneratedMessageLite_2_t6FCB86E904843057A67C5E4EA401994F784489FA
{
public:
	// System.Boolean proto.PhoneEvent_Types_DepthMapEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent_Types_DepthMapEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent_Types_DepthMapEvent::hasWidth
	bool ___hasWidth_7;
	// System.Int32 proto.PhoneEvent_Types_DepthMapEvent::width_
	int32_t ___width__8;
	// System.Boolean proto.PhoneEvent_Types_DepthMapEvent::hasHeight
	bool ___hasHeight_10;
	// System.Int32 proto.PhoneEvent_Types_DepthMapEvent::height_
	int32_t ___height__11;
	// System.Int32 proto.PhoneEvent_Types_DepthMapEvent::zDistancesMemoizedSerializedSize
	int32_t ___zDistancesMemoizedSerializedSize_13;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single> proto.PhoneEvent_Types_DepthMapEvent::zDistances_
	PopsicleList_1_t95715200FE3EBBDE1714079383707539CB8BC801 * ___zDistances__14;
	// System.Int32 proto.PhoneEvent_Types_DepthMapEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasWidth_7() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___hasWidth_7)); }
	inline bool get_hasWidth_7() const { return ___hasWidth_7; }
	inline bool* get_address_of_hasWidth_7() { return &___hasWidth_7; }
	inline void set_hasWidth_7(bool value)
	{
		___hasWidth_7 = value;
	}

	inline static int32_t get_offset_of_width__8() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___width__8)); }
	inline int32_t get_width__8() const { return ___width__8; }
	inline int32_t* get_address_of_width__8() { return &___width__8; }
	inline void set_width__8(int32_t value)
	{
		___width__8 = value;
	}

	inline static int32_t get_offset_of_hasHeight_10() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___hasHeight_10)); }
	inline bool get_hasHeight_10() const { return ___hasHeight_10; }
	inline bool* get_address_of_hasHeight_10() { return &___hasHeight_10; }
	inline void set_hasHeight_10(bool value)
	{
		___hasHeight_10 = value;
	}

	inline static int32_t get_offset_of_height__11() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___height__11)); }
	inline int32_t get_height__11() const { return ___height__11; }
	inline int32_t* get_address_of_height__11() { return &___height__11; }
	inline void set_height__11(int32_t value)
	{
		___height__11 = value;
	}

	inline static int32_t get_offset_of_zDistancesMemoizedSerializedSize_13() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___zDistancesMemoizedSerializedSize_13)); }
	inline int32_t get_zDistancesMemoizedSerializedSize_13() const { return ___zDistancesMemoizedSerializedSize_13; }
	inline int32_t* get_address_of_zDistancesMemoizedSerializedSize_13() { return &___zDistancesMemoizedSerializedSize_13; }
	inline void set_zDistancesMemoizedSerializedSize_13(int32_t value)
	{
		___zDistancesMemoizedSerializedSize_13 = value;
	}

	inline static int32_t get_offset_of_zDistances__14() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___zDistances__14)); }
	inline PopsicleList_1_t95715200FE3EBBDE1714079383707539CB8BC801 * get_zDistances__14() const { return ___zDistances__14; }
	inline PopsicleList_1_t95715200FE3EBBDE1714079383707539CB8BC801 ** get_address_of_zDistances__14() { return &___zDistances__14; }
	inline void set_zDistances__14(PopsicleList_1_t95715200FE3EBBDE1714079383707539CB8BC801 * value)
	{
		___zDistances__14 = value;
		Il2CppCodeGenWriteBarrier((&___zDistances__14), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields
{
public:
	// proto.PhoneEvent_Types_DepthMapEvent proto.PhoneEvent_Types_DepthMapEvent::defaultInstance
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_DepthMapEvent::_depthMapEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____depthMapEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_DepthMapEvent::_depthMapEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____depthMapEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields, ___defaultInstance_0)); }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldNames_1() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields, ____depthMapEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__depthMapEventFieldNames_1() const { return ____depthMapEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__depthMapEventFieldNames_1() { return &____depthMapEventFieldNames_1; }
	inline void set__depthMapEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____depthMapEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldTags_2() { return static_cast<int32_t>(offsetof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields, ____depthMapEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__depthMapEventFieldTags_2() const { return ____depthMapEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__depthMapEventFieldTags_2() { return &____depthMapEventFieldTags_2; }
	inline void set__depthMapEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____depthMapEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHMAPEVENT_TE655CB2D2B764E6BEDE058F70764D529BEA00571_H
#ifndef BUILDER_T8D4FAD3FA57F88210975300FD1136B3D6C120613_H
#define BUILDER_T8D4FAD3FA57F88210975300FD1136B3D6C120613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_DepthMapEvent_Builder
struct  Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613  : public GeneratedBuilderLite_2_t718FB2342CB3BC4A823EFA51BE7B0ABF31845656
{
public:
	// System.Boolean proto.PhoneEvent_Types_DepthMapEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_DepthMapEvent proto.PhoneEvent_Types_DepthMapEvent_Builder::result
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613, ___result_1)); }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * get_result_1() const { return ___result_1; }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T8D4FAD3FA57F88210975300FD1136B3D6C120613_H
#ifndef GYROSCOPEEVENT_T43B4FB209A9EEA862ABA6917622C810575AFD764_H
#define GYROSCOPEEVENT_T43B4FB209A9EEA862ABA6917622C810575AFD764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_GyroscopeEvent
struct  GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764  : public GeneratedMessageLite_2_t91D39F4FF47A74575D85083883EDDAC443F96FB3
{
public:
	// System.Boolean proto.PhoneEvent_Types_GyroscopeEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent_Types_GyroscopeEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent_Types_GyroscopeEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent_Types_GyroscopeEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent_Types_GyroscopeEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent_Types_GyroscopeEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent_Types_GyroscopeEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent_Types_GyroscopeEvent::z_
	float ___z__14;
	// System.Int32 proto.PhoneEvent_Types_GyroscopeEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields
{
public:
	// proto.PhoneEvent_Types_GyroscopeEvent proto.PhoneEvent_Types_GyroscopeEvent::defaultInstance
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_GyroscopeEvent::_gyroscopeEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____gyroscopeEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_GyroscopeEvent::_gyroscopeEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____gyroscopeEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields, ___defaultInstance_0)); }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldNames_1() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields, ____gyroscopeEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__gyroscopeEventFieldNames_1() const { return ____gyroscopeEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__gyroscopeEventFieldNames_1() { return &____gyroscopeEventFieldNames_1; }
	inline void set__gyroscopeEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____gyroscopeEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldTags_2() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields, ____gyroscopeEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__gyroscopeEventFieldTags_2() const { return ____gyroscopeEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__gyroscopeEventFieldTags_2() { return &____gyroscopeEventFieldTags_2; }
	inline void set__gyroscopeEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____gyroscopeEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPEEVENT_T43B4FB209A9EEA862ABA6917622C810575AFD764_H
#ifndef BUILDER_T23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9_H
#define BUILDER_T23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_GyroscopeEvent_Builder
struct  Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9  : public GeneratedBuilderLite_2_t4D48615E09F7819C6505B73B82FAECCE7650526D
{
public:
	// System.Boolean proto.PhoneEvent_Types_GyroscopeEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_GyroscopeEvent proto.PhoneEvent_Types_GyroscopeEvent_Builder::result
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9, ___result_1)); }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * get_result_1() const { return ___result_1; }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9_H
#ifndef KEYEVENT_TBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_H
#define KEYEVENT_TBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_KeyEvent
struct  KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C  : public GeneratedMessageLite_2_t6EEEF2CFF7BEC92FEA73FAB4528C25ABB7C34E95
{
public:
	// System.Boolean proto.PhoneEvent_Types_KeyEvent::hasAction
	bool ___hasAction_4;
	// System.Int32 proto.PhoneEvent_Types_KeyEvent::action_
	int32_t ___action__5;
	// System.Boolean proto.PhoneEvent_Types_KeyEvent::hasCode
	bool ___hasCode_7;
	// System.Int32 proto.PhoneEvent_Types_KeyEvent::code_
	int32_t ___code__8;
	// System.Int32 proto.PhoneEvent_Types_KeyEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_9;

public:
	inline static int32_t get_offset_of_hasAction_4() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C, ___hasAction_4)); }
	inline bool get_hasAction_4() const { return ___hasAction_4; }
	inline bool* get_address_of_hasAction_4() { return &___hasAction_4; }
	inline void set_hasAction_4(bool value)
	{
		___hasAction_4 = value;
	}

	inline static int32_t get_offset_of_action__5() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C, ___action__5)); }
	inline int32_t get_action__5() const { return ___action__5; }
	inline int32_t* get_address_of_action__5() { return &___action__5; }
	inline void set_action__5(int32_t value)
	{
		___action__5 = value;
	}

	inline static int32_t get_offset_of_hasCode_7() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C, ___hasCode_7)); }
	inline bool get_hasCode_7() const { return ___hasCode_7; }
	inline bool* get_address_of_hasCode_7() { return &___hasCode_7; }
	inline void set_hasCode_7(bool value)
	{
		___hasCode_7 = value;
	}

	inline static int32_t get_offset_of_code__8() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C, ___code__8)); }
	inline int32_t get_code__8() const { return ___code__8; }
	inline int32_t* get_address_of_code__8() { return &___code__8; }
	inline void set_code__8(int32_t value)
	{
		___code__8 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_9() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C, ___memoizedSerializedSize_9)); }
	inline int32_t get_memoizedSerializedSize_9() const { return ___memoizedSerializedSize_9; }
	inline int32_t* get_address_of_memoizedSerializedSize_9() { return &___memoizedSerializedSize_9; }
	inline void set_memoizedSerializedSize_9(int32_t value)
	{
		___memoizedSerializedSize_9 = value;
	}
};

struct KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields
{
public:
	// proto.PhoneEvent_Types_KeyEvent proto.PhoneEvent_Types_KeyEvent::defaultInstance
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_KeyEvent::_keyEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____keyEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_KeyEvent::_keyEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____keyEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields, ___defaultInstance_0)); }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__keyEventFieldNames_1() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields, ____keyEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__keyEventFieldNames_1() const { return ____keyEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__keyEventFieldNames_1() { return &____keyEventFieldNames_1; }
	inline void set__keyEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____keyEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__keyEventFieldTags_2() { return static_cast<int32_t>(offsetof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields, ____keyEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__keyEventFieldTags_2() const { return ____keyEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__keyEventFieldTags_2() { return &____keyEventFieldTags_2; }
	inline void set__keyEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____keyEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_TBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_H
#ifndef BUILDER_T6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E_H
#define BUILDER_T6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_KeyEvent_Builder
struct  Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E  : public GeneratedBuilderLite_2_t7F66FD94BEE7EB747665A42D112B1D1D2B3BF222
{
public:
	// System.Boolean proto.PhoneEvent_Types_KeyEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_KeyEvent proto.PhoneEvent_Types_KeyEvent_Builder::result
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E, ___result_1)); }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * get_result_1() const { return ___result_1; }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E_H
#ifndef MOTIONEVENT_TC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_H
#define MOTIONEVENT_TC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_MotionEvent
struct  MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3  : public GeneratedMessageLite_2_t4059E40E4014A616AB6F919B2B40705CBA5BF168
{
public:
	// System.Boolean proto.PhoneEvent_Types_MotionEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent_Types_MotionEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent_Types_MotionEvent::hasAction
	bool ___hasAction_7;
	// System.Int32 proto.PhoneEvent_Types_MotionEvent::action_
	int32_t ___action__8;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent_Types_MotionEvent_Types_Pointer> proto.PhoneEvent_Types_MotionEvent::pointers_
	PopsicleList_1_t14390BBA7993164E3E4B3BAEE383DBA40E2C8120 * ___pointers__10;
	// System.Int32 proto.PhoneEvent_Types_MotionEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_11;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasAction_7() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___hasAction_7)); }
	inline bool get_hasAction_7() const { return ___hasAction_7; }
	inline bool* get_address_of_hasAction_7() { return &___hasAction_7; }
	inline void set_hasAction_7(bool value)
	{
		___hasAction_7 = value;
	}

	inline static int32_t get_offset_of_action__8() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___action__8)); }
	inline int32_t get_action__8() const { return ___action__8; }
	inline int32_t* get_address_of_action__8() { return &___action__8; }
	inline void set_action__8(int32_t value)
	{
		___action__8 = value;
	}

	inline static int32_t get_offset_of_pointers__10() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___pointers__10)); }
	inline PopsicleList_1_t14390BBA7993164E3E4B3BAEE383DBA40E2C8120 * get_pointers__10() const { return ___pointers__10; }
	inline PopsicleList_1_t14390BBA7993164E3E4B3BAEE383DBA40E2C8120 ** get_address_of_pointers__10() { return &___pointers__10; }
	inline void set_pointers__10(PopsicleList_1_t14390BBA7993164E3E4B3BAEE383DBA40E2C8120 * value)
	{
		___pointers__10 = value;
		Il2CppCodeGenWriteBarrier((&___pointers__10), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_11() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3, ___memoizedSerializedSize_11)); }
	inline int32_t get_memoizedSerializedSize_11() const { return ___memoizedSerializedSize_11; }
	inline int32_t* get_address_of_memoizedSerializedSize_11() { return &___memoizedSerializedSize_11; }
	inline void set_memoizedSerializedSize_11(int32_t value)
	{
		___memoizedSerializedSize_11 = value;
	}
};

struct MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields
{
public:
	// proto.PhoneEvent_Types_MotionEvent proto.PhoneEvent_Types_MotionEvent::defaultInstance
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_MotionEvent::_motionEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____motionEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_MotionEvent::_motionEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____motionEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields, ___defaultInstance_0)); }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__motionEventFieldNames_1() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields, ____motionEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__motionEventFieldNames_1() const { return ____motionEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__motionEventFieldNames_1() { return &____motionEventFieldNames_1; }
	inline void set__motionEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____motionEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__motionEventFieldTags_2() { return static_cast<int32_t>(offsetof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields, ____motionEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__motionEventFieldTags_2() const { return ____motionEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__motionEventFieldTags_2() { return &____motionEventFieldTags_2; }
	inline void set__motionEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____motionEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONEVENT_TC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_H
#ifndef BUILDER_T788A156BB792686149E9E866F1C7D953296A19D1_H
#define BUILDER_T788A156BB792686149E9E866F1C7D953296A19D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_MotionEvent_Builder
struct  Builder_t788A156BB792686149E9E866F1C7D953296A19D1  : public GeneratedBuilderLite_2_t7476B897B3679878A8A39DD9C4F09DD99BE4F183
{
public:
	// System.Boolean proto.PhoneEvent_Types_MotionEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_MotionEvent proto.PhoneEvent_Types_MotionEvent_Builder::result
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t788A156BB792686149E9E866F1C7D953296A19D1, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t788A156BB792686149E9E866F1C7D953296A19D1, ___result_1)); }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * get_result_1() const { return ___result_1; }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T788A156BB792686149E9E866F1C7D953296A19D1_H
#ifndef POINTER_T5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_H
#define POINTER_T5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_MotionEvent_Types_Pointer
struct  Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1  : public GeneratedMessageLite_2_t828F0AA7CD042862A1C0224BDFE22C528C63FB93
{
public:
	// System.Boolean proto.PhoneEvent_Types_MotionEvent_Types_Pointer::hasId
	bool ___hasId_4;
	// System.Int32 proto.PhoneEvent_Types_MotionEvent_Types_Pointer::id_
	int32_t ___id__5;
	// System.Boolean proto.PhoneEvent_Types_MotionEvent_Types_Pointer::hasNormalizedX
	bool ___hasNormalizedX_7;
	// System.Single proto.PhoneEvent_Types_MotionEvent_Types_Pointer::normalizedX_
	float ___normalizedX__8;
	// System.Boolean proto.PhoneEvent_Types_MotionEvent_Types_Pointer::hasNormalizedY
	bool ___hasNormalizedY_10;
	// System.Single proto.PhoneEvent_Types_MotionEvent_Types_Pointer::normalizedY_
	float ___normalizedY__11;
	// System.Int32 proto.PhoneEvent_Types_MotionEvent_Types_Pointer::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_12;

public:
	inline static int32_t get_offset_of_hasId_4() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___hasId_4)); }
	inline bool get_hasId_4() const { return ___hasId_4; }
	inline bool* get_address_of_hasId_4() { return &___hasId_4; }
	inline void set_hasId_4(bool value)
	{
		___hasId_4 = value;
	}

	inline static int32_t get_offset_of_id__5() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___id__5)); }
	inline int32_t get_id__5() const { return ___id__5; }
	inline int32_t* get_address_of_id__5() { return &___id__5; }
	inline void set_id__5(int32_t value)
	{
		___id__5 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedX_7() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___hasNormalizedX_7)); }
	inline bool get_hasNormalizedX_7() const { return ___hasNormalizedX_7; }
	inline bool* get_address_of_hasNormalizedX_7() { return &___hasNormalizedX_7; }
	inline void set_hasNormalizedX_7(bool value)
	{
		___hasNormalizedX_7 = value;
	}

	inline static int32_t get_offset_of_normalizedX__8() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___normalizedX__8)); }
	inline float get_normalizedX__8() const { return ___normalizedX__8; }
	inline float* get_address_of_normalizedX__8() { return &___normalizedX__8; }
	inline void set_normalizedX__8(float value)
	{
		___normalizedX__8 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedY_10() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___hasNormalizedY_10)); }
	inline bool get_hasNormalizedY_10() const { return ___hasNormalizedY_10; }
	inline bool* get_address_of_hasNormalizedY_10() { return &___hasNormalizedY_10; }
	inline void set_hasNormalizedY_10(bool value)
	{
		___hasNormalizedY_10 = value;
	}

	inline static int32_t get_offset_of_normalizedY__11() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___normalizedY__11)); }
	inline float get_normalizedY__11() const { return ___normalizedY__11; }
	inline float* get_address_of_normalizedY__11() { return &___normalizedY__11; }
	inline void set_normalizedY__11(float value)
	{
		___normalizedY__11 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_12() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1, ___memoizedSerializedSize_12)); }
	inline int32_t get_memoizedSerializedSize_12() const { return ___memoizedSerializedSize_12; }
	inline int32_t* get_address_of_memoizedSerializedSize_12() { return &___memoizedSerializedSize_12; }
	inline void set_memoizedSerializedSize_12(int32_t value)
	{
		___memoizedSerializedSize_12 = value;
	}
};

struct Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields
{
public:
	// proto.PhoneEvent_Types_MotionEvent_Types_Pointer proto.PhoneEvent_Types_MotionEvent_Types_Pointer::defaultInstance
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_MotionEvent_Types_Pointer::_pointerFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____pointerFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_MotionEvent_Types_Pointer::_pointerFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____pointerFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields, ___defaultInstance_0)); }
	inline Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__pointerFieldNames_1() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields, ____pointerFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__pointerFieldNames_1() const { return ____pointerFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__pointerFieldNames_1() { return &____pointerFieldNames_1; }
	inline void set__pointerFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____pointerFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldNames_1), value);
	}

	inline static int32_t get_offset_of__pointerFieldTags_2() { return static_cast<int32_t>(offsetof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields, ____pointerFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__pointerFieldTags_2() const { return ____pointerFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__pointerFieldTags_2() { return &____pointerFieldTags_2; }
	inline void set__pointerFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____pointerFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_T5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_H
#ifndef BUILDER_T26593F11EB307FF14A9F5E5D7C27F75A27E1997F_H
#define BUILDER_T26593F11EB307FF14A9F5E5D7C27F75A27E1997F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder
struct  Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F  : public GeneratedBuilderLite_2_t9620BCC5A37FA1FDDB9943F022CC552E33D177D1
{
public:
	// System.Boolean proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_MotionEvent_Types_Pointer proto.PhoneEvent_Types_MotionEvent_Types_Pointer_Builder::result
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F, ___result_1)); }
	inline Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * get_result_1() const { return ___result_1; }
	inline Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T26593F11EB307FF14A9F5E5D7C27F75A27E1997F_H
#ifndef ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#define ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_OrientationEvent
struct  OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011  : public GeneratedMessageLite_2_tF07B6BF883A996942E93FD145584FDDE91AC5219
{
public:
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent_Types_OrientationEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent_Types_OrientationEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent_Types_OrientationEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent_Types_OrientationEvent::z_
	float ___z__14;
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent::hasW
	bool ___hasW_16;
	// System.Single proto.PhoneEvent_Types_OrientationEvent::w_
	float ___w__17;
	// System.Int32 proto.PhoneEvent_Types_OrientationEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_18;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_hasW_16() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___hasW_16)); }
	inline bool get_hasW_16() const { return ___hasW_16; }
	inline bool* get_address_of_hasW_16() { return &___hasW_16; }
	inline void set_hasW_16(bool value)
	{
		___hasW_16 = value;
	}

	inline static int32_t get_offset_of_w__17() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___w__17)); }
	inline float get_w__17() const { return ___w__17; }
	inline float* get_address_of_w__17() { return &___w__17; }
	inline void set_w__17(float value)
	{
		___w__17 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_18() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011, ___memoizedSerializedSize_18)); }
	inline int32_t get_memoizedSerializedSize_18() const { return ___memoizedSerializedSize_18; }
	inline int32_t* get_address_of_memoizedSerializedSize_18() { return &___memoizedSerializedSize_18; }
	inline void set_memoizedSerializedSize_18(int32_t value)
	{
		___memoizedSerializedSize_18 = value;
	}
};

struct OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields
{
public:
	// proto.PhoneEvent_Types_OrientationEvent proto.PhoneEvent_Types_OrientationEvent::defaultInstance
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent_Types_OrientationEvent::_orientationEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____orientationEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent_Types_OrientationEvent::_orientationEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____orientationEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ___defaultInstance_0)); }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldNames_1() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ____orientationEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__orientationEventFieldNames_1() const { return ____orientationEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__orientationEventFieldNames_1() { return &____orientationEventFieldNames_1; }
	inline void set__orientationEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____orientationEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldTags_2() { return static_cast<int32_t>(offsetof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields, ____orientationEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__orientationEventFieldTags_2() const { return ____orientationEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__orientationEventFieldTags_2() { return &____orientationEventFieldTags_2; }
	inline void set__orientationEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____orientationEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONEVENT_TE448386384E4E5CC5C4FFBB5A22055845E0A9011_H
#ifndef BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#define BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_OrientationEvent_Builder
struct  Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49  : public GeneratedBuilderLite_2_t5FE12C42B399AD2099894359F40D69FC579B1B9A
{
public:
	// System.Boolean proto.PhoneEvent_Types_OrientationEvent_Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent_Types_OrientationEvent proto.PhoneEvent_Types_OrientationEvent_Builder::result
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49, ___result_1)); }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * get_result_1() const { return ___result_1; }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_TCAC10B81FDF15941DE7FAC380E33D08FDF542C49_H
#ifndef TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#define TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_Type
struct  Type_tE66B948E7250AA336E8768E46508619266BABE96 
{
public:
	// System.Int32 proto.PhoneEvent_Types_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tE66B948E7250AA336E8768E46508619266BABE96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#ifndef CONTROLLERSTATE_T0BE3F1601302488E0F1DE33AFF299E9EED574451_H
#define CONTROLLERSTATE_T0BE3F1601302488E0F1DE33AFF299E9EED574451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerState
struct  ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451  : public RuntimeObject
{
public:
	// GvrConnectionState Gvr.Internal.ControllerState::connectionState
	int32_t ___connectionState_0;
	// GvrControllerApiStatus Gvr.Internal.ControllerState::apiStatus
	int32_t ___apiStatus_1;
	// UnityEngine.Quaternion Gvr.Internal.ControllerState::orientation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___orientation_2;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_3;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::gyro
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___gyro_4;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::accel
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___accel_5;
	// UnityEngine.Vector2 Gvr.Internal.ControllerState::touchPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPos_6;
	// System.Boolean Gvr.Internal.ControllerState::recentered
	bool ___recentered_7;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsState
	int32_t ___buttonsState_8;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsDown
	int32_t ___buttonsDown_9;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsUp
	int32_t ___buttonsUp_10;
	// System.String Gvr.Internal.ControllerState::errorDetails
	String_t* ___errorDetails_11;
	// System.IntPtr Gvr.Internal.ControllerState::gvrPtr
	intptr_t ___gvrPtr_12;
	// System.Boolean Gvr.Internal.ControllerState::isCharging
	bool ___isCharging_13;
	// GvrControllerBatteryLevel Gvr.Internal.ControllerState::batteryLevel
	int32_t ___batteryLevel_14;

public:
	inline static int32_t get_offset_of_connectionState_0() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___connectionState_0)); }
	inline int32_t get_connectionState_0() const { return ___connectionState_0; }
	inline int32_t* get_address_of_connectionState_0() { return &___connectionState_0; }
	inline void set_connectionState_0(int32_t value)
	{
		___connectionState_0 = value;
	}

	inline static int32_t get_offset_of_apiStatus_1() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___apiStatus_1)); }
	inline int32_t get_apiStatus_1() const { return ___apiStatus_1; }
	inline int32_t* get_address_of_apiStatus_1() { return &___apiStatus_1; }
	inline void set_apiStatus_1(int32_t value)
	{
		___apiStatus_1 = value;
	}

	inline static int32_t get_offset_of_orientation_2() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___orientation_2)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_orientation_2() const { return ___orientation_2; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_orientation_2() { return &___orientation_2; }
	inline void set_orientation_2(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___orientation_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___position_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_3() const { return ___position_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_gyro_4() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___gyro_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_gyro_4() const { return ___gyro_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_gyro_4() { return &___gyro_4; }
	inline void set_gyro_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___gyro_4 = value;
	}

	inline static int32_t get_offset_of_accel_5() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___accel_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_accel_5() const { return ___accel_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_accel_5() { return &___accel_5; }
	inline void set_accel_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___accel_5 = value;
	}

	inline static int32_t get_offset_of_touchPos_6() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___touchPos_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchPos_6() const { return ___touchPos_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchPos_6() { return &___touchPos_6; }
	inline void set_touchPos_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchPos_6 = value;
	}

	inline static int32_t get_offset_of_recentered_7() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___recentered_7)); }
	inline bool get_recentered_7() const { return ___recentered_7; }
	inline bool* get_address_of_recentered_7() { return &___recentered_7; }
	inline void set_recentered_7(bool value)
	{
		___recentered_7 = value;
	}

	inline static int32_t get_offset_of_buttonsState_8() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___buttonsState_8)); }
	inline int32_t get_buttonsState_8() const { return ___buttonsState_8; }
	inline int32_t* get_address_of_buttonsState_8() { return &___buttonsState_8; }
	inline void set_buttonsState_8(int32_t value)
	{
		___buttonsState_8 = value;
	}

	inline static int32_t get_offset_of_buttonsDown_9() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___buttonsDown_9)); }
	inline int32_t get_buttonsDown_9() const { return ___buttonsDown_9; }
	inline int32_t* get_address_of_buttonsDown_9() { return &___buttonsDown_9; }
	inline void set_buttonsDown_9(int32_t value)
	{
		___buttonsDown_9 = value;
	}

	inline static int32_t get_offset_of_buttonsUp_10() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___buttonsUp_10)); }
	inline int32_t get_buttonsUp_10() const { return ___buttonsUp_10; }
	inline int32_t* get_address_of_buttonsUp_10() { return &___buttonsUp_10; }
	inline void set_buttonsUp_10(int32_t value)
	{
		___buttonsUp_10 = value;
	}

	inline static int32_t get_offset_of_errorDetails_11() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___errorDetails_11)); }
	inline String_t* get_errorDetails_11() const { return ___errorDetails_11; }
	inline String_t** get_address_of_errorDetails_11() { return &___errorDetails_11; }
	inline void set_errorDetails_11(String_t* value)
	{
		___errorDetails_11 = value;
		Il2CppCodeGenWriteBarrier((&___errorDetails_11), value);
	}

	inline static int32_t get_offset_of_gvrPtr_12() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___gvrPtr_12)); }
	inline intptr_t get_gvrPtr_12() const { return ___gvrPtr_12; }
	inline intptr_t* get_address_of_gvrPtr_12() { return &___gvrPtr_12; }
	inline void set_gvrPtr_12(intptr_t value)
	{
		___gvrPtr_12 = value;
	}

	inline static int32_t get_offset_of_isCharging_13() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___isCharging_13)); }
	inline bool get_isCharging_13() const { return ___isCharging_13; }
	inline bool* get_address_of_isCharging_13() { return &___isCharging_13; }
	inline void set_isCharging_13(bool value)
	{
		___isCharging_13 = value;
	}

	inline static int32_t get_offset_of_batteryLevel_14() { return static_cast<int32_t>(offsetof(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451, ___batteryLevel_14)); }
	inline int32_t get_batteryLevel_14() const { return ___batteryLevel_14; }
	inline int32_t* get_address_of_batteryLevel_14() { return &___batteryLevel_14; }
	inline void set_batteryLevel_14(int32_t value)
	{
		___batteryLevel_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSTATE_T0BE3F1601302488E0F1DE33AFF299E9EED574451_H
#ifndef EMULATORBUTTONEVENT_TF3B0BF0210629C8389331911A44B3285536BC125_H
#define EMULATORBUTTONEVENT_TF3B0BF0210629C8389331911A44B3285536BC125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorButtonEvent
struct  EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125 
{
public:
	// Gvr.Internal.EmulatorButtonEvent_ButtonCode Gvr.Internal.EmulatorButtonEvent::code
	int32_t ___code_0;
	// System.Boolean Gvr.Internal.EmulatorButtonEvent::down
	bool ___down_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_down_1() { return static_cast<int32_t>(offsetof(EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125, ___down_1)); }
	inline bool get_down_1() const { return ___down_1; }
	inline bool* get_address_of_down_1() { return &___down_1; }
	inline void set_down_1(bool value)
	{
		___down_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.EmulatorButtonEvent
struct EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125_marshaled_pinvoke
{
	int32_t ___code_0;
	int32_t ___down_1;
};
// Native definition for COM marshalling of Gvr.Internal.EmulatorButtonEvent
struct EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125_marshaled_com
{
	int32_t ___code_0;
	int32_t ___down_1;
};
#endif // EMULATORBUTTONEVENT_TF3B0BF0210629C8389331911A44B3285536BC125_H
#ifndef HEADSETSTATE_T1DE0929297DCC52D513D29C437055C86783C94AA_H
#define HEADSETSTATE_T1DE0929297DCC52D513D29C437055C86783C94AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetState
struct  HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA 
{
public:
	// GvrEventType Gvr.Internal.HeadsetState::eventType
	int32_t ___eventType_0;
	// System.Int32 Gvr.Internal.HeadsetState::eventFlags
	int32_t ___eventFlags_1;
	// System.Int64 Gvr.Internal.HeadsetState::eventTimestampNs
	int64_t ___eventTimestampNs_2;
	// GvrRecenterEventType Gvr.Internal.HeadsetState::recenterEventType
	int32_t ___recenterEventType_3;
	// System.UInt32 Gvr.Internal.HeadsetState::recenterEventFlags
	uint32_t ___recenterEventFlags_4;
	// UnityEngine.Vector3 Gvr.Internal.HeadsetState::recenteredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___recenteredPosition_5;
	// UnityEngine.Quaternion Gvr.Internal.HeadsetState::recenteredRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___recenteredRotation_6;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_eventFlags_1() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___eventFlags_1)); }
	inline int32_t get_eventFlags_1() const { return ___eventFlags_1; }
	inline int32_t* get_address_of_eventFlags_1() { return &___eventFlags_1; }
	inline void set_eventFlags_1(int32_t value)
	{
		___eventFlags_1 = value;
	}

	inline static int32_t get_offset_of_eventTimestampNs_2() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___eventTimestampNs_2)); }
	inline int64_t get_eventTimestampNs_2() const { return ___eventTimestampNs_2; }
	inline int64_t* get_address_of_eventTimestampNs_2() { return &___eventTimestampNs_2; }
	inline void set_eventTimestampNs_2(int64_t value)
	{
		___eventTimestampNs_2 = value;
	}

	inline static int32_t get_offset_of_recenterEventType_3() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___recenterEventType_3)); }
	inline int32_t get_recenterEventType_3() const { return ___recenterEventType_3; }
	inline int32_t* get_address_of_recenterEventType_3() { return &___recenterEventType_3; }
	inline void set_recenterEventType_3(int32_t value)
	{
		___recenterEventType_3 = value;
	}

	inline static int32_t get_offset_of_recenterEventFlags_4() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___recenterEventFlags_4)); }
	inline uint32_t get_recenterEventFlags_4() const { return ___recenterEventFlags_4; }
	inline uint32_t* get_address_of_recenterEventFlags_4() { return &___recenterEventFlags_4; }
	inline void set_recenterEventFlags_4(uint32_t value)
	{
		___recenterEventFlags_4 = value;
	}

	inline static int32_t get_offset_of_recenteredPosition_5() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___recenteredPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_recenteredPosition_5() const { return ___recenteredPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_recenteredPosition_5() { return &___recenteredPosition_5; }
	inline void set_recenteredPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___recenteredPosition_5 = value;
	}

	inline static int32_t get_offset_of_recenteredRotation_6() { return static_cast<int32_t>(offsetof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA, ___recenteredRotation_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_recenteredRotation_6() const { return ___recenteredRotation_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_recenteredRotation_6() { return &___recenteredRotation_6; }
	inline void set_recenteredRotation_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___recenteredRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETSTATE_T1DE0929297DCC52D513D29C437055C86783C94AA_H
#ifndef MOUSECONTROLLERPROVIDER_T0D8E5D4784755448297378BB77B4529844D90D6D_H
#define MOUSECONTROLLERPROVIDER_T0D8E5D4784755448297378BB77B4529844D90D6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.MouseControllerProvider
struct  MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D  : public RuntimeObject
{
public:
	// Gvr.Internal.ControllerState Gvr.Internal.MouseControllerProvider::state
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * ___state_2;
	// UnityEngine.Vector2 Gvr.Internal.MouseControllerProvider::mouseDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mouseDelta_3;
	// System.Boolean Gvr.Internal.MouseControllerProvider::wasTouching
	bool ___wasTouching_4;
	// GvrControllerButton Gvr.Internal.MouseControllerProvider::lastButtonsState
	int32_t ___lastButtonsState_5;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D, ___state_2)); }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * get_state_2() const { return ___state_2; }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_mouseDelta_3() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D, ___mouseDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mouseDelta_3() const { return ___mouseDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mouseDelta_3() { return &___mouseDelta_3; }
	inline void set_mouseDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mouseDelta_3 = value;
	}

	inline static int32_t get_offset_of_wasTouching_4() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D, ___wasTouching_4)); }
	inline bool get_wasTouching_4() const { return ___wasTouching_4; }
	inline bool* get_address_of_wasTouching_4() { return &___wasTouching_4; }
	inline void set_wasTouching_4(bool value)
	{
		___wasTouching_4 = value;
	}

	inline static int32_t get_offset_of_lastButtonsState_5() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D, ___lastButtonsState_5)); }
	inline int32_t get_lastButtonsState_5() const { return ___lastButtonsState_5; }
	inline int32_t* get_address_of_lastButtonsState_5() { return &___lastButtonsState_5; }
	inline void set_lastButtonsState_5(int32_t value)
	{
		___lastButtonsState_5 = value;
	}
};

struct MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D_StaticFields
{
public:
	// UnityEngine.Vector3 Gvr.Internal.MouseControllerProvider::INVERT_Y
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___INVERT_Y_8;

public:
	inline static int32_t get_offset_of_INVERT_Y_8() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D_StaticFields, ___INVERT_Y_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_INVERT_Y_8() const { return ___INVERT_Y_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_INVERT_Y_8() { return &___INVERT_Y_8; }
	inline void set_INVERT_Y_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___INVERT_Y_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSECONTROLLERPROVIDER_T0D8E5D4784755448297378BB77B4529844D90D6D_H
#ifndef KEYBOARDSTATE_TF84F64C0158DA96525380E14D1C65C2E9923B9BB_H
#define KEYBOARDSTATE_TF84F64C0158DA96525380E14D1C65C2E9923B9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardState
struct  KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB  : public RuntimeObject
{
public:
	// System.String KeyboardState::editorText
	String_t* ___editorText_0;
	// GvrKeyboardInputMode KeyboardState::mode
	int32_t ___mode_1;
	// System.Boolean KeyboardState::isValid
	bool ___isValid_2;
	// System.Boolean KeyboardState::isReady
	bool ___isReady_3;
	// UnityEngine.Matrix4x4 KeyboardState::worldMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldMatrix_4;

public:
	inline static int32_t get_offset_of_editorText_0() { return static_cast<int32_t>(offsetof(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB, ___editorText_0)); }
	inline String_t* get_editorText_0() const { return ___editorText_0; }
	inline String_t** get_address_of_editorText_0() { return &___editorText_0; }
	inline void set_editorText_0(String_t* value)
	{
		___editorText_0 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_0), value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_isValid_2() { return static_cast<int32_t>(offsetof(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB, ___isValid_2)); }
	inline bool get_isValid_2() const { return ___isValid_2; }
	inline bool* get_address_of_isValid_2() { return &___isValid_2; }
	inline void set_isValid_2(bool value)
	{
		___isValid_2 = value;
	}

	inline static int32_t get_offset_of_isReady_3() { return static_cast<int32_t>(offsetof(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB, ___isReady_3)); }
	inline bool get_isReady_3() const { return ___isReady_3; }
	inline bool* get_address_of_isReady_3() { return &___isReady_3; }
	inline void set_isReady_3(bool value)
	{
		___isReady_3 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_4() { return static_cast<int32_t>(offsetof(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB, ___worldMatrix_4)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldMatrix_4() const { return ___worldMatrix_4; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldMatrix_4() { return &___worldMatrix_4; }
	inline void set_worldMatrix_4(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldMatrix_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDSTATE_TF84F64C0158DA96525380E14D1C65C2E9923B9BB_H
#ifndef MUTABLEPOSE3D_TAEFBBCFD0FFEBA819AC3A920371D3B52AEE9DADA_H
#define MUTABLEPOSE3D_TAEFBBCFD0FFEBA819AC3A920371D3B52AEE9DADA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MutablePose3D
struct  MutablePose3D_tAEFBBCFD0FFEBA819AC3A920371D3B52AEE9DADA  : public Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUTABLEPOSE3D_TAEFBBCFD0FFEBA819AC3A920371D3B52AEE9DADA_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef PHONEEVENT_T1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_H
#define PHONEEVENT_T1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent
struct  PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66  : public GeneratedMessageLite_2_tB81B19DA2DBB49691CB4C7EF70E3486F006F4805
{
public:
	// System.Boolean proto.PhoneEvent::hasType
	bool ___hasType_4;
	// proto.PhoneEvent_Types_Type proto.PhoneEvent::type_
	int32_t ___type__5;
	// System.Boolean proto.PhoneEvent::hasMotionEvent
	bool ___hasMotionEvent_7;
	// proto.PhoneEvent_Types_MotionEvent proto.PhoneEvent::motionEvent_
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * ___motionEvent__8;
	// System.Boolean proto.PhoneEvent::hasGyroscopeEvent
	bool ___hasGyroscopeEvent_10;
	// proto.PhoneEvent_Types_GyroscopeEvent proto.PhoneEvent::gyroscopeEvent_
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * ___gyroscopeEvent__11;
	// System.Boolean proto.PhoneEvent::hasAccelerometerEvent
	bool ___hasAccelerometerEvent_13;
	// proto.PhoneEvent_Types_AccelerometerEvent proto.PhoneEvent::accelerometerEvent_
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * ___accelerometerEvent__14;
	// System.Boolean proto.PhoneEvent::hasDepthMapEvent
	bool ___hasDepthMapEvent_16;
	// proto.PhoneEvent_Types_DepthMapEvent proto.PhoneEvent::depthMapEvent_
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * ___depthMapEvent__17;
	// System.Boolean proto.PhoneEvent::hasOrientationEvent
	bool ___hasOrientationEvent_19;
	// proto.PhoneEvent_Types_OrientationEvent proto.PhoneEvent::orientationEvent_
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * ___orientationEvent__20;
	// System.Boolean proto.PhoneEvent::hasKeyEvent
	bool ___hasKeyEvent_22;
	// proto.PhoneEvent_Types_KeyEvent proto.PhoneEvent::keyEvent_
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * ___keyEvent__23;
	// System.Int32 proto.PhoneEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_24;

public:
	inline static int32_t get_offset_of_hasType_4() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasType_4)); }
	inline bool get_hasType_4() const { return ___hasType_4; }
	inline bool* get_address_of_hasType_4() { return &___hasType_4; }
	inline void set_hasType_4(bool value)
	{
		___hasType_4 = value;
	}

	inline static int32_t get_offset_of_type__5() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___type__5)); }
	inline int32_t get_type__5() const { return ___type__5; }
	inline int32_t* get_address_of_type__5() { return &___type__5; }
	inline void set_type__5(int32_t value)
	{
		___type__5 = value;
	}

	inline static int32_t get_offset_of_hasMotionEvent_7() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasMotionEvent_7)); }
	inline bool get_hasMotionEvent_7() const { return ___hasMotionEvent_7; }
	inline bool* get_address_of_hasMotionEvent_7() { return &___hasMotionEvent_7; }
	inline void set_hasMotionEvent_7(bool value)
	{
		___hasMotionEvent_7 = value;
	}

	inline static int32_t get_offset_of_motionEvent__8() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___motionEvent__8)); }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * get_motionEvent__8() const { return ___motionEvent__8; }
	inline MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 ** get_address_of_motionEvent__8() { return &___motionEvent__8; }
	inline void set_motionEvent__8(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3 * value)
	{
		___motionEvent__8 = value;
		Il2CppCodeGenWriteBarrier((&___motionEvent__8), value);
	}

	inline static int32_t get_offset_of_hasGyroscopeEvent_10() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasGyroscopeEvent_10)); }
	inline bool get_hasGyroscopeEvent_10() const { return ___hasGyroscopeEvent_10; }
	inline bool* get_address_of_hasGyroscopeEvent_10() { return &___hasGyroscopeEvent_10; }
	inline void set_hasGyroscopeEvent_10(bool value)
	{
		___hasGyroscopeEvent_10 = value;
	}

	inline static int32_t get_offset_of_gyroscopeEvent__11() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___gyroscopeEvent__11)); }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * get_gyroscopeEvent__11() const { return ___gyroscopeEvent__11; }
	inline GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 ** get_address_of_gyroscopeEvent__11() { return &___gyroscopeEvent__11; }
	inline void set_gyroscopeEvent__11(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764 * value)
	{
		___gyroscopeEvent__11 = value;
		Il2CppCodeGenWriteBarrier((&___gyroscopeEvent__11), value);
	}

	inline static int32_t get_offset_of_hasAccelerometerEvent_13() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasAccelerometerEvent_13)); }
	inline bool get_hasAccelerometerEvent_13() const { return ___hasAccelerometerEvent_13; }
	inline bool* get_address_of_hasAccelerometerEvent_13() { return &___hasAccelerometerEvent_13; }
	inline void set_hasAccelerometerEvent_13(bool value)
	{
		___hasAccelerometerEvent_13 = value;
	}

	inline static int32_t get_offset_of_accelerometerEvent__14() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___accelerometerEvent__14)); }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * get_accelerometerEvent__14() const { return ___accelerometerEvent__14; }
	inline AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F ** get_address_of_accelerometerEvent__14() { return &___accelerometerEvent__14; }
	inline void set_accelerometerEvent__14(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F * value)
	{
		___accelerometerEvent__14 = value;
		Il2CppCodeGenWriteBarrier((&___accelerometerEvent__14), value);
	}

	inline static int32_t get_offset_of_hasDepthMapEvent_16() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasDepthMapEvent_16)); }
	inline bool get_hasDepthMapEvent_16() const { return ___hasDepthMapEvent_16; }
	inline bool* get_address_of_hasDepthMapEvent_16() { return &___hasDepthMapEvent_16; }
	inline void set_hasDepthMapEvent_16(bool value)
	{
		___hasDepthMapEvent_16 = value;
	}

	inline static int32_t get_offset_of_depthMapEvent__17() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___depthMapEvent__17)); }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * get_depthMapEvent__17() const { return ___depthMapEvent__17; }
	inline DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 ** get_address_of_depthMapEvent__17() { return &___depthMapEvent__17; }
	inline void set_depthMapEvent__17(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571 * value)
	{
		___depthMapEvent__17 = value;
		Il2CppCodeGenWriteBarrier((&___depthMapEvent__17), value);
	}

	inline static int32_t get_offset_of_hasOrientationEvent_19() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasOrientationEvent_19)); }
	inline bool get_hasOrientationEvent_19() const { return ___hasOrientationEvent_19; }
	inline bool* get_address_of_hasOrientationEvent_19() { return &___hasOrientationEvent_19; }
	inline void set_hasOrientationEvent_19(bool value)
	{
		___hasOrientationEvent_19 = value;
	}

	inline static int32_t get_offset_of_orientationEvent__20() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___orientationEvent__20)); }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * get_orientationEvent__20() const { return ___orientationEvent__20; }
	inline OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 ** get_address_of_orientationEvent__20() { return &___orientationEvent__20; }
	inline void set_orientationEvent__20(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011 * value)
	{
		___orientationEvent__20 = value;
		Il2CppCodeGenWriteBarrier((&___orientationEvent__20), value);
	}

	inline static int32_t get_offset_of_hasKeyEvent_22() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___hasKeyEvent_22)); }
	inline bool get_hasKeyEvent_22() const { return ___hasKeyEvent_22; }
	inline bool* get_address_of_hasKeyEvent_22() { return &___hasKeyEvent_22; }
	inline void set_hasKeyEvent_22(bool value)
	{
		___hasKeyEvent_22 = value;
	}

	inline static int32_t get_offset_of_keyEvent__23() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___keyEvent__23)); }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * get_keyEvent__23() const { return ___keyEvent__23; }
	inline KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C ** get_address_of_keyEvent__23() { return &___keyEvent__23; }
	inline void set_keyEvent__23(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C * value)
	{
		___keyEvent__23 = value;
		Il2CppCodeGenWriteBarrier((&___keyEvent__23), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_24() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66, ___memoizedSerializedSize_24)); }
	inline int32_t get_memoizedSerializedSize_24() const { return ___memoizedSerializedSize_24; }
	inline int32_t* get_address_of_memoizedSerializedSize_24() { return &___memoizedSerializedSize_24; }
	inline void set_memoizedSerializedSize_24(int32_t value)
	{
		___memoizedSerializedSize_24 = value;
	}
};

struct PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields
{
public:
	// proto.PhoneEvent proto.PhoneEvent::defaultInstance
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent::_phoneEventFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____phoneEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent::_phoneEventFieldTags
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____phoneEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields, ___defaultInstance_0)); }
	inline PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__phoneEventFieldNames_1() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields, ____phoneEventFieldNames_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__phoneEventFieldNames_1() const { return ____phoneEventFieldNames_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__phoneEventFieldNames_1() { return &____phoneEventFieldNames_1; }
	inline void set__phoneEventFieldNames_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____phoneEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____phoneEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__phoneEventFieldTags_2() { return static_cast<int32_t>(offsetof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields, ____phoneEventFieldTags_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__phoneEventFieldTags_2() const { return ____phoneEventFieldTags_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__phoneEventFieldTags_2() { return &____phoneEventFieldTags_2; }
	inline void set__phoneEventFieldTags_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____phoneEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____phoneEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_H
#ifndef DUMMYHEADSETPROVIDER_T78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D_H
#define DUMMYHEADSETPROVIDER_T78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyHeadsetProvider
struct  DummyHeadsetProvider_t78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.DummyHeadsetProvider::dummyState
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyHeadsetProvider_t78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D, ___dummyState_0)); }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYHEADSETPROVIDER_T78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D_H
#ifndef EDITORHEADSETPROVIDER_T9BF4A3F07FDAA378510721BF96BB0746854CAC59_H
#define EDITORHEADSETPROVIDER_T9BF4A3F07FDAA378510721BF96BB0746854CAC59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EditorHeadsetProvider
struct  EditorHeadsetProvider_t9BF4A3F07FDAA378510721BF96BB0746854CAC59  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.EditorHeadsetProvider::dummyState
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(EditorHeadsetProvider_t9BF4A3F07FDAA378510721BF96BB0746854CAC59, ___dummyState_0)); }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHEADSETPROVIDER_T9BF4A3F07FDAA378510721BF96BB0746854CAC59_H
#ifndef ONRECENTEREVENT_TE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A_H
#define ONRECENTEREVENT_TE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset_OnRecenterEvent
struct  OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECENTEREVENT_TE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A_H
#ifndef ONSAFETYREGIONEVENT_T22FC4164839C97972ECFD232F25C1ACDA5F00FB6_H
#define ONSAFETYREGIONEVENT_T22FC4164839C97972ECFD232F25C1ACDA5F00FB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset_OnSafetyRegionEvent
struct  OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSAFETYREGIONEVENT_T22FC4164839C97972ECFD232F25C1ACDA5F00FB6_H
#ifndef EDITTEXTCALLBACK_T7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A_H
#define EDITTEXTCALLBACK_T7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard_EditTextCallback
struct  EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITTEXTCALLBACK_T7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A_H
#ifndef ERRORCALLBACK_T8916A2402C92902DA76B58F0CBA3265E462ECDA1_H
#define ERRORCALLBACK_T8916A2402C92902DA76B58F0CBA3265E462ECDA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard_ErrorCallback
struct  ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCALLBACK_T8916A2402C92902DA76B58F0CBA3265E462ECDA1_H
#ifndef KEYBOARDCALLBACK_T7116BC2C90CC8642350FAB4C362B8B07F2DAAB45_H
#define KEYBOARDCALLBACK_T7116BC2C90CC8642350FAB4C362B8B07F2DAAB45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard_KeyboardCallback
struct  KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T7116BC2C90CC8642350FAB4C362B8B07F2DAAB45_H
#ifndef STANDARDCALLBACK_T76BBD536FAAD61296741018AC73FFCDB83013DF9_H
#define STANDARDCALLBACK_T76BBD536FAAD61296741018AC73FFCDB83013DF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard_StandardCallback
struct  StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDCALLBACK_T76BBD536FAAD61296741018AC73FFCDB83013DF9_H
#ifndef ONEXCEPTIONCALLBACK_T242241ACCD84605752389B455720ED334739A581_H
#define ONEXCEPTIONCALLBACK_T242241ACCD84605752389B455720ED334739A581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_OnExceptionCallback
struct  OnExceptionCallback_t242241ACCD84605752389B455720ED334739A581  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXCEPTIONCALLBACK_T242241ACCD84605752389B455720ED334739A581_H
#ifndef ONVIDEOEVENTCALLBACK_T421705FACDFDD6D7E7FE943A515EFD2CBE12E0EA_H
#define ONVIDEOEVENTCALLBACK_T421705FACDFDD6D7E7FE943A515EFD2CBE12E0EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture_OnVideoEventCallback
struct  OnVideoEventCallback_t421705FACDFDD6D7E7FE943A515EFD2CBE12E0EA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVIDEOEVENTCALLBACK_T421705FACDFDD6D7E7FE943A515EFD2CBE12E0EA_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GHOSTFREEROAMCAMERA_T503FCDB2A6DC93073EE765359D313EE7EA184065_H
#define GHOSTFREEROAMCAMERA_T503FCDB2A6DC93073EE765359D313EE7EA184065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostFreeRoamCamera
struct  GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single GhostFreeRoamCamera::initialSpeed
	float ___initialSpeed_4;
	// System.Single GhostFreeRoamCamera::increaseSpeed
	float ___increaseSpeed_5;
	// System.Boolean GhostFreeRoamCamera::allowMovement
	bool ___allowMovement_6;
	// System.Boolean GhostFreeRoamCamera::allowRotation
	bool ___allowRotation_7;
	// UnityEngine.KeyCode GhostFreeRoamCamera::forwardButton
	int32_t ___forwardButton_8;
	// UnityEngine.KeyCode GhostFreeRoamCamera::backwardButton
	int32_t ___backwardButton_9;
	// UnityEngine.KeyCode GhostFreeRoamCamera::rightButton
	int32_t ___rightButton_10;
	// UnityEngine.KeyCode GhostFreeRoamCamera::leftButton
	int32_t ___leftButton_11;
	// System.Single GhostFreeRoamCamera::cursorSensitivity
	float ___cursorSensitivity_12;
	// System.Boolean GhostFreeRoamCamera::cursorToggleAllowed
	bool ___cursorToggleAllowed_13;
	// UnityEngine.KeyCode GhostFreeRoamCamera::cursorToggleButton
	int32_t ___cursorToggleButton_14;
	// System.Single GhostFreeRoamCamera::currentSpeed
	float ___currentSpeed_15;
	// System.Boolean GhostFreeRoamCamera::moving
	bool ___moving_16;
	// System.Boolean GhostFreeRoamCamera::togglePressed
	bool ___togglePressed_17;

public:
	inline static int32_t get_offset_of_initialSpeed_4() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___initialSpeed_4)); }
	inline float get_initialSpeed_4() const { return ___initialSpeed_4; }
	inline float* get_address_of_initialSpeed_4() { return &___initialSpeed_4; }
	inline void set_initialSpeed_4(float value)
	{
		___initialSpeed_4 = value;
	}

	inline static int32_t get_offset_of_increaseSpeed_5() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___increaseSpeed_5)); }
	inline float get_increaseSpeed_5() const { return ___increaseSpeed_5; }
	inline float* get_address_of_increaseSpeed_5() { return &___increaseSpeed_5; }
	inline void set_increaseSpeed_5(float value)
	{
		___increaseSpeed_5 = value;
	}

	inline static int32_t get_offset_of_allowMovement_6() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___allowMovement_6)); }
	inline bool get_allowMovement_6() const { return ___allowMovement_6; }
	inline bool* get_address_of_allowMovement_6() { return &___allowMovement_6; }
	inline void set_allowMovement_6(bool value)
	{
		___allowMovement_6 = value;
	}

	inline static int32_t get_offset_of_allowRotation_7() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___allowRotation_7)); }
	inline bool get_allowRotation_7() const { return ___allowRotation_7; }
	inline bool* get_address_of_allowRotation_7() { return &___allowRotation_7; }
	inline void set_allowRotation_7(bool value)
	{
		___allowRotation_7 = value;
	}

	inline static int32_t get_offset_of_forwardButton_8() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___forwardButton_8)); }
	inline int32_t get_forwardButton_8() const { return ___forwardButton_8; }
	inline int32_t* get_address_of_forwardButton_8() { return &___forwardButton_8; }
	inline void set_forwardButton_8(int32_t value)
	{
		___forwardButton_8 = value;
	}

	inline static int32_t get_offset_of_backwardButton_9() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___backwardButton_9)); }
	inline int32_t get_backwardButton_9() const { return ___backwardButton_9; }
	inline int32_t* get_address_of_backwardButton_9() { return &___backwardButton_9; }
	inline void set_backwardButton_9(int32_t value)
	{
		___backwardButton_9 = value;
	}

	inline static int32_t get_offset_of_rightButton_10() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___rightButton_10)); }
	inline int32_t get_rightButton_10() const { return ___rightButton_10; }
	inline int32_t* get_address_of_rightButton_10() { return &___rightButton_10; }
	inline void set_rightButton_10(int32_t value)
	{
		___rightButton_10 = value;
	}

	inline static int32_t get_offset_of_leftButton_11() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___leftButton_11)); }
	inline int32_t get_leftButton_11() const { return ___leftButton_11; }
	inline int32_t* get_address_of_leftButton_11() { return &___leftButton_11; }
	inline void set_leftButton_11(int32_t value)
	{
		___leftButton_11 = value;
	}

	inline static int32_t get_offset_of_cursorSensitivity_12() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___cursorSensitivity_12)); }
	inline float get_cursorSensitivity_12() const { return ___cursorSensitivity_12; }
	inline float* get_address_of_cursorSensitivity_12() { return &___cursorSensitivity_12; }
	inline void set_cursorSensitivity_12(float value)
	{
		___cursorSensitivity_12 = value;
	}

	inline static int32_t get_offset_of_cursorToggleAllowed_13() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___cursorToggleAllowed_13)); }
	inline bool get_cursorToggleAllowed_13() const { return ___cursorToggleAllowed_13; }
	inline bool* get_address_of_cursorToggleAllowed_13() { return &___cursorToggleAllowed_13; }
	inline void set_cursorToggleAllowed_13(bool value)
	{
		___cursorToggleAllowed_13 = value;
	}

	inline static int32_t get_offset_of_cursorToggleButton_14() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___cursorToggleButton_14)); }
	inline int32_t get_cursorToggleButton_14() const { return ___cursorToggleButton_14; }
	inline int32_t* get_address_of_cursorToggleButton_14() { return &___cursorToggleButton_14; }
	inline void set_cursorToggleButton_14(int32_t value)
	{
		___cursorToggleButton_14 = value;
	}

	inline static int32_t get_offset_of_currentSpeed_15() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___currentSpeed_15)); }
	inline float get_currentSpeed_15() const { return ___currentSpeed_15; }
	inline float* get_address_of_currentSpeed_15() { return &___currentSpeed_15; }
	inline void set_currentSpeed_15(float value)
	{
		___currentSpeed_15 = value;
	}

	inline static int32_t get_offset_of_moving_16() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___moving_16)); }
	inline bool get_moving_16() const { return ___moving_16; }
	inline bool* get_address_of_moving_16() { return &___moving_16; }
	inline void set_moving_16(bool value)
	{
		___moving_16 = value;
	}

	inline static int32_t get_offset_of_togglePressed_17() { return static_cast<int32_t>(offsetof(GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065, ___togglePressed_17)); }
	inline bool get_togglePressed_17() const { return ___togglePressed_17; }
	inline bool* get_address_of_togglePressed_17() { return &___togglePressed_17; }
	inline void set_togglePressed_17(bool value)
	{
		___togglePressed_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GHOSTFREEROAMCAMERA_T503FCDB2A6DC93073EE765359D313EE7EA184065_H
#ifndef EMULATORCONFIG_T6A187DA843514341C61D3449704A427E6B2A4772_H
#define EMULATORCONFIG_T6A187DA843514341C61D3449704A427E6B2A4772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorConfig
struct  EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Gvr.Internal.EmulatorConfig_Mode Gvr.Internal.EmulatorConfig::PHONE_EVENT_MODE
	int32_t ___PHONE_EVENT_MODE_5;

public:
	inline static int32_t get_offset_of_PHONE_EVENT_MODE_5() { return static_cast<int32_t>(offsetof(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772, ___PHONE_EVENT_MODE_5)); }
	inline int32_t get_PHONE_EVENT_MODE_5() const { return ___PHONE_EVENT_MODE_5; }
	inline int32_t* get_address_of_PHONE_EVENT_MODE_5() { return &___PHONE_EVENT_MODE_5; }
	inline void set_PHONE_EVENT_MODE_5(int32_t value)
	{
		___PHONE_EVENT_MODE_5 = value;
	}
};

struct EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields
{
public:
	// Gvr.Internal.EmulatorConfig Gvr.Internal.EmulatorConfig::instance
	EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772 * ___instance_4;
	// System.String Gvr.Internal.EmulatorConfig::USB_SERVER_IP
	String_t* ___USB_SERVER_IP_6;
	// System.String Gvr.Internal.EmulatorConfig::WIFI_SERVER_IP
	String_t* ___WIFI_SERVER_IP_7;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields, ___instance_4)); }
	inline EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772 * get_instance_4() const { return ___instance_4; }
	inline EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_USB_SERVER_IP_6() { return static_cast<int32_t>(offsetof(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields, ___USB_SERVER_IP_6)); }
	inline String_t* get_USB_SERVER_IP_6() const { return ___USB_SERVER_IP_6; }
	inline String_t** get_address_of_USB_SERVER_IP_6() { return &___USB_SERVER_IP_6; }
	inline void set_USB_SERVER_IP_6(String_t* value)
	{
		___USB_SERVER_IP_6 = value;
		Il2CppCodeGenWriteBarrier((&___USB_SERVER_IP_6), value);
	}

	inline static int32_t get_offset_of_WIFI_SERVER_IP_7() { return static_cast<int32_t>(offsetof(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields, ___WIFI_SERVER_IP_7)); }
	inline String_t* get_WIFI_SERVER_IP_7() const { return ___WIFI_SERVER_IP_7; }
	inline String_t** get_address_of_WIFI_SERVER_IP_7() { return &___WIFI_SERVER_IP_7; }
	inline void set_WIFI_SERVER_IP_7(String_t* value)
	{
		___WIFI_SERVER_IP_7 = value;
		Il2CppCodeGenWriteBarrier((&___WIFI_SERVER_IP_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORCONFIG_T6A187DA843514341C61D3449704A427E6B2A4772_H
#ifndef INSTANTPREVIEW_T711D6A079A7E30F637B5C2309E9E29EFB8793F6E_H
#define INSTANTPREVIEW_T711D6A079A7E30F637B5C2309E9E29EFB8793F6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview
struct  InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Gvr.Internal.InstantPreview_Resolutions Gvr.Internal.InstantPreview::OutputResolution
	int32_t ___OutputResolution_7;
	// Gvr.Internal.InstantPreview_MultisampleCounts Gvr.Internal.InstantPreview::MultisampleCount
	int32_t ___MultisampleCount_8;
	// Gvr.Internal.InstantPreview_BitRates Gvr.Internal.InstantPreview::BitRate
	int32_t ___BitRate_9;
	// System.Boolean Gvr.Internal.InstantPreview::InstallApkOnRun
	bool ___InstallApkOnRun_10;
	// UnityEngine.Object Gvr.Internal.InstantPreview::InstantPreviewApk
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___InstantPreviewApk_11;

public:
	inline static int32_t get_offset_of_OutputResolution_7() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E, ___OutputResolution_7)); }
	inline int32_t get_OutputResolution_7() const { return ___OutputResolution_7; }
	inline int32_t* get_address_of_OutputResolution_7() { return &___OutputResolution_7; }
	inline void set_OutputResolution_7(int32_t value)
	{
		___OutputResolution_7 = value;
	}

	inline static int32_t get_offset_of_MultisampleCount_8() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E, ___MultisampleCount_8)); }
	inline int32_t get_MultisampleCount_8() const { return ___MultisampleCount_8; }
	inline int32_t* get_address_of_MultisampleCount_8() { return &___MultisampleCount_8; }
	inline void set_MultisampleCount_8(int32_t value)
	{
		___MultisampleCount_8 = value;
	}

	inline static int32_t get_offset_of_BitRate_9() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E, ___BitRate_9)); }
	inline int32_t get_BitRate_9() const { return ___BitRate_9; }
	inline int32_t* get_address_of_BitRate_9() { return &___BitRate_9; }
	inline void set_BitRate_9(int32_t value)
	{
		___BitRate_9 = value;
	}

	inline static int32_t get_offset_of_InstallApkOnRun_10() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E, ___InstallApkOnRun_10)); }
	inline bool get_InstallApkOnRun_10() const { return ___InstallApkOnRun_10; }
	inline bool* get_address_of_InstallApkOnRun_10() { return &___InstallApkOnRun_10; }
	inline void set_InstallApkOnRun_10(bool value)
	{
		___InstallApkOnRun_10 = value;
	}

	inline static int32_t get_offset_of_InstantPreviewApk_11() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E, ___InstantPreviewApk_11)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_InstantPreviewApk_11() const { return ___InstantPreviewApk_11; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_InstantPreviewApk_11() { return &___InstantPreviewApk_11; }
	inline void set_InstantPreviewApk_11(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___InstantPreviewApk_11 = value;
		Il2CppCodeGenWriteBarrier((&___InstantPreviewApk_11), value);
	}
};

struct InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E_StaticFields
{
public:
	// Gvr.Internal.InstantPreview Gvr.Internal.InstantPreview::<Instance>k__BackingField
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEW_T711D6A079A7E30F637B5C2309E9E29EFB8793F6E_H
#ifndef GVRHEADSET_T738AC1680EDB890C18B3267D2CAAE3236EAFB799_H
#define GVRHEADSET_T738AC1680EDB890C18B3267D2CAAE3236EAFB799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset
struct  GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Gvr.Internal.IHeadsetProvider GvrHeadset::headsetProvider
	RuntimeObject* ___headsetProvider_5;
	// Gvr.Internal.HeadsetState GvrHeadset::headsetState
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  ___headsetState_6;
	// System.Collections.IEnumerator GvrHeadset::standaloneUpdate
	RuntimeObject* ___standaloneUpdate_7;
	// UnityEngine.WaitForEndOfFrame GvrHeadset::waitForEndOfFrame
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___waitForEndOfFrame_8;
	// GvrHeadset_OnSafetyRegionEvent GvrHeadset::safetyRegionDelegate
	OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6 * ___safetyRegionDelegate_9;
	// GvrHeadset_OnRecenterEvent GvrHeadset::recenterDelegate
	OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A * ___recenterDelegate_10;

public:
	inline static int32_t get_offset_of_headsetProvider_5() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___headsetProvider_5)); }
	inline RuntimeObject* get_headsetProvider_5() const { return ___headsetProvider_5; }
	inline RuntimeObject** get_address_of_headsetProvider_5() { return &___headsetProvider_5; }
	inline void set_headsetProvider_5(RuntimeObject* value)
	{
		___headsetProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___headsetProvider_5), value);
	}

	inline static int32_t get_offset_of_headsetState_6() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___headsetState_6)); }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  get_headsetState_6() const { return ___headsetState_6; }
	inline HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA * get_address_of_headsetState_6() { return &___headsetState_6; }
	inline void set_headsetState_6(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA  value)
	{
		___headsetState_6 = value;
	}

	inline static int32_t get_offset_of_standaloneUpdate_7() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___standaloneUpdate_7)); }
	inline RuntimeObject* get_standaloneUpdate_7() const { return ___standaloneUpdate_7; }
	inline RuntimeObject** get_address_of_standaloneUpdate_7() { return &___standaloneUpdate_7; }
	inline void set_standaloneUpdate_7(RuntimeObject* value)
	{
		___standaloneUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___standaloneUpdate_7), value);
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_8() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___waitForEndOfFrame_8)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_waitForEndOfFrame_8() const { return ___waitForEndOfFrame_8; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_waitForEndOfFrame_8() { return &___waitForEndOfFrame_8; }
	inline void set_waitForEndOfFrame_8(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___waitForEndOfFrame_8 = value;
		Il2CppCodeGenWriteBarrier((&___waitForEndOfFrame_8), value);
	}

	inline static int32_t get_offset_of_safetyRegionDelegate_9() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___safetyRegionDelegate_9)); }
	inline OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6 * get_safetyRegionDelegate_9() const { return ___safetyRegionDelegate_9; }
	inline OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6 ** get_address_of_safetyRegionDelegate_9() { return &___safetyRegionDelegate_9; }
	inline void set_safetyRegionDelegate_9(OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6 * value)
	{
		___safetyRegionDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___safetyRegionDelegate_9), value);
	}

	inline static int32_t get_offset_of_recenterDelegate_10() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799, ___recenterDelegate_10)); }
	inline OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A * get_recenterDelegate_10() const { return ___recenterDelegate_10; }
	inline OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A ** get_address_of_recenterDelegate_10() { return &___recenterDelegate_10; }
	inline void set_recenterDelegate_10(OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A * value)
	{
		___recenterDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___recenterDelegate_10), value);
	}
};

struct GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799_StaticFields
{
public:
	// GvrHeadset GvrHeadset::instance
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799_StaticFields, ___instance_4)); }
	inline GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * get_instance_4() const { return ___instance_4; }
	inline GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRHEADSET_T738AC1680EDB890C18B3267D2CAAE3236EAFB799_H
#ifndef GVRKEYBOARD_T1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_H
#define GVRKEYBOARD_T1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard
struct  GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// KeyboardState GvrKeyboard::keyboardState
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * ___keyboardState_6;
	// System.Collections.IEnumerator GvrKeyboard::keyboardUpdate
	RuntimeObject* ___keyboardUpdate_7;
	// GvrKeyboard_ErrorCallback GvrKeyboard::errorCallback
	ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1 * ___errorCallback_8;
	// GvrKeyboard_StandardCallback GvrKeyboard::showCallback
	StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * ___showCallback_9;
	// GvrKeyboard_StandardCallback GvrKeyboard::hideCallback
	StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * ___hideCallback_10;
	// GvrKeyboard_EditTextCallback GvrKeyboard::updateCallback
	EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * ___updateCallback_11;
	// GvrKeyboard_EditTextCallback GvrKeyboard::enterCallback
	EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * ___enterCallback_12;
	// System.Boolean GvrKeyboard::isKeyboardHidden
	bool ___isKeyboardHidden_13;
	// GvrKeyboardDelegateBase GvrKeyboard::keyboardDelegate
	GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A * ___keyboardDelegate_17;
	// GvrKeyboardInputMode GvrKeyboard::inputMode
	int32_t ___inputMode_18;
	// System.Boolean GvrKeyboard::useRecommended
	bool ___useRecommended_19;
	// System.Single GvrKeyboard::distance
	float ___distance_20;

public:
	inline static int32_t get_offset_of_keyboardState_6() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___keyboardState_6)); }
	inline KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * get_keyboardState_6() const { return ___keyboardState_6; }
	inline KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB ** get_address_of_keyboardState_6() { return &___keyboardState_6; }
	inline void set_keyboardState_6(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * value)
	{
		___keyboardState_6 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardState_6), value);
	}

	inline static int32_t get_offset_of_keyboardUpdate_7() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___keyboardUpdate_7)); }
	inline RuntimeObject* get_keyboardUpdate_7() const { return ___keyboardUpdate_7; }
	inline RuntimeObject** get_address_of_keyboardUpdate_7() { return &___keyboardUpdate_7; }
	inline void set_keyboardUpdate_7(RuntimeObject* value)
	{
		___keyboardUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardUpdate_7), value);
	}

	inline static int32_t get_offset_of_errorCallback_8() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___errorCallback_8)); }
	inline ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1 * get_errorCallback_8() const { return ___errorCallback_8; }
	inline ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1 ** get_address_of_errorCallback_8() { return &___errorCallback_8; }
	inline void set_errorCallback_8(ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1 * value)
	{
		___errorCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_8), value);
	}

	inline static int32_t get_offset_of_showCallback_9() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___showCallback_9)); }
	inline StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * get_showCallback_9() const { return ___showCallback_9; }
	inline StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 ** get_address_of_showCallback_9() { return &___showCallback_9; }
	inline void set_showCallback_9(StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * value)
	{
		___showCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___showCallback_9), value);
	}

	inline static int32_t get_offset_of_hideCallback_10() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___hideCallback_10)); }
	inline StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * get_hideCallback_10() const { return ___hideCallback_10; }
	inline StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 ** get_address_of_hideCallback_10() { return &___hideCallback_10; }
	inline void set_hideCallback_10(StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9 * value)
	{
		___hideCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___hideCallback_10), value);
	}

	inline static int32_t get_offset_of_updateCallback_11() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___updateCallback_11)); }
	inline EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * get_updateCallback_11() const { return ___updateCallback_11; }
	inline EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A ** get_address_of_updateCallback_11() { return &___updateCallback_11; }
	inline void set_updateCallback_11(EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * value)
	{
		___updateCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___updateCallback_11), value);
	}

	inline static int32_t get_offset_of_enterCallback_12() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___enterCallback_12)); }
	inline EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * get_enterCallback_12() const { return ___enterCallback_12; }
	inline EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A ** get_address_of_enterCallback_12() { return &___enterCallback_12; }
	inline void set_enterCallback_12(EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A * value)
	{
		___enterCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___enterCallback_12), value);
	}

	inline static int32_t get_offset_of_isKeyboardHidden_13() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___isKeyboardHidden_13)); }
	inline bool get_isKeyboardHidden_13() const { return ___isKeyboardHidden_13; }
	inline bool* get_address_of_isKeyboardHidden_13() { return &___isKeyboardHidden_13; }
	inline void set_isKeyboardHidden_13(bool value)
	{
		___isKeyboardHidden_13 = value;
	}

	inline static int32_t get_offset_of_keyboardDelegate_17() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___keyboardDelegate_17)); }
	inline GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A * get_keyboardDelegate_17() const { return ___keyboardDelegate_17; }
	inline GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A ** get_address_of_keyboardDelegate_17() { return &___keyboardDelegate_17; }
	inline void set_keyboardDelegate_17(GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A * value)
	{
		___keyboardDelegate_17 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardDelegate_17), value);
	}

	inline static int32_t get_offset_of_inputMode_18() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___inputMode_18)); }
	inline int32_t get_inputMode_18() const { return ___inputMode_18; }
	inline int32_t* get_address_of_inputMode_18() { return &___inputMode_18; }
	inline void set_inputMode_18(int32_t value)
	{
		___inputMode_18 = value;
	}

	inline static int32_t get_offset_of_useRecommended_19() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___useRecommended_19)); }
	inline bool get_useRecommended_19() const { return ___useRecommended_19; }
	inline bool* get_address_of_useRecommended_19() { return &___useRecommended_19; }
	inline void set_useRecommended_19(bool value)
	{
		___useRecommended_19 = value;
	}

	inline static int32_t get_offset_of_distance_20() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22, ___distance_20)); }
	inline float get_distance_20() const { return ___distance_20; }
	inline float* get_address_of_distance_20() { return &___distance_20; }
	inline void set_distance_20(float value)
	{
		___distance_20 = value;
	}
};

struct GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields
{
public:
	// GvrKeyboard GvrKeyboard::instance
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * ___instance_4;
	// Gvr.Internal.IKeyboardProvider GvrKeyboard::keyboardProvider
	RuntimeObject* ___keyboardProvider_5;
	// System.Collections.Generic.List`1<GvrKeyboardEvent> GvrKeyboard::threadSafeCallbacks
	List_1_t18E8F7C1D3B24CBB1ACFC0EEE5F30E81390C8ED3 * ___threadSafeCallbacks_15;
	// System.Object GvrKeyboard::callbacksLock
	RuntimeObject * ___callbacksLock_16;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields, ___instance_4)); }
	inline GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * get_instance_4() const { return ___instance_4; }
	inline GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_keyboardProvider_5() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields, ___keyboardProvider_5)); }
	inline RuntimeObject* get_keyboardProvider_5() const { return ___keyboardProvider_5; }
	inline RuntimeObject** get_address_of_keyboardProvider_5() { return &___keyboardProvider_5; }
	inline void set_keyboardProvider_5(RuntimeObject* value)
	{
		___keyboardProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardProvider_5), value);
	}

	inline static int32_t get_offset_of_threadSafeCallbacks_15() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields, ___threadSafeCallbacks_15)); }
	inline List_1_t18E8F7C1D3B24CBB1ACFC0EEE5F30E81390C8ED3 * get_threadSafeCallbacks_15() const { return ___threadSafeCallbacks_15; }
	inline List_1_t18E8F7C1D3B24CBB1ACFC0EEE5F30E81390C8ED3 ** get_address_of_threadSafeCallbacks_15() { return &___threadSafeCallbacks_15; }
	inline void set_threadSafeCallbacks_15(List_1_t18E8F7C1D3B24CBB1ACFC0EEE5F30E81390C8ED3 * value)
	{
		___threadSafeCallbacks_15 = value;
		Il2CppCodeGenWriteBarrier((&___threadSafeCallbacks_15), value);
	}

	inline static int32_t get_offset_of_callbacksLock_16() { return static_cast<int32_t>(offsetof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields, ___callbacksLock_16)); }
	inline RuntimeObject * get_callbacksLock_16() const { return ___callbacksLock_16; }
	inline RuntimeObject ** get_address_of_callbacksLock_16() { return &___callbacksLock_16; }
	inline void set_callbacksLock_16(RuntimeObject * value)
	{
		___callbacksLock_16 = value;
		Il2CppCodeGenWriteBarrier((&___callbacksLock_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARD_T1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_H
#ifndef GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#define GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#ifndef GVRVIDEOPLAYERTEXTURE_TA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_H
#define GVRVIDEOPLAYERTEXTURE_TA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture
struct  GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.IntPtr GvrVideoPlayerTexture::videoPlayerPtr
	intptr_t ___videoPlayerPtr_4;
	// System.Int32 GvrVideoPlayerTexture::videoPlayerEventBase
	int32_t ___videoPlayerEventBase_5;
	// UnityEngine.Texture GvrVideoPlayerTexture::initialTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___initialTexture_6;
	// UnityEngine.Texture GvrVideoPlayerTexture::surfaceTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___surfaceTexture_7;
	// System.Single[] GvrVideoPlayerTexture::videoMatrixRaw
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___videoMatrixRaw_8;
	// UnityEngine.Matrix4x4 GvrVideoPlayerTexture::videoMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___videoMatrix_9;
	// System.Int32 GvrVideoPlayerTexture::videoMatrixPropertyId
	int32_t ___videoMatrixPropertyId_10;
	// System.Int64 GvrVideoPlayerTexture::lastVideoTimestamp
	int64_t ___lastVideoTimestamp_11;
	// System.Boolean GvrVideoPlayerTexture::initialized
	bool ___initialized_12;
	// System.Int32 GvrVideoPlayerTexture::texWidth
	int32_t ___texWidth_13;
	// System.Int32 GvrVideoPlayerTexture::texHeight
	int32_t ___texHeight_14;
	// System.Int64 GvrVideoPlayerTexture::lastBufferedPosition
	int64_t ___lastBufferedPosition_15;
	// System.Single GvrVideoPlayerTexture::framecount
	float ___framecount_16;
	// UnityEngine.Renderer GvrVideoPlayerTexture::screen
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___screen_17;
	// System.IntPtr GvrVideoPlayerTexture::renderEventFunction
	intptr_t ___renderEventFunction_18;
	// System.Boolean GvrVideoPlayerTexture::playOnResume
	bool ___playOnResume_19;
	// System.Collections.Generic.List`1<System.Action`1<System.Int32>> GvrVideoPlayerTexture::onEventCallbacks
	List_1_tE6DF805E6081D53922B71B07436272A8E4D07156 * ___onEventCallbacks_20;
	// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>> GvrVideoPlayerTexture::onExceptionCallbacks
	List_1_t9977425BDAA7A83F2247096EC315496A7FFEA1B7 * ___onExceptionCallbacks_21;
	// UnityEngine.UI.Text GvrVideoPlayerTexture::statusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statusText_23;
	// GvrVideoPlayerTexture_VideoType GvrVideoPlayerTexture::videoType
	int32_t ___videoType_24;
	// System.String GvrVideoPlayerTexture::videoURL
	String_t* ___videoURL_25;
	// System.String GvrVideoPlayerTexture::videoContentID
	String_t* ___videoContentID_26;
	// System.String GvrVideoPlayerTexture::videoProviderId
	String_t* ___videoProviderId_27;
	// GvrVideoPlayerTexture_VideoResolution GvrVideoPlayerTexture::initialResolution
	int32_t ___initialResolution_28;
	// System.Boolean GvrVideoPlayerTexture::adjustAspectRatio
	bool ___adjustAspectRatio_29;
	// System.Boolean GvrVideoPlayerTexture::useSecurePath
	bool ___useSecurePath_30;

public:
	inline static int32_t get_offset_of_videoPlayerPtr_4() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoPlayerPtr_4)); }
	inline intptr_t get_videoPlayerPtr_4() const { return ___videoPlayerPtr_4; }
	inline intptr_t* get_address_of_videoPlayerPtr_4() { return &___videoPlayerPtr_4; }
	inline void set_videoPlayerPtr_4(intptr_t value)
	{
		___videoPlayerPtr_4 = value;
	}

	inline static int32_t get_offset_of_videoPlayerEventBase_5() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoPlayerEventBase_5)); }
	inline int32_t get_videoPlayerEventBase_5() const { return ___videoPlayerEventBase_5; }
	inline int32_t* get_address_of_videoPlayerEventBase_5() { return &___videoPlayerEventBase_5; }
	inline void set_videoPlayerEventBase_5(int32_t value)
	{
		___videoPlayerEventBase_5 = value;
	}

	inline static int32_t get_offset_of_initialTexture_6() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___initialTexture_6)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_initialTexture_6() const { return ___initialTexture_6; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_initialTexture_6() { return &___initialTexture_6; }
	inline void set_initialTexture_6(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___initialTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___initialTexture_6), value);
	}

	inline static int32_t get_offset_of_surfaceTexture_7() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___surfaceTexture_7)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_surfaceTexture_7() const { return ___surfaceTexture_7; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_surfaceTexture_7() { return &___surfaceTexture_7; }
	inline void set_surfaceTexture_7(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___surfaceTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceTexture_7), value);
	}

	inline static int32_t get_offset_of_videoMatrixRaw_8() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoMatrixRaw_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_videoMatrixRaw_8() const { return ___videoMatrixRaw_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_videoMatrixRaw_8() { return &___videoMatrixRaw_8; }
	inline void set_videoMatrixRaw_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___videoMatrixRaw_8 = value;
		Il2CppCodeGenWriteBarrier((&___videoMatrixRaw_8), value);
	}

	inline static int32_t get_offset_of_videoMatrix_9() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoMatrix_9)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_videoMatrix_9() const { return ___videoMatrix_9; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_videoMatrix_9() { return &___videoMatrix_9; }
	inline void set_videoMatrix_9(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___videoMatrix_9 = value;
	}

	inline static int32_t get_offset_of_videoMatrixPropertyId_10() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoMatrixPropertyId_10)); }
	inline int32_t get_videoMatrixPropertyId_10() const { return ___videoMatrixPropertyId_10; }
	inline int32_t* get_address_of_videoMatrixPropertyId_10() { return &___videoMatrixPropertyId_10; }
	inline void set_videoMatrixPropertyId_10(int32_t value)
	{
		___videoMatrixPropertyId_10 = value;
	}

	inline static int32_t get_offset_of_lastVideoTimestamp_11() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___lastVideoTimestamp_11)); }
	inline int64_t get_lastVideoTimestamp_11() const { return ___lastVideoTimestamp_11; }
	inline int64_t* get_address_of_lastVideoTimestamp_11() { return &___lastVideoTimestamp_11; }
	inline void set_lastVideoTimestamp_11(int64_t value)
	{
		___lastVideoTimestamp_11 = value;
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}

	inline static int32_t get_offset_of_texWidth_13() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___texWidth_13)); }
	inline int32_t get_texWidth_13() const { return ___texWidth_13; }
	inline int32_t* get_address_of_texWidth_13() { return &___texWidth_13; }
	inline void set_texWidth_13(int32_t value)
	{
		___texWidth_13 = value;
	}

	inline static int32_t get_offset_of_texHeight_14() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___texHeight_14)); }
	inline int32_t get_texHeight_14() const { return ___texHeight_14; }
	inline int32_t* get_address_of_texHeight_14() { return &___texHeight_14; }
	inline void set_texHeight_14(int32_t value)
	{
		___texHeight_14 = value;
	}

	inline static int32_t get_offset_of_lastBufferedPosition_15() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___lastBufferedPosition_15)); }
	inline int64_t get_lastBufferedPosition_15() const { return ___lastBufferedPosition_15; }
	inline int64_t* get_address_of_lastBufferedPosition_15() { return &___lastBufferedPosition_15; }
	inline void set_lastBufferedPosition_15(int64_t value)
	{
		___lastBufferedPosition_15 = value;
	}

	inline static int32_t get_offset_of_framecount_16() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___framecount_16)); }
	inline float get_framecount_16() const { return ___framecount_16; }
	inline float* get_address_of_framecount_16() { return &___framecount_16; }
	inline void set_framecount_16(float value)
	{
		___framecount_16 = value;
	}

	inline static int32_t get_offset_of_screen_17() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___screen_17)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_screen_17() const { return ___screen_17; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_screen_17() { return &___screen_17; }
	inline void set_screen_17(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___screen_17 = value;
		Il2CppCodeGenWriteBarrier((&___screen_17), value);
	}

	inline static int32_t get_offset_of_renderEventFunction_18() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___renderEventFunction_18)); }
	inline intptr_t get_renderEventFunction_18() const { return ___renderEventFunction_18; }
	inline intptr_t* get_address_of_renderEventFunction_18() { return &___renderEventFunction_18; }
	inline void set_renderEventFunction_18(intptr_t value)
	{
		___renderEventFunction_18 = value;
	}

	inline static int32_t get_offset_of_playOnResume_19() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___playOnResume_19)); }
	inline bool get_playOnResume_19() const { return ___playOnResume_19; }
	inline bool* get_address_of_playOnResume_19() { return &___playOnResume_19; }
	inline void set_playOnResume_19(bool value)
	{
		___playOnResume_19 = value;
	}

	inline static int32_t get_offset_of_onEventCallbacks_20() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___onEventCallbacks_20)); }
	inline List_1_tE6DF805E6081D53922B71B07436272A8E4D07156 * get_onEventCallbacks_20() const { return ___onEventCallbacks_20; }
	inline List_1_tE6DF805E6081D53922B71B07436272A8E4D07156 ** get_address_of_onEventCallbacks_20() { return &___onEventCallbacks_20; }
	inline void set_onEventCallbacks_20(List_1_tE6DF805E6081D53922B71B07436272A8E4D07156 * value)
	{
		___onEventCallbacks_20 = value;
		Il2CppCodeGenWriteBarrier((&___onEventCallbacks_20), value);
	}

	inline static int32_t get_offset_of_onExceptionCallbacks_21() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___onExceptionCallbacks_21)); }
	inline List_1_t9977425BDAA7A83F2247096EC315496A7FFEA1B7 * get_onExceptionCallbacks_21() const { return ___onExceptionCallbacks_21; }
	inline List_1_t9977425BDAA7A83F2247096EC315496A7FFEA1B7 ** get_address_of_onExceptionCallbacks_21() { return &___onExceptionCallbacks_21; }
	inline void set_onExceptionCallbacks_21(List_1_t9977425BDAA7A83F2247096EC315496A7FFEA1B7 * value)
	{
		___onExceptionCallbacks_21 = value;
		Il2CppCodeGenWriteBarrier((&___onExceptionCallbacks_21), value);
	}

	inline static int32_t get_offset_of_statusText_23() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___statusText_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statusText_23() const { return ___statusText_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statusText_23() { return &___statusText_23; }
	inline void set_statusText_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statusText_23 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_23), value);
	}

	inline static int32_t get_offset_of_videoType_24() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoType_24)); }
	inline int32_t get_videoType_24() const { return ___videoType_24; }
	inline int32_t* get_address_of_videoType_24() { return &___videoType_24; }
	inline void set_videoType_24(int32_t value)
	{
		___videoType_24 = value;
	}

	inline static int32_t get_offset_of_videoURL_25() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoURL_25)); }
	inline String_t* get_videoURL_25() const { return ___videoURL_25; }
	inline String_t** get_address_of_videoURL_25() { return &___videoURL_25; }
	inline void set_videoURL_25(String_t* value)
	{
		___videoURL_25 = value;
		Il2CppCodeGenWriteBarrier((&___videoURL_25), value);
	}

	inline static int32_t get_offset_of_videoContentID_26() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoContentID_26)); }
	inline String_t* get_videoContentID_26() const { return ___videoContentID_26; }
	inline String_t** get_address_of_videoContentID_26() { return &___videoContentID_26; }
	inline void set_videoContentID_26(String_t* value)
	{
		___videoContentID_26 = value;
		Il2CppCodeGenWriteBarrier((&___videoContentID_26), value);
	}

	inline static int32_t get_offset_of_videoProviderId_27() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___videoProviderId_27)); }
	inline String_t* get_videoProviderId_27() const { return ___videoProviderId_27; }
	inline String_t** get_address_of_videoProviderId_27() { return &___videoProviderId_27; }
	inline void set_videoProviderId_27(String_t* value)
	{
		___videoProviderId_27 = value;
		Il2CppCodeGenWriteBarrier((&___videoProviderId_27), value);
	}

	inline static int32_t get_offset_of_initialResolution_28() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___initialResolution_28)); }
	inline int32_t get_initialResolution_28() const { return ___initialResolution_28; }
	inline int32_t* get_address_of_initialResolution_28() { return &___initialResolution_28; }
	inline void set_initialResolution_28(int32_t value)
	{
		___initialResolution_28 = value;
	}

	inline static int32_t get_offset_of_adjustAspectRatio_29() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___adjustAspectRatio_29)); }
	inline bool get_adjustAspectRatio_29() const { return ___adjustAspectRatio_29; }
	inline bool* get_address_of_adjustAspectRatio_29() { return &___adjustAspectRatio_29; }
	inline void set_adjustAspectRatio_29(bool value)
	{
		___adjustAspectRatio_29 = value;
	}

	inline static int32_t get_offset_of_useSecurePath_30() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231, ___useSecurePath_30)); }
	inline bool get_useSecurePath_30() const { return ___useSecurePath_30; }
	inline bool* get_address_of_useSecurePath_30() { return &___useSecurePath_30; }
	inline void set_useSecurePath_30(bool value)
	{
		___useSecurePath_30 = value;
	}
};

struct GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> GvrVideoPlayerTexture::ExecuteOnMainThread
	Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * ___ExecuteOnMainThread_22;

public:
	inline static int32_t get_offset_of_ExecuteOnMainThread_22() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_StaticFields, ___ExecuteOnMainThread_22)); }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * get_ExecuteOnMainThread_22() const { return ___ExecuteOnMainThread_22; }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA ** get_address_of_ExecuteOnMainThread_22() { return &___ExecuteOnMainThread_22; }
	inline void set_ExecuteOnMainThread_22(Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * value)
	{
		___ExecuteOnMainThread_22 = value;
		Il2CppCodeGenWriteBarrier((&___ExecuteOnMainThread_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRVIDEOPLAYERTEXTURE_TA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_H
#ifndef INSTANTPREVIEWHELPER_TFF78920717830306651E9CD85F55999644F5ACDD_H
#define INSTANTPREVIEWHELPER_TFF78920717830306651E9CD85F55999644F5ACDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantPreviewHelper
struct  InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD_StaticFields
{
public:
	// System.String InstantPreviewHelper::AdbPath
	String_t* ___AdbPath_4;

public:
	inline static int32_t get_offset_of_AdbPath_4() { return static_cast<int32_t>(offsetof(InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD_StaticFields, ___AdbPath_4)); }
	inline String_t* get_AdbPath_4() const { return ___AdbPath_4; }
	inline String_t** get_address_of_AdbPath_4() { return &___AdbPath_4; }
	inline void set_AdbPath_4(String_t* value)
	{
		___AdbPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___AdbPath_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWHELPER_TFF78920717830306651E9CD85F55999644F5ACDD_H
#ifndef MOVECAMERA_T4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717_H
#define MOVECAMERA_T4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveCamera
struct  MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MoveCamera::turnSpeed
	float ___turnSpeed_4;
	// System.Single MoveCamera::panSpeed
	float ___panSpeed_5;
	// System.Single MoveCamera::zoomSpeed
	float ___zoomSpeed_6;
	// UnityEngine.Vector3 MoveCamera::mouseOrigin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mouseOrigin_7;
	// System.Boolean MoveCamera::isPanning
	bool ___isPanning_8;
	// System.Boolean MoveCamera::isRotating
	bool ___isRotating_9;
	// System.Boolean MoveCamera::isZooming
	bool ___isZooming_10;

public:
	inline static int32_t get_offset_of_turnSpeed_4() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___turnSpeed_4)); }
	inline float get_turnSpeed_4() const { return ___turnSpeed_4; }
	inline float* get_address_of_turnSpeed_4() { return &___turnSpeed_4; }
	inline void set_turnSpeed_4(float value)
	{
		___turnSpeed_4 = value;
	}

	inline static int32_t get_offset_of_panSpeed_5() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___panSpeed_5)); }
	inline float get_panSpeed_5() const { return ___panSpeed_5; }
	inline float* get_address_of_panSpeed_5() { return &___panSpeed_5; }
	inline void set_panSpeed_5(float value)
	{
		___panSpeed_5 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_6() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___zoomSpeed_6)); }
	inline float get_zoomSpeed_6() const { return ___zoomSpeed_6; }
	inline float* get_address_of_zoomSpeed_6() { return &___zoomSpeed_6; }
	inline void set_zoomSpeed_6(float value)
	{
		___zoomSpeed_6 = value;
	}

	inline static int32_t get_offset_of_mouseOrigin_7() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___mouseOrigin_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mouseOrigin_7() const { return ___mouseOrigin_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mouseOrigin_7() { return &___mouseOrigin_7; }
	inline void set_mouseOrigin_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mouseOrigin_7 = value;
	}

	inline static int32_t get_offset_of_isPanning_8() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___isPanning_8)); }
	inline bool get_isPanning_8() const { return ___isPanning_8; }
	inline bool* get_address_of_isPanning_8() { return &___isPanning_8; }
	inline void set_isPanning_8(bool value)
	{
		___isPanning_8 = value;
	}

	inline static int32_t get_offset_of_isRotating_9() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___isRotating_9)); }
	inline bool get_isRotating_9() const { return ___isRotating_9; }
	inline bool* get_address_of_isRotating_9() { return &___isRotating_9; }
	inline void set_isRotating_9(bool value)
	{
		___isRotating_9 = value;
	}

	inline static int32_t get_offset_of_isZooming_10() { return static_cast<int32_t>(offsetof(MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717, ___isZooming_10)); }
	inline bool get_isZooming_10() const { return ___isZooming_10; }
	inline bool* get_address_of_isZooming_10() { return &___isZooming_10; }
	inline void set_isZooming_10(bool value)
	{
		___isZooming_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVECAMERA_T4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable_SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef DROPDOWN_TF6331401084B1213CAB10587A6EC81461501930F_H
#define DROPDOWN_TF6331401084B1213CAB10587A6EC81461501930F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_tF6331401084B1213CAB10587A6EC81461501930F  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Template_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_CaptionText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_CaptionImage_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ItemText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_ItemImage_22;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_23;
	// UnityEngine.UI.Dropdown_OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * ___m_Options_24;
	// UnityEngine.UI.Dropdown_DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * ___m_OnValueChanged_25;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Dropdown_26;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Blocker_27;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown_DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t9CE24C9765CEA576BA5850425955BF1016C0B607 * ___m_Items_28;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * ___m_AlphaTweenRunner_29;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_30;

public:
	inline static int32_t get_offset_of_m_Template_18() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Template_18)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Template_18() const { return ___m_Template_18; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Template_18() { return &___m_Template_18; }
	inline void set_m_Template_18(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Template_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_18), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_19() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_CaptionText_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_CaptionText_19() const { return ___m_CaptionText_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_CaptionText_19() { return &___m_CaptionText_19; }
	inline void set_m_CaptionText_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_CaptionText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_19), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_20() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_CaptionImage_20)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_CaptionImage_20() const { return ___m_CaptionImage_20; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_CaptionImage_20() { return &___m_CaptionImage_20; }
	inline void set_m_CaptionImage_20(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_CaptionImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_20), value);
	}

	inline static int32_t get_offset_of_m_ItemText_21() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_ItemText_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ItemText_21() const { return ___m_ItemText_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ItemText_21() { return &___m_ItemText_21; }
	inline void set_m_ItemText_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ItemText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_21), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_22() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_ItemImage_22)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_ItemImage_22() const { return ___m_ItemImage_22; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_ItemImage_22() { return &___m_ItemImage_22; }
	inline void set_m_ItemImage_22(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_ItemImage_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_22), value);
	}

	inline static int32_t get_offset_of_m_Value_23() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Value_23)); }
	inline int32_t get_m_Value_23() const { return ___m_Value_23; }
	inline int32_t* get_address_of_m_Value_23() { return &___m_Value_23; }
	inline void set_m_Value_23(int32_t value)
	{
		___m_Value_23 = value;
	}

	inline static int32_t get_offset_of_m_Options_24() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Options_24)); }
	inline OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * get_m_Options_24() const { return ___m_Options_24; }
	inline OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D ** get_address_of_m_Options_24() { return &___m_Options_24; }
	inline void set_m_Options_24(OptionDataList_tE70C398434952658ED61EEEDC56766239E2C856D * value)
	{
		___m_Options_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_24), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_OnValueChanged_25)); }
	inline DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(DropdownEvent_t429FBB093ED3586F5D49859EBD338125EAB76306 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_26() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Dropdown_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Dropdown_26() const { return ___m_Dropdown_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Dropdown_26() { return &___m_Dropdown_26; }
	inline void set_m_Dropdown_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Dropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_26), value);
	}

	inline static int32_t get_offset_of_m_Blocker_27() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Blocker_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Blocker_27() const { return ___m_Blocker_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Blocker_27() { return &___m_Blocker_27; }
	inline void set_m_Blocker_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Blocker_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_27), value);
	}

	inline static int32_t get_offset_of_m_Items_28() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_Items_28)); }
	inline List_1_t9CE24C9765CEA576BA5850425955BF1016C0B607 * get_m_Items_28() const { return ___m_Items_28; }
	inline List_1_t9CE24C9765CEA576BA5850425955BF1016C0B607 ** get_address_of_m_Items_28() { return &___m_Items_28; }
	inline void set_m_Items_28(List_1_t9CE24C9765CEA576BA5850425955BF1016C0B607 * value)
	{
		___m_Items_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_28), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_29() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___m_AlphaTweenRunner_29)); }
	inline TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * get_m_AlphaTweenRunner_29() const { return ___m_AlphaTweenRunner_29; }
	inline TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF ** get_address_of_m_AlphaTweenRunner_29() { return &___m_AlphaTweenRunner_29; }
	inline void set_m_AlphaTweenRunner_29(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * value)
	{
		___m_AlphaTweenRunner_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_29), value);
	}

	inline static int32_t get_offset_of_validTemplate_30() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F, ___validTemplate_30)); }
	inline bool get_validTemplate_30() const { return ___validTemplate_30; }
	inline bool* get_address_of_validTemplate_30() { return &___validTemplate_30; }
	inline void set_validTemplate_30(bool value)
	{
		___validTemplate_30 = value;
	}
};

struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F_StaticFields
{
public:
	// UnityEngine.UI.Dropdown_OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * ___s_NoOptionData_31;

public:
	inline static int32_t get_offset_of_s_NoOptionData_31() { return static_cast<int32_t>(offsetof(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F_StaticFields, ___s_NoOptionData_31)); }
	inline OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * get_s_NoOptionData_31() const { return ___s_NoOptionData_31; }
	inline OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 ** get_address_of_s_NoOptionData_31() { return &___s_NoOptionData_31; }
	inline void set_s_NoOptionData_31(OptionData_t5522C87AD5C3F1C8D3748D1FF1825A24F3835831 * value)
	{
		___s_NoOptionData_31 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_TF6331401084B1213CAB10587A6EC81461501930F_H
#ifndef GVRDROPDOWN_TEECE530E6010547F47D061423CEAB8E286D61E7E_H
#define GVRDROPDOWN_TEECE530E6010547F47D061423CEAB8E286D61E7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDropdown
struct  GvrDropdown_tEECE530E6010547F47D061423CEAB8E286D61E7E  : public Dropdown_tF6331401084B1213CAB10587A6EC81461501930F
{
public:
	// UnityEngine.GameObject GvrDropdown::currentBlocker
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentBlocker_32;

public:
	inline static int32_t get_offset_of_currentBlocker_32() { return static_cast<int32_t>(offsetof(GvrDropdown_tEECE530E6010547F47D061423CEAB8E286D61E7E, ___currentBlocker_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentBlocker_32() const { return ___currentBlocker_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentBlocker_32() { return &___currentBlocker_32; }
	inline void set_currentBlocker_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentBlocker_32 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlocker_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDROPDOWN_TEECE530E6010547F47D061423CEAB8E286D61E7E_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (GvrSettings_tDD84E5C6B4BAE16BC00E9EB6BBEF7D6F84B1906A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (ViewerPlatformType_t972B7912D65EEB94F6A1BF223C97D35B5D11EA6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2701[4] = 
{
	ViewerPlatformType_t972B7912D65EEB94F6A1BF223C97D35B5D11EA6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2702[4] = 
{
	UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (GvrUnitySdkVersion_t3EA8C55A1AA07CE5F11623B831B2125B5A159D54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799), -1, sizeof(GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2704[7] = 
{
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799_StaticFields::get_offset_of_instance_4(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_headsetProvider_5(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_headsetState_6(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_standaloneUpdate_7(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_waitForEndOfFrame_8(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_safetyRegionDelegate_9(),
	GvrHeadset_t738AC1680EDB890C18B3267D2CAAE3236EAFB799::get_offset_of_recenterDelegate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (OnSafetyRegionEvent_t22FC4164839C97972ECFD232F25C1ACDA5F00FB6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (OnRecenterEvent_tE05A511F1044AA9B68DA20CE8654A9DA35BD3F0A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[3] = 
{
	U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E::get_offset_of_U3CU3E1__state_0(),
	U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E::get_offset_of_U3CU3E2__current_1(),
	U3CEndOfFrameU3Ed__28_tEDA04539C2348C215D96216CB150618E561DB92E::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (GvrEventType_tC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2708[5] = 
{
	GvrEventType_tC6D71EF7C804BE70C6AA7D1ED3C6C10E6A1ABF14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (GvrRecenterEventType_t4A5C521A229315E6BF682CCDB9DA79069389E2E2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2709[4] = 
{
	GvrRecenterEventType_t4A5C521A229315E6BF682CCDB9DA79069389E2E2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (GvrRecenterFlags_tD91F69BA36CF2697A175C1DE357032ABEA0E194D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2710[2] = 
{
	GvrRecenterFlags_tD91F69BA36CF2697A175C1DE357032ABEA0E194D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (GvrErrorType_tEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2711[6] = 
{
	GvrErrorType_tEBD568DDAD3EA462EA8CF1CCC7EC62610E6C1CBA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (GvrSafetyRegionType_tB79667D62FE7D043A07C5562A527A0F35BC731B6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	GvrSafetyRegionType_tB79667D62FE7D043A07C5562A527A0F35BC731B6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD), -1, sizeof(InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2713[1] = 
{
	InstantPreviewHelper_tFF78920717830306651E9CD85F55999644F5ACDD_StaticFields::get_offset_of_AdbPath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (GvrKeyboardEvent_tE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2714[9] = 
{
	GvrKeyboardEvent_tE666C68AE3F02E0C388708CE74BB0F28DD7DB9A9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (GvrKeyboardError_tDC237522A4F6C62FE108DEEF4FE3882DD60E42D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2715[5] = 
{
	GvrKeyboardError_tDC237522A4F6C62FE108DEEF4FE3882DD60E42D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[3] = 
{
	GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22), -1, sizeof(GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2717[17] = 
{
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields::get_offset_of_instance_4(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields::get_offset_of_keyboardProvider_5(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_keyboardState_6(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_keyboardUpdate_7(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_errorCallback_8(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_showCallback_9(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_hideCallback_10(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_updateCallback_11(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_enterCallback_12(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_isKeyboardHidden_13(),
	0,
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields::get_offset_of_threadSafeCallbacks_15(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22_StaticFields::get_offset_of_callbacksLock_16(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_keyboardDelegate_17(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_inputMode_18(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_useRecommended_19(),
	GvrKeyboard_t1D110BF12D5ED911B1DCBF6C7F649C15C608BC22::get_offset_of_distance_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (StandardCallback_t76BBD536FAAD61296741018AC73FFCDB83013DF9), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (EditTextCallback_t7B4227B0E2FD7D66DDA3667E4A4DF278ABABB40A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (ErrorCallback_t8916A2402C92902DA76B58F0CBA3265E462ECDA1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[3] = 
{
	U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B::get_offset_of_U3CU3E1__state_0(),
	U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B::get_offset_of_U3CU3E2__current_1(),
	U3CExecuterU3Ed__44_t9E8C3F83B8FC4335640C612A804854691D2B9D7B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516), -1, sizeof(GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2724[3] = 
{
	0,
	0,
	GvrKeyboardIntent_t97F3D1B43AFDBB1F6D519F6C9E5A2CC5FFC76516_StaticFields::get_offset_of_theInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (KeyboardCallback_t08BF5F9BC9534D07B8D85B578585310A598E6CAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[5] = 
{
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB::get_offset_of_editorText_0(),
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB::get_offset_of_mode_1(),
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB::get_offset_of_isValid_2(),
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB::get_offset_of_isReady_3(),
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB::get_offset_of_worldMatrix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54), -1, sizeof(Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2727[4] = 
{
	Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54_StaticFields::get_offset_of_FLIP_Z_0(),
	Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t5AA21E36568E430CFD8D466575DB75CB62E1FB54::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (MutablePose3D_tAEFBBCFD0FFEBA819AC3A920371D3B52AEE9DADA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[2] = 
{
	GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF::get_offset_of_text_0(),
	GvrInfo_t8439C7B5D9A17F3900584550E2DC83531645E0FF::get_offset_of_numLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (GvrDropdown_tEECE530E6010547F47D061423CEAB8E286D61E7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[1] = 
{
	GvrDropdown_tEECE530E6010547F47D061423CEAB8E286D61E7E::get_offset_of_currentBlocker_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (GvrActivityHelper_tFC98059228A65C623CB507DD6D6AC9B9CDB1D1FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2), -1, sizeof(GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[5] = 
{
	0,
	0,
	0,
	0,
	GvrDaydreamApi_tEFF4FFDBC89FB604B66AAC16B86AF15725EEDBA2_StaticFields::get_offset_of_m_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (U3CU3Ec__DisplayClass12_0_t2B0191D0E254B1E5F8CC7BBC30C6138F86859C84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2B0191D0E254B1E5F8CC7BBC30C6138F86859C84::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (GvrIntent_t94EC573B8DC2FF531EFEE3F2A901AC977B0CA568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (GvrMathHelpers_t59635576885D632A9739B3333BD5FE56418A19AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (GvrUIHelpers_t6EAF84EB92DA6F8D9D4F9A05541D2CE62833A629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (GvrVRHelpers_t18660FD90F25F1BA1E804F476A6921633E1BBF9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231), -1, sizeof(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[28] = 
{
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoPlayerPtr_4(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoPlayerEventBase_5(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_initialTexture_6(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_surfaceTexture_7(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoMatrixRaw_8(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoMatrix_9(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoMatrixPropertyId_10(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_lastVideoTimestamp_11(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_initialized_12(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_texWidth_13(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_texHeight_14(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_lastBufferedPosition_15(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_framecount_16(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_screen_17(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_renderEventFunction_18(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_playOnResume_19(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_onEventCallbacks_20(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_onExceptionCallbacks_21(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231_StaticFields::get_offset_of_ExecuteOnMainThread_22(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_statusText_23(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231::get_offset_of_useSecurePath_30(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (VideoType_tC41DCEDD05448446169FAD1C41F1ED3964792D10)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2739[4] = 
{
	VideoType_tC41DCEDD05448446169FAD1C41F1ED3964792D10::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (VideoResolution_t80D666F578BADB8489D0894CB58BC868564B9D7F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2740[6] = 
{
	VideoResolution_t80D666F578BADB8489D0894CB58BC868564B9D7F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (VideoPlayerState_tF88330EB1305982C0484EE2EAFA554BE509395DB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2741[6] = 
{
	VideoPlayerState_tF88330EB1305982C0484EE2EAFA554BE509395DB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (VideoEvents_tCF660099ABF0BA02C6A29F1F205D17B13668E349)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2742[6] = 
{
	VideoEvents_tCF660099ABF0BA02C6A29F1F205D17B13668E349::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (StereoMode_tF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2743[5] = 
{
	StereoMode_tF0E4F8AD1E8DF753171D4D1E43B814D83A6835B7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (RenderCommand_t41B8C75B0D75BAB6F2E700B30B7351CD18DACE43)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2744[8] = 
{
	RenderCommand_t41B8C75B0D75BAB6F2E700B30B7351CD18DACE43::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (OnVideoEventCallback_t421705FACDFDD6D7E7FE943A515EFD2CBE12E0EA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (OnExceptionCallback_t242241ACCD84605752389B455720ED334739A581), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649), -1, sizeof(U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD9F253A483F30D6C2F08A8994A7AFFB292E8D649_StaticFields::get_offset_of_U3CU3E9__69_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[2] = 
{
	U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334::get_offset_of_player_0(),
	U3CU3Ec__DisplayClass90_0_tCD41FE97FBF2BEBC02FC883D25C0D53AFE76B334::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[3] = 
{
	U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E::get_offset_of_player_0(),
	U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E::get_offset_of_type_1(),
	U3CU3Ec__DisplayClass92_0_t154921651FC9A68B8DC7FC27C1E8522A363E801E::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[7] = 
{
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_turnSpeed_4(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_panSpeed_5(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_zoomSpeed_6(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_mouseOrigin_7(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_isPanning_8(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_isRotating_9(),
	MoveCamera_t4AAFFD5563F6281CDC5A4A6DE319AD10CD8C1717::get_offset_of_isZooming_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[14] = 
{
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_initialSpeed_4(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_increaseSpeed_5(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_allowMovement_6(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_allowRotation_7(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_forwardButton_8(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_backwardButton_9(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_rightButton_10(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_leftButton_11(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_cursorSensitivity_12(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_cursorToggleAllowed_13(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_cursorToggleButton_14(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_currentSpeed_15(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_moving_16(),
	GhostFreeRoamCamera_t503FCDB2A6DC93073EE765359D313EE7EA184065::get_offset_of_togglePressed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66), -1, sizeof(PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2752[25] = 
{
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields::get_offset_of_defaultInstance_0(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields::get_offset_of__phoneEventFieldNames_1(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66_StaticFields::get_offset_of__phoneEventFieldTags_2(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasType_4(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_type__5(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasMotionEvent_7(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_motionEvent__8(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasGyroscopeEvent_10(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_gyroscopeEvent__11(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasAccelerometerEvent_13(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_accelerometerEvent__14(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasDepthMapEvent_16(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_depthMapEvent__17(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasOrientationEvent_19(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_orientationEvent__20(),
	0,
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_hasKeyEvent_22(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_keyEvent__23(),
	PhoneEvent_t1D3392A01A6A42FF80D8573F8E1DCDBD8854BF66::get_offset_of_memoizedSerializedSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (Types_t4E1129DB2DA1976763A5E1940C85EBA8EBB73210), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (Type_tE66B948E7250AA336E8768E46508619266BABE96)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2754[7] = 
{
	Type_tE66B948E7250AA336E8768E46508619266BABE96::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3), -1, sizeof(MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[12] = 
{
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields::get_offset_of_defaultInstance_0(),
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields::get_offset_of__motionEventFieldNames_1(),
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3_StaticFields::get_offset_of__motionEventFieldTags_2(),
	0,
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_hasTimestamp_4(),
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_timestamp__5(),
	0,
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_hasAction_7(),
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_action__8(),
	0,
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_pointers__10(),
	MotionEvent_tC40991EE6BE0F076050A3C4F4CB28EE5321C3BB3::get_offset_of_memoizedSerializedSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (Types_tB6BA8DE659576B2D1767BA1503423D241112EE98), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1), -1, sizeof(Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[13] = 
{
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields::get_offset_of_defaultInstance_0(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields::get_offset_of__pointerFieldNames_1(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1_StaticFields::get_offset_of__pointerFieldTags_2(),
	0,
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_hasId_4(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_id__5(),
	0,
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_hasNormalizedX_7(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_normalizedX__8(),
	0,
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_hasNormalizedY_10(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_normalizedY__11(),
	Pointer_t5B82BC63FC5C619C95D79E3D9ABA017C16D584D1::get_offset_of_memoizedSerializedSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[2] = 
{
	Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F::get_offset_of_resultIsReadOnly_0(),
	Builder_t26593F11EB307FF14A9F5E5D7C27F75A27E1997F::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (Builder_t788A156BB792686149E9E866F1C7D953296A19D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[2] = 
{
	Builder_t788A156BB792686149E9E866F1C7D953296A19D1::get_offset_of_resultIsReadOnly_0(),
	Builder_t788A156BB792686149E9E866F1C7D953296A19D1::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764), -1, sizeof(GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2760[16] = 
{
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields::get_offset_of_defaultInstance_0(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields::get_offset_of__gyroscopeEventFieldNames_1(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764_StaticFields::get_offset_of__gyroscopeEventFieldTags_2(),
	0,
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_hasTimestamp_4(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_timestamp__5(),
	0,
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_hasX_7(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_x__8(),
	0,
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_hasY_10(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_y__11(),
	0,
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_hasZ_13(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_z__14(),
	GyroscopeEvent_t43B4FB209A9EEA862ABA6917622C810575AFD764::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[2] = 
{
	Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9::get_offset_of_resultIsReadOnly_0(),
	Builder_t23D29EDCC5B390ADCFA717D8578BF86DC74B0FC9::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F), -1, sizeof(AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2762[16] = 
{
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields::get_offset_of_defaultInstance_0(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields::get_offset_of__accelerometerEventFieldNames_1(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F_StaticFields::get_offset_of__accelerometerEventFieldTags_2(),
	0,
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_hasTimestamp_4(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_timestamp__5(),
	0,
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_hasX_7(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_x__8(),
	0,
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_hasY_10(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_y__11(),
	0,
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_hasZ_13(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_z__14(),
	AccelerometerEvent_tB158AB76F365C24CE204B273BC3FFD429C12F01F::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[2] = 
{
	Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055::get_offset_of_resultIsReadOnly_0(),
	Builder_t17E7B4A37A494267F90C1C0E60A2854A44757055::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571), -1, sizeof(DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2764[16] = 
{
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields::get_offset_of_defaultInstance_0(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields::get_offset_of__depthMapEventFieldNames_1(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571_StaticFields::get_offset_of__depthMapEventFieldTags_2(),
	0,
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_hasTimestamp_4(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_timestamp__5(),
	0,
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_hasWidth_7(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_width__8(),
	0,
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_hasHeight_10(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_height__11(),
	0,
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_zDistancesMemoizedSerializedSize_13(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_zDistances__14(),
	DepthMapEvent_tE655CB2D2B764E6BEDE058F70764D529BEA00571::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[2] = 
{
	Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613::get_offset_of_resultIsReadOnly_0(),
	Builder_t8D4FAD3FA57F88210975300FD1136B3D6C120613::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011), -1, sizeof(OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2766[19] = 
{
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields::get_offset_of_defaultInstance_0(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields::get_offset_of__orientationEventFieldNames_1(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011_StaticFields::get_offset_of__orientationEventFieldTags_2(),
	0,
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_hasTimestamp_4(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_timestamp__5(),
	0,
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_hasX_7(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_x__8(),
	0,
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_hasY_10(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_y__11(),
	0,
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_hasZ_13(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_z__14(),
	0,
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_hasW_16(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_w__17(),
	OrientationEvent_tE448386384E4E5CC5C4FFBB5A22055845E0A9011::get_offset_of_memoizedSerializedSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[2] = 
{
	Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49::get_offset_of_resultIsReadOnly_0(),
	Builder_tCAC10B81FDF15941DE7FAC380E33D08FDF542C49::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C), -1, sizeof(KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[10] = 
{
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields::get_offset_of_defaultInstance_0(),
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields::get_offset_of__keyEventFieldNames_1(),
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C_StaticFields::get_offset_of__keyEventFieldTags_2(),
	0,
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C::get_offset_of_hasAction_4(),
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C::get_offset_of_action__5(),
	0,
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C::get_offset_of_hasCode_7(),
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C::get_offset_of_code__8(),
	KeyEvent_tBFBA69BC341F62C43218C77DB2A93E4054DA0B5C::get_offset_of_memoizedSerializedSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[2] = 
{
	Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E::get_offset_of_resultIsReadOnly_0(),
	Builder_t6B925F90C58BDE4B9FC6B70D743BBD4AB534E37E::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[2] = 
{
	Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB::get_offset_of_resultIsReadOnly_0(),
	Builder_t9A3B43148D02AB6C7D345FE90B54F2288BC522CB::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072), -1, sizeof(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2771[1] = 
{
	PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (ControllerProviderFactory_t1109E019479E73C5F60CC087DFD5A7C17D0E276A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (DummyControllerProvider_t0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[1] = 
{
	DummyControllerProvider_t0263376DEB28F1ACAFBC61AC5F8FD9F3D7F72B96::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D), -1, sizeof(MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2774[9] = 
{
	0,
	0,
	MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D::get_offset_of_state_2(),
	MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D::get_offset_of_mouseDelta_3(),
	MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D::get_offset_of_wasTouching_4(),
	MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D::get_offset_of_lastButtonsState_5(),
	0,
	0,
	MouseControllerProvider_t0D8E5D4784755448297378BB77B4529844D90D6D_StaticFields::get_offset_of_INVERT_Y_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[15] = 
{
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_connectionState_0(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_apiStatus_1(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_orientation_2(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_position_3(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_gyro_4(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_accel_5(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_touchPos_6(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_recentered_7(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_buttonsState_8(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_buttonsDown_9(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_buttonsUp_10(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_errorDetails_11(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_gvrPtr_12(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_isCharging_13(),
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451::get_offset_of_batteryLevel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D), -1, sizeof(ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2776[1] = 
{
	ControllerUtils_t5499B7AF2E0C401DFB9D28DBFF108B8002AA864D_StaticFields::get_offset_of_AllHands_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772), -1, sizeof(EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2777[4] = 
{
	EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields::get_offset_of_instance_4(),
	EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772::get_offset_of_PHONE_EVENT_MODE_5(),
	EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields::get_offset_of_USB_SERVER_IP_6(),
	EmulatorConfig_t6A187DA843514341C61D3449704A427E6B2A4772_StaticFields::get_offset_of_WIFI_SERVER_IP_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (Mode_t075FFF717D0EFAE94D1B7E5F4B400603D6238B48)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2778[4] = 
{
	Mode_t075FFF717D0EFAE94D1B7E5F4B400603D6238B48::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93)+ sizeof (RuntimeObject), sizeof(EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[2] = 
{
	EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorGyroEvent_t01030B7841C1F666BBFF45B9CC7BF70AA14ACC93::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1)+ sizeof (RuntimeObject), sizeof(EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2780[2] = 
{
	EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorAccelEvent_tDCD33679FD8E98A3B307D52F6148569846B047D1::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E)+ sizeof (RuntimeObject), -1, sizeof(EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2781[6] = 
{
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E::get_offset_of_action_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E::get_offset_of_relativeTimestamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E::get_offset_of_pointers_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields::get_offset_of_ACTION_POINTER_INDEX_SHIFT_3(),
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields::get_offset_of_ACTION_POINTER_INDEX_MASK_4(),
	EmulatorTouchEvent_t621AB662684F3BA6D159C02C9AFB6020BA76C18E_StaticFields::get_offset_of_ACTION_MASK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (Action_t1152BDDBD0A80C8D7203F5FE66E73A1B8651805D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[10] = 
{
	Action_t1152BDDBD0A80C8D7203F5FE66E73A1B8651805D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F)+ sizeof (RuntimeObject), sizeof(Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F ), 0, 0 };
extern const int32_t g_FieldOffsetTable2783[3] = 
{
	Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F::get_offset_of_fingerId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F::get_offset_of_normalizedX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Pointer_tE4CEDEA82E3FC63C990C824277BE5F151E19907F::get_offset_of_normalizedY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA)+ sizeof (RuntimeObject), sizeof(EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2784[2] = 
{
	EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorOrientationEvent_t13D24B09418528D66FCA783094801F43F2EAE0CA::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125)+ sizeof (RuntimeObject), sizeof(EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2785[2] = 
{
	EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125::get_offset_of_code_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorButtonEvent_tF3B0BF0210629C8389331911A44B3285536BC125::get_offset_of_down_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (ButtonCode_t4A94C0D8F14CD6BB4655AB140F26E629D03748B5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2786[7] = 
{
	ButtonCode_t4A94C0D8F14CD6BB4655AB140F26E629D03748B5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (HeadsetProviderFactory_tA8CF38CBC95F9E1D7003E63264FEEB89E85FF65F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (gvr_feature_tF0F811D9C8BF77586B015C5FA76A34EC608187DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2789[2] = 
{
	gvr_feature_tF0F811D9C8BF77586B015C5FA76A34EC608187DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (gvr_property_type_tA9521BDBC33D00B4491D09EABF7DAFA201200553)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[6] = 
{
	gvr_property_type_tA9521BDBC33D00B4491D09EABF7DAFA201200553::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (gvr_value_type_t9506B044915CA4F1202BCC5976BCB808E9107900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[15] = 
{
	gvr_value_type_t9506B044915CA4F1202BCC5976BCB808E9107900::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (gvr_recenter_flags_tA234601F30F88671225972AD827D96CD306F1F56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	gvr_recenter_flags_tA234601F30F88671225972AD827D96CD306F1F56::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (DummyHeadsetProvider_t78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[1] = 
{
	DummyHeadsetProvider_t78D60D7FE4227A80F8DD869CE6FB2B739FDF6D8D::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (EditorHeadsetProvider_t9BF4A3F07FDAA378510721BF96BB0746854CAC59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[1] = 
{
	EditorHeadsetProvider_t9BF4A3F07FDAA378510721BF96BB0746854CAC59::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA)+ sizeof (RuntimeObject), sizeof(HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2795[7] = 
{
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_eventFlags_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_eventTimestampNs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_recenterEventType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_recenterEventFlags_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_recenteredPosition_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t1DE0929297DCC52D513D29C437055C86783C94AA::get_offset_of_recenteredRotation_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E), -1, sizeof(InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2797[8] = 
{
	0,
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
	0,
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E::get_offset_of_OutputResolution_7(),
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E::get_offset_of_MultisampleCount_8(),
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E::get_offset_of_BitRate_9(),
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E::get_offset_of_InstallApkOnRun_10(),
	InstantPreview_t711D6A079A7E30F637B5C2309E9E29EFB8793F6E::get_offset_of_InstantPreviewApk_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (Resolutions_tA438755150C18D8A2A0C3C87A34C7E741E4BC424)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[4] = 
{
	Resolutions_tA438755150C18D8A2A0C3C87A34C7E741E4BC424::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4)+ sizeof (RuntimeObject), sizeof(ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2799[2] = 
{
	ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ResolutionSize_t6CDDCD67E1BE1D80E34B593670F582DEAF116EC4::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
