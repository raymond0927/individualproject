﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaHideAnimalController : MonoBehaviour
{
    public static GameObject obj_Octopus;//	Octopus
    public static GameObject obj_Crab;//	Crab
    public static GameObject obj_Starfish;//	Starfish
    public static GameObject obj_Dolphin;//	Dolphin
    public static GameObject obj_Sea_Urchin;//	Sea ​​urchin
    public static GameObject obj_Moray_Eel;//	Moray eel
    public static GameObject obj_Long_Finned_Pilot_Whale;//	Long finned pilot whale
    public static GameObject obj_Killer_Whale;//	Killer whale
    public static GameObject obj_Basking_Shark;//	Basking shark
    public static GameObject obj_Great_White_Shark;//	Great white shark
    public static GameObject obj_Squid;//	Squid 

    // Start is called before the first frame update
    void Start()
    {
        obj_Octopus = GameObject.Find("OctopusLocation");
        obj_Crab = GameObject.Find("CrabLocation");
        obj_Sea_Urchin = GameObject.Find("urchinLocation");
        obj_Starfish = GameObject.Find("StarfishLocation");
        obj_Dolphin = GameObject.Find("DolphinLocation");
        obj_Moray_Eel = GameObject.Find("MorayEelLocation");
        obj_Long_Finned_Pilot_Whale = GameObject.Find("LongFinnedPilotWhaleLocation");
        obj_Killer_Whale = GameObject.Find("KillerWhaleLocation");
        obj_Basking_Shark = GameObject.Find("BaskingSharkLocation");
        obj_Great_White_Shark = GameObject.Find("GreatWhiteSharkLocation");
        obj_Squid = GameObject.Find("SquidLocation");

    }

    // Update is called once per frame
    void Update()
    {
    }

    public static void hideAnimals(String name)
    {
        Debug.Log("name " + name);
        if (name.Length > 0)
        {
            obj_Octopus.SetActive(false);
            obj_Crab.SetActive(false);
            obj_Sea_Urchin.SetActive(false);
            obj_Starfish.SetActive(false);
            obj_Dolphin.SetActive(false);
            obj_Moray_Eel.SetActive(false);
            obj_Long_Finned_Pilot_Whale.SetActive(false);
            obj_Killer_Whale.SetActive(false);
            obj_Basking_Shark.SetActive(false);
            obj_Great_White_Shark.SetActive(false);
            obj_Squid.SetActive(false);

        }
        else
        {
            obj_Octopus.SetActive(true);
            obj_Crab.SetActive(true);
            obj_Sea_Urchin.SetActive(true);
            obj_Starfish.SetActive(true);
            obj_Dolphin.SetActive(true);
            obj_Moray_Eel.SetActive(true);
            obj_Long_Finned_Pilot_Whale.SetActive(true);
            obj_Killer_Whale.SetActive(true);
            obj_Basking_Shark.SetActive(true);
            obj_Great_White_Shark.SetActive(true);
            obj_Squid.SetActive(true);

        }


        switch (name)
        {
            case "Octopus":
                obj_Octopus.SetActive(true);
                break;
            case "Crab":
                obj_Crab.SetActive(true);
                break;
            case "Sea ​Urchin":
                obj_Sea_Urchin.SetActive(true);
                break;
            case "Starfish":
                obj_Starfish.SetActive(true);
                break;
            case "Dolphin":
                obj_Dolphin.SetActive(true);
                break;
            case "Moray eel":
                obj_Moray_Eel.SetActive(true);
                break;
            case "Long Finned Pilot Whale":
                obj_Long_Finned_Pilot_Whale.SetActive(true);
                break;
            case "Killer Whale":
                obj_Killer_Whale.SetActive(true);
                break;
            case "Basking Shark":
                obj_Basking_Shark.SetActive(true);
                break;
            case "Great White Shark":
                obj_Great_White_Shark.SetActive(true);
                break;
            case "Squid":
                obj_Squid.SetActive(true);
                break;
        }
    }

}