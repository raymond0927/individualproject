﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SeaMenu : MonoBehaviour
{
    public float buttontimer;
    public bool buttontime = false;
    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair
    public GameObject obj_reminderCanvas;    //friendly reminder

    private bool isQuiz = false, isLow = false, isHigh = false;
    private int restTime = 3600;
    public float remindertimer;
    public bool remindertime = false;

    // Use this for initialization
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;  //the monitor never close
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true; //the screen rotation
        Screen.orientation = ScreenOrientation.AutoRotation;
        RestTimer.isStop = false;
    }

    // Update is called once per frame
    void Update()
    {
        // reminder 

        if (RestTimer.timer >= restTime)
        {
            remindertime = true;
            obj_reminderCanvas.SetActive(true);
            RestTimer.isStop = true;
            RestTimer.timer = 0;
        }

        if (remindertime)
        {
            remindertimer += Time.deltaTime;
        }
        if (remindertimer >= 5)
        {
            obj_reminderCanvas.SetActive(false);
            remindertimer = 0;
            remindertime = false;
            RestTimer.isStop = false;
        }

        //===========END========

        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isQuiz)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("SeaQuiz");
            isQuiz = false;
        }
        else if (buttontimer >= 2 && isHigh)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("AridAreaScenes");
            isHigh = false;
        }
        else if (buttontimer >= 2 && isLow)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("ForestScenes");
            isLow = false;
        }
    }



    public void goToQuiz()
    {
        isQuiz = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void goToLow()
    {
        isLow = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void goToHigh()
    {
        isHigh = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        buttontime = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        isQuiz = false;
        isLow = false;
        isHigh = false;
        isQuiz = false;
        obj_shoot.SetActive(true);
        //取消select個button
    }
}
