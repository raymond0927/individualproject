﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AridAreaQuizMenu : MonoBehaviour
{

    public float buttontimer;
    public bool buttontime = false;
    public Image Bar;    // loading circle
    public GameObject obj_Bar; // loading circle
    public GameObject obj_shoot;    //cross hair

    private bool isTutorial = false;

    public GameObject obj_reminderCanvas;    //friendly reminder
    private int restTime = 3600;
    public float remindertimer;
    public bool remindertime = false;

    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;  //the monitor never close
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true; //the screen rotation
        Screen.orientation = ScreenOrientation.AutoRotation;
        RestTimer.isStop = false;
    }

    // Update is called once per frame
    void Update()
    {

        // reminder 

        if (RestTimer.timer >= restTime)
        {
            remindertime = true;
            obj_reminderCanvas.SetActive(true);
            RestTimer.isStop = true;
            RestTimer.timer = 0;
        }

        if (remindertime)
        {
            remindertimer += Time.deltaTime;
        }
        if (remindertimer >= 5)
        {
            obj_reminderCanvas.SetActive(false);
            remindertimer = 0;
            remindertime = false;
            RestTimer.isStop = false;
        }

        //===========END========

        if (buttontime == true)
        {
            obj_Bar.SetActive(true);
            buttontimer += Time.deltaTime;
            Bar.fillAmount = buttontimer / 2;  //loading circle
        }

        if (buttontimer >= 2 && isTutorial)
        {
            obj_Bar.SetActive(false);
            buttontimer = 0;
            Bar.fillAmount = 0;  //loading circle
            buttontime = false;
            obj_shoot.SetActive(true);
            SceneManager.LoadScene("AridAreaScenes");
            isTutorial = false;
        }
    }

    public void backTutorial()
    {
        isTutorial = true;
        obj_shoot.SetActive(false);
        buttontime = true;
    }

    public void cancel()
    {
        Debug.Log("cancel");
        obj_Bar.SetActive(false);
        buttontime = false;
        buttontimer = 0;
        Bar.fillAmount = 0;
        isTutorial = false;
        obj_shoot.SetActive(true);
        //取消select個button
    }
}
