﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GoogleVR.Demos.DemoInputManager
struct DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571;
// GoogleVR.HelloVR.HeadsetDemoManager
struct HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1;
// GoogleVR.VideoDemo.ButtonEvent
struct ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A;
// GoogleVR.VideoDemo.MenuHandler
struct MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010;
// GoogleVR.VideoDemo.VideoControlsManager
struct VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1;
// GvrControllerHand[]
struct GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157;
// GvrKeyboard/KeyboardCallback
struct KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231;
// KeyboardState
struct KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.String>
struct Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T18935990A85C9535562F448C7009B80FBBE6C0E5_H
#define U3CU3EC_T18935990A85C9535562F448C7009B80FBBE6C0E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoInputManager_<>c
struct  U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields
{
public:
	// GoogleVR.Demos.DemoInputManager_<>c GoogleVR.Demos.DemoInputManager_<>c::<>9
	U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5 * ___U3CU3E9_0;
	// System.Predicate`1<System.String> GoogleVR.Demos.DemoInputManager_<>c::<>9__31_0
	Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * ___U3CU3E9__31_0_1;
	// System.Predicate`1<System.String> GoogleVR.Demos.DemoInputManager_<>c::<>9__32_0
	Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * ___U3CU3E9__32_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields, ___U3CU3E9__32_0_2)); }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * get_U3CU3E9__32_0_2() const { return ___U3CU3E9__32_0_2; }
	inline Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 ** get_address_of_U3CU3E9__32_0_2() { return &___U3CU3E9__32_0_2; }
	inline void set_U3CU3E9__32_0_2(Predicate_1_t1659E8BF0558E8AFCAE8175EB0B5FD7E43E5F9C1 * value)
	{
		___U3CU3E9__32_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T18935990A85C9535562F448C7009B80FBBE6C0E5_H
#ifndef U3CSTATUSUPDATELOOPU3ED__13_TB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA_H
#define U3CSTATUSUPDATELOOPU3ED__13_TB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HeadsetDemoManager_<StatusUpdateLoop>d__13
struct  U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA  : public RuntimeObject
{
public:
	// System.Int32 GoogleVR.HelloVR.HeadsetDemoManager_<StatusUpdateLoop>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GoogleVR.HelloVR.HeadsetDemoManager_<StatusUpdateLoop>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GoogleVR.HelloVR.HeadsetDemoManager GoogleVR.HelloVR.HeadsetDemoManager_<StatusUpdateLoop>d__13::<>4__this
	HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA, ___U3CU3E4__this_2)); }
	inline HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTATUSUPDATELOOPU3ED__13_TB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA_H
#ifndef U3CDOAPPEARU3ED__3_T6B532654E07C7B430E4AE55DB88B37856F5F7419_H
#define U3CDOAPPEARU3ED__3_T6B532654E07C7B430E4AE55DB88B37856F5F7419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler_<DoAppear>d__3
struct  U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419  : public RuntimeObject
{
public:
	// System.Int32 GoogleVR.VideoDemo.MenuHandler_<DoAppear>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GoogleVR.VideoDemo.MenuHandler_<DoAppear>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GoogleVR.VideoDemo.MenuHandler GoogleVR.VideoDemo.MenuHandler_<DoAppear>d__3::<>4__this
	MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * ___U3CU3E4__this_2;
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.MenuHandler_<DoAppear>d__3::<cg>5__2
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___U3CcgU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419, ___U3CU3E4__this_2)); }
	inline MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcgU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419, ___U3CcgU3E5__2_3)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_U3CcgU3E5__2_3() const { return ___U3CcgU3E5__2_3; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_U3CcgU3E5__2_3() { return &___U3CcgU3E5__2_3; }
	inline void set_U3CcgU3E5__2_3(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___U3CcgU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOAPPEARU3ED__3_T6B532654E07C7B430E4AE55DB88B37856F5F7419_H
#ifndef U3CDOFADEU3ED__4_T06CBEC19196B912BA6EF0F39A682572BD1D2029B_H
#define U3CDOFADEU3ED__4_T06CBEC19196B912BA6EF0F39A682572BD1D2029B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler_<DoFade>d__4
struct  U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B  : public RuntimeObject
{
public:
	// System.Int32 GoogleVR.VideoDemo.MenuHandler_<DoFade>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GoogleVR.VideoDemo.MenuHandler_<DoFade>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GoogleVR.VideoDemo.MenuHandler GoogleVR.VideoDemo.MenuHandler_<DoFade>d__4::<>4__this
	MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * ___U3CU3E4__this_2;
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.MenuHandler_<DoFade>d__4::<cg>5__2
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___U3CcgU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B, ___U3CU3E4__this_2)); }
	inline MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcgU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B, ___U3CcgU3E5__2_3)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_U3CcgU3E5__2_3() const { return ___U3CcgU3E5__2_3; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_U3CcgU3E5__2_3() { return &___U3CcgU3E5__2_3; }
	inline void set_U3CcgU3E5__2_3(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___U3CcgU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3ED__4_T06CBEC19196B912BA6EF0F39A682572BD1D2029B_H
#ifndef U3CDOAPPEARU3ED__25_TC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65_H
#define U3CDOAPPEARU3ED__25_TC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager_<DoAppear>d__25
struct  U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65  : public RuntimeObject
{
public:
	// System.Int32 GoogleVR.VideoDemo.VideoControlsManager_<DoAppear>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GoogleVR.VideoDemo.VideoControlsManager_<DoAppear>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.VideoControlsManager_<DoAppear>d__25::<>4__this
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * ___U3CU3E4__this_2;
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.VideoControlsManager_<DoAppear>d__25::<cg>5__2
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___U3CcgU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65, ___U3CU3E4__this_2)); }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcgU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65, ___U3CcgU3E5__2_3)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_U3CcgU3E5__2_3() const { return ___U3CcgU3E5__2_3; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_U3CcgU3E5__2_3() { return &___U3CcgU3E5__2_3; }
	inline void set_U3CcgU3E5__2_3(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___U3CcgU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOAPPEARU3ED__25_TC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65_H
#ifndef U3CDOFADEU3ED__26_T90D135FFAD6D45A84F0F712BBC40EBB9FE047336_H
#define U3CDOFADEU3ED__26_T90D135FFAD6D45A84F0F712BBC40EBB9FE047336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager_<DoFade>d__26
struct  U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336  : public RuntimeObject
{
public:
	// System.Int32 GoogleVR.VideoDemo.VideoControlsManager_<DoFade>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GoogleVR.VideoDemo.VideoControlsManager_<DoFade>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.VideoControlsManager_<DoFade>d__26::<>4__this
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * ___U3CU3E4__this_2;
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.VideoControlsManager_<DoFade>d__26::<cg>5__2
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___U3CcgU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336, ___U3CU3E4__this_2)); }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcgU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336, ___U3CcgU3E5__2_3)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_U3CcgU3E5__2_3() const { return ___U3CcgU3E5__2_3; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_U3CcgU3E5__2_3() { return &___U3CcgU3E5__2_3; }
	inline void set_U3CcgU3E5__2_3(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___U3CcgU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3ED__26_T90D135FFAD6D45A84F0F712BBC40EBB9FE047336_H
#ifndef DUMMYKEYBOARDPROVIDER_T950A5DD732832E0D14A0AA89B0A15AE645F7023B_H
#define DUMMYKEYBOARDPROVIDER_T950A5DD732832E0D14A0AA89B0A15AE645F7023B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyKeyboardProvider
struct  DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B  : public RuntimeObject
{
public:
	// KeyboardState Gvr.Internal.DummyKeyboardProvider::dummyState
	KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * ___dummyState_0;
	// System.String Gvr.Internal.DummyKeyboardProvider::<EditorText>k__BackingField
	String_t* ___U3CEditorTextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B, ___dummyState_0)); }
	inline KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * get_dummyState_0() const { return ___dummyState_0; }
	inline KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB ** get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(KeyboardState_tF84F64C0158DA96525380E14D1C65C2E9923B9BB * value)
	{
		___dummyState_0 = value;
		Il2CppCodeGenWriteBarrier((&___dummyState_0), value);
	}

	inline static int32_t get_offset_of_U3CEditorTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B, ___U3CEditorTextU3Ek__BackingField_1)); }
	inline String_t* get_U3CEditorTextU3Ek__BackingField_1() const { return ___U3CEditorTextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEditorTextU3Ek__BackingField_1() { return &___U3CEditorTextU3Ek__BackingField_1; }
	inline void set_U3CEditorTextU3Ek__BackingField_1(String_t* value)
	{
		___U3CEditorTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEditorTextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYKEYBOARDPROVIDER_T950A5DD732832E0D14A0AA89B0A15AE645F7023B_H
#ifndef GVRCURSORHELPER_T8B940F59FA02670945160E04E66B62614FF9DD7B_H
#define GVRCURSORHELPER_T8B940F59FA02670945160E04E66B62614FF9DD7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.GvrCursorHelper
struct  GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B  : public RuntimeObject
{
public:

public:
};

struct GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields
{
public:
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedHeadEmulationActive
	bool ___cachedHeadEmulationActive_0;
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedControllerEmulationActive
	bool ___cachedControllerEmulationActive_1;

public:
	inline static int32_t get_offset_of_cachedHeadEmulationActive_0() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields, ___cachedHeadEmulationActive_0)); }
	inline bool get_cachedHeadEmulationActive_0() const { return ___cachedHeadEmulationActive_0; }
	inline bool* get_address_of_cachedHeadEmulationActive_0() { return &___cachedHeadEmulationActive_0; }
	inline void set_cachedHeadEmulationActive_0(bool value)
	{
		___cachedHeadEmulationActive_0 = value;
	}

	inline static int32_t get_offset_of_cachedControllerEmulationActive_1() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields, ___cachedControllerEmulationActive_1)); }
	inline bool get_cachedControllerEmulationActive_1() const { return ___cachedControllerEmulationActive_1; }
	inline bool* get_address_of_cachedControllerEmulationActive_1() { return &___cachedControllerEmulationActive_1; }
	inline void set_cachedControllerEmulationActive_1(bool value)
	{
		___cachedControllerEmulationActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCURSORHELPER_T8B940F59FA02670945160E04E66B62614FF9DD7B_H
#ifndef KEYBOARDPROVIDERFACTORY_T2260D5A8F515013574E200073B95229F0F798E2D_H
#define KEYBOARDPROVIDERFACTORY_T2260D5A8F515013574E200073B95229F0F798E2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.KeyboardProviderFactory
struct  KeyboardProviderFactory_t2260D5A8F515013574E200073B95229F0F798E2D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDPROVIDERFACTORY_T2260D5A8F515013574E200073B95229F0F798E2D_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#define __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T65235398342E0C6C387633EE4B2E7F326A539C91_H
#ifndef __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#define __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16
struct  __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D16_T6EB025C40E80FDD74132718092D59E92446BD13D_H
#ifndef __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#define __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20
struct  __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382__padding[20];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D20_TD65589242911778C66D1E5AC9009597568746382_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#define __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_TD2672E72DE7681E58346994836A2016B555BF4C1_H
#ifndef GVR_CLOCK_TIME_POINT_TBCB5294A08D28AD8465B28C34010F8F281846D3D_H
#define GVR_CLOCK_TIME_POINT_TBCB5294A08D28AD8465B28C34010F8F281846D3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider_gvr_clock_time_point
struct  gvr_clock_time_point_tBCB5294A08D28AD8465B28C34010F8F281846D3D 
{
public:
	// System.Int64 Gvr.Internal.AndroidNativeKeyboardProvider_gvr_clock_time_point::monotonic_system_time_nanos
	int64_t ___monotonic_system_time_nanos_0;

public:
	inline static int32_t get_offset_of_monotonic_system_time_nanos_0() { return static_cast<int32_t>(offsetof(gvr_clock_time_point_tBCB5294A08D28AD8465B28C34010F8F281846D3D, ___monotonic_system_time_nanos_0)); }
	inline int64_t get_monotonic_system_time_nanos_0() const { return ___monotonic_system_time_nanos_0; }
	inline int64_t* get_address_of_monotonic_system_time_nanos_0() { return &___monotonic_system_time_nanos_0; }
	inline void set_monotonic_system_time_nanos_0(int64_t value)
	{
		___monotonic_system_time_nanos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_CLOCK_TIME_POINT_TBCB5294A08D28AD8465B28C34010F8F281846D3D_H
#ifndef GVR_RECTI_T5F7F0591CE0D13FFEF134C886F9ECA35B341C035_H
#define GVR_RECTI_T5F7F0591CE0D13FFEF134C886F9ECA35B341C035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider_gvr_recti
struct  gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035 
{
public:
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider_gvr_recti::left
	int32_t ___left_0;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider_gvr_recti::right
	int32_t ___right_1;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider_gvr_recti::bottom
	int32_t ___bottom_2;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider_gvr_recti::top
	int32_t ___top_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}

	inline static int32_t get_offset_of_bottom_2() { return static_cast<int32_t>(offsetof(gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035, ___bottom_2)); }
	inline int32_t get_bottom_2() const { return ___bottom_2; }
	inline int32_t* get_address_of_bottom_2() { return &___bottom_2; }
	inline void set_bottom_2(int32_t value)
	{
		___bottom_2 = value;
	}

	inline static int32_t get_offset_of_top_3() { return static_cast<int32_t>(offsetof(gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035, ___top_3)); }
	inline int32_t get_top_3() const { return ___top_3; }
	inline int32_t* get_address_of_top_3() { return &___top_3; }
	inline void set_top_3(int32_t value)
	{
		___top_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECTI_T5F7F0591CE0D13FFEF134C886F9ECA35B341C035_H
#ifndef UNITYRECT_T1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7_H
#define UNITYRECT_T1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_UnityRect
struct  UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7 
{
public:
	// System.Single Gvr.Internal.InstantPreview_UnityRect::right
	float ___right_0;
	// System.Single Gvr.Internal.InstantPreview_UnityRect::left
	float ___left_1;
	// System.Single Gvr.Internal.InstantPreview_UnityRect::top
	float ___top_2;
	// System.Single Gvr.Internal.InstantPreview_UnityRect::bottom
	float ___bottom_3;

public:
	inline static int32_t get_offset_of_right_0() { return static_cast<int32_t>(offsetof(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7, ___right_0)); }
	inline float get_right_0() const { return ___right_0; }
	inline float* get_address_of_right_0() { return &___right_0; }
	inline void set_right_0(float value)
	{
		___right_0 = value;
	}

	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7, ___left_1)); }
	inline float get_left_1() const { return ___left_1; }
	inline float* get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(float value)
	{
		___left_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7, ___top_2)); }
	inline float get_top_2() const { return ___top_2; }
	inline float* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(float value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7, ___bottom_3)); }
	inline float get_bottom_3() const { return ___bottom_3; }
	inline float* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(float value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRECT_T1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#define UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6FE5C79FD433599728A9AA732E588823AB88FDB5_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#define UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct  UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TDBAAAC82108B473A2F8F69586B33ED12EA5D86CE_H
#ifndef UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#define UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Transform>
struct  UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T0194CB708072293BCFF0F7A26737132AC2BA7C62_H
#ifndef UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#define UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T88E036FD5956DB491BCC160FA57EF4F9584042B9_H
#ifndef UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#define UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct  UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::16E2B412E9C2B8E31B780DE46254349320CCAAA0
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::25B4B83D2A43393F4E18624598DDA694217A6622
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___25B4B83D2A43393F4E18624598DDA694217A6622_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::311441405B64B3EA9097AC8E07F3274962EC6BB4
	__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F
	__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D16 <PrivateImplementationDetails>::D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8
	__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D20 <PrivateImplementationDetails>::FADC743710841EB901D5F6FBC97F555D4BD94310
	__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  ___FADC743710841EB901D5F6FBC97F555D4BD94310_5;

public:
	inline static int32_t get_offset_of_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() const { return ___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0() { return &___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0; }
	inline void set_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___16E2B412E9C2B8E31B780DE46254349320CCAAA0_0 = value;
	}

	inline static int32_t get_offset_of_U325B4B83D2A43393F4E18624598DDA694217A6622_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___25B4B83D2A43393F4E18624598DDA694217A6622_1)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_U325B4B83D2A43393F4E18624598DDA694217A6622_1() const { return ___25B4B83D2A43393F4E18624598DDA694217A6622_1; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_U325B4B83D2A43393F4E18624598DDA694217A6622_1() { return &___25B4B83D2A43393F4E18624598DDA694217A6622_1; }
	inline void set_U325B4B83D2A43393F4E18624598DDA694217A6622_1(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___25B4B83D2A43393F4E18624598DDA694217A6622_1 = value;
	}

	inline static int32_t get_offset_of_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2)); }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  get_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() const { return ___311441405B64B3EA9097AC8E07F3274962EC6BB4_2; }
	inline __StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 * get_address_of_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2() { return &___311441405B64B3EA9097AC8E07F3274962EC6BB4_2; }
	inline void set_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91  value)
	{
		___311441405B64B3EA9097AC8E07F3274962EC6BB4_2 = value;
	}

	inline static int32_t get_offset_of_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3)); }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  get_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() const { return ___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3; }
	inline __StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 * get_address_of_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3() { return &___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3; }
	inline void set_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1  value)
	{
		___C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3 = value;
	}

	inline static int32_t get_offset_of_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4)); }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  get_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() const { return ___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4; }
	inline __StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D * get_address_of_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4() { return &___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4; }
	inline void set_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D  value)
	{
		___D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4 = value;
	}

	inline static int32_t get_offset_of_FADC743710841EB901D5F6FBC97F555D4BD94310_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___FADC743710841EB901D5F6FBC97F555D4BD94310_5)); }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  get_FADC743710841EB901D5F6FBC97F555D4BD94310_5() const { return ___FADC743710841EB901D5F6FBC97F555D4BD94310_5; }
	inline __StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 * get_address_of_FADC743710841EB901D5F6FBC97F555D4BD94310_5() { return &___FADC743710841EB901D5F6FBC97F555D4BD94310_5; }
	inline void set_FADC743710841EB901D5F6FBC97F555D4BD94310_5(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382  value)
	{
		___FADC743710841EB901D5F6FBC97F555D4BD94310_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef EMULATEDPLATFORMTYPE_T0EE14329F304A519675FDF6F32C6A7B4F41B7CD0_H
#define EMULATEDPLATFORMTYPE_T0EE14329F304A519675FDF6F32C6A7B4F41B7CD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoInputManager_EmulatedPlatformType
struct  EmulatedPlatformType_t0EE14329F304A519675FDF6F32C6A7B4F41B7CD0 
{
public:
	// System.Int32 GoogleVR.Demos.DemoInputManager_EmulatedPlatformType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EmulatedPlatformType_t0EE14329F304A519675FDF6F32C6A7B4F41B7CD0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATEDPLATFORMTYPE_T0EE14329F304A519675FDF6F32C6A7B4F41B7CD0_H
#ifndef BOOLEVENT_T8D8A85D2328338C6A5406C80278BB5C6CA3D4D7D_H
#define BOOLEVENT_T8D8A85D2328338C6A5406C80278BB5C6CA3D4D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.BoolEvent
struct  BoolEvent_t8D8A85D2328338C6A5406C80278BB5C6CA3D4D7D  : public UnityEvent_1_t6FE5C79FD433599728A9AA732E588823AB88FDB5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_T8D8A85D2328338C6A5406C80278BB5C6CA3D4D7D_H
#ifndef BUTTONEVENT_T2B1EED42EEAC841B77E2136C2ECE9E9CC593088A_H
#define BUTTONEVENT_T2B1EED42EEAC841B77E2136C2ECE9E9CC593088A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ButtonEvent
struct  ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONEVENT_T2B1EED42EEAC841B77E2136C2ECE9E9CC593088A_H
#ifndef FLOATEVENT_T2530B0F15127DD0D34E564E894964A97ACEFC04B_H
#define FLOATEVENT_T2530B0F15127DD0D34E564E894964A97ACEFC04B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.FloatEvent
struct  FloatEvent_t2530B0F15127DD0D34E564E894964A97ACEFC04B  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATEVENT_T2530B0F15127DD0D34E564E894964A97ACEFC04B_H
#ifndef GAMEOBJECTEVENT_T76A1A2480DF4CE09119627E426013592A3D9CDCD_H
#define GAMEOBJECTEVENT_T76A1A2480DF4CE09119627E426013592A3D9CDCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.GameObjectEvent
struct  GameObjectEvent_t76A1A2480DF4CE09119627E426013592A3D9CDCD  : public UnityEvent_1_tDBAAAC82108B473A2F8F69586B33ED12EA5D86CE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEVENT_T76A1A2480DF4CE09119627E426013592A3D9CDCD_H
#ifndef TOUCHPADEVENT_T26A136076D38E0702174A5F1A15E380FFCF601ED_H
#define TOUCHPADEVENT_T26A136076D38E0702174A5F1A15E380FFCF601ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.TouchPadEvent
struct  TouchPadEvent_t26A136076D38E0702174A5F1A15E380FFCF601ED  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPADEVENT_T26A136076D38E0702174A5F1A15E380FFCF601ED_H
#ifndef TRANSFORMEVENT_T58048A882E00659D70E91039823C69CDEB673A1A_H
#define TRANSFORMEVENT_T58048A882E00659D70E91039823C69CDEB673A1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.TransformEvent
struct  TransformEvent_t58048A882E00659D70E91039823C69CDEB673A1A  : public UnityEvent_1_t0194CB708072293BCFF0F7A26737132AC2BA7C62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEVENT_T58048A882E00659D70E91039823C69CDEB673A1A_H
#ifndef VECTOR2EVENT_T05C88DC2526402EFF784CB9D111BBFDAC99EEA23_H
#define VECTOR2EVENT_T05C88DC2526402EFF784CB9D111BBFDAC99EEA23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.Vector2Event
struct  Vector2Event_t05C88DC2526402EFF784CB9D111BBFDAC99EEA23  : public UnityEvent_1_t88E036FD5956DB491BCC160FA57EF4F9584042B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2EVENT_T05C88DC2526402EFF784CB9D111BBFDAC99EEA23_H
#ifndef VECTOR3EVENT_TA27A82D5475BF7D6032A8D42C55D74658C91C253_H
#define VECTOR3EVENT_TA27A82D5475BF7D6032A8D42C55D74658C91C253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.Vector3Event
struct  Vector3Event_tA27A82D5475BF7D6032A8D42C55D74658C91C253  : public UnityEvent_1_t5D12B60679DCDB91E0DF66ABA5F8D3C7AD66C766
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3EVENT_TA27A82D5475BF7D6032A8D42C55D74658C91C253_H
#ifndef BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#define BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_BitRates
struct  BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_BitRates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITRATES_TB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7_H
#ifndef MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#define MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_MultisampleCounts
struct  MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview_MultisampleCounts::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISAMPLECOUNTS_TB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361_H
#ifndef UNITYEYEVIEWS_T0D204F2D57DEF889A843F0B3368F35F7A64278EC_H
#define UNITYEYEVIEWS_T0D204F2D57DEF889A843F0B3368F35F7A64278EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview_UnityEyeViews
struct  UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC 
{
public:
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview_UnityEyeViews::leftEyePose
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___leftEyePose_0;
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview_UnityEyeViews::rightEyePose
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___rightEyePose_1;
	// Gvr.Internal.InstantPreview_UnityRect Gvr.Internal.InstantPreview_UnityEyeViews::leftEyeViewSize
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  ___leftEyeViewSize_2;
	// Gvr.Internal.InstantPreview_UnityRect Gvr.Internal.InstantPreview_UnityEyeViews::rightEyeViewSize
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  ___rightEyeViewSize_3;

public:
	inline static int32_t get_offset_of_leftEyePose_0() { return static_cast<int32_t>(offsetof(UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC, ___leftEyePose_0)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_leftEyePose_0() const { return ___leftEyePose_0; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_leftEyePose_0() { return &___leftEyePose_0; }
	inline void set_leftEyePose_0(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___leftEyePose_0 = value;
	}

	inline static int32_t get_offset_of_rightEyePose_1() { return static_cast<int32_t>(offsetof(UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC, ___rightEyePose_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_rightEyePose_1() const { return ___rightEyePose_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_rightEyePose_1() { return &___rightEyePose_1; }
	inline void set_rightEyePose_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___rightEyePose_1 = value;
	}

	inline static int32_t get_offset_of_leftEyeViewSize_2() { return static_cast<int32_t>(offsetof(UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC, ___leftEyeViewSize_2)); }
	inline UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  get_leftEyeViewSize_2() const { return ___leftEyeViewSize_2; }
	inline UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7 * get_address_of_leftEyeViewSize_2() { return &___leftEyeViewSize_2; }
	inline void set_leftEyeViewSize_2(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  value)
	{
		___leftEyeViewSize_2 = value;
	}

	inline static int32_t get_offset_of_rightEyeViewSize_3() { return static_cast<int32_t>(offsetof(UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC, ___rightEyeViewSize_3)); }
	inline UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  get_rightEyeViewSize_3() const { return ___rightEyeViewSize_3; }
	inline UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7 * get_address_of_rightEyeViewSize_3() { return &___rightEyeViewSize_3; }
	inline void set_rightEyeViewSize_3(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7  value)
	{
		___rightEyeViewSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEYEVIEWS_T0D204F2D57DEF889A843F0B3368F35F7A64278EC_H
#ifndef GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#define GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardInputMode
struct  GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED 
{
public:
	// System.Int32 GvrKeyboardInputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrKeyboardInputMode_tE9AC20AB0020925D20C4C6D1FD844766A54068ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINPUTMODE_TE9AC20AB0020925D20C4C6D1FD844766A54068ED_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ANDROIDNATIVEKEYBOARDPROVIDER_T4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0_H
#define ANDROIDNATIVEKEYBOARDPROVIDER_T4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider
struct  AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0  : public RuntimeObject
{
public:
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::renderEventFunction
	intptr_t ___renderEventFunction_0;
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::keyboard_context
	intptr_t ___keyboard_context_9;
	// GvrKeyboardInputMode Gvr.Internal.AndroidNativeKeyboardProvider::mode
	int32_t ___mode_16;
	// System.String Gvr.Internal.AndroidNativeKeyboardProvider::editorText
	String_t* ___editorText_17;
	// UnityEngine.Matrix4x4 Gvr.Internal.AndroidNativeKeyboardProvider::worldMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldMatrix_18;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isValid
	bool ___isValid_19;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isReady
	bool ___isReady_20;

public:
	inline static int32_t get_offset_of_renderEventFunction_0() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___renderEventFunction_0)); }
	inline intptr_t get_renderEventFunction_0() const { return ___renderEventFunction_0; }
	inline intptr_t* get_address_of_renderEventFunction_0() { return &___renderEventFunction_0; }
	inline void set_renderEventFunction_0(intptr_t value)
	{
		___renderEventFunction_0 = value;
	}

	inline static int32_t get_offset_of_keyboard_context_9() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___keyboard_context_9)); }
	inline intptr_t get_keyboard_context_9() const { return ___keyboard_context_9; }
	inline intptr_t* get_address_of_keyboard_context_9() { return &___keyboard_context_9; }
	inline void set_keyboard_context_9(intptr_t value)
	{
		___keyboard_context_9 = value;
	}

	inline static int32_t get_offset_of_mode_16() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___mode_16)); }
	inline int32_t get_mode_16() const { return ___mode_16; }
	inline int32_t* get_address_of_mode_16() { return &___mode_16; }
	inline void set_mode_16(int32_t value)
	{
		___mode_16 = value;
	}

	inline static int32_t get_offset_of_editorText_17() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___editorText_17)); }
	inline String_t* get_editorText_17() const { return ___editorText_17; }
	inline String_t** get_address_of_editorText_17() { return &___editorText_17; }
	inline void set_editorText_17(String_t* value)
	{
		___editorText_17 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_17), value);
	}

	inline static int32_t get_offset_of_worldMatrix_18() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___worldMatrix_18)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldMatrix_18() const { return ___worldMatrix_18; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldMatrix_18() { return &___worldMatrix_18; }
	inline void set_worldMatrix_18(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldMatrix_18 = value;
	}

	inline static int32_t get_offset_of_isValid_19() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___isValid_19)); }
	inline bool get_isValid_19() const { return ___isValid_19; }
	inline bool* get_address_of_isValid_19() { return &___isValid_19; }
	inline void set_isValid_19(bool value)
	{
		___isValid_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDNATIVEKEYBOARDPROVIDER_T4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0_H
#ifndef EMULATORKEYBOARDPROVIDER_T227587127742C438970D2A7AE8C2CB541DEE09B8_H
#define EMULATORKEYBOARDPROVIDER_T227587127742C438970D2A7AE8C2CB541DEE09B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorKeyboardProvider
struct  EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Gvr.Internal.EmulatorKeyboardProvider::stub
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stub_0;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::showing
	bool ___showing_1;
	// GvrKeyboard_KeyboardCallback Gvr.Internal.EmulatorKeyboardProvider::keyboardCallback
	KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45 * ___keyboardCallback_2;
	// System.String Gvr.Internal.EmulatorKeyboardProvider::editorText
	String_t* ___editorText_3;
	// GvrKeyboardInputMode Gvr.Internal.EmulatorKeyboardProvider::mode
	int32_t ___mode_4;
	// UnityEngine.Matrix4x4 Gvr.Internal.EmulatorKeyboardProvider::worldMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldMatrix_5;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::isValid
	bool ___isValid_6;

public:
	inline static int32_t get_offset_of_stub_0() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___stub_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stub_0() const { return ___stub_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stub_0() { return &___stub_0; }
	inline void set_stub_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stub_0 = value;
		Il2CppCodeGenWriteBarrier((&___stub_0), value);
	}

	inline static int32_t get_offset_of_showing_1() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___showing_1)); }
	inline bool get_showing_1() const { return ___showing_1; }
	inline bool* get_address_of_showing_1() { return &___showing_1; }
	inline void set_showing_1(bool value)
	{
		___showing_1 = value;
	}

	inline static int32_t get_offset_of_keyboardCallback_2() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___keyboardCallback_2)); }
	inline KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45 * get_keyboardCallback_2() const { return ___keyboardCallback_2; }
	inline KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45 ** get_address_of_keyboardCallback_2() { return &___keyboardCallback_2; }
	inline void set_keyboardCallback_2(KeyboardCallback_t7116BC2C90CC8642350FAB4C362B8B07F2DAAB45 * value)
	{
		___keyboardCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardCallback_2), value);
	}

	inline static int32_t get_offset_of_editorText_3() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___editorText_3)); }
	inline String_t* get_editorText_3() const { return ___editorText_3; }
	inline String_t** get_address_of_editorText_3() { return &___editorText_3; }
	inline void set_editorText_3(String_t* value)
	{
		___editorText_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_5() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___worldMatrix_5)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldMatrix_5() const { return ___worldMatrix_5; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldMatrix_5() { return &___worldMatrix_5; }
	inline void set_worldMatrix_5(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldMatrix_5 = value;
	}

	inline static int32_t get_offset_of_isValid_6() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8, ___isValid_6)); }
	inline bool get_isValid_6() const { return ___isValid_6; }
	inline bool* get_address_of_isValid_6() { return &___isValid_6; }
	inline void set_isValid_6(bool value)
	{
		___isValid_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORKEYBOARDPROVIDER_T227587127742C438970D2A7AE8C2CB541DEE09B8_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DEMOINPUTMANAGER_T31B96B5B7F0C416768C8634C0B40272ADEB36571_H
#define DEMOINPUTMANAGER_T31B96B5B7F0C416768C8634C0B40272ADEB36571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoInputManager
struct  DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GoogleVR.Demos.DemoInputManager::isDaydream
	bool ___isDaydream_18;
	// System.Int32 GoogleVR.Demos.DemoInputManager::activeControllerPointer
	int32_t ___activeControllerPointer_19;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::controllerMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controllerMain_21;
	// UnityEngine.GameObject[] GoogleVR.Demos.DemoInputManager::controllerPointers
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___controllerPointers_23;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::reticlePointer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___reticlePointer_25;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::messageCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___messageCanvas_27;
	// UnityEngine.UI.Text GoogleVR.Demos.DemoInputManager::messageText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___messageText_28;
	// GoogleVR.Demos.DemoInputManager_EmulatedPlatformType GoogleVR.Demos.DemoInputManager::gvrEmulatedPlatformType
	int32_t ___gvrEmulatedPlatformType_29;

public:
	inline static int32_t get_offset_of_isDaydream_18() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___isDaydream_18)); }
	inline bool get_isDaydream_18() const { return ___isDaydream_18; }
	inline bool* get_address_of_isDaydream_18() { return &___isDaydream_18; }
	inline void set_isDaydream_18(bool value)
	{
		___isDaydream_18 = value;
	}

	inline static int32_t get_offset_of_activeControllerPointer_19() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___activeControllerPointer_19)); }
	inline int32_t get_activeControllerPointer_19() const { return ___activeControllerPointer_19; }
	inline int32_t* get_address_of_activeControllerPointer_19() { return &___activeControllerPointer_19; }
	inline void set_activeControllerPointer_19(int32_t value)
	{
		___activeControllerPointer_19 = value;
	}

	inline static int32_t get_offset_of_controllerMain_21() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___controllerMain_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controllerMain_21() const { return ___controllerMain_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controllerMain_21() { return &___controllerMain_21; }
	inline void set_controllerMain_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controllerMain_21 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMain_21), value);
	}

	inline static int32_t get_offset_of_controllerPointers_23() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___controllerPointers_23)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_controllerPointers_23() const { return ___controllerPointers_23; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_controllerPointers_23() { return &___controllerPointers_23; }
	inline void set_controllerPointers_23(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___controllerPointers_23 = value;
		Il2CppCodeGenWriteBarrier((&___controllerPointers_23), value);
	}

	inline static int32_t get_offset_of_reticlePointer_25() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___reticlePointer_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_reticlePointer_25() const { return ___reticlePointer_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_reticlePointer_25() { return &___reticlePointer_25; }
	inline void set_reticlePointer_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___reticlePointer_25 = value;
		Il2CppCodeGenWriteBarrier((&___reticlePointer_25), value);
	}

	inline static int32_t get_offset_of_messageCanvas_27() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___messageCanvas_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_messageCanvas_27() const { return ___messageCanvas_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_messageCanvas_27() { return &___messageCanvas_27; }
	inline void set_messageCanvas_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___messageCanvas_27 = value;
		Il2CppCodeGenWriteBarrier((&___messageCanvas_27), value);
	}

	inline static int32_t get_offset_of_messageText_28() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___messageText_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_messageText_28() const { return ___messageText_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_messageText_28() { return &___messageText_28; }
	inline void set_messageText_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___messageText_28 = value;
		Il2CppCodeGenWriteBarrier((&___messageText_28), value);
	}

	inline static int32_t get_offset_of_gvrEmulatedPlatformType_29() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571, ___gvrEmulatedPlatformType_29)); }
	inline int32_t get_gvrEmulatedPlatformType_29() const { return ___gvrEmulatedPlatformType_29; }
	inline int32_t* get_address_of_gvrEmulatedPlatformType_29() { return &___gvrEmulatedPlatformType_29; }
	inline void set_gvrEmulatedPlatformType_29(int32_t value)
	{
		___gvrEmulatedPlatformType_29 = value;
	}
};

struct DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields
{
public:
	// GvrControllerHand[] GoogleVR.Demos.DemoInputManager::AllHands
	GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* ___AllHands_20;
	// System.String GoogleVR.Demos.DemoInputManager::CONTROLLER_MAIN_PROP_NAME
	String_t* ___CONTROLLER_MAIN_PROP_NAME_22;
	// System.String GoogleVR.Demos.DemoInputManager::CONTROLLER_POINTER_PROP_NAME
	String_t* ___CONTROLLER_POINTER_PROP_NAME_24;
	// System.String GoogleVR.Demos.DemoInputManager::RETICLE_POINTER_PROP_NAME
	String_t* ___RETICLE_POINTER_PROP_NAME_26;
	// System.String GoogleVR.Demos.DemoInputManager::EMULATED_PLATFORM_PROP_NAME
	String_t* ___EMULATED_PLATFORM_PROP_NAME_30;

public:
	inline static int32_t get_offset_of_AllHands_20() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields, ___AllHands_20)); }
	inline GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* get_AllHands_20() const { return ___AllHands_20; }
	inline GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157** get_address_of_AllHands_20() { return &___AllHands_20; }
	inline void set_AllHands_20(GvrControllerHandU5BU5D_t6728C03092C4927EC8770D8AAB484DA12ECA9157* value)
	{
		___AllHands_20 = value;
		Il2CppCodeGenWriteBarrier((&___AllHands_20), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_MAIN_PROP_NAME_22() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields, ___CONTROLLER_MAIN_PROP_NAME_22)); }
	inline String_t* get_CONTROLLER_MAIN_PROP_NAME_22() const { return ___CONTROLLER_MAIN_PROP_NAME_22; }
	inline String_t** get_address_of_CONTROLLER_MAIN_PROP_NAME_22() { return &___CONTROLLER_MAIN_PROP_NAME_22; }
	inline void set_CONTROLLER_MAIN_PROP_NAME_22(String_t* value)
	{
		___CONTROLLER_MAIN_PROP_NAME_22 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_MAIN_PROP_NAME_22), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_POINTER_PROP_NAME_24() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields, ___CONTROLLER_POINTER_PROP_NAME_24)); }
	inline String_t* get_CONTROLLER_POINTER_PROP_NAME_24() const { return ___CONTROLLER_POINTER_PROP_NAME_24; }
	inline String_t** get_address_of_CONTROLLER_POINTER_PROP_NAME_24() { return &___CONTROLLER_POINTER_PROP_NAME_24; }
	inline void set_CONTROLLER_POINTER_PROP_NAME_24(String_t* value)
	{
		___CONTROLLER_POINTER_PROP_NAME_24 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_POINTER_PROP_NAME_24), value);
	}

	inline static int32_t get_offset_of_RETICLE_POINTER_PROP_NAME_26() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields, ___RETICLE_POINTER_PROP_NAME_26)); }
	inline String_t* get_RETICLE_POINTER_PROP_NAME_26() const { return ___RETICLE_POINTER_PROP_NAME_26; }
	inline String_t** get_address_of_RETICLE_POINTER_PROP_NAME_26() { return &___RETICLE_POINTER_PROP_NAME_26; }
	inline void set_RETICLE_POINTER_PROP_NAME_26(String_t* value)
	{
		___RETICLE_POINTER_PROP_NAME_26 = value;
		Il2CppCodeGenWriteBarrier((&___RETICLE_POINTER_PROP_NAME_26), value);
	}

	inline static int32_t get_offset_of_EMULATED_PLATFORM_PROP_NAME_30() { return static_cast<int32_t>(offsetof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields, ___EMULATED_PLATFORM_PROP_NAME_30)); }
	inline String_t* get_EMULATED_PLATFORM_PROP_NAME_30() const { return ___EMULATED_PLATFORM_PROP_NAME_30; }
	inline String_t** get_address_of_EMULATED_PLATFORM_PROP_NAME_30() { return &___EMULATED_PLATFORM_PROP_NAME_30; }
	inline void set_EMULATED_PLATFORM_PROP_NAME_30(String_t* value)
	{
		___EMULATED_PLATFORM_PROP_NAME_30 = value;
		Il2CppCodeGenWriteBarrier((&___EMULATED_PLATFORM_PROP_NAME_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINPUTMANAGER_T31B96B5B7F0C416768C8634C0B40272ADEB36571_H
#ifndef DEMOSCENEMANAGER_T8E4E210E9BE8D289F8497474E522ADBE8E374899_H
#define DEMOSCENEMANAGER_T8E4E210E9BE8D289F8497474E522ADBE8E374899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoSceneManager
struct  DemoSceneManager_t8E4E210E9BE8D289F8497474E522ADBE8E374899  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T8E4E210E9BE8D289F8497474E522ADBE8E374899_H
#ifndef HEADSETDEMOMANAGER_TC4A3940686081E95109A6ED574942B1E352344D1_H
#define HEADSETDEMOMANAGER_TC4A3940686081E95109A6ED574942B1E352344D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HeadsetDemoManager
struct  HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GoogleVR.HelloVR.HeadsetDemoManager::safetyRing
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___safetyRing_4;
	// System.Boolean GoogleVR.HelloVR.HeadsetDemoManager::enableDebugLog
	bool ___enableDebugLog_5;
	// UnityEngine.WaitForSeconds GoogleVR.HelloVR.HeadsetDemoManager::waitFourSeconds
	WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * ___waitFourSeconds_6;

public:
	inline static int32_t get_offset_of_safetyRing_4() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1, ___safetyRing_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_safetyRing_4() const { return ___safetyRing_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_safetyRing_4() { return &___safetyRing_4; }
	inline void set_safetyRing_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___safetyRing_4 = value;
		Il2CppCodeGenWriteBarrier((&___safetyRing_4), value);
	}

	inline static int32_t get_offset_of_enableDebugLog_5() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1, ___enableDebugLog_5)); }
	inline bool get_enableDebugLog_5() const { return ___enableDebugLog_5; }
	inline bool* get_address_of_enableDebugLog_5() { return &___enableDebugLog_5; }
	inline void set_enableDebugLog_5(bool value)
	{
		___enableDebugLog_5 = value;
	}

	inline static int32_t get_offset_of_waitFourSeconds_6() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1, ___waitFourSeconds_6)); }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * get_waitFourSeconds_6() const { return ___waitFourSeconds_6; }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 ** get_address_of_waitFourSeconds_6() { return &___waitFourSeconds_6; }
	inline void set_waitFourSeconds_6(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * value)
	{
		___waitFourSeconds_6 = value;
		Il2CppCodeGenWriteBarrier((&___waitFourSeconds_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETDEMOMANAGER_TC4A3940686081E95109A6ED574942B1E352344D1_H
#ifndef HELLOVRMANAGER_T335D27EA4271D1281DAC2116CD665485721CC25A_H
#define HELLOVRMANAGER_T335D27EA4271D1281DAC2116CD665485721CC25A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HelloVRManager
struct  HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GoogleVR.HelloVR.HelloVRManager::m_launchVrHomeButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_launchVrHomeButton_4;
	// GoogleVR.Demos.DemoInputManager GoogleVR.HelloVR.HelloVRManager::m_demoInputManager
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571 * ___m_demoInputManager_5;

public:
	inline static int32_t get_offset_of_m_launchVrHomeButton_4() { return static_cast<int32_t>(offsetof(HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A, ___m_launchVrHomeButton_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_launchVrHomeButton_4() const { return ___m_launchVrHomeButton_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_launchVrHomeButton_4() { return &___m_launchVrHomeButton_4; }
	inline void set_m_launchVrHomeButton_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_launchVrHomeButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_launchVrHomeButton_4), value);
	}

	inline static int32_t get_offset_of_m_demoInputManager_5() { return static_cast<int32_t>(offsetof(HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A, ___m_demoInputManager_5)); }
	inline DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571 * get_m_demoInputManager_5() const { return ___m_demoInputManager_5; }
	inline DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571 ** get_address_of_m_demoInputManager_5() { return &___m_demoInputManager_5; }
	inline void set_m_demoInputManager_5(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571 * value)
	{
		___m_demoInputManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_demoInputManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELLOVRMANAGER_T335D27EA4271D1281DAC2116CD665485721CC25A_H
#ifndef OBJECTCONTROLLER_TDC3585FEC676081794820D1FEC5819B34C18BCAC_H
#define OBJECTCONTROLLER_TDC3585FEC676081794820D1FEC5819B34C18BCAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.ObjectController
struct  ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 GoogleVR.HelloVR.ObjectController::startingPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startingPosition_4;
	// UnityEngine.Renderer GoogleVR.HelloVR.ObjectController::myRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___myRenderer_5;
	// UnityEngine.Material GoogleVR.HelloVR.ObjectController::inactiveMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___inactiveMaterial_6;
	// UnityEngine.Material GoogleVR.HelloVR.ObjectController::gazedAtMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___gazedAtMaterial_7;

public:
	inline static int32_t get_offset_of_startingPosition_4() { return static_cast<int32_t>(offsetof(ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC, ___startingPosition_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startingPosition_4() const { return ___startingPosition_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startingPosition_4() { return &___startingPosition_4; }
	inline void set_startingPosition_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startingPosition_4 = value;
	}

	inline static int32_t get_offset_of_myRenderer_5() { return static_cast<int32_t>(offsetof(ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC, ___myRenderer_5)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_myRenderer_5() const { return ___myRenderer_5; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_myRenderer_5() { return &___myRenderer_5; }
	inline void set_myRenderer_5(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___myRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___myRenderer_5), value);
	}

	inline static int32_t get_offset_of_inactiveMaterial_6() { return static_cast<int32_t>(offsetof(ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC, ___inactiveMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_inactiveMaterial_6() const { return ___inactiveMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_inactiveMaterial_6() { return &___inactiveMaterial_6; }
	inline void set_inactiveMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___inactiveMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___inactiveMaterial_6), value);
	}

	inline static int32_t get_offset_of_gazedAtMaterial_7() { return static_cast<int32_t>(offsetof(ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC, ___gazedAtMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_gazedAtMaterial_7() const { return ___gazedAtMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_gazedAtMaterial_7() { return &___gazedAtMaterial_7; }
	inline void set_gazedAtMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___gazedAtMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___gazedAtMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCONTROLLER_TDC3585FEC676081794820D1FEC5819B34C18BCAC_H
#ifndef APPBUTTONINPUT_T84B81B8FB9381ED9FC37A9A005236E45BCC2FF57_H
#define APPBUTTONINPUT_T84B81B8FB9381ED9FC37A9A005236E45BCC2FF57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.AppButtonInput
struct  AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GoogleVR.VideoDemo.ButtonEvent GoogleVR.VideoDemo.AppButtonInput::OnAppUp
	ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * ___OnAppUp_4;
	// GoogleVR.VideoDemo.ButtonEvent GoogleVR.VideoDemo.AppButtonInput::OnAppDown
	ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * ___OnAppDown_5;

public:
	inline static int32_t get_offset_of_OnAppUp_4() { return static_cast<int32_t>(offsetof(AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57, ___OnAppUp_4)); }
	inline ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * get_OnAppUp_4() const { return ___OnAppUp_4; }
	inline ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A ** get_address_of_OnAppUp_4() { return &___OnAppUp_4; }
	inline void set_OnAppUp_4(ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * value)
	{
		___OnAppUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAppUp_4), value);
	}

	inline static int32_t get_offset_of_OnAppDown_5() { return static_cast<int32_t>(offsetof(AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57, ___OnAppDown_5)); }
	inline ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * get_OnAppDown_5() const { return ___OnAppDown_5; }
	inline ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A ** get_address_of_OnAppDown_5() { return &___OnAppDown_5; }
	inline void set_OnAppDown_5(ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A * value)
	{
		___OnAppDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAppDown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBUTTONINPUT_T84B81B8FB9381ED9FC37A9A005236E45BCC2FF57_H
#ifndef AUTOPLAYVIDEO_TB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A_H
#define AUTOPLAYVIDEO_TB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.AutoPlayVideo
struct  AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GoogleVR.VideoDemo.AutoPlayVideo::done
	bool ___done_4;
	// System.Single GoogleVR.VideoDemo.AutoPlayVideo::t
	float ___t_5;
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.AutoPlayVideo::player
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * ___player_6;
	// System.Single GoogleVR.VideoDemo.AutoPlayVideo::delay
	float ___delay_7;
	// System.Boolean GoogleVR.VideoDemo.AutoPlayVideo::loop
	bool ___loop_8;

public:
	inline static int32_t get_offset_of_done_4() { return static_cast<int32_t>(offsetof(AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A, ___done_4)); }
	inline bool get_done_4() const { return ___done_4; }
	inline bool* get_address_of_done_4() { return &___done_4; }
	inline void set_done_4(bool value)
	{
		___done_4 = value;
	}

	inline static int32_t get_offset_of_t_5() { return static_cast<int32_t>(offsetof(AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A, ___t_5)); }
	inline float get_t_5() const { return ___t_5; }
	inline float* get_address_of_t_5() { return &___t_5; }
	inline void set_t_5(float value)
	{
		___t_5 = value;
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A, ___player_6)); }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * get_player_6() const { return ___player_6; }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A, ___loop_8)); }
	inline bool get_loop_8() const { return ___loop_8; }
	inline bool* get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(bool value)
	{
		___loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAYVIDEO_TB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A_H
#ifndef MENUHANDLER_T3397DC2249CA7401D5B6A9B9DA140BA584218010_H
#define MENUHANDLER_T3397DC2249CA7401D5B6A9B9DA140BA584218010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler
struct  MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] GoogleVR.VideoDemo.MenuHandler::menuObjects
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___menuObjects_4;

public:
	inline static int32_t get_offset_of_menuObjects_4() { return static_cast<int32_t>(offsetof(MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010, ___menuObjects_4)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_menuObjects_4() const { return ___menuObjects_4; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_menuObjects_4() { return &___menuObjects_4; }
	inline void set_menuObjects_4(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___menuObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuObjects_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUHANDLER_T3397DC2249CA7401D5B6A9B9DA140BA584218010_H
#ifndef POSITIONSWAPPER_TD565EE8AE9BB936BA8785A1C09FDE37CC27A5441_H
#define POSITIONSWAPPER_TD565EE8AE9BB936BA8785A1C09FDE37CC27A5441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.PositionSwapper
struct  PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GoogleVR.VideoDemo.PositionSwapper::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.Vector3[] GoogleVR.VideoDemo.PositionSwapper::Positions
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Positions_5;

public:
	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_Positions_5() { return static_cast<int32_t>(offsetof(PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441, ___Positions_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Positions_5() const { return ___Positions_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Positions_5() { return &___Positions_5; }
	inline void set_Positions_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Positions_5 = value;
		Il2CppCodeGenWriteBarrier((&___Positions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONSWAPPER_TD565EE8AE9BB936BA8785A1C09FDE37CC27A5441_H
#ifndef SCRUBBEREVENTS_T6A04020D9CD24317601D5E7EC633FF5CE859D99A_H
#define SCRUBBEREVENTS_T6A04020D9CD24317601D5E7EC633FF5CE859D99A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ScrubberEvents
struct  ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.ScrubberEvents::newPositionHandle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___newPositionHandle_4;
	// UnityEngine.Vector3[] GoogleVR.VideoDemo.ScrubberEvents::corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___corners_5;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.ScrubberEvents::slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___slider_6;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.ScrubberEvents::mgr
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * ___mgr_7;

public:
	inline static int32_t get_offset_of_newPositionHandle_4() { return static_cast<int32_t>(offsetof(ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A, ___newPositionHandle_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_newPositionHandle_4() const { return ___newPositionHandle_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_newPositionHandle_4() { return &___newPositionHandle_4; }
	inline void set_newPositionHandle_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___newPositionHandle_4 = value;
		Il2CppCodeGenWriteBarrier((&___newPositionHandle_4), value);
	}

	inline static int32_t get_offset_of_corners_5() { return static_cast<int32_t>(offsetof(ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A, ___corners_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_corners_5() const { return ___corners_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_corners_5() { return &___corners_5; }
	inline void set_corners_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___corners_5 = value;
		Il2CppCodeGenWriteBarrier((&___corners_5), value);
	}

	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A, ___slider_6)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_slider_6() const { return ___slider_6; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}

	inline static int32_t get_offset_of_mgr_7() { return static_cast<int32_t>(offsetof(ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A, ___mgr_7)); }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * get_mgr_7() const { return ___mgr_7; }
	inline VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 ** get_address_of_mgr_7() { return &___mgr_7; }
	inline void set_mgr_7(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1 * value)
	{
		___mgr_7 = value;
		Il2CppCodeGenWriteBarrier((&___mgr_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRUBBEREVENTS_T6A04020D9CD24317601D5E7EC633FF5CE859D99A_H
#ifndef SWITCHVIDEOS_T0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B_H
#define SWITCHVIDEOS_T0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.SwitchVideos
struct  SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::localVideoSample
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___localVideoSample_4;
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::dashVideoSample
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___dashVideoSample_5;
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::panoVideoSample
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___panoVideoSample_6;
	// UnityEngine.GameObject[] GoogleVR.VideoDemo.SwitchVideos::videoSamples
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___videoSamples_7;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.SwitchVideos::missingLibText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___missingLibText_8;

public:
	inline static int32_t get_offset_of_localVideoSample_4() { return static_cast<int32_t>(offsetof(SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B, ___localVideoSample_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_localVideoSample_4() const { return ___localVideoSample_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_localVideoSample_4() { return &___localVideoSample_4; }
	inline void set_localVideoSample_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___localVideoSample_4 = value;
		Il2CppCodeGenWriteBarrier((&___localVideoSample_4), value);
	}

	inline static int32_t get_offset_of_dashVideoSample_5() { return static_cast<int32_t>(offsetof(SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B, ___dashVideoSample_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_dashVideoSample_5() const { return ___dashVideoSample_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_dashVideoSample_5() { return &___dashVideoSample_5; }
	inline void set_dashVideoSample_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___dashVideoSample_5 = value;
		Il2CppCodeGenWriteBarrier((&___dashVideoSample_5), value);
	}

	inline static int32_t get_offset_of_panoVideoSample_6() { return static_cast<int32_t>(offsetof(SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B, ___panoVideoSample_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_panoVideoSample_6() const { return ___panoVideoSample_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_panoVideoSample_6() { return &___panoVideoSample_6; }
	inline void set_panoVideoSample_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___panoVideoSample_6 = value;
		Il2CppCodeGenWriteBarrier((&___panoVideoSample_6), value);
	}

	inline static int32_t get_offset_of_videoSamples_7() { return static_cast<int32_t>(offsetof(SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B, ___videoSamples_7)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_videoSamples_7() const { return ___videoSamples_7; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_videoSamples_7() { return &___videoSamples_7; }
	inline void set_videoSamples_7(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___videoSamples_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoSamples_7), value);
	}

	inline static int32_t get_offset_of_missingLibText_8() { return static_cast<int32_t>(offsetof(SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B, ___missingLibText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_missingLibText_8() const { return ___missingLibText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_missingLibText_8() { return &___missingLibText_8; }
	inline void set_missingLibText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___missingLibText_8 = value;
		Il2CppCodeGenWriteBarrier((&___missingLibText_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVIDEOS_T0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B_H
#ifndef TOGGLEACTION_T98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B_H
#define TOGGLEACTION_T98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ToggleAction
struct  ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single GoogleVR.VideoDemo.ToggleAction::lastUsage
	float ___lastUsage_4;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::on
	bool ___on_5;
	// UnityEngine.Events.UnityEvent GoogleVR.VideoDemo.ToggleAction::OnToggleOn
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnToggleOn_6;
	// UnityEngine.Events.UnityEvent GoogleVR.VideoDemo.ToggleAction::OnToggleOff
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnToggleOff_7;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::InitialState
	bool ___InitialState_8;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::RaiseEventForInitialState
	bool ___RaiseEventForInitialState_9;
	// System.Single GoogleVR.VideoDemo.ToggleAction::Cooldown
	float ___Cooldown_10;

public:
	inline static int32_t get_offset_of_lastUsage_4() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___lastUsage_4)); }
	inline float get_lastUsage_4() const { return ___lastUsage_4; }
	inline float* get_address_of_lastUsage_4() { return &___lastUsage_4; }
	inline void set_lastUsage_4(float value)
	{
		___lastUsage_4 = value;
	}

	inline static int32_t get_offset_of_on_5() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___on_5)); }
	inline bool get_on_5() const { return ___on_5; }
	inline bool* get_address_of_on_5() { return &___on_5; }
	inline void set_on_5(bool value)
	{
		___on_5 = value;
	}

	inline static int32_t get_offset_of_OnToggleOn_6() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___OnToggleOn_6)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnToggleOn_6() const { return ___OnToggleOn_6; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnToggleOn_6() { return &___OnToggleOn_6; }
	inline void set_OnToggleOn_6(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnToggleOn_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOn_6), value);
	}

	inline static int32_t get_offset_of_OnToggleOff_7() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___OnToggleOff_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnToggleOff_7() const { return ___OnToggleOff_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnToggleOff_7() { return &___OnToggleOff_7; }
	inline void set_OnToggleOff_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnToggleOff_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOff_7), value);
	}

	inline static int32_t get_offset_of_InitialState_8() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___InitialState_8)); }
	inline bool get_InitialState_8() const { return ___InitialState_8; }
	inline bool* get_address_of_InitialState_8() { return &___InitialState_8; }
	inline void set_InitialState_8(bool value)
	{
		___InitialState_8 = value;
	}

	inline static int32_t get_offset_of_RaiseEventForInitialState_9() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___RaiseEventForInitialState_9)); }
	inline bool get_RaiseEventForInitialState_9() const { return ___RaiseEventForInitialState_9; }
	inline bool* get_address_of_RaiseEventForInitialState_9() { return &___RaiseEventForInitialState_9; }
	inline void set_RaiseEventForInitialState_9(bool value)
	{
		___RaiseEventForInitialState_9 = value;
	}

	inline static int32_t get_offset_of_Cooldown_10() { return static_cast<int32_t>(offsetof(ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B, ___Cooldown_10)); }
	inline float get_Cooldown_10() const { return ___Cooldown_10; }
	inline float* get_address_of_Cooldown_10() { return &___Cooldown_10; }
	inline void set_Cooldown_10(float value)
	{
		___Cooldown_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEACTION_T98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B_H
#ifndef VIDEOCONTROLSMANAGER_T4CAD8096EA8D61B31B14A255284959B84010D8D1_H
#define VIDEOCONTROLSMANAGER_T4CAD8096EA8D61B31B14A255284959B84010D8D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager
struct  VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::pauseSprite
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseSprite_4;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::playSprite
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___playSprite_5;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.VideoControlsManager::videoScrubber
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___videoScrubber_6;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.VideoControlsManager::volumeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___volumeSlider_7;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::volumeWidget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___volumeWidget_8;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::settingsPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___settingsPanel_9;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::bufferedBackground
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bufferedBackground_10;
	// UnityEngine.Vector3 GoogleVR.VideoDemo.VideoControlsManager::basePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___basePosition_11;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.VideoControlsManager::videoPosition
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoPosition_12;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.VideoControlsManager::videoDuration
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___videoDuration_13;
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.VideoControlsManager::<Player>k__BackingField
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * ___U3CPlayerU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_pauseSprite_4() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___pauseSprite_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseSprite_4() const { return ___pauseSprite_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseSprite_4() { return &___pauseSprite_4; }
	inline void set_pauseSprite_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___pauseSprite_4), value);
	}

	inline static int32_t get_offset_of_playSprite_5() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___playSprite_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_playSprite_5() const { return ___playSprite_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_playSprite_5() { return &___playSprite_5; }
	inline void set_playSprite_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___playSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___playSprite_5), value);
	}

	inline static int32_t get_offset_of_videoScrubber_6() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___videoScrubber_6)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_videoScrubber_6() const { return ___videoScrubber_6; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_videoScrubber_6() { return &___videoScrubber_6; }
	inline void set_videoScrubber_6(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___videoScrubber_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoScrubber_6), value);
	}

	inline static int32_t get_offset_of_volumeSlider_7() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___volumeSlider_7)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_volumeSlider_7() const { return ___volumeSlider_7; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_volumeSlider_7() { return &___volumeSlider_7; }
	inline void set_volumeSlider_7(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___volumeSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___volumeSlider_7), value);
	}

	inline static int32_t get_offset_of_volumeWidget_8() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___volumeWidget_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_volumeWidget_8() const { return ___volumeWidget_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_volumeWidget_8() { return &___volumeWidget_8; }
	inline void set_volumeWidget_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___volumeWidget_8 = value;
		Il2CppCodeGenWriteBarrier((&___volumeWidget_8), value);
	}

	inline static int32_t get_offset_of_settingsPanel_9() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___settingsPanel_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_settingsPanel_9() const { return ___settingsPanel_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_settingsPanel_9() { return &___settingsPanel_9; }
	inline void set_settingsPanel_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___settingsPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___settingsPanel_9), value);
	}

	inline static int32_t get_offset_of_bufferedBackground_10() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___bufferedBackground_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bufferedBackground_10() const { return ___bufferedBackground_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bufferedBackground_10() { return &___bufferedBackground_10; }
	inline void set_bufferedBackground_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bufferedBackground_10 = value;
		Il2CppCodeGenWriteBarrier((&___bufferedBackground_10), value);
	}

	inline static int32_t get_offset_of_basePosition_11() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___basePosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_basePosition_11() const { return ___basePosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_basePosition_11() { return &___basePosition_11; }
	inline void set_basePosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___basePosition_11 = value;
	}

	inline static int32_t get_offset_of_videoPosition_12() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___videoPosition_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoPosition_12() const { return ___videoPosition_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoPosition_12() { return &___videoPosition_12; }
	inline void set_videoPosition_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoPosition_12 = value;
		Il2CppCodeGenWriteBarrier((&___videoPosition_12), value);
	}

	inline static int32_t get_offset_of_videoDuration_13() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___videoDuration_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_videoDuration_13() const { return ___videoDuration_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_videoDuration_13() { return &___videoDuration_13; }
	inline void set_videoDuration_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___videoDuration_13 = value;
		Il2CppCodeGenWriteBarrier((&___videoDuration_13), value);
	}

	inline static int32_t get_offset_of_U3CPlayerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1, ___U3CPlayerU3Ek__BackingField_14)); }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * get_U3CPlayerU3Ek__BackingField_14() const { return ___U3CPlayerU3Ek__BackingField_14; }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 ** get_address_of_U3CPlayerU3Ek__BackingField_14() { return &___U3CPlayerU3Ek__BackingField_14; }
	inline void set_U3CPlayerU3Ek__BackingField_14(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * value)
	{
		___U3CPlayerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCONTROLSMANAGER_T4CAD8096EA8D61B31B14A255284959B84010D8D1_H
#ifndef VIDEOPLAYERREFERENCE_T4CC9246223F70EFDF8E65CEF6D501FB4C1018876_H
#define VIDEOPLAYERREFERENCE_T4CC9246223F70EFDF8E65CEF6D501FB4C1018876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoPlayerReference
struct  VideoPlayerReference_t4CC9246223F70EFDF8E65CEF6D501FB4C1018876  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.VideoPlayerReference::player
	GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * ___player_4;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(VideoPlayerReference_t4CC9246223F70EFDF8E65CEF6D501FB4C1018876, ___player_4)); }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * get_player_4() const { return ___player_4; }
	inline GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GvrVideoPlayerTexture_tA7F76F5D6DABCFBBA3A2AE3A467442E17EFD8231 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYERREFERENCE_T4CC9246223F70EFDF8E65CEF6D501FB4C1018876_H
#ifndef GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#define GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A_H
#ifndef KEYBOARDDELEGATEEXAMPLE_T7A1234FD814AB88256673D69E9C7D47E0B18B57F_H
#define KEYBOARDDELEGATEEXAMPLE_T7A1234FD814AB88256673D69E9C7D47E0B18B57F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.KeyboardDemo.KeyboardDelegateExample
struct  KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F  : public GvrKeyboardDelegateBase_t8E4BEBA1A582DEEDF7D1D3B5D841F860659C8E8A
{
public:
	// UnityEngine.UI.Text GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___KeyboardText_4;
	// UnityEngine.Canvas GoogleVR.KeyboardDemo.KeyboardDelegateExample::UpdateCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___UpdateCanvas_5;
	// System.EventHandler GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardHidden
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___KeyboardHidden_6;
	// System.EventHandler GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardShown
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___KeyboardShown_7;

public:
	inline static int32_t get_offset_of_KeyboardText_4() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F, ___KeyboardText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_KeyboardText_4() const { return ___KeyboardText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_KeyboardText_4() { return &___KeyboardText_4; }
	inline void set_KeyboardText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___KeyboardText_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardText_4), value);
	}

	inline static int32_t get_offset_of_UpdateCanvas_5() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F, ___UpdateCanvas_5)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_UpdateCanvas_5() const { return ___UpdateCanvas_5; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_UpdateCanvas_5() { return &___UpdateCanvas_5; }
	inline void set_UpdateCanvas_5(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___UpdateCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateCanvas_5), value);
	}

	inline static int32_t get_offset_of_KeyboardHidden_6() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F, ___KeyboardHidden_6)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_KeyboardHidden_6() const { return ___KeyboardHidden_6; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_KeyboardHidden_6() { return &___KeyboardHidden_6; }
	inline void set_KeyboardHidden_6(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___KeyboardHidden_6 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardHidden_6), value);
	}

	inline static int32_t get_offset_of_KeyboardShown_7() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F, ___KeyboardShown_7)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_KeyboardShown_7() const { return ___KeyboardShown_7; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_KeyboardShown_7() { return &___KeyboardShown_7; }
	inline void set_KeyboardShown_7(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___KeyboardShown_7 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardShown_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDDELEGATEEXAMPLE_T7A1234FD814AB88256673D69E9C7D47E0B18B57F_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2800[5] = 
{
	MultisampleCounts_tB8A1F86C6AC13D5AAFD0876EA5B0B79B19154361::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[7] = 
{
	BitRates_tB6E00D0A6E1278FCD21D1A3D9584E9B9108CE3B7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7)+ sizeof (RuntimeObject), sizeof(UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2802[4] = 
{
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7::get_offset_of_right_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7::get_offset_of_left_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7::get_offset_of_top_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t1D1984F2F4787C31C75CAD7ED679216C5B4FCDA7::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC)+ sizeof (RuntimeObject), sizeof(UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC ), 0, 0 };
extern const int32_t g_FieldOffsetTable2803[4] = 
{
	UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC::get_offset_of_leftEyePose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC::get_offset_of_rightEyePose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC::get_offset_of_leftEyeViewSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t0D204F2D57DEF889A843F0B3368F35F7A64278EC::get_offset_of_rightEyeViewSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (KeyboardProviderFactory_t2260D5A8F515013574E200073B95229F0F798E2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[21] = 
{
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_renderEventFunction_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_keyboard_context_9(),
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_mode_16(),
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_editorText_17(),
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_worldMatrix_18(),
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_isValid_19(),
	AndroidNativeKeyboardProvider_t4641A232A4F7B1480B6A9D8A9B8E3282BB9F6BA0::get_offset_of_isReady_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (gvr_clock_time_point_tBCB5294A08D28AD8465B28C34010F8F281846D3D)+ sizeof (RuntimeObject), sizeof(gvr_clock_time_point_tBCB5294A08D28AD8465B28C34010F8F281846D3D ), 0, 0 };
extern const int32_t g_FieldOffsetTable2807[1] = 
{
	gvr_clock_time_point_tBCB5294A08D28AD8465B28C34010F8F281846D3D::get_offset_of_monotonic_system_time_nanos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035)+ sizeof (RuntimeObject), sizeof(gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2808[4] = 
{
	gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035::get_offset_of_left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035::get_offset_of_right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035::get_offset_of_bottom_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t5F7F0591CE0D13FFEF134C886F9ECA35B341C035::get_offset_of_top_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[2] = 
{
	DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B::get_offset_of_dummyState_0(),
	DummyKeyboardProvider_t950A5DD732832E0D14A0AA89B0A15AE645F7023B::get_offset_of_U3CEditorTextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[7] = 
{
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_stub_0(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_showing_1(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_keyboardCallback_2(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_editorText_3(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_mode_4(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_worldMatrix_5(),
	EmulatorKeyboardProvider_t227587127742C438970D2A7AE8C2CB541DEE09B8::get_offset_of_isValid_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B), -1, sizeof(GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2811[2] = 
{
	GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields::get_offset_of_cachedHeadEmulationActive_0(),
	GvrCursorHelper_t8B940F59FA02670945160E04E66B62614FF9DD7B_StaticFields::get_offset_of_cachedControllerEmulationActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[2] = 
{
	AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57::get_offset_of_OnAppUp_4(),
	AppButtonInput_t84B81B8FB9381ED9FC37A9A005236E45BCC2FF57::get_offset_of_OnAppDown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[5] = 
{
	AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A::get_offset_of_done_4(),
	AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A::get_offset_of_t_5(),
	AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A::get_offset_of_player_6(),
	AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A::get_offset_of_delay_7(),
	AutoPlayVideo_tB18AA40474E8F2CD09B6102D0AF191D2D08A0B3A::get_offset_of_loop_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (Vector3Event_tA27A82D5475BF7D6032A8D42C55D74658C91C253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (Vector2Event_t05C88DC2526402EFF784CB9D111BBFDAC99EEA23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (FloatEvent_t2530B0F15127DD0D34E564E894964A97ACEFC04B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (BoolEvent_t8D8A85D2328338C6A5406C80278BB5C6CA3D4D7D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (ButtonEvent_t2B1EED42EEAC841B77E2136C2ECE9E9CC593088A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (TouchPadEvent_t26A136076D38E0702174A5F1A15E380FFCF601ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (TransformEvent_t58048A882E00659D70E91039823C69CDEB673A1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (GameObjectEvent_t76A1A2480DF4CE09119627E426013592A3D9CDCD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[1] = 
{
	MenuHandler_t3397DC2249CA7401D5B6A9B9DA140BA584218010::get_offset_of_menuObjects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[4] = 
{
	U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419::get_offset_of_U3CU3E1__state_0(),
	U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419::get_offset_of_U3CU3E2__current_1(),
	U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419::get_offset_of_U3CU3E4__this_2(),
	U3CDoAppearU3Ed__3_t6B532654E07C7B430E4AE55DB88B37856F5F7419::get_offset_of_U3CcgU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B::get_offset_of_U3CU3E1__state_0(),
	U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B::get_offset_of_U3CU3E2__current_1(),
	U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B::get_offset_of_U3CU3E4__this_2(),
	U3CDoFadeU3Ed__4_t06CBEC19196B912BA6EF0F39A682572BD1D2029B::get_offset_of_U3CcgU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[2] = 
{
	PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441::get_offset_of_currentIndex_4(),
	PositionSwapper_tD565EE8AE9BB936BA8785A1C09FDE37CC27A5441::get_offset_of_Positions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[4] = 
{
	ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A::get_offset_of_newPositionHandle_4(),
	ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A::get_offset_of_corners_5(),
	ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A::get_offset_of_slider_6(),
	ScrubberEvents_t6A04020D9CD24317601D5E7EC633FF5CE859D99A::get_offset_of_mgr_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[5] = 
{
	SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B::get_offset_of_localVideoSample_4(),
	SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B::get_offset_of_dashVideoSample_5(),
	SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B::get_offset_of_panoVideoSample_6(),
	SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B::get_offset_of_videoSamples_7(),
	SwitchVideos_t0B7EF239D0C2291FF411EF6C1C2CA2FE178A351B::get_offset_of_missingLibText_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[7] = 
{
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_lastUsage_4(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_on_5(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_OnToggleOn_6(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_OnToggleOff_7(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_InitialState_8(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_RaiseEventForInitialState_9(),
	ToggleAction_t98A52B25FB33DD7F47C772EEAAEC7153D59E7B0B::get_offset_of_Cooldown_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[11] = 
{
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_pauseSprite_4(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_playSprite_5(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_videoScrubber_6(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_volumeSlider_7(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_volumeWidget_8(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_settingsPanel_9(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_bufferedBackground_10(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_basePosition_11(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_videoPosition_12(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_videoDuration_13(),
	VideoControlsManager_t4CAD8096EA8D61B31B14A255284959B84010D8D1::get_offset_of_U3CPlayerU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[4] = 
{
	U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65::get_offset_of_U3CU3E1__state_0(),
	U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65::get_offset_of_U3CU3E2__current_1(),
	U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65::get_offset_of_U3CU3E4__this_2(),
	U3CDoAppearU3Ed__25_tC1C7BB15A2F066266C90A3D34BB77A4DCFC9BF65::get_offset_of_U3CcgU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[4] = 
{
	U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336::get_offset_of_U3CU3E1__state_0(),
	U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336::get_offset_of_U3CU3E2__current_1(),
	U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336::get_offset_of_U3CU3E4__this_2(),
	U3CDoFadeU3Ed__26_t90D135FFAD6D45A84F0F712BBC40EBB9FE047336::get_offset_of_U3CcgU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (VideoPlayerReference_t4CC9246223F70EFDF8E65CEF6D501FB4C1018876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[1] = 
{
	VideoPlayerReference_t4CC9246223F70EFDF8E65CEF6D501FB4C1018876::get_offset_of_player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[5] = 
{
	KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F::get_offset_of_KeyboardText_4(),
	KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F::get_offset_of_UpdateCanvas_5(),
	KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F::get_offset_of_KeyboardHidden_6(),
	KeyboardDelegateExample_t7A1234FD814AB88256673D69E9C7D47E0B18B57F::get_offset_of_KeyboardShown_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[3] = 
{
	HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1::get_offset_of_safetyRing_4(),
	HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1::get_offset_of_enableDebugLog_5(),
	HeadsetDemoManager_tC4A3940686081E95109A6ED574942B1E352344D1::get_offset_of_waitFourSeconds_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[3] = 
{
	U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA::get_offset_of_U3CU3E1__state_0(),
	U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA::get_offset_of_U3CU3E2__current_1(),
	U3CStatusUpdateLoopU3Ed__13_tB7473C465F84951D238D0BC0B79D2EAD7F4F5AFA::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[2] = 
{
	HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A::get_offset_of_m_launchVrHomeButton_4(),
	HelloVRManager_t335D27EA4271D1281DAC2116CD665485721CC25A::get_offset_of_m_demoInputManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[4] = 
{
	ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC::get_offset_of_startingPosition_4(),
	ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC::get_offset_of_myRenderer_5(),
	ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC::get_offset_of_inactiveMaterial_6(),
	ObjectController_tDC3585FEC676081794820D1FEC5819B34C18BCAC::get_offset_of_gazedAtMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571), -1, sizeof(DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2838[27] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_isDaydream_18(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_activeControllerPointer_19(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields::get_offset_of_AllHands_20(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_controllerMain_21(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields::get_offset_of_CONTROLLER_MAIN_PROP_NAME_22(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_controllerPointers_23(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields::get_offset_of_CONTROLLER_POINTER_PROP_NAME_24(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_reticlePointer_25(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields::get_offset_of_RETICLE_POINTER_PROP_NAME_26(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_messageCanvas_27(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_messageText_28(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571::get_offset_of_gvrEmulatedPlatformType_29(),
	DemoInputManager_t31B96B5B7F0C416768C8634C0B40272ADEB36571_StaticFields::get_offset_of_EMULATED_PLATFORM_PROP_NAME_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (EmulatedPlatformType_t0EE14329F304A519675FDF6F32C6A7B4F41B7CD0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[3] = 
{
	EmulatedPlatformType_t0EE14329F304A519675FDF6F32C6A7B4F41B7CD0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5), -1, sizeof(U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2840[3] = 
{
	U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
	U3CU3Ec_t18935990A85C9535562F448C7009B80FBBE6C0E5_StaticFields::get_offset_of_U3CU3E9__32_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (DemoSceneManager_t8E4E210E9BE8D289F8497474E522ADBE8E374899), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2842[6] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U316E2B412E9C2B8E31B780DE46254349320CCAAA0_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U325B4B83D2A43393F4E18624598DDA694217A6622_1(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U3311441405B64B3EA9097AC8E07F3274962EC6BB4_2(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_3(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_4(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_FADC743710841EB901D5F6FBC97F555D4BD94310_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t65235398342E0C6C387633EE4B2E7F326A539C91 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D16_t6EB025C40E80FDD74132718092D59E92446BD13D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D20_tD65589242911778C66D1E5AC9009597568746382 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_tD2672E72DE7681E58346994836A2016B555BF4C1 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
