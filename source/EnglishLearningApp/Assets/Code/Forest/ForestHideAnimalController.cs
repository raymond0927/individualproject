﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestHideAnimalController : MonoBehaviour
{

    public static  GameObject obj_Bear;//	Bear
    public static  GameObject obj_Cattle;//	Cattle
    public static  GameObject obj_Deer;//	Deer
    public static  GameObject obj_Ibex;//	Ibex
    public static  GameObject obj_Wild_boar;//	Wild boar
    public static  GameObject obj_Wolf;//	Wolf
    public static  GameObject obj_Rabbit;//	Rabbit
    public static  GameObject obj_Crocodile;//	Crocodile
    public static  GameObject obj_Lion;//	Lion
    public static  GameObject obj_Tiger;//	Tiger
    public static GameObject obj_Elephant;//	Elephant

    // Start is called before the first frame update
    void Start()
    {
        obj_Bear = GameObject.Find("BearLocation");
        obj_Cattle = GameObject.Find("CattleLocation");
        obj_Deer = GameObject.Find("DeerLocation");
        obj_Ibex = GameObject.Find("IbexLocation");
        obj_Wild_boar = GameObject.Find("BoarLocation");
        obj_Wolf = GameObject.Find("WolfLocation");
        obj_Rabbit = GameObject.Find("RabbitLocation");
        obj_Crocodile = GameObject.Find("CrocodileLocation");
        obj_Lion = GameObject.Find("LionLocation");
        obj_Tiger = GameObject.Find("TigerLocation");
        obj_Elephant = GameObject.Find("ElephantLocation");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void hideAnimals(String name)
    {
        Debug.Log("name " + name);
        if (name.Length > 0)
        {
            obj_Bear.SetActive(false);
            obj_Cattle.SetActive(false);
            obj_Deer.SetActive(false);
            obj_Ibex.SetActive(false);
            obj_Wild_boar.SetActive(false);
            obj_Wolf.SetActive(false);
            obj_Rabbit.SetActive(false);
            obj_Crocodile.SetActive(false);
            obj_Lion.SetActive(false);
            obj_Tiger.SetActive(false);
            obj_Elephant.SetActive(false);
        }
        else
        {
            obj_Bear.SetActive(true);
            obj_Cattle.SetActive(true);
            obj_Deer.SetActive(true);
            obj_Ibex.SetActive(true);
            obj_Wild_boar.SetActive(true);
            obj_Wolf.SetActive(true);
            obj_Rabbit.SetActive(true);
            obj_Crocodile.SetActive(true);
            obj_Lion.SetActive(true);
            obj_Tiger.SetActive(true);
            obj_Elephant.SetActive(true);
        }


        switch (name)
        {
            case "Bear":
                obj_Bear.SetActive(true);
                break;
            case "Cattle":
                obj_Cattle.SetActive(true);
                break;
            case "Deer":
                obj_Deer.SetActive(true);
                break;
            case "Ibex":
                obj_Ibex.SetActive(true);
                break;
            case "Wild Boar":
                obj_Wild_boar.SetActive(true);
                break;
            case "Wolf":
                obj_Wolf.SetActive(true);
                break;
            case "Rabbit":
                obj_Rabbit.SetActive(true);
                break;
            case "Crocodile":
                obj_Crocodile.SetActive(true);
                break;
            case "Lion":
                obj_Lion.SetActive(true);
                break;
            case "Tiger":
                obj_Tiger.SetActive(true);
                break;
            case "Elephant":
                obj_Elephant.SetActive(true);
                break;
        }
    }
}
