﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Google.ProtocolBuffers.ExtensionRegistry
struct ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>
struct Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>>
struct Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B;
// System.String
struct String_t;

extern RuntimeClass* PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var;
extern const uint32_t PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
#define EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ExtensionRegistry
struct  ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>> Google.ProtocolBuffers.ExtensionRegistry::extensionsByName
	Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * ___extensionsByName_1;
	// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry_ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite> Google.ProtocolBuffers.ExtensionRegistry::extensionsByNumber
	Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * ___extensionsByNumber_2;
	// System.Boolean Google.ProtocolBuffers.ExtensionRegistry::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_extensionsByName_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___extensionsByName_1)); }
	inline Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * get_extensionsByName_1() const { return ___extensionsByName_1; }
	inline Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B ** get_address_of_extensionsByName_1() { return &___extensionsByName_1; }
	inline void set_extensionsByName_1(Dictionary_2_t418F30137BF00E0E9EFF2A2DB75AB3A010190D9B * value)
	{
		___extensionsByName_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByName_1), value);
	}

	inline static int32_t get_offset_of_extensionsByNumber_2() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___extensionsByNumber_2)); }
	inline Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * get_extensionsByNumber_2() const { return ___extensionsByNumber_2; }
	inline Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 ** get_address_of_extensionsByNumber_2() { return &___extensionsByNumber_2; }
	inline void set_extensionsByNumber_2(Dictionary_2_t36E2ECEC38081A5C8DF6DED6117C7CE5B2491886 * value)
	{
		___extensionsByNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByNumber_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_StaticFields
{
public:
	// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::empty
	ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_StaticFields, ___empty_0)); }
	inline ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * get_empty_0() const { return ___empty_0; }
	inline ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_TAC8BBAEAD627B80DF852E93F941EF92FB4C8558E_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#define PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072  : public RuntimeObject
{
public:

public:
};

struct PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::Descriptor
	RuntimeObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields, ___Descriptor_0)); }
	inline RuntimeObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline RuntimeObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(RuntimeObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___Descriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T1B2F1E73A882C8864EE4369121AC6D00EDBFF072_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#define TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent_Types_Type
struct  Type_tE66B948E7250AA336E8768E46508619266BABE96 
{
public:
	// System.Int32 proto.PhoneEvent_Types_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tE66B948E7250AA336E8768E46508619266BABE96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TE66B948E7250AA336E8768E46508619266BABE96_H



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.Proto.PhoneEvent::RegisterAllExtensions(Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent_RegisterAllExtensions_m61F8709869646B3E266AEAD2BD72BBA6A057B964 (ExtensionRegistry_tAC8BBAEAD627B80DF852E93F941EF92FB4C8558E * ___registry0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void proto.Proto.PhoneEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhoneEvent__cctor_m43334645A72D0A443EDF366DB61D7C2A42CA69C1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t1B2F1E73A882C8864EE4369121AC6D00EDBFF072_il2cpp_TypeInfo_var))->set_Descriptor_0(NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
