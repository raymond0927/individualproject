﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AridAreaQuizController
struct AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E;
// ForestQuizController
struct ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA;
// Gvr.Internal.ControllerState
struct ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451;
// Gvr.Internal.IControllerProvider
struct IControllerProvider_t39EA581C62BB1FBFB1A87AF2FCEE5C191B8ED5F6;
// GvrAllEventsTrigger/TriggerEvent
struct TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6;
// GvrBaseArmModel
struct GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE;
// GvrBasePointer
struct GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D;
// GvrControllerInput/OnStateChangedEvent
struct OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F;
// GvrControllerInputDevice
struct GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7;
// GvrControllerInputDevice[]
struct GvrControllerInputDeviceU5BU5D_t54B7B7395F76D730CAEF030BB0B0A283916EB480;
// GvrControllerReticleVisual
struct GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6;
// GvrEventExecutor
struct GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3;
// GvrLaserVisual
struct GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633;
// GvrLaserVisual/GetPointForDistanceDelegate
struct GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869;
// GvrPointerInputModuleImpl
struct GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF;
// GvrPointerPhysicsRaycaster/HitComparer
struct HitComparer_tFB834727888EDA249326245CA376445F484ADD2F;
// GvrPointerScrollInput
struct GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485;
// IGvrEventExecutor
struct IGvrEventExecutor_t3C1159C790F15D853123FF8DA1D4EC9215769F7D;
// IGvrInputModuleController
struct IGvrInputModuleController_tDE92AFFBD8128F59F198A6066C165B8789722DBC;
// IGvrScrollSettings
struct IGvrScrollSettings_tA8F3AF7DA24DCE79E4E0AAEA4DFDCC700EC66C57;
// SeaQuizController
struct SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor/EventDelegate>
struct Dictionary_2_t400E7A959CF5D566B11898292176870BF720B38A;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput/ScrollInfo>
struct Dictionary_2_tD9F0CA6833A2D3582E291E2B9EA2B042213670C1;
// System.Collections.Generic.List`1<GvrAudioRoom>
struct List_1_tFA4CC0DD6F9E5631D1206C343993D0858CED9E93;
// System.Collections.Generic.List`1<QuestionList>
struct List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t5031A3D1172F5D774E43B5AE7EF4F0F79CE5796A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animation
struct Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler>
struct EventFunction_1_t465DE7E63A41B0A8B4A01C982900269C09940B9B;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t5F37B944987342C401FA9A231A75AD2991A66165;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#define U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITERU3ED__68_T79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502_H
#define U3CWAITERU3ED__68_T79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AridAreaQuizController_<waiter>d__68
struct  U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502  : public RuntimeObject
{
public:
	// System.Int32 AridAreaQuizController_<waiter>d__68::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AridAreaQuizController_<waiter>d__68::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AridAreaQuizController AridAreaQuizController_<waiter>d__68::<>4__this
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502, ___U3CU3E4__this_2)); }
	inline AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITERU3ED__68_T79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502_H
#ifndef U3CWAITERU3ED__68_T67EF5C9E90319BF27F94F060EE2FAE75B53D9868_H
#define U3CWAITERU3ED__68_T67EF5C9E90319BF27F94F060EE2FAE75B53D9868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForestQuizController_<waiter>d__68
struct  U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868  : public RuntimeObject
{
public:
	// System.Int32 ForestQuizController_<waiter>d__68::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ForestQuizController_<waiter>d__68::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ForestQuizController ForestQuizController_<waiter>d__68::<>4__this
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868, ___U3CU3E4__this_2)); }
	inline ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITERU3ED__68_T67EF5C9E90319BF27F94F060EE2FAE75B53D9868_H
#ifndef GVRCARDBOARDHELPERS_TF9D758761742FA1F97490A6E7AE979CA2CBFC500_H
#define GVRCARDBOARDHELPERS_TF9D758761742FA1F97490A6E7AE979CA2CBFC500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrCardboardHelpers
struct  GvrCardboardHelpers_tF9D758761742FA1F97490A6E7AE979CA2CBFC500  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCARDBOARDHELPERS_TF9D758761742FA1F97490A6E7AE979CA2CBFC500_H
#ifndef GVREVENTEXECUTOR_T898582E56497D9854114651E438F61B403245CD3_H
#define GVREVENTEXECUTOR_T898582E56497D9854114651E438F61B403245CD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor
struct  GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor_EventDelegate> GvrEventExecutor::eventTable
	Dictionary_2_t400E7A959CF5D566B11898292176870BF720B38A * ___eventTable_0;

public:
	inline static int32_t get_offset_of_eventTable_0() { return static_cast<int32_t>(offsetof(GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3, ___eventTable_0)); }
	inline Dictionary_2_t400E7A959CF5D566B11898292176870BF720B38A * get_eventTable_0() const { return ___eventTable_0; }
	inline Dictionary_2_t400E7A959CF5D566B11898292176870BF720B38A ** get_address_of_eventTable_0() { return &___eventTable_0; }
	inline void set_eventTable_0(Dictionary_2_t400E7A959CF5D566B11898292176870BF720B38A * value)
	{
		___eventTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTEXECUTOR_T898582E56497D9854114651E438F61B403245CD3_H
#ifndef GVREXECUTEEVENTSEXTENSION_TAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_H
#define GVREXECUTEEVENTSEXTENSION_TAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrExecuteEventsExtension
struct  GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790  : public RuntimeObject
{
public:

public:
};

struct GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::s_HoverHandler
	EventFunction_1_t465DE7E63A41B0A8B4A01C982900269C09940B9B * ___s_HoverHandler_0;

public:
	inline static int32_t get_offset_of_s_HoverHandler_0() { return static_cast<int32_t>(offsetof(GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_StaticFields, ___s_HoverHandler_0)); }
	inline EventFunction_1_t465DE7E63A41B0A8B4A01C982900269C09940B9B * get_s_HoverHandler_0() const { return ___s_HoverHandler_0; }
	inline EventFunction_1_t465DE7E63A41B0A8B4A01C982900269C09940B9B ** get_address_of_s_HoverHandler_0() { return &___s_HoverHandler_0; }
	inline void set_s_HoverHandler_0(EventFunction_1_t465DE7E63A41B0A8B4A01C982900269C09940B9B * value)
	{
		___s_HoverHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_HoverHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREXECUTEEVENTSEXTENSION_TAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_H
#ifndef U3CU3EC_T35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_H
#define U3CU3EC_T35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster_<>c
struct  U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields
{
public:
	// GvrPointerGraphicRaycaster_<>c GvrPointerGraphicRaycaster_<>c::<>9
	U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster_<>c::<>9__17_0
	Comparison_1_t5031A3D1172F5D774E43B5AE7EF4F0F79CE5796A * ___U3CU3E9__17_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Comparison_1_t5031A3D1172F5D774E43B5AE7EF4F0F79CE5796A * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Comparison_1_t5031A3D1172F5D774E43B5AE7EF4F0F79CE5796A ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Comparison_1_t5031A3D1172F5D774E43B5AE7EF4F0F79CE5796A * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_H
#ifndef HITCOMPARER_TFB834727888EDA249326245CA376445F484ADD2F_H
#define HITCOMPARER_TFB834727888EDA249326245CA376445F484ADD2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster_HitComparer
struct  HitComparer_tFB834727888EDA249326245CA376445F484ADD2F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCOMPARER_TFB834727888EDA249326245CA376445F484ADD2F_H
#ifndef GVRPOINTERSCROLLINPUT_TDA3CEE0132364FDF0C4E5A1707327F7075C77485_H
#define GVRPOINTERSCROLLINPUT_TDA3CEE0132364FDF0C4E5A1707327F7075C77485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput
struct  GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput::inertia
	bool ___inertia_2;
	// System.Single GvrPointerScrollInput::decelerationRate
	float ___decelerationRate_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput_ScrollInfo> GvrPointerScrollInput::scrollHandlers
	Dictionary_2_tD9F0CA6833A2D3582E291E2B9EA2B042213670C1 * ___scrollHandlers_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GvrPointerScrollInput::scrollingObjects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___scrollingObjects_16;

public:
	inline static int32_t get_offset_of_inertia_2() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485, ___inertia_2)); }
	inline bool get_inertia_2() const { return ___inertia_2; }
	inline bool* get_address_of_inertia_2() { return &___inertia_2; }
	inline void set_inertia_2(bool value)
	{
		___inertia_2 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_3() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485, ___decelerationRate_3)); }
	inline float get_decelerationRate_3() const { return ___decelerationRate_3; }
	inline float* get_address_of_decelerationRate_3() { return &___decelerationRate_3; }
	inline void set_decelerationRate_3(float value)
	{
		___decelerationRate_3 = value;
	}

	inline static int32_t get_offset_of_scrollHandlers_15() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485, ___scrollHandlers_15)); }
	inline Dictionary_2_tD9F0CA6833A2D3582E291E2B9EA2B042213670C1 * get_scrollHandlers_15() const { return ___scrollHandlers_15; }
	inline Dictionary_2_tD9F0CA6833A2D3582E291E2B9EA2B042213670C1 ** get_address_of_scrollHandlers_15() { return &___scrollHandlers_15; }
	inline void set_scrollHandlers_15(Dictionary_2_tD9F0CA6833A2D3582E291E2B9EA2B042213670C1 * value)
	{
		___scrollHandlers_15 = value;
		Il2CppCodeGenWriteBarrier((&___scrollHandlers_15), value);
	}

	inline static int32_t get_offset_of_scrollingObjects_16() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485, ___scrollingObjects_16)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_scrollingObjects_16() const { return ___scrollingObjects_16; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_scrollingObjects_16() { return &___scrollingObjects_16; }
	inline void set_scrollingObjects_16(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___scrollingObjects_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollingObjects_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERSCROLLINPUT_TDA3CEE0132364FDF0C4E5A1707327F7075C77485_H
#ifndef QUESTIONLIST_T55C63A912E36A8A65CAF13574DF965F8C28D9265_H
#define QUESTIONLIST_T55C63A912E36A8A65CAF13574DF965F8C28D9265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionList
struct  QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265  : public RuntimeObject
{
public:
	// System.String QuestionList::question
	String_t* ___question_0;
	// System.String QuestionList::choiceA
	String_t* ___choiceA_1;
	// System.String QuestionList::choiceB
	String_t* ___choiceB_2;
	// System.String QuestionList::choiceC
	String_t* ___choiceC_3;
	// System.String QuestionList::choiceD
	String_t* ___choiceD_4;
	// System.String QuestionList::ans
	String_t* ___ans_5;

public:
	inline static int32_t get_offset_of_question_0() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___question_0)); }
	inline String_t* get_question_0() const { return ___question_0; }
	inline String_t** get_address_of_question_0() { return &___question_0; }
	inline void set_question_0(String_t* value)
	{
		___question_0 = value;
		Il2CppCodeGenWriteBarrier((&___question_0), value);
	}

	inline static int32_t get_offset_of_choiceA_1() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___choiceA_1)); }
	inline String_t* get_choiceA_1() const { return ___choiceA_1; }
	inline String_t** get_address_of_choiceA_1() { return &___choiceA_1; }
	inline void set_choiceA_1(String_t* value)
	{
		___choiceA_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceA_1), value);
	}

	inline static int32_t get_offset_of_choiceB_2() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___choiceB_2)); }
	inline String_t* get_choiceB_2() const { return ___choiceB_2; }
	inline String_t** get_address_of_choiceB_2() { return &___choiceB_2; }
	inline void set_choiceB_2(String_t* value)
	{
		___choiceB_2 = value;
		Il2CppCodeGenWriteBarrier((&___choiceB_2), value);
	}

	inline static int32_t get_offset_of_choiceC_3() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___choiceC_3)); }
	inline String_t* get_choiceC_3() const { return ___choiceC_3; }
	inline String_t** get_address_of_choiceC_3() { return &___choiceC_3; }
	inline void set_choiceC_3(String_t* value)
	{
		___choiceC_3 = value;
		Il2CppCodeGenWriteBarrier((&___choiceC_3), value);
	}

	inline static int32_t get_offset_of_choiceD_4() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___choiceD_4)); }
	inline String_t* get_choiceD_4() const { return ___choiceD_4; }
	inline String_t** get_address_of_choiceD_4() { return &___choiceD_4; }
	inline void set_choiceD_4(String_t* value)
	{
		___choiceD_4 = value;
		Il2CppCodeGenWriteBarrier((&___choiceD_4), value);
	}

	inline static int32_t get_offset_of_ans_5() { return static_cast<int32_t>(offsetof(QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265, ___ans_5)); }
	inline String_t* get_ans_5() const { return ___ans_5; }
	inline String_t** get_address_of_ans_5() { return &___ans_5; }
	inline void set_ans_5(String_t* value)
	{
		___ans_5 = value;
		Il2CppCodeGenWriteBarrier((&___ans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUESTIONLIST_T55C63A912E36A8A65CAF13574DF965F8C28D9265_H
#ifndef U3CWAITERU3ED__67_T699BB7F04DCCB6AFC4034EB61734765ED8A25828_H
#define U3CWAITERU3ED__67_T699BB7F04DCCB6AFC4034EB61734765ED8A25828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaQuizController_<waiter>d__67
struct  U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828  : public RuntimeObject
{
public:
	// System.Int32 SeaQuizController_<waiter>d__67::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SeaQuizController_<waiter>d__67::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SeaQuizController SeaQuizController_<waiter>d__67::<>4__this
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828, ___U3CU3E4__this_2)); }
	inline SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITERU3ED__67_T699BB7F04DCCB6AFC4034EB61734765ED8A25828_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef FACECAMERADATA_T5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_H
#define FACECAMERADATA_T5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerReticleVisual_FaceCameraData
struct  FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428 
{
public:
	// System.Boolean GvrControllerReticleVisual_FaceCameraData::alongXAxis
	bool ___alongXAxis_0;
	// System.Boolean GvrControllerReticleVisual_FaceCameraData::alongYAxis
	bool ___alongYAxis_1;
	// System.Boolean GvrControllerReticleVisual_FaceCameraData::alongZAxis
	bool ___alongZAxis_2;

public:
	inline static int32_t get_offset_of_alongXAxis_0() { return static_cast<int32_t>(offsetof(FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428, ___alongXAxis_0)); }
	inline bool get_alongXAxis_0() const { return ___alongXAxis_0; }
	inline bool* get_address_of_alongXAxis_0() { return &___alongXAxis_0; }
	inline void set_alongXAxis_0(bool value)
	{
		___alongXAxis_0 = value;
	}

	inline static int32_t get_offset_of_alongYAxis_1() { return static_cast<int32_t>(offsetof(FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428, ___alongYAxis_1)); }
	inline bool get_alongYAxis_1() const { return ___alongYAxis_1; }
	inline bool* get_address_of_alongYAxis_1() { return &___alongYAxis_1; }
	inline void set_alongYAxis_1(bool value)
	{
		___alongYAxis_1 = value;
	}

	inline static int32_t get_offset_of_alongZAxis_2() { return static_cast<int32_t>(offsetof(FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428, ___alongZAxis_2)); }
	inline bool get_alongZAxis_2() const { return ___alongZAxis_2; }
	inline bool* get_address_of_alongZAxis_2() { return &___alongZAxis_2; }
	inline void set_alongZAxis_2(bool value)
	{
		___alongZAxis_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GvrControllerReticleVisual/FaceCameraData
struct FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_marshaled_pinvoke
{
	int32_t ___alongXAxis_0;
	int32_t ___alongYAxis_1;
	int32_t ___alongZAxis_2;
};
// Native definition for COM marshalling of GvrControllerReticleVisual/FaceCameraData
struct FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_marshaled_com
{
	int32_t ___alongXAxis_0;
	int32_t ___alongYAxis_1;
	int32_t ___alongZAxis_2;
};
#endif // FACECAMERADATA_T5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#define ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_2_T6C8464C0E516C72121DBE352CB269172500E09BD_H
#define UNITYEVENT_2_T6C8464C0E516C72121DBE352CB269172500E09BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData>
struct  UnityEvent_2_t6C8464C0E516C72121DBE352CB269172500E09BD  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t6C8464C0E516C72121DBE352CB269172500E09BD, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T6C8464C0E516C72121DBE352CB269172500E09BD_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef TRIGGEREVENT_T65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6_H
#define TRIGGEREVENT_T65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger_TriggerEvent
struct  TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6  : public UnityEvent_2_t6C8464C0E516C72121DBE352CB269172500E09BD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6_H
#ifndef QUALITY_T2A8FDC622752170001EBDA0DF3DCEFE60042982E_H
#define QUALITY_T2A8FDC622752170001EBDA0DF3DCEFE60042982E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio_Quality
struct  Quality_t2A8FDC622752170001EBDA0DF3DCEFE60042982E 
{
public:
	// System.Int32 GvrAudio_Quality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Quality_t2A8FDC622752170001EBDA0DF3DCEFE60042982E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITY_T2A8FDC622752170001EBDA0DF3DCEFE60042982E_H
#ifndef SPATIALIZERDATA_T2093C207741ACE666293262EF123349EB3B7F1C5_H
#define SPATIALIZERDATA_T2093C207741ACE666293262EF123349EB3B7F1C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio_SpatializerData
struct  SpatializerData_t2093C207741ACE666293262EF123349EB3B7F1C5 
{
public:
	// System.Int32 GvrAudio_SpatializerData::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatializerData_t2093C207741ACE666293262EF123349EB3B7F1C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALIZERDATA_T2093C207741ACE666293262EF123349EB3B7F1C5_H
#ifndef SPATIALIZERTYPE_TD983F6F76C53B8E648DB5EAC2F18763BB309246F_H
#define SPATIALIZERTYPE_TD983F6F76C53B8E648DB5EAC2F18763BB309246F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio_SpatializerType
struct  SpatializerType_tD983F6F76C53B8E648DB5EAC2F18763BB309246F 
{
public:
	// System.Int32 GvrAudio_SpatializerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatializerType_tD983F6F76C53B8E648DB5EAC2F18763BB309246F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALIZERTYPE_TD983F6F76C53B8E648DB5EAC2F18763BB309246F_H
#ifndef SURFACEMATERIAL_T304A18E7AE00061A3E42922D9140E64184AC3573_H
#define SURFACEMATERIAL_T304A18E7AE00061A3E42922D9140E64184AC3573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioRoom_SurfaceMaterial
struct  SurfaceMaterial_t304A18E7AE00061A3E42922D9140E64184AC3573 
{
public:
	// System.Int32 GvrAudioRoom_SurfaceMaterial::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceMaterial_t304A18E7AE00061A3E42922D9140E64184AC3573, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEMATERIAL_T304A18E7AE00061A3E42922D9140E64184AC3573_H
#ifndef RAYCASTMODE_TD7D5952B8515A9A9E4A0EA315926DBFDE3A24183_H
#define RAYCASTMODE_TD7D5952B8515A9A9E4A0EA315926DBFDE3A24183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer_RaycastMode
struct  RaycastMode_tD7D5952B8515A9A9E4A0EA315926DBFDE3A24183 
{
public:
	// System.Int32 GvrBasePointer_RaycastMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastMode_tD7D5952B8515A9A9E4A0EA315926DBFDE3A24183, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMODE_TD7D5952B8515A9A9E4A0EA315926DBFDE3A24183_H
#ifndef GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#define GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrConnectionState
struct  GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982 
{
public:
	// System.Int32 GvrConnectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONNECTIONSTATE_TBEA19BB79DA64E9E956AB059FCA945ACC6803982_H
#ifndef GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#define GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerApiStatus
struct  GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85 
{
public:
	// System.Int32 GvrControllerApiStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERAPISTATUS_T039908D8922D8E98CBBFABB19FA503ACE3887A85_H
#ifndef GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#define GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerBatteryLevel
struct  GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904 
{
public:
	// System.Int32 GvrControllerBatteryLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBATTERYLEVEL_T48A4D5107F5C9B9CC137363AD21B7C0EBBC71904_H
#ifndef GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#define GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerButton
struct  GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645 
{
public:
	// System.Int32 GvrControllerButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBUTTON_TE7A7A32A9D09E43D05C67221E70C1D44625EA645_H
#ifndef GVRCONTROLLERHAND_T378A8CB3F703F3F3BE381830AD8D36CE65C4B806_H
#define GVRCONTROLLERHAND_T378A8CB3F703F3F3BE381830AD8D36CE65C4B806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerHand
struct  GvrControllerHand_t378A8CB3F703F3F3BE381830AD8D36CE65C4B806 
{
public:
	// System.Int32 GvrControllerHand::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GvrControllerHand_t378A8CB3F703F3F3BE381830AD8D36CE65C4B806, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERHAND_T378A8CB3F703F3F3BE381830AD8D36CE65C4B806_H
#ifndef EMULATORCONNECTIONMODE_T2B86B91C3C252BF9BC47401216315ACE8FF6C199_H
#define EMULATORCONNECTIONMODE_T2B86B91C3C252BF9BC47401216315ACE8FF6C199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput_EmulatorConnectionMode
struct  EmulatorConnectionMode_t2B86B91C3C252BF9BC47401216315ACE8FF6C199 
{
public:
	// System.Int32 GvrControllerInput_EmulatorConnectionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EmulatorConnectionMode_t2B86B91C3C252BF9BC47401216315ACE8FF6C199, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORCONNECTIONMODE_T2B86B91C3C252BF9BC47401216315ACE8FF6C199_H
#ifndef GVRCONTROLLERINPUTDEVICE_T8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7_H
#define GVRCONTROLLERINPUTDEVICE_T8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInputDevice
struct  GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7  : public RuntimeObject
{
public:
	// Gvr.Internal.IControllerProvider GvrControllerInputDevice::controllerProvider
	RuntimeObject* ___controllerProvider_0;
	// System.Int32 GvrControllerInputDevice::controllerId
	int32_t ___controllerId_1;
	// Gvr.Internal.ControllerState GvrControllerInputDevice::controllerState
	ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * ___controllerState_2;
	// UnityEngine.Vector2 GvrControllerInputDevice::touchPosCentered
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPosCentered_3;
	// System.Int32 GvrControllerInputDevice::lastUpdatedFrameCount
	int32_t ___lastUpdatedFrameCount_4;
	// System.Boolean GvrControllerInputDevice::valid
	bool ___valid_5;
	// GvrControllerInput_OnStateChangedEvent GvrControllerInputDevice::OnStateChanged
	OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F * ___OnStateChanged_6;

public:
	inline static int32_t get_offset_of_controllerProvider_0() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___controllerProvider_0)); }
	inline RuntimeObject* get_controllerProvider_0() const { return ___controllerProvider_0; }
	inline RuntimeObject** get_address_of_controllerProvider_0() { return &___controllerProvider_0; }
	inline void set_controllerProvider_0(RuntimeObject* value)
	{
		___controllerProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___controllerProvider_0), value);
	}

	inline static int32_t get_offset_of_controllerId_1() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___controllerId_1)); }
	inline int32_t get_controllerId_1() const { return ___controllerId_1; }
	inline int32_t* get_address_of_controllerId_1() { return &___controllerId_1; }
	inline void set_controllerId_1(int32_t value)
	{
		___controllerId_1 = value;
	}

	inline static int32_t get_offset_of_controllerState_2() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___controllerState_2)); }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * get_controllerState_2() const { return ___controllerState_2; }
	inline ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 ** get_address_of_controllerState_2() { return &___controllerState_2; }
	inline void set_controllerState_2(ControllerState_t0BE3F1601302488E0F1DE33AFF299E9EED574451 * value)
	{
		___controllerState_2 = value;
		Il2CppCodeGenWriteBarrier((&___controllerState_2), value);
	}

	inline static int32_t get_offset_of_touchPosCentered_3() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___touchPosCentered_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchPosCentered_3() const { return ___touchPosCentered_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchPosCentered_3() { return &___touchPosCentered_3; }
	inline void set_touchPosCentered_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchPosCentered_3 = value;
	}

	inline static int32_t get_offset_of_lastUpdatedFrameCount_4() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___lastUpdatedFrameCount_4)); }
	inline int32_t get_lastUpdatedFrameCount_4() const { return ___lastUpdatedFrameCount_4; }
	inline int32_t* get_address_of_lastUpdatedFrameCount_4() { return &___lastUpdatedFrameCount_4; }
	inline void set_lastUpdatedFrameCount_4(int32_t value)
	{
		___lastUpdatedFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_valid_5() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___valid_5)); }
	inline bool get_valid_5() const { return ___valid_5; }
	inline bool* get_address_of_valid_5() { return &___valid_5; }
	inline void set_valid_5(bool value)
	{
		___valid_5 = value;
	}

	inline static int32_t get_offset_of_OnStateChanged_6() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7, ___OnStateChanged_6)); }
	inline OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F * get_OnStateChanged_6() const { return ___OnStateChanged_6; }
	inline OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F ** get_address_of_OnStateChanged_6() { return &___OnStateChanged_6; }
	inline void set_OnStateChanged_6(OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F * value)
	{
		___OnStateChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERINPUTDEVICE_T8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7_H
#ifndef BLOCKINGOBJECTS_TFBC510CA4D09993D8A31624F4DAD506EBCC60B69_H
#define BLOCKINGOBJECTS_TFBC510CA4D09993D8A31624F4DAD506EBCC60B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster_BlockingObjects
struct  BlockingObjects_tFBC510CA4D09993D8A31624F4DAD506EBCC60B69 
{
public:
	// System.Int32 GvrPointerGraphicRaycaster_BlockingObjects::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlockingObjects_tFBC510CA4D09993D8A31624F4DAD506EBCC60B69, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_TFBC510CA4D09993D8A31624F4DAD506EBCC60B69_H
#ifndef GVRPOINTERINPUTMODULEIMPL_TAA775CFC6E299454B1CF55E6B80423281A5287AF_H
#define GVRPOINTERINPUTMODULEIMPL_TAA775CFC6E299454B1CF55E6B80423281A5287AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModuleImpl
struct  GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF  : public RuntimeObject
{
public:
	// IGvrInputModuleController GvrPointerInputModuleImpl::<ModuleController>k__BackingField
	RuntimeObject* ___U3CModuleControllerU3Ek__BackingField_0;
	// IGvrEventExecutor GvrPointerInputModuleImpl::<EventExecutor>k__BackingField
	RuntimeObject* ___U3CEventExecutorU3Ek__BackingField_1;
	// System.Boolean GvrPointerInputModuleImpl::<VrModeOnly>k__BackingField
	bool ___U3CVrModeOnlyU3Ek__BackingField_2;
	// GvrPointerScrollInput GvrPointerInputModuleImpl::<ScrollInput>k__BackingField
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * ___U3CScrollInputU3Ek__BackingField_3;
	// UnityEngine.EventSystems.PointerEventData GvrPointerInputModuleImpl::<CurrentEventData>k__BackingField
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___U3CCurrentEventDataU3Ek__BackingField_4;
	// GvrBasePointer GvrPointerInputModuleImpl::pointer
	GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D * ___pointer_5;
	// UnityEngine.Vector2 GvrPointerInputModuleImpl::lastPose
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lastPose_6;
	// System.Boolean GvrPointerInputModuleImpl::isPointerHovering
	bool ___isPointerHovering_7;
	// System.Boolean GvrPointerInputModuleImpl::isActive
	bool ___isActive_8;

public:
	inline static int32_t get_offset_of_U3CModuleControllerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___U3CModuleControllerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CModuleControllerU3Ek__BackingField_0() const { return ___U3CModuleControllerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CModuleControllerU3Ek__BackingField_0() { return &___U3CModuleControllerU3Ek__BackingField_0; }
	inline void set_U3CModuleControllerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CModuleControllerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModuleControllerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___U3CEventExecutorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CEventExecutorU3Ek__BackingField_1() const { return ___U3CEventExecutorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CEventExecutorU3Ek__BackingField_1() { return &___U3CEventExecutorU3Ek__BackingField_1; }
	inline void set_U3CEventExecutorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CEventExecutorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___U3CVrModeOnlyU3Ek__BackingField_2)); }
	inline bool get_U3CVrModeOnlyU3Ek__BackingField_2() const { return ___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CVrModeOnlyU3Ek__BackingField_2() { return &___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline void set_U3CVrModeOnlyU3Ek__BackingField_2(bool value)
	{
		___U3CVrModeOnlyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CScrollInputU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___U3CScrollInputU3Ek__BackingField_3)); }
	inline GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * get_U3CScrollInputU3Ek__BackingField_3() const { return ___U3CScrollInputU3Ek__BackingField_3; }
	inline GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 ** get_address_of_U3CScrollInputU3Ek__BackingField_3() { return &___U3CScrollInputU3Ek__BackingField_3; }
	inline void set_U3CScrollInputU3Ek__BackingField_3(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * value)
	{
		___U3CScrollInputU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScrollInputU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___U3CCurrentEventDataU3Ek__BackingField_4)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_U3CCurrentEventDataU3Ek__BackingField_4() const { return ___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_U3CCurrentEventDataU3Ek__BackingField_4() { return &___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline void set_U3CCurrentEventDataU3Ek__BackingField_4(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___U3CCurrentEventDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentEventDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_pointer_5() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___pointer_5)); }
	inline GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D * get_pointer_5() const { return ___pointer_5; }
	inline GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D ** get_address_of_pointer_5() { return &___pointer_5; }
	inline void set_pointer_5(GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D * value)
	{
		___pointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_5), value);
	}

	inline static int32_t get_offset_of_lastPose_6() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___lastPose_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_lastPose_6() const { return ___lastPose_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_lastPose_6() { return &___lastPose_6; }
	inline void set_lastPose_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___lastPose_6 = value;
	}

	inline static int32_t get_offset_of_isPointerHovering_7() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___isPointerHovering_7)); }
	inline bool get_isPointerHovering_7() const { return ___isPointerHovering_7; }
	inline bool* get_address_of_isPointerHovering_7() { return &___isPointerHovering_7; }
	inline void set_isPointerHovering_7(bool value)
	{
		___isPointerHovering_7 = value;
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULEIMPL_TAA775CFC6E299454B1CF55E6B80423281A5287AF_H
#ifndef SCROLLINFO_T0B9CDE332B831B35646C30041820D159435C3E1C_H
#define SCROLLINFO_T0B9CDE332B831B35646C30041820D159435C3E1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput_ScrollInfo
struct  ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput_ScrollInfo::isScrollingX
	bool ___isScrollingX_0;
	// System.Boolean GvrPointerScrollInput_ScrollInfo::isScrollingY
	bool ___isScrollingY_1;
	// UnityEngine.Vector2 GvrPointerScrollInput_ScrollInfo::initScroll
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___initScroll_2;
	// UnityEngine.Vector2 GvrPointerScrollInput_ScrollInfo::lastScroll
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lastScroll_3;
	// UnityEngine.Vector2 GvrPointerScrollInput_ScrollInfo::scrollVelocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollVelocity_4;
	// IGvrScrollSettings GvrPointerScrollInput_ScrollInfo::scrollSettings
	RuntimeObject* ___scrollSettings_5;

public:
	inline static int32_t get_offset_of_isScrollingX_0() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___isScrollingX_0)); }
	inline bool get_isScrollingX_0() const { return ___isScrollingX_0; }
	inline bool* get_address_of_isScrollingX_0() { return &___isScrollingX_0; }
	inline void set_isScrollingX_0(bool value)
	{
		___isScrollingX_0 = value;
	}

	inline static int32_t get_offset_of_isScrollingY_1() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___isScrollingY_1)); }
	inline bool get_isScrollingY_1() const { return ___isScrollingY_1; }
	inline bool* get_address_of_isScrollingY_1() { return &___isScrollingY_1; }
	inline void set_isScrollingY_1(bool value)
	{
		___isScrollingY_1 = value;
	}

	inline static int32_t get_offset_of_initScroll_2() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___initScroll_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_initScroll_2() const { return ___initScroll_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_initScroll_2() { return &___initScroll_2; }
	inline void set_initScroll_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___initScroll_2 = value;
	}

	inline static int32_t get_offset_of_lastScroll_3() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___lastScroll_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_lastScroll_3() const { return ___lastScroll_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_lastScroll_3() { return &___lastScroll_3; }
	inline void set_lastScroll_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___lastScroll_3 = value;
	}

	inline static int32_t get_offset_of_scrollVelocity_4() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___scrollVelocity_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollVelocity_4() const { return ___scrollVelocity_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollVelocity_4() { return &___scrollVelocity_4; }
	inline void set_scrollVelocity_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollVelocity_4 = value;
	}

	inline static int32_t get_offset_of_scrollSettings_5() { return static_cast<int32_t>(offsetof(ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C, ___scrollSettings_5)); }
	inline RuntimeObject* get_scrollSettings_5() const { return ___scrollSettings_5; }
	inline RuntimeObject** get_address_of_scrollSettings_5() { return &___scrollSettings_5; }
	inline void set_scrollSettings_5(RuntimeObject* value)
	{
		___scrollSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINFO_T0B9CDE332B831B35646C30041820D159435C3E1C_H
#ifndef USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#define USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings_UserPrefsHandedness
struct  UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE 
{
public:
	// System.Int32 GvrSettings_UserPrefsHandedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UserPrefsHandedness_t5CEA6D83C529B753626E3B436AEEE53E1C84CCEE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPREFSHANDEDNESS_T5CEA6D83C529B753626E3B436AEEE53E1C84CCEE_H
#ifndef LOCATION_T762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D_H
#define LOCATION_T762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip_Location
struct  Location_t762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D 
{
public:
	// System.Int32 GvrTooltip_Location::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Location_t762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef AUDIOROLLOFFMODE_T6E80F49565F8A29A85F0EE610614DACE40BEDA61_H
#define AUDIOROLLOFFMODE_T6E80F49565F8A29A85F0EE610614DACE40BEDA61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioRolloffMode
struct  AudioRolloffMode_t6E80F49565F8A29A85F0EE610614DACE40BEDA61 
{
public:
	// System.Int32 UnityEngine.AudioRolloffMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioRolloffMode_t6E80F49565F8A29A85F0EE610614DACE40BEDA61, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOROLLOFFMODE_T6E80F49565F8A29A85F0EE610614DACE40BEDA61_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef GVRAUDIO_TA153F5F402364F6F8EF30AE98C56FCAB74669D96_H
#define GVRAUDIO_TA153F5F402364F6F8EF30AE98C56FCAB74669D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio
struct  GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96  : public RuntimeObject
{
public:

public:
};

struct GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields
{
public:
	// System.Int32 GvrAudio::sampleRate
	int32_t ___sampleRate_0;
	// System.Int32 GvrAudio::numChannels
	int32_t ___numChannels_1;
	// System.Int32 GvrAudio::framesPerBuffer
	int32_t ___framesPerBuffer_2;
	// UnityEngine.Color GvrAudio::listenerDirectivityColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___listenerDirectivityColor_3;
	// UnityEngine.Color GvrAudio::sourceDirectivityColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___sourceDirectivityColor_4;
	// UnityEngine.Bounds GvrAudio::bounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___bounds_17;
	// System.Collections.Generic.List`1<GvrAudioRoom> GvrAudio::enabledRooms
	List_1_tFA4CC0DD6F9E5631D1206C343993D0858CED9E93 * ___enabledRooms_18;
	// System.Boolean GvrAudio::initialized
	bool ___initialized_19;
	// UnityEngine.Transform GvrAudio::listenerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___listenerTransform_20;
	// UnityEngine.RaycastHit[] GvrAudio::occlusionHits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___occlusionHits_21;
	// System.Int32 GvrAudio::occlusionMaskValue
	int32_t ___occlusionMaskValue_22;
	// UnityEngine.Matrix4x4 GvrAudio::transformMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transformMatrix_23;

public:
	inline static int32_t get_offset_of_sampleRate_0() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___sampleRate_0)); }
	inline int32_t get_sampleRate_0() const { return ___sampleRate_0; }
	inline int32_t* get_address_of_sampleRate_0() { return &___sampleRate_0; }
	inline void set_sampleRate_0(int32_t value)
	{
		___sampleRate_0 = value;
	}

	inline static int32_t get_offset_of_numChannels_1() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___numChannels_1)); }
	inline int32_t get_numChannels_1() const { return ___numChannels_1; }
	inline int32_t* get_address_of_numChannels_1() { return &___numChannels_1; }
	inline void set_numChannels_1(int32_t value)
	{
		___numChannels_1 = value;
	}

	inline static int32_t get_offset_of_framesPerBuffer_2() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___framesPerBuffer_2)); }
	inline int32_t get_framesPerBuffer_2() const { return ___framesPerBuffer_2; }
	inline int32_t* get_address_of_framesPerBuffer_2() { return &___framesPerBuffer_2; }
	inline void set_framesPerBuffer_2(int32_t value)
	{
		___framesPerBuffer_2 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivityColor_3() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___listenerDirectivityColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_listenerDirectivityColor_3() const { return ___listenerDirectivityColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_listenerDirectivityColor_3() { return &___listenerDirectivityColor_3; }
	inline void set_listenerDirectivityColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___listenerDirectivityColor_3 = value;
	}

	inline static int32_t get_offset_of_sourceDirectivityColor_4() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___sourceDirectivityColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_sourceDirectivityColor_4() const { return ___sourceDirectivityColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_sourceDirectivityColor_4() { return &___sourceDirectivityColor_4; }
	inline void set_sourceDirectivityColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___sourceDirectivityColor_4 = value;
	}

	inline static int32_t get_offset_of_bounds_17() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___bounds_17)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_bounds_17() const { return ___bounds_17; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_bounds_17() { return &___bounds_17; }
	inline void set_bounds_17(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___bounds_17 = value;
	}

	inline static int32_t get_offset_of_enabledRooms_18() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___enabledRooms_18)); }
	inline List_1_tFA4CC0DD6F9E5631D1206C343993D0858CED9E93 * get_enabledRooms_18() const { return ___enabledRooms_18; }
	inline List_1_tFA4CC0DD6F9E5631D1206C343993D0858CED9E93 ** get_address_of_enabledRooms_18() { return &___enabledRooms_18; }
	inline void set_enabledRooms_18(List_1_tFA4CC0DD6F9E5631D1206C343993D0858CED9E93 * value)
	{
		___enabledRooms_18 = value;
		Il2CppCodeGenWriteBarrier((&___enabledRooms_18), value);
	}

	inline static int32_t get_offset_of_initialized_19() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___initialized_19)); }
	inline bool get_initialized_19() const { return ___initialized_19; }
	inline bool* get_address_of_initialized_19() { return &___initialized_19; }
	inline void set_initialized_19(bool value)
	{
		___initialized_19 = value;
	}

	inline static int32_t get_offset_of_listenerTransform_20() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___listenerTransform_20)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_listenerTransform_20() const { return ___listenerTransform_20; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_listenerTransform_20() { return &___listenerTransform_20; }
	inline void set_listenerTransform_20(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___listenerTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___listenerTransform_20), value);
	}

	inline static int32_t get_offset_of_occlusionHits_21() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___occlusionHits_21)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_occlusionHits_21() const { return ___occlusionHits_21; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_occlusionHits_21() { return &___occlusionHits_21; }
	inline void set_occlusionHits_21(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___occlusionHits_21 = value;
		Il2CppCodeGenWriteBarrier((&___occlusionHits_21), value);
	}

	inline static int32_t get_offset_of_occlusionMaskValue_22() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___occlusionMaskValue_22)); }
	inline int32_t get_occlusionMaskValue_22() const { return ___occlusionMaskValue_22; }
	inline int32_t* get_address_of_occlusionMaskValue_22() { return &___occlusionMaskValue_22; }
	inline void set_occlusionMaskValue_22(int32_t value)
	{
		___occlusionMaskValue_22 = value;
	}

	inline static int32_t get_offset_of_transformMatrix_23() { return static_cast<int32_t>(offsetof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields, ___transformMatrix_23)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_transformMatrix_23() const { return ___transformMatrix_23; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_transformMatrix_23() { return &___transformMatrix_23; }
	inline void set_transformMatrix_23(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___transformMatrix_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIO_TA153F5F402364F6F8EF30AE98C56FCAB74669D96_H
#ifndef ROOMPROPERTIES_T38CEF2E715BD4DC611ACB600288C87E43A59A683_H
#define ROOMPROPERTIES_T38CEF2E715BD4DC611ACB600288C87E43A59A683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio_RoomProperties
struct  RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683 
{
public:
	// System.Single GvrAudio_RoomProperties::positionX
	float ___positionX_0;
	// System.Single GvrAudio_RoomProperties::positionY
	float ___positionY_1;
	// System.Single GvrAudio_RoomProperties::positionZ
	float ___positionZ_2;
	// System.Single GvrAudio_RoomProperties::rotationX
	float ___rotationX_3;
	// System.Single GvrAudio_RoomProperties::rotationY
	float ___rotationY_4;
	// System.Single GvrAudio_RoomProperties::rotationZ
	float ___rotationZ_5;
	// System.Single GvrAudio_RoomProperties::rotationW
	float ___rotationW_6;
	// System.Single GvrAudio_RoomProperties::dimensionsX
	float ___dimensionsX_7;
	// System.Single GvrAudio_RoomProperties::dimensionsY
	float ___dimensionsY_8;
	// System.Single GvrAudio_RoomProperties::dimensionsZ
	float ___dimensionsZ_9;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialLeft
	int32_t ___materialLeft_10;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialRight
	int32_t ___materialRight_11;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialBottom
	int32_t ___materialBottom_12;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialTop
	int32_t ___materialTop_13;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialFront
	int32_t ___materialFront_14;
	// GvrAudioRoom_SurfaceMaterial GvrAudio_RoomProperties::materialBack
	int32_t ___materialBack_15;
	// System.Single GvrAudio_RoomProperties::reflectionScalar
	float ___reflectionScalar_16;
	// System.Single GvrAudio_RoomProperties::reverbGain
	float ___reverbGain_17;
	// System.Single GvrAudio_RoomProperties::reverbTime
	float ___reverbTime_18;
	// System.Single GvrAudio_RoomProperties::reverbBrightness
	float ___reverbBrightness_19;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___positionX_0)); }
	inline float get_positionX_0() const { return ___positionX_0; }
	inline float* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(float value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___positionY_1)); }
	inline float get_positionY_1() const { return ___positionY_1; }
	inline float* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(float value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_positionZ_2() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___positionZ_2)); }
	inline float get_positionZ_2() const { return ___positionZ_2; }
	inline float* get_address_of_positionZ_2() { return &___positionZ_2; }
	inline void set_positionZ_2(float value)
	{
		___positionZ_2 = value;
	}

	inline static int32_t get_offset_of_rotationX_3() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___rotationX_3)); }
	inline float get_rotationX_3() const { return ___rotationX_3; }
	inline float* get_address_of_rotationX_3() { return &___rotationX_3; }
	inline void set_rotationX_3(float value)
	{
		___rotationX_3 = value;
	}

	inline static int32_t get_offset_of_rotationY_4() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___rotationY_4)); }
	inline float get_rotationY_4() const { return ___rotationY_4; }
	inline float* get_address_of_rotationY_4() { return &___rotationY_4; }
	inline void set_rotationY_4(float value)
	{
		___rotationY_4 = value;
	}

	inline static int32_t get_offset_of_rotationZ_5() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___rotationZ_5)); }
	inline float get_rotationZ_5() const { return ___rotationZ_5; }
	inline float* get_address_of_rotationZ_5() { return &___rotationZ_5; }
	inline void set_rotationZ_5(float value)
	{
		___rotationZ_5 = value;
	}

	inline static int32_t get_offset_of_rotationW_6() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___rotationW_6)); }
	inline float get_rotationW_6() const { return ___rotationW_6; }
	inline float* get_address_of_rotationW_6() { return &___rotationW_6; }
	inline void set_rotationW_6(float value)
	{
		___rotationW_6 = value;
	}

	inline static int32_t get_offset_of_dimensionsX_7() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___dimensionsX_7)); }
	inline float get_dimensionsX_7() const { return ___dimensionsX_7; }
	inline float* get_address_of_dimensionsX_7() { return &___dimensionsX_7; }
	inline void set_dimensionsX_7(float value)
	{
		___dimensionsX_7 = value;
	}

	inline static int32_t get_offset_of_dimensionsY_8() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___dimensionsY_8)); }
	inline float get_dimensionsY_8() const { return ___dimensionsY_8; }
	inline float* get_address_of_dimensionsY_8() { return &___dimensionsY_8; }
	inline void set_dimensionsY_8(float value)
	{
		___dimensionsY_8 = value;
	}

	inline static int32_t get_offset_of_dimensionsZ_9() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___dimensionsZ_9)); }
	inline float get_dimensionsZ_9() const { return ___dimensionsZ_9; }
	inline float* get_address_of_dimensionsZ_9() { return &___dimensionsZ_9; }
	inline void set_dimensionsZ_9(float value)
	{
		___dimensionsZ_9 = value;
	}

	inline static int32_t get_offset_of_materialLeft_10() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialLeft_10)); }
	inline int32_t get_materialLeft_10() const { return ___materialLeft_10; }
	inline int32_t* get_address_of_materialLeft_10() { return &___materialLeft_10; }
	inline void set_materialLeft_10(int32_t value)
	{
		___materialLeft_10 = value;
	}

	inline static int32_t get_offset_of_materialRight_11() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialRight_11)); }
	inline int32_t get_materialRight_11() const { return ___materialRight_11; }
	inline int32_t* get_address_of_materialRight_11() { return &___materialRight_11; }
	inline void set_materialRight_11(int32_t value)
	{
		___materialRight_11 = value;
	}

	inline static int32_t get_offset_of_materialBottom_12() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialBottom_12)); }
	inline int32_t get_materialBottom_12() const { return ___materialBottom_12; }
	inline int32_t* get_address_of_materialBottom_12() { return &___materialBottom_12; }
	inline void set_materialBottom_12(int32_t value)
	{
		___materialBottom_12 = value;
	}

	inline static int32_t get_offset_of_materialTop_13() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialTop_13)); }
	inline int32_t get_materialTop_13() const { return ___materialTop_13; }
	inline int32_t* get_address_of_materialTop_13() { return &___materialTop_13; }
	inline void set_materialTop_13(int32_t value)
	{
		___materialTop_13 = value;
	}

	inline static int32_t get_offset_of_materialFront_14() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialFront_14)); }
	inline int32_t get_materialFront_14() const { return ___materialFront_14; }
	inline int32_t* get_address_of_materialFront_14() { return &___materialFront_14; }
	inline void set_materialFront_14(int32_t value)
	{
		___materialFront_14 = value;
	}

	inline static int32_t get_offset_of_materialBack_15() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___materialBack_15)); }
	inline int32_t get_materialBack_15() const { return ___materialBack_15; }
	inline int32_t* get_address_of_materialBack_15() { return &___materialBack_15; }
	inline void set_materialBack_15(int32_t value)
	{
		___materialBack_15 = value;
	}

	inline static int32_t get_offset_of_reflectionScalar_16() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___reflectionScalar_16)); }
	inline float get_reflectionScalar_16() const { return ___reflectionScalar_16; }
	inline float* get_address_of_reflectionScalar_16() { return &___reflectionScalar_16; }
	inline void set_reflectionScalar_16(float value)
	{
		___reflectionScalar_16 = value;
	}

	inline static int32_t get_offset_of_reverbGain_17() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___reverbGain_17)); }
	inline float get_reverbGain_17() const { return ___reverbGain_17; }
	inline float* get_address_of_reverbGain_17() { return &___reverbGain_17; }
	inline void set_reverbGain_17(float value)
	{
		___reverbGain_17 = value;
	}

	inline static int32_t get_offset_of_reverbTime_18() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___reverbTime_18)); }
	inline float get_reverbTime_18() const { return ___reverbTime_18; }
	inline float* get_address_of_reverbTime_18() { return &___reverbTime_18; }
	inline void set_reverbTime_18(float value)
	{
		___reverbTime_18 = value;
	}

	inline static int32_t get_offset_of_reverbBrightness_19() { return static_cast<int32_t>(offsetof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683, ___reverbBrightness_19)); }
	inline float get_reverbBrightness_19() const { return ___reverbBrightness_19; }
	inline float* get_address_of_reverbBrightness_19() { return &___reverbBrightness_19; }
	inline void set_reverbBrightness_19(float value)
	{
		___reverbBrightness_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMPROPERTIES_T38CEF2E715BD4DC611ACB600288C87E43A59A683_H
#ifndef POINTERRAY_T8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED_H
#define POINTERRAY_T8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer_PointerRay
struct  PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED 
{
public:
	// UnityEngine.Ray GvrBasePointer_PointerRay::ray
	Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  ___ray_0;
	// System.Single GvrBasePointer_PointerRay::distanceFromStart
	float ___distanceFromStart_1;
	// System.Single GvrBasePointer_PointerRay::distance
	float ___distance_2;

public:
	inline static int32_t get_offset_of_ray_0() { return static_cast<int32_t>(offsetof(PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED, ___ray_0)); }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  get_ray_0() const { return ___ray_0; }
	inline Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 * get_address_of_ray_0() { return &___ray_0; }
	inline void set_ray_0(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2  value)
	{
		___ray_0 = value;
	}

	inline static int32_t get_offset_of_distanceFromStart_1() { return static_cast<int32_t>(offsetof(PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED, ___distanceFromStart_1)); }
	inline float get_distanceFromStart_1() const { return ___distanceFromStart_1; }
	inline float* get_address_of_distanceFromStart_1() { return &___distanceFromStart_1; }
	inline void set_distanceFromStart_1(float value)
	{
		___distanceFromStart_1 = value;
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERRAY_T8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED_H
#ifndef CONTROLLERDISPLAYSTATE_T656EBDB6918E1D539467855EEE9EF99312CCC00F_H
#define CONTROLLERDISPLAYSTATE_T656EBDB6918E1D539467855EEE9EF99312CCC00F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisual_ControllerDisplayState
struct  ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F 
{
public:
	// GvrControllerBatteryLevel GvrControllerVisual_ControllerDisplayState::batteryLevel
	int32_t ___batteryLevel_0;
	// System.Boolean GvrControllerVisual_ControllerDisplayState::batteryCharging
	bool ___batteryCharging_1;
	// System.Boolean GvrControllerVisual_ControllerDisplayState::clickButton
	bool ___clickButton_2;
	// System.Boolean GvrControllerVisual_ControllerDisplayState::appButton
	bool ___appButton_3;
	// System.Boolean GvrControllerVisual_ControllerDisplayState::homeButton
	bool ___homeButton_4;
	// System.Boolean GvrControllerVisual_ControllerDisplayState::touching
	bool ___touching_5;
	// UnityEngine.Vector2 GvrControllerVisual_ControllerDisplayState::touchPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPos_6;

public:
	inline static int32_t get_offset_of_batteryLevel_0() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___batteryLevel_0)); }
	inline int32_t get_batteryLevel_0() const { return ___batteryLevel_0; }
	inline int32_t* get_address_of_batteryLevel_0() { return &___batteryLevel_0; }
	inline void set_batteryLevel_0(int32_t value)
	{
		___batteryLevel_0 = value;
	}

	inline static int32_t get_offset_of_batteryCharging_1() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___batteryCharging_1)); }
	inline bool get_batteryCharging_1() const { return ___batteryCharging_1; }
	inline bool* get_address_of_batteryCharging_1() { return &___batteryCharging_1; }
	inline void set_batteryCharging_1(bool value)
	{
		___batteryCharging_1 = value;
	}

	inline static int32_t get_offset_of_clickButton_2() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___clickButton_2)); }
	inline bool get_clickButton_2() const { return ___clickButton_2; }
	inline bool* get_address_of_clickButton_2() { return &___clickButton_2; }
	inline void set_clickButton_2(bool value)
	{
		___clickButton_2 = value;
	}

	inline static int32_t get_offset_of_appButton_3() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___appButton_3)); }
	inline bool get_appButton_3() const { return ___appButton_3; }
	inline bool* get_address_of_appButton_3() { return &___appButton_3; }
	inline void set_appButton_3(bool value)
	{
		___appButton_3 = value;
	}

	inline static int32_t get_offset_of_homeButton_4() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___homeButton_4)); }
	inline bool get_homeButton_4() const { return ___homeButton_4; }
	inline bool* get_address_of_homeButton_4() { return &___homeButton_4; }
	inline void set_homeButton_4(bool value)
	{
		___homeButton_4 = value;
	}

	inline static int32_t get_offset_of_touching_5() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___touching_5)); }
	inline bool get_touching_5() const { return ___touching_5; }
	inline bool* get_address_of_touching_5() { return &___touching_5; }
	inline void set_touching_5(bool value)
	{
		___touching_5 = value;
	}

	inline static int32_t get_offset_of_touchPos_6() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F, ___touchPos_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchPos_6() const { return ___touchPos_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchPos_6() { return &___touchPos_6; }
	inline void set_touchPos_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GvrControllerVisual/ControllerDisplayState
struct ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F_marshaled_pinvoke
{
	int32_t ___batteryLevel_0;
	int32_t ___batteryCharging_1;
	int32_t ___clickButton_2;
	int32_t ___appButton_3;
	int32_t ___homeButton_4;
	int32_t ___touching_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPos_6;
};
// Native definition for COM marshalling of GvrControllerVisual/ControllerDisplayState
struct ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F_marshaled_com
{
	int32_t ___batteryLevel_0;
	int32_t ___batteryCharging_1;
	int32_t ___clickButton_2;
	int32_t ___appButton_3;
	int32_t ___homeButton_4;
	int32_t ___touching_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchPos_6;
};
#endif // CONTROLLERDISPLAYSTATE_T656EBDB6918E1D539467855EEE9EF99312CCC00F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ONSTATECHANGEDEVENT_TA3E765C2974C2B6A1E9EE90009E5C437ADE3319F_H
#define ONSTATECHANGEDEVENT_TA3E765C2974C2B6A1E9EE90009E5C437ADE3319F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput_OnStateChangedEvent
struct  OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGEDEVENT_TA3E765C2974C2B6A1E9EE90009E5C437ADE3319F_H
#ifndef EVENTDELEGATE_TA7EE228FD3A939EF5CCB9EE55F0A66907AB18995_H
#define EVENTDELEGATE_TA7EE228FD3A939EF5CCB9EE55F0A66907AB18995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor_EventDelegate
struct  EventDelegate_tA7EE228FD3A939EF5CCB9EE55F0A66907AB18995  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_TA7EE228FD3A939EF5CCB9EE55F0A66907AB18995_H
#ifndef GETPOINTFORDISTANCEDELEGATE_T42D8A94340A612C14C21D3F9B20BE15A9306F869_H
#define GETPOINTFORDISTANCEDELEGATE_T42D8A94340A612C14C21D3F9B20BE15A9306F869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserVisual_GetPointForDistanceDelegate
struct  GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPOINTFORDISTANCEDELEGATE_T42D8A94340A612C14C21D3F9B20BE15A9306F869_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ANIMALSCONTROLLER_T43F4FC976544E7B722CF2B1782037E4E22B5E16C_H
#define ANIMALSCONTROLLER_T43F4FC976544E7B722CF2B1782037E4E22B5E16C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimalsController
struct  AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform AnimalsController::player
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___player_4;
	// UnityEngine.Transform AnimalsController::originalLocation
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___originalLocation_5;
	// System.Single AnimalsController::buttontimer
	float ___buttontimer_6;
	// System.Boolean AnimalsController::buttontime
	bool ___buttontime_7;
	// System.Double AnimalsController::arrivedPosition
	double ___arrivedPosition_9;
	// System.Double AnimalsController::Runtimer
	double ___Runtimer_10;
	// System.Boolean AnimalsController::walk
	bool ___walk_11;
	// System.Double AnimalsController::backtimer
	double ___backtimer_12;
	// System.Boolean AnimalsController::isConfirmButton
	bool ___isConfirmButton_13;
	// System.Boolean AnimalsController::isSoundButton
	bool ___isSoundButton_14;
	// System.Boolean AnimalsController::isTranslateButton
	bool ___isTranslateButton_15;
	// System.Boolean AnimalsController::isBack
	bool ___isBack_16;
	// System.Boolean AnimalsController::isEng
	bool ___isEng_17;
	// UnityEngine.UI.Image AnimalsController::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_18;
	// UnityEngine.GameObject AnimalsController::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_19;
	// UnityEngine.GameObject AnimalsController::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_20;
	// UnityEngine.GameObject AnimalsController::obj_animals
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_animals_21;
	// UnityEngine.GameObject AnimalsController::obj_original
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_original_22;
	// UnityEngine.GameObject AnimalsController::obj_infocanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_infocanvas_23;
	// UnityEngine.UI.Text AnimalsController::gui_title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_title_24;
	// UnityEngine.UI.Text AnimalsController::gui_message
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_message_25;
	// System.String AnimalsController::str_title
	String_t* ___str_title_26;
	// System.String AnimalsController::str_title_chi
	String_t* ___str_title_chi_27;
	// System.String AnimalsController::str_message
	String_t* ___str_message_28;
	// System.String AnimalsController::str_message_chi
	String_t* ___str_message_chi_29;
	// UnityEngine.AudioClip AnimalsController::sound_pronunciation
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_pronunciation_30;
	// UnityEngine.AudioClip AnimalsController::sound_animal
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_animal_31;
	// UnityEngine.Animator AnimalsController::Anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___Anim_32;
	// UnityEngine.AnimatorStateInfo AnimalsController::BS
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___BS_33;
	// UnityEngine.Animation AnimalsController::anima_tion
	Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * ___anima_tion_34;
	// System.String AnimalsController::str_animation_walk
	String_t* ___str_animation_walk_35;
	// System.String AnimalsController::str_animation_idle
	String_t* ___str_animation_idle_36;
	// System.String AnimalsController::sceneName
	String_t* ___sceneName_37;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___player_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_player_4() const { return ___player_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_originalLocation_5() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___originalLocation_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_originalLocation_5() const { return ___originalLocation_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_originalLocation_5() { return &___originalLocation_5; }
	inline void set_originalLocation_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___originalLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalLocation_5), value);
	}

	inline static int32_t get_offset_of_buttontimer_6() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___buttontimer_6)); }
	inline float get_buttontimer_6() const { return ___buttontimer_6; }
	inline float* get_address_of_buttontimer_6() { return &___buttontimer_6; }
	inline void set_buttontimer_6(float value)
	{
		___buttontimer_6 = value;
	}

	inline static int32_t get_offset_of_buttontime_7() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___buttontime_7)); }
	inline bool get_buttontime_7() const { return ___buttontime_7; }
	inline bool* get_address_of_buttontime_7() { return &___buttontime_7; }
	inline void set_buttontime_7(bool value)
	{
		___buttontime_7 = value;
	}

	inline static int32_t get_offset_of_arrivedPosition_9() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___arrivedPosition_9)); }
	inline double get_arrivedPosition_9() const { return ___arrivedPosition_9; }
	inline double* get_address_of_arrivedPosition_9() { return &___arrivedPosition_9; }
	inline void set_arrivedPosition_9(double value)
	{
		___arrivedPosition_9 = value;
	}

	inline static int32_t get_offset_of_Runtimer_10() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___Runtimer_10)); }
	inline double get_Runtimer_10() const { return ___Runtimer_10; }
	inline double* get_address_of_Runtimer_10() { return &___Runtimer_10; }
	inline void set_Runtimer_10(double value)
	{
		___Runtimer_10 = value;
	}

	inline static int32_t get_offset_of_walk_11() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___walk_11)); }
	inline bool get_walk_11() const { return ___walk_11; }
	inline bool* get_address_of_walk_11() { return &___walk_11; }
	inline void set_walk_11(bool value)
	{
		___walk_11 = value;
	}

	inline static int32_t get_offset_of_backtimer_12() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___backtimer_12)); }
	inline double get_backtimer_12() const { return ___backtimer_12; }
	inline double* get_address_of_backtimer_12() { return &___backtimer_12; }
	inline void set_backtimer_12(double value)
	{
		___backtimer_12 = value;
	}

	inline static int32_t get_offset_of_isConfirmButton_13() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___isConfirmButton_13)); }
	inline bool get_isConfirmButton_13() const { return ___isConfirmButton_13; }
	inline bool* get_address_of_isConfirmButton_13() { return &___isConfirmButton_13; }
	inline void set_isConfirmButton_13(bool value)
	{
		___isConfirmButton_13 = value;
	}

	inline static int32_t get_offset_of_isSoundButton_14() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___isSoundButton_14)); }
	inline bool get_isSoundButton_14() const { return ___isSoundButton_14; }
	inline bool* get_address_of_isSoundButton_14() { return &___isSoundButton_14; }
	inline void set_isSoundButton_14(bool value)
	{
		___isSoundButton_14 = value;
	}

	inline static int32_t get_offset_of_isTranslateButton_15() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___isTranslateButton_15)); }
	inline bool get_isTranslateButton_15() const { return ___isTranslateButton_15; }
	inline bool* get_address_of_isTranslateButton_15() { return &___isTranslateButton_15; }
	inline void set_isTranslateButton_15(bool value)
	{
		___isTranslateButton_15 = value;
	}

	inline static int32_t get_offset_of_isBack_16() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___isBack_16)); }
	inline bool get_isBack_16() const { return ___isBack_16; }
	inline bool* get_address_of_isBack_16() { return &___isBack_16; }
	inline void set_isBack_16(bool value)
	{
		___isBack_16 = value;
	}

	inline static int32_t get_offset_of_isEng_17() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___isEng_17)); }
	inline bool get_isEng_17() const { return ___isEng_17; }
	inline bool* get_address_of_isEng_17() { return &___isEng_17; }
	inline void set_isEng_17(bool value)
	{
		___isEng_17 = value;
	}

	inline static int32_t get_offset_of_Bar_18() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___Bar_18)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_18() const { return ___Bar_18; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_18() { return &___Bar_18; }
	inline void set_Bar_18(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_18 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_18), value);
	}

	inline static int32_t get_offset_of_obj_Bar_19() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___obj_Bar_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_19() const { return ___obj_Bar_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_19() { return &___obj_Bar_19; }
	inline void set_obj_Bar_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_19 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_19), value);
	}

	inline static int32_t get_offset_of_obj_shoot_20() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___obj_shoot_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_20() const { return ___obj_shoot_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_20() { return &___obj_shoot_20; }
	inline void set_obj_shoot_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_20), value);
	}

	inline static int32_t get_offset_of_obj_animals_21() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___obj_animals_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_animals_21() const { return ___obj_animals_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_animals_21() { return &___obj_animals_21; }
	inline void set_obj_animals_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_animals_21 = value;
		Il2CppCodeGenWriteBarrier((&___obj_animals_21), value);
	}

	inline static int32_t get_offset_of_obj_original_22() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___obj_original_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_original_22() const { return ___obj_original_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_original_22() { return &___obj_original_22; }
	inline void set_obj_original_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_original_22 = value;
		Il2CppCodeGenWriteBarrier((&___obj_original_22), value);
	}

	inline static int32_t get_offset_of_obj_infocanvas_23() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___obj_infocanvas_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_infocanvas_23() const { return ___obj_infocanvas_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_infocanvas_23() { return &___obj_infocanvas_23; }
	inline void set_obj_infocanvas_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_infocanvas_23 = value;
		Il2CppCodeGenWriteBarrier((&___obj_infocanvas_23), value);
	}

	inline static int32_t get_offset_of_gui_title_24() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___gui_title_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_title_24() const { return ___gui_title_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_title_24() { return &___gui_title_24; }
	inline void set_gui_title_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_title_24 = value;
		Il2CppCodeGenWriteBarrier((&___gui_title_24), value);
	}

	inline static int32_t get_offset_of_gui_message_25() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___gui_message_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_message_25() const { return ___gui_message_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_message_25() { return &___gui_message_25; }
	inline void set_gui_message_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_message_25 = value;
		Il2CppCodeGenWriteBarrier((&___gui_message_25), value);
	}

	inline static int32_t get_offset_of_str_title_26() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_title_26)); }
	inline String_t* get_str_title_26() const { return ___str_title_26; }
	inline String_t** get_address_of_str_title_26() { return &___str_title_26; }
	inline void set_str_title_26(String_t* value)
	{
		___str_title_26 = value;
		Il2CppCodeGenWriteBarrier((&___str_title_26), value);
	}

	inline static int32_t get_offset_of_str_title_chi_27() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_title_chi_27)); }
	inline String_t* get_str_title_chi_27() const { return ___str_title_chi_27; }
	inline String_t** get_address_of_str_title_chi_27() { return &___str_title_chi_27; }
	inline void set_str_title_chi_27(String_t* value)
	{
		___str_title_chi_27 = value;
		Il2CppCodeGenWriteBarrier((&___str_title_chi_27), value);
	}

	inline static int32_t get_offset_of_str_message_28() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_message_28)); }
	inline String_t* get_str_message_28() const { return ___str_message_28; }
	inline String_t** get_address_of_str_message_28() { return &___str_message_28; }
	inline void set_str_message_28(String_t* value)
	{
		___str_message_28 = value;
		Il2CppCodeGenWriteBarrier((&___str_message_28), value);
	}

	inline static int32_t get_offset_of_str_message_chi_29() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_message_chi_29)); }
	inline String_t* get_str_message_chi_29() const { return ___str_message_chi_29; }
	inline String_t** get_address_of_str_message_chi_29() { return &___str_message_chi_29; }
	inline void set_str_message_chi_29(String_t* value)
	{
		___str_message_chi_29 = value;
		Il2CppCodeGenWriteBarrier((&___str_message_chi_29), value);
	}

	inline static int32_t get_offset_of_sound_pronunciation_30() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___sound_pronunciation_30)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_pronunciation_30() const { return ___sound_pronunciation_30; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_pronunciation_30() { return &___sound_pronunciation_30; }
	inline void set_sound_pronunciation_30(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_pronunciation_30 = value;
		Il2CppCodeGenWriteBarrier((&___sound_pronunciation_30), value);
	}

	inline static int32_t get_offset_of_sound_animal_31() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___sound_animal_31)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_animal_31() const { return ___sound_animal_31; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_animal_31() { return &___sound_animal_31; }
	inline void set_sound_animal_31(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_animal_31 = value;
		Il2CppCodeGenWriteBarrier((&___sound_animal_31), value);
	}

	inline static int32_t get_offset_of_Anim_32() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___Anim_32)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_Anim_32() const { return ___Anim_32; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_Anim_32() { return &___Anim_32; }
	inline void set_Anim_32(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___Anim_32 = value;
		Il2CppCodeGenWriteBarrier((&___Anim_32), value);
	}

	inline static int32_t get_offset_of_BS_33() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___BS_33)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_BS_33() const { return ___BS_33; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_BS_33() { return &___BS_33; }
	inline void set_BS_33(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___BS_33 = value;
	}

	inline static int32_t get_offset_of_anima_tion_34() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___anima_tion_34)); }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * get_anima_tion_34() const { return ___anima_tion_34; }
	inline Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C ** get_address_of_anima_tion_34() { return &___anima_tion_34; }
	inline void set_anima_tion_34(Animation_tCFC171459D159DDEC6500B55543A76219D49BB9C * value)
	{
		___anima_tion_34 = value;
		Il2CppCodeGenWriteBarrier((&___anima_tion_34), value);
	}

	inline static int32_t get_offset_of_str_animation_walk_35() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_animation_walk_35)); }
	inline String_t* get_str_animation_walk_35() const { return ___str_animation_walk_35; }
	inline String_t** get_address_of_str_animation_walk_35() { return &___str_animation_walk_35; }
	inline void set_str_animation_walk_35(String_t* value)
	{
		___str_animation_walk_35 = value;
		Il2CppCodeGenWriteBarrier((&___str_animation_walk_35), value);
	}

	inline static int32_t get_offset_of_str_animation_idle_36() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___str_animation_idle_36)); }
	inline String_t* get_str_animation_idle_36() const { return ___str_animation_idle_36; }
	inline String_t** get_address_of_str_animation_idle_36() { return &___str_animation_idle_36; }
	inline void set_str_animation_idle_36(String_t* value)
	{
		___str_animation_idle_36 = value;
		Il2CppCodeGenWriteBarrier((&___str_animation_idle_36), value);
	}

	inline static int32_t get_offset_of_sceneName_37() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C, ___sceneName_37)); }
	inline String_t* get_sceneName_37() const { return ___sceneName_37; }
	inline String_t** get_address_of_sceneName_37() { return &___sceneName_37; }
	inline void set_sceneName_37(String_t* value)
	{
		___sceneName_37 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_37), value);
	}
};

struct AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C_StaticFields
{
public:
	// System.Boolean AnimalsController::isEnable
	bool ___isEnable_8;

public:
	inline static int32_t get_offset_of_isEnable_8() { return static_cast<int32_t>(offsetof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C_StaticFields, ___isEnable_8)); }
	inline bool get_isEnable_8() const { return ___isEnable_8; }
	inline bool* get_address_of_isEnable_8() { return &___isEnable_8; }
	inline void set_isEnable_8(bool value)
	{
		___isEnable_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMALSCONTROLLER_T43F4FC976544E7B722CF2B1782037E4E22B5E16C_H
#ifndef ARIDAREAMENU_T9B77C362683097C83FCD5F3E8DF516BD705CFC18_H
#define ARIDAREAMENU_T9B77C362683097C83FCD5F3E8DF516BD705CFC18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AridAreaMenu
struct  AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single AridAreaMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean AridAreaMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image AridAreaMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject AridAreaMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject AridAreaMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean AridAreaMenu::isQuiz
	bool ___isQuiz_9;
	// System.Boolean AridAreaMenu::isMid
	bool ___isMid_10;
	// System.Boolean AridAreaMenu::isLow
	bool ___isLow_11;
	// UnityEngine.GameObject AridAreaMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_12;
	// System.Int32 AridAreaMenu::restTime
	int32_t ___restTime_13;
	// System.Single AridAreaMenu::remindertimer
	float ___remindertimer_14;
	// System.Boolean AridAreaMenu::remindertime
	bool ___remindertime_15;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isQuiz_9() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___isQuiz_9)); }
	inline bool get_isQuiz_9() const { return ___isQuiz_9; }
	inline bool* get_address_of_isQuiz_9() { return &___isQuiz_9; }
	inline void set_isQuiz_9(bool value)
	{
		___isQuiz_9 = value;
	}

	inline static int32_t get_offset_of_isMid_10() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___isMid_10)); }
	inline bool get_isMid_10() const { return ___isMid_10; }
	inline bool* get_address_of_isMid_10() { return &___isMid_10; }
	inline void set_isMid_10(bool value)
	{
		___isMid_10 = value;
	}

	inline static int32_t get_offset_of_isLow_11() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___isLow_11)); }
	inline bool get_isLow_11() const { return ___isLow_11; }
	inline bool* get_address_of_isLow_11() { return &___isLow_11; }
	inline void set_isLow_11(bool value)
	{
		___isLow_11 = value;
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_12() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___obj_reminderCanvas_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_12() const { return ___obj_reminderCanvas_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_12() { return &___obj_reminderCanvas_12; }
	inline void set_obj_reminderCanvas_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_12), value);
	}

	inline static int32_t get_offset_of_restTime_13() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___restTime_13)); }
	inline int32_t get_restTime_13() const { return ___restTime_13; }
	inline int32_t* get_address_of_restTime_13() { return &___restTime_13; }
	inline void set_restTime_13(int32_t value)
	{
		___restTime_13 = value;
	}

	inline static int32_t get_offset_of_remindertimer_14() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___remindertimer_14)); }
	inline float get_remindertimer_14() const { return ___remindertimer_14; }
	inline float* get_address_of_remindertimer_14() { return &___remindertimer_14; }
	inline void set_remindertimer_14(float value)
	{
		___remindertimer_14 = value;
	}

	inline static int32_t get_offset_of_remindertime_15() { return static_cast<int32_t>(offsetof(AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18, ___remindertime_15)); }
	inline bool get_remindertime_15() const { return ___remindertime_15; }
	inline bool* get_address_of_remindertime_15() { return &___remindertime_15; }
	inline void set_remindertime_15(bool value)
	{
		___remindertime_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARIDAREAMENU_T9B77C362683097C83FCD5F3E8DF516BD705CFC18_H
#ifndef ARIDAREAQUIZCONTROLLER_T51ABC34B4230DB477242891DCFF108B1BF431F5E_H
#define ARIDAREAQUIZCONTROLLER_T51ABC34B4230DB477242891DCFF108B1BF431F5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AridAreaQuizController
struct  AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Int32> AridAreaQuizController::questionQueueList
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___questionQueueList_4;
	// System.Collections.Generic.List`1<QuestionList> AridAreaQuizController::questionLists
	List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * ___questionLists_5;
	// System.Int32 AridAreaQuizController::limitTime
	int32_t ___limitTime_6;
	// System.Single AridAreaQuizController::time
	float ___time_7;
	// System.Int32 AridAreaQuizController::currentQuestionNum
	int32_t ___currentQuestionNum_8;
	// System.Int32 AridAreaQuizController::correctCount
	int32_t ___correctCount_9;
	// System.Single AridAreaQuizController::buttontimer
	float ___buttontimer_10;
	// System.Boolean AridAreaQuizController::buttontime
	bool ___buttontime_11;
	// System.Boolean AridAreaQuizController::isSelectA
	bool ___isSelectA_12;
	// System.Boolean AridAreaQuizController::isSelectB
	bool ___isSelectB_13;
	// System.Boolean AridAreaQuizController::isSelectC
	bool ___isSelectC_14;
	// System.Boolean AridAreaQuizController::isSelectD
	bool ___isSelectD_15;
	// System.Boolean AridAreaQuizController::isStart
	bool ___isStart_16;
	// System.Boolean AridAreaQuizController::isResumebutton
	bool ___isResumebutton_17;
	// System.Boolean AridAreaQuizController::isPausebutton
	bool ___isPausebutton_18;
	// System.Boolean AridAreaQuizController::isStartbutton
	bool ___isStartbutton_19;
	// System.Boolean AridAreaQuizController::isConfirmbutton
	bool ___isConfirmbutton_20;
	// System.Boolean AridAreaQuizController::isQuitbutton
	bool ___isQuitbutton_21;
	// System.Int32 AridAreaQuizController::animalsCount
	int32_t ___animalsCount_22;
	// System.Boolean AridAreaQuizController::canAnswer
	bool ___canAnswer_23;
	// UnityEngine.GameObject AridAreaQuizController::obj_Kangaroo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Kangaroo_24;
	// UnityEngine.GameObject AridAreaQuizController::obj_TasmanianDevil
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_TasmanianDevil_25;
	// UnityEngine.GameObject AridAreaQuizController::obj_Koala
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Koala_26;
	// UnityEngine.GameObject AridAreaQuizController::obj_Ornitorinco
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ornitorinco_27;
	// UnityEngine.GameObject AridAreaQuizController::obj_Ostrich
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ostrich_28;
	// UnityEngine.GameObject AridAreaQuizController::obj_LizardCollar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_LizardCollar_29;
	// UnityEngine.GameObject AridAreaQuizController::obj_Scorpion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Scorpion_30;
	// UnityEngine.GameObject AridAreaQuizController::obj_Panther
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Panther_31;
	// UnityEngine.GameObject AridAreaQuizController::obj_Giraffe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Giraffe_32;
	// UnityEngine.GameObject AridAreaQuizController::obj_Rhinoceros
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Rhinoceros_33;
	// UnityEngine.GameObject AridAreaQuizController::obj_Zebra
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Zebra_34;
	// UnityEngine.UI.Text AridAreaQuizController::gui_time
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_time_35;
	// UnityEngine.UI.Text AridAreaQuizController::gui_round
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_round_36;
	// UnityEngine.UI.Image AridAreaQuizController::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_37;
	// UnityEngine.GameObject AridAreaQuizController::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_38;
	// UnityEngine.GameObject AridAreaQuizController::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_39;
	// UnityEngine.GameObject AridAreaQuizController::canvas_question
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_question_40;
	// UnityEngine.GameObject AridAreaQuizController::canvas_pause
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_pause_41;
	// UnityEngine.GameObject AridAreaQuizController::obj_btn_pause
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_btn_pause_42;
	// UnityEngine.GameObject AridAreaQuizController::obj_start_menu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_start_menu_43;
	// UnityEngine.UI.Text AridAreaQuizController::gui_mark
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_mark_44;
	// UnityEngine.GameObject AridAreaQuizController::canvas_finish
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_finish_45;
	// UnityEngine.UI.Text AridAreaQuizController::gui_choiceA
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceA_46;
	// UnityEngine.UI.Text AridAreaQuizController::gui_choiceB
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceB_47;
	// UnityEngine.UI.Text AridAreaQuizController::gui_choiceC
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceC_48;
	// UnityEngine.UI.Text AridAreaQuizController::gui_choiceD
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceD_49;
	// UnityEngine.GameObject AridAreaQuizController::btn_choiceA
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceA_50;
	// UnityEngine.GameObject AridAreaQuizController::btn_choiceB
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceB_51;
	// UnityEngine.GameObject AridAreaQuizController::btn_choiceC
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceC_52;
	// UnityEngine.GameObject AridAreaQuizController::btn_choiceD
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceD_53;

public:
	inline static int32_t get_offset_of_questionQueueList_4() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___questionQueueList_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_questionQueueList_4() const { return ___questionQueueList_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_questionQueueList_4() { return &___questionQueueList_4; }
	inline void set_questionQueueList_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___questionQueueList_4 = value;
		Il2CppCodeGenWriteBarrier((&___questionQueueList_4), value);
	}

	inline static int32_t get_offset_of_questionLists_5() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___questionLists_5)); }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * get_questionLists_5() const { return ___questionLists_5; }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D ** get_address_of_questionLists_5() { return &___questionLists_5; }
	inline void set_questionLists_5(List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * value)
	{
		___questionLists_5 = value;
		Il2CppCodeGenWriteBarrier((&___questionLists_5), value);
	}

	inline static int32_t get_offset_of_limitTime_6() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___limitTime_6)); }
	inline int32_t get_limitTime_6() const { return ___limitTime_6; }
	inline int32_t* get_address_of_limitTime_6() { return &___limitTime_6; }
	inline void set_limitTime_6(int32_t value)
	{
		___limitTime_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_currentQuestionNum_8() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___currentQuestionNum_8)); }
	inline int32_t get_currentQuestionNum_8() const { return ___currentQuestionNum_8; }
	inline int32_t* get_address_of_currentQuestionNum_8() { return &___currentQuestionNum_8; }
	inline void set_currentQuestionNum_8(int32_t value)
	{
		___currentQuestionNum_8 = value;
	}

	inline static int32_t get_offset_of_correctCount_9() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___correctCount_9)); }
	inline int32_t get_correctCount_9() const { return ___correctCount_9; }
	inline int32_t* get_address_of_correctCount_9() { return &___correctCount_9; }
	inline void set_correctCount_9(int32_t value)
	{
		___correctCount_9 = value;
	}

	inline static int32_t get_offset_of_buttontimer_10() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___buttontimer_10)); }
	inline float get_buttontimer_10() const { return ___buttontimer_10; }
	inline float* get_address_of_buttontimer_10() { return &___buttontimer_10; }
	inline void set_buttontimer_10(float value)
	{
		___buttontimer_10 = value;
	}

	inline static int32_t get_offset_of_buttontime_11() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___buttontime_11)); }
	inline bool get_buttontime_11() const { return ___buttontime_11; }
	inline bool* get_address_of_buttontime_11() { return &___buttontime_11; }
	inline void set_buttontime_11(bool value)
	{
		___buttontime_11 = value;
	}

	inline static int32_t get_offset_of_isSelectA_12() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isSelectA_12)); }
	inline bool get_isSelectA_12() const { return ___isSelectA_12; }
	inline bool* get_address_of_isSelectA_12() { return &___isSelectA_12; }
	inline void set_isSelectA_12(bool value)
	{
		___isSelectA_12 = value;
	}

	inline static int32_t get_offset_of_isSelectB_13() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isSelectB_13)); }
	inline bool get_isSelectB_13() const { return ___isSelectB_13; }
	inline bool* get_address_of_isSelectB_13() { return &___isSelectB_13; }
	inline void set_isSelectB_13(bool value)
	{
		___isSelectB_13 = value;
	}

	inline static int32_t get_offset_of_isSelectC_14() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isSelectC_14)); }
	inline bool get_isSelectC_14() const { return ___isSelectC_14; }
	inline bool* get_address_of_isSelectC_14() { return &___isSelectC_14; }
	inline void set_isSelectC_14(bool value)
	{
		___isSelectC_14 = value;
	}

	inline static int32_t get_offset_of_isSelectD_15() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isSelectD_15)); }
	inline bool get_isSelectD_15() const { return ___isSelectD_15; }
	inline bool* get_address_of_isSelectD_15() { return &___isSelectD_15; }
	inline void set_isSelectD_15(bool value)
	{
		___isSelectD_15 = value;
	}

	inline static int32_t get_offset_of_isStart_16() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isStart_16)); }
	inline bool get_isStart_16() const { return ___isStart_16; }
	inline bool* get_address_of_isStart_16() { return &___isStart_16; }
	inline void set_isStart_16(bool value)
	{
		___isStart_16 = value;
	}

	inline static int32_t get_offset_of_isResumebutton_17() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isResumebutton_17)); }
	inline bool get_isResumebutton_17() const { return ___isResumebutton_17; }
	inline bool* get_address_of_isResumebutton_17() { return &___isResumebutton_17; }
	inline void set_isResumebutton_17(bool value)
	{
		___isResumebutton_17 = value;
	}

	inline static int32_t get_offset_of_isPausebutton_18() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isPausebutton_18)); }
	inline bool get_isPausebutton_18() const { return ___isPausebutton_18; }
	inline bool* get_address_of_isPausebutton_18() { return &___isPausebutton_18; }
	inline void set_isPausebutton_18(bool value)
	{
		___isPausebutton_18 = value;
	}

	inline static int32_t get_offset_of_isStartbutton_19() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isStartbutton_19)); }
	inline bool get_isStartbutton_19() const { return ___isStartbutton_19; }
	inline bool* get_address_of_isStartbutton_19() { return &___isStartbutton_19; }
	inline void set_isStartbutton_19(bool value)
	{
		___isStartbutton_19 = value;
	}

	inline static int32_t get_offset_of_isConfirmbutton_20() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isConfirmbutton_20)); }
	inline bool get_isConfirmbutton_20() const { return ___isConfirmbutton_20; }
	inline bool* get_address_of_isConfirmbutton_20() { return &___isConfirmbutton_20; }
	inline void set_isConfirmbutton_20(bool value)
	{
		___isConfirmbutton_20 = value;
	}

	inline static int32_t get_offset_of_isQuitbutton_21() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___isQuitbutton_21)); }
	inline bool get_isQuitbutton_21() const { return ___isQuitbutton_21; }
	inline bool* get_address_of_isQuitbutton_21() { return &___isQuitbutton_21; }
	inline void set_isQuitbutton_21(bool value)
	{
		___isQuitbutton_21 = value;
	}

	inline static int32_t get_offset_of_animalsCount_22() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___animalsCount_22)); }
	inline int32_t get_animalsCount_22() const { return ___animalsCount_22; }
	inline int32_t* get_address_of_animalsCount_22() { return &___animalsCount_22; }
	inline void set_animalsCount_22(int32_t value)
	{
		___animalsCount_22 = value;
	}

	inline static int32_t get_offset_of_canAnswer_23() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___canAnswer_23)); }
	inline bool get_canAnswer_23() const { return ___canAnswer_23; }
	inline bool* get_address_of_canAnswer_23() { return &___canAnswer_23; }
	inline void set_canAnswer_23(bool value)
	{
		___canAnswer_23 = value;
	}

	inline static int32_t get_offset_of_obj_Kangaroo_24() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Kangaroo_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Kangaroo_24() const { return ___obj_Kangaroo_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Kangaroo_24() { return &___obj_Kangaroo_24; }
	inline void set_obj_Kangaroo_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Kangaroo_24 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Kangaroo_24), value);
	}

	inline static int32_t get_offset_of_obj_TasmanianDevil_25() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_TasmanianDevil_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_TasmanianDevil_25() const { return ___obj_TasmanianDevil_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_TasmanianDevil_25() { return &___obj_TasmanianDevil_25; }
	inline void set_obj_TasmanianDevil_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_TasmanianDevil_25 = value;
		Il2CppCodeGenWriteBarrier((&___obj_TasmanianDevil_25), value);
	}

	inline static int32_t get_offset_of_obj_Koala_26() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Koala_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Koala_26() const { return ___obj_Koala_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Koala_26() { return &___obj_Koala_26; }
	inline void set_obj_Koala_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Koala_26 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Koala_26), value);
	}

	inline static int32_t get_offset_of_obj_Ornitorinco_27() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Ornitorinco_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ornitorinco_27() const { return ___obj_Ornitorinco_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ornitorinco_27() { return &___obj_Ornitorinco_27; }
	inline void set_obj_Ornitorinco_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ornitorinco_27 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ornitorinco_27), value);
	}

	inline static int32_t get_offset_of_obj_Ostrich_28() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Ostrich_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ostrich_28() const { return ___obj_Ostrich_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ostrich_28() { return &___obj_Ostrich_28; }
	inline void set_obj_Ostrich_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ostrich_28 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ostrich_28), value);
	}

	inline static int32_t get_offset_of_obj_LizardCollar_29() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_LizardCollar_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_LizardCollar_29() const { return ___obj_LizardCollar_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_LizardCollar_29() { return &___obj_LizardCollar_29; }
	inline void set_obj_LizardCollar_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_LizardCollar_29 = value;
		Il2CppCodeGenWriteBarrier((&___obj_LizardCollar_29), value);
	}

	inline static int32_t get_offset_of_obj_Scorpion_30() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Scorpion_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Scorpion_30() const { return ___obj_Scorpion_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Scorpion_30() { return &___obj_Scorpion_30; }
	inline void set_obj_Scorpion_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Scorpion_30 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Scorpion_30), value);
	}

	inline static int32_t get_offset_of_obj_Panther_31() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Panther_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Panther_31() const { return ___obj_Panther_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Panther_31() { return &___obj_Panther_31; }
	inline void set_obj_Panther_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Panther_31 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Panther_31), value);
	}

	inline static int32_t get_offset_of_obj_Giraffe_32() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Giraffe_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Giraffe_32() const { return ___obj_Giraffe_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Giraffe_32() { return &___obj_Giraffe_32; }
	inline void set_obj_Giraffe_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Giraffe_32 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Giraffe_32), value);
	}

	inline static int32_t get_offset_of_obj_Rhinoceros_33() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Rhinoceros_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Rhinoceros_33() const { return ___obj_Rhinoceros_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Rhinoceros_33() { return &___obj_Rhinoceros_33; }
	inline void set_obj_Rhinoceros_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Rhinoceros_33 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Rhinoceros_33), value);
	}

	inline static int32_t get_offset_of_obj_Zebra_34() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Zebra_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Zebra_34() const { return ___obj_Zebra_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Zebra_34() { return &___obj_Zebra_34; }
	inline void set_obj_Zebra_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Zebra_34 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Zebra_34), value);
	}

	inline static int32_t get_offset_of_gui_time_35() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_time_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_time_35() const { return ___gui_time_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_time_35() { return &___gui_time_35; }
	inline void set_gui_time_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_time_35 = value;
		Il2CppCodeGenWriteBarrier((&___gui_time_35), value);
	}

	inline static int32_t get_offset_of_gui_round_36() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_round_36)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_round_36() const { return ___gui_round_36; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_round_36() { return &___gui_round_36; }
	inline void set_gui_round_36(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_round_36 = value;
		Il2CppCodeGenWriteBarrier((&___gui_round_36), value);
	}

	inline static int32_t get_offset_of_Bar_37() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___Bar_37)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_37() const { return ___Bar_37; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_37() { return &___Bar_37; }
	inline void set_Bar_37(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_37 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_37), value);
	}

	inline static int32_t get_offset_of_obj_Bar_38() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_Bar_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_38() const { return ___obj_Bar_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_38() { return &___obj_Bar_38; }
	inline void set_obj_Bar_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_38 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_38), value);
	}

	inline static int32_t get_offset_of_obj_shoot_39() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_shoot_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_39() const { return ___obj_shoot_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_39() { return &___obj_shoot_39; }
	inline void set_obj_shoot_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_39 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_39), value);
	}

	inline static int32_t get_offset_of_canvas_question_40() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___canvas_question_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_question_40() const { return ___canvas_question_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_question_40() { return &___canvas_question_40; }
	inline void set_canvas_question_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_question_40 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_question_40), value);
	}

	inline static int32_t get_offset_of_canvas_pause_41() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___canvas_pause_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_pause_41() const { return ___canvas_pause_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_pause_41() { return &___canvas_pause_41; }
	inline void set_canvas_pause_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_pause_41 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_pause_41), value);
	}

	inline static int32_t get_offset_of_obj_btn_pause_42() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_btn_pause_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_btn_pause_42() const { return ___obj_btn_pause_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_btn_pause_42() { return &___obj_btn_pause_42; }
	inline void set_obj_btn_pause_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_btn_pause_42 = value;
		Il2CppCodeGenWriteBarrier((&___obj_btn_pause_42), value);
	}

	inline static int32_t get_offset_of_obj_start_menu_43() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___obj_start_menu_43)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_start_menu_43() const { return ___obj_start_menu_43; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_start_menu_43() { return &___obj_start_menu_43; }
	inline void set_obj_start_menu_43(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_start_menu_43 = value;
		Il2CppCodeGenWriteBarrier((&___obj_start_menu_43), value);
	}

	inline static int32_t get_offset_of_gui_mark_44() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_mark_44)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_mark_44() const { return ___gui_mark_44; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_mark_44() { return &___gui_mark_44; }
	inline void set_gui_mark_44(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_mark_44 = value;
		Il2CppCodeGenWriteBarrier((&___gui_mark_44), value);
	}

	inline static int32_t get_offset_of_canvas_finish_45() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___canvas_finish_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_finish_45() const { return ___canvas_finish_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_finish_45() { return &___canvas_finish_45; }
	inline void set_canvas_finish_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_finish_45 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_finish_45), value);
	}

	inline static int32_t get_offset_of_gui_choiceA_46() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_choiceA_46)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceA_46() const { return ___gui_choiceA_46; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceA_46() { return &___gui_choiceA_46; }
	inline void set_gui_choiceA_46(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceA_46 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceA_46), value);
	}

	inline static int32_t get_offset_of_gui_choiceB_47() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_choiceB_47)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceB_47() const { return ___gui_choiceB_47; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceB_47() { return &___gui_choiceB_47; }
	inline void set_gui_choiceB_47(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceB_47 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceB_47), value);
	}

	inline static int32_t get_offset_of_gui_choiceC_48() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_choiceC_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceC_48() const { return ___gui_choiceC_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceC_48() { return &___gui_choiceC_48; }
	inline void set_gui_choiceC_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceC_48 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceC_48), value);
	}

	inline static int32_t get_offset_of_gui_choiceD_49() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___gui_choiceD_49)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceD_49() const { return ___gui_choiceD_49; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceD_49() { return &___gui_choiceD_49; }
	inline void set_gui_choiceD_49(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceD_49 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceD_49), value);
	}

	inline static int32_t get_offset_of_btn_choiceA_50() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___btn_choiceA_50)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceA_50() const { return ___btn_choiceA_50; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceA_50() { return &___btn_choiceA_50; }
	inline void set_btn_choiceA_50(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceA_50 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceA_50), value);
	}

	inline static int32_t get_offset_of_btn_choiceB_51() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___btn_choiceB_51)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceB_51() const { return ___btn_choiceB_51; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceB_51() { return &___btn_choiceB_51; }
	inline void set_btn_choiceB_51(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceB_51 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceB_51), value);
	}

	inline static int32_t get_offset_of_btn_choiceC_52() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___btn_choiceC_52)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceC_52() const { return ___btn_choiceC_52; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceC_52() { return &___btn_choiceC_52; }
	inline void set_btn_choiceC_52(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceC_52 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceC_52), value);
	}

	inline static int32_t get_offset_of_btn_choiceD_53() { return static_cast<int32_t>(offsetof(AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E, ___btn_choiceD_53)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceD_53() const { return ___btn_choiceD_53; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceD_53() { return &___btn_choiceD_53; }
	inline void set_btn_choiceD_53(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceD_53 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceD_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARIDAREAQUIZCONTROLLER_T51ABC34B4230DB477242891DCFF108B1BF431F5E_H
#ifndef ARIDAREAQUIZMENU_TB70D225F1023B865D11456144E86061C60216371_H
#define ARIDAREAQUIZMENU_TB70D225F1023B865D11456144E86061C60216371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AridAreaQuizMenu
struct  AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single AridAreaQuizMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean AridAreaQuizMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image AridAreaQuizMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject AridAreaQuizMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject AridAreaQuizMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean AridAreaQuizMenu::isTutorial
	bool ___isTutorial_9;
	// UnityEngine.GameObject AridAreaQuizMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_10;
	// System.Int32 AridAreaQuizMenu::restTime
	int32_t ___restTime_11;
	// System.Single AridAreaQuizMenu::remindertimer
	float ___remindertimer_12;
	// System.Boolean AridAreaQuizMenu::remindertime
	bool ___remindertime_13;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isTutorial_9() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___isTutorial_9)); }
	inline bool get_isTutorial_9() const { return ___isTutorial_9; }
	inline bool* get_address_of_isTutorial_9() { return &___isTutorial_9; }
	inline void set_isTutorial_9(bool value)
	{
		___isTutorial_9 = value;
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_10() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___obj_reminderCanvas_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_10() const { return ___obj_reminderCanvas_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_10() { return &___obj_reminderCanvas_10; }
	inline void set_obj_reminderCanvas_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_10), value);
	}

	inline static int32_t get_offset_of_restTime_11() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___restTime_11)); }
	inline int32_t get_restTime_11() const { return ___restTime_11; }
	inline int32_t* get_address_of_restTime_11() { return &___restTime_11; }
	inline void set_restTime_11(int32_t value)
	{
		___restTime_11 = value;
	}

	inline static int32_t get_offset_of_remindertimer_12() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___remindertimer_12)); }
	inline float get_remindertimer_12() const { return ___remindertimer_12; }
	inline float* get_address_of_remindertimer_12() { return &___remindertimer_12; }
	inline void set_remindertimer_12(float value)
	{
		___remindertimer_12 = value;
	}

	inline static int32_t get_offset_of_remindertime_13() { return static_cast<int32_t>(offsetof(AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371, ___remindertime_13)); }
	inline bool get_remindertime_13() const { return ___remindertime_13; }
	inline bool* get_address_of_remindertime_13() { return &___remindertime_13; }
	inline void set_remindertime_13(bool value)
	{
		___remindertime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARIDAREAQUIZMENU_TB70D225F1023B865D11456144E86061C60216371_H
#ifndef ARIDHIDEANIMALCONTROLLER_TAB9B160A5D66291A4549C57853FFE5F51BBB9388_H
#define ARIDHIDEANIMALCONTROLLER_TAB9B160A5D66291A4549C57853FFE5F51BBB9388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AridHideAnimalController
struct  AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields
{
public:
	// UnityEngine.GameObject AridHideAnimalController::obj_Kangaroo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Kangaroo_4;
	// UnityEngine.GameObject AridHideAnimalController::obj_TasmanianDevil
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_TasmanianDevil_5;
	// UnityEngine.GameObject AridHideAnimalController::obj_Koala
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Koala_6;
	// UnityEngine.GameObject AridHideAnimalController::obj_Ornitorinco
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ornitorinco_7;
	// UnityEngine.GameObject AridHideAnimalController::obj_Ostrich
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ostrich_8;
	// UnityEngine.GameObject AridHideAnimalController::obj_LizardCollar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_LizardCollar_9;
	// UnityEngine.GameObject AridHideAnimalController::obj_Scorpion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Scorpion_10;
	// UnityEngine.GameObject AridHideAnimalController::obj_Panther
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Panther_11;
	// UnityEngine.GameObject AridHideAnimalController::obj_Giraffe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Giraffe_12;
	// UnityEngine.GameObject AridHideAnimalController::obj_Rhinoceros
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Rhinoceros_13;
	// UnityEngine.GameObject AridHideAnimalController::obj_Zebra
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Zebra_14;

public:
	inline static int32_t get_offset_of_obj_Kangaroo_4() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Kangaroo_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Kangaroo_4() const { return ___obj_Kangaroo_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Kangaroo_4() { return &___obj_Kangaroo_4; }
	inline void set_obj_Kangaroo_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Kangaroo_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Kangaroo_4), value);
	}

	inline static int32_t get_offset_of_obj_TasmanianDevil_5() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_TasmanianDevil_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_TasmanianDevil_5() const { return ___obj_TasmanianDevil_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_TasmanianDevil_5() { return &___obj_TasmanianDevil_5; }
	inline void set_obj_TasmanianDevil_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_TasmanianDevil_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_TasmanianDevil_5), value);
	}

	inline static int32_t get_offset_of_obj_Koala_6() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Koala_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Koala_6() const { return ___obj_Koala_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Koala_6() { return &___obj_Koala_6; }
	inline void set_obj_Koala_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Koala_6 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Koala_6), value);
	}

	inline static int32_t get_offset_of_obj_Ornitorinco_7() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Ornitorinco_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ornitorinco_7() const { return ___obj_Ornitorinco_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ornitorinco_7() { return &___obj_Ornitorinco_7; }
	inline void set_obj_Ornitorinco_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ornitorinco_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ornitorinco_7), value);
	}

	inline static int32_t get_offset_of_obj_Ostrich_8() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Ostrich_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ostrich_8() const { return ___obj_Ostrich_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ostrich_8() { return &___obj_Ostrich_8; }
	inline void set_obj_Ostrich_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ostrich_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ostrich_8), value);
	}

	inline static int32_t get_offset_of_obj_LizardCollar_9() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_LizardCollar_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_LizardCollar_9() const { return ___obj_LizardCollar_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_LizardCollar_9() { return &___obj_LizardCollar_9; }
	inline void set_obj_LizardCollar_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_LizardCollar_9 = value;
		Il2CppCodeGenWriteBarrier((&___obj_LizardCollar_9), value);
	}

	inline static int32_t get_offset_of_obj_Scorpion_10() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Scorpion_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Scorpion_10() const { return ___obj_Scorpion_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Scorpion_10() { return &___obj_Scorpion_10; }
	inline void set_obj_Scorpion_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Scorpion_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Scorpion_10), value);
	}

	inline static int32_t get_offset_of_obj_Panther_11() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Panther_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Panther_11() const { return ___obj_Panther_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Panther_11() { return &___obj_Panther_11; }
	inline void set_obj_Panther_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Panther_11 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Panther_11), value);
	}

	inline static int32_t get_offset_of_obj_Giraffe_12() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Giraffe_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Giraffe_12() const { return ___obj_Giraffe_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Giraffe_12() { return &___obj_Giraffe_12; }
	inline void set_obj_Giraffe_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Giraffe_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Giraffe_12), value);
	}

	inline static int32_t get_offset_of_obj_Rhinoceros_13() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Rhinoceros_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Rhinoceros_13() const { return ___obj_Rhinoceros_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Rhinoceros_13() { return &___obj_Rhinoceros_13; }
	inline void set_obj_Rhinoceros_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Rhinoceros_13 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Rhinoceros_13), value);
	}

	inline static int32_t get_offset_of_obj_Zebra_14() { return static_cast<int32_t>(offsetof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields, ___obj_Zebra_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Zebra_14() const { return ___obj_Zebra_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Zebra_14() { return &___obj_Zebra_14; }
	inline void set_obj_Zebra_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Zebra_14 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Zebra_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARIDHIDEANIMALCONTROLLER_TAB9B160A5D66291A4549C57853FFE5F51BBB9388_H
#ifndef FORESTHIDEANIMALCONTROLLER_T3DFE138C1330EE03A9E18856F58B956605D436D5_H
#define FORESTHIDEANIMALCONTROLLER_T3DFE138C1330EE03A9E18856F58B956605D436D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForestHideAnimalController
struct  ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields
{
public:
	// UnityEngine.GameObject ForestHideAnimalController::obj_Bear
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bear_4;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Cattle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Cattle_5;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Deer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Deer_6;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Ibex
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ibex_7;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Wild_boar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Wild_boar_8;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Wolf
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Wolf_9;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Rabbit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Rabbit_10;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Crocodile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Crocodile_11;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Lion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Lion_12;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Tiger
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Tiger_13;
	// UnityEngine.GameObject ForestHideAnimalController::obj_Elephant
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Elephant_14;

public:
	inline static int32_t get_offset_of_obj_Bear_4() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Bear_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bear_4() const { return ___obj_Bear_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bear_4() { return &___obj_Bear_4; }
	inline void set_obj_Bear_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bear_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bear_4), value);
	}

	inline static int32_t get_offset_of_obj_Cattle_5() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Cattle_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Cattle_5() const { return ___obj_Cattle_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Cattle_5() { return &___obj_Cattle_5; }
	inline void set_obj_Cattle_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Cattle_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Cattle_5), value);
	}

	inline static int32_t get_offset_of_obj_Deer_6() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Deer_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Deer_6() const { return ___obj_Deer_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Deer_6() { return &___obj_Deer_6; }
	inline void set_obj_Deer_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Deer_6 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Deer_6), value);
	}

	inline static int32_t get_offset_of_obj_Ibex_7() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Ibex_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ibex_7() const { return ___obj_Ibex_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ibex_7() { return &___obj_Ibex_7; }
	inline void set_obj_Ibex_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ibex_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ibex_7), value);
	}

	inline static int32_t get_offset_of_obj_Wild_boar_8() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Wild_boar_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Wild_boar_8() const { return ___obj_Wild_boar_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Wild_boar_8() { return &___obj_Wild_boar_8; }
	inline void set_obj_Wild_boar_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Wild_boar_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Wild_boar_8), value);
	}

	inline static int32_t get_offset_of_obj_Wolf_9() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Wolf_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Wolf_9() const { return ___obj_Wolf_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Wolf_9() { return &___obj_Wolf_9; }
	inline void set_obj_Wolf_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Wolf_9 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Wolf_9), value);
	}

	inline static int32_t get_offset_of_obj_Rabbit_10() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Rabbit_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Rabbit_10() const { return ___obj_Rabbit_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Rabbit_10() { return &___obj_Rabbit_10; }
	inline void set_obj_Rabbit_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Rabbit_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Rabbit_10), value);
	}

	inline static int32_t get_offset_of_obj_Crocodile_11() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Crocodile_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Crocodile_11() const { return ___obj_Crocodile_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Crocodile_11() { return &___obj_Crocodile_11; }
	inline void set_obj_Crocodile_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Crocodile_11 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Crocodile_11), value);
	}

	inline static int32_t get_offset_of_obj_Lion_12() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Lion_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Lion_12() const { return ___obj_Lion_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Lion_12() { return &___obj_Lion_12; }
	inline void set_obj_Lion_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Lion_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Lion_12), value);
	}

	inline static int32_t get_offset_of_obj_Tiger_13() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Tiger_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Tiger_13() const { return ___obj_Tiger_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Tiger_13() { return &___obj_Tiger_13; }
	inline void set_obj_Tiger_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Tiger_13 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Tiger_13), value);
	}

	inline static int32_t get_offset_of_obj_Elephant_14() { return static_cast<int32_t>(offsetof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields, ___obj_Elephant_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Elephant_14() const { return ___obj_Elephant_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Elephant_14() { return &___obj_Elephant_14; }
	inline void set_obj_Elephant_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Elephant_14 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Elephant_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORESTHIDEANIMALCONTROLLER_T3DFE138C1330EE03A9E18856F58B956605D436D5_H
#ifndef FORESTMENU_T0B2C5BB757D1D339E4F3F19BD558962D3F2C211D_H
#define FORESTMENU_T0B2C5BB757D1D339E4F3F19BD558962D3F2C211D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForestMenu
struct  ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ForestMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean ForestMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image ForestMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject ForestMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject ForestMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean ForestMenu::isQuiz
	bool ___isQuiz_9;
	// System.Boolean ForestMenu::isMid
	bool ___isMid_10;
	// System.Boolean ForestMenu::isHigh
	bool ___isHigh_11;
	// UnityEngine.GameObject ForestMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_12;
	// System.Int32 ForestMenu::restTime
	int32_t ___restTime_13;
	// System.Single ForestMenu::remindertimer
	float ___remindertimer_14;
	// System.Boolean ForestMenu::remindertime
	bool ___remindertime_15;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isQuiz_9() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___isQuiz_9)); }
	inline bool get_isQuiz_9() const { return ___isQuiz_9; }
	inline bool* get_address_of_isQuiz_9() { return &___isQuiz_9; }
	inline void set_isQuiz_9(bool value)
	{
		___isQuiz_9 = value;
	}

	inline static int32_t get_offset_of_isMid_10() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___isMid_10)); }
	inline bool get_isMid_10() const { return ___isMid_10; }
	inline bool* get_address_of_isMid_10() { return &___isMid_10; }
	inline void set_isMid_10(bool value)
	{
		___isMid_10 = value;
	}

	inline static int32_t get_offset_of_isHigh_11() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___isHigh_11)); }
	inline bool get_isHigh_11() const { return ___isHigh_11; }
	inline bool* get_address_of_isHigh_11() { return &___isHigh_11; }
	inline void set_isHigh_11(bool value)
	{
		___isHigh_11 = value;
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_12() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___obj_reminderCanvas_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_12() const { return ___obj_reminderCanvas_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_12() { return &___obj_reminderCanvas_12; }
	inline void set_obj_reminderCanvas_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_12), value);
	}

	inline static int32_t get_offset_of_restTime_13() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___restTime_13)); }
	inline int32_t get_restTime_13() const { return ___restTime_13; }
	inline int32_t* get_address_of_restTime_13() { return &___restTime_13; }
	inline void set_restTime_13(int32_t value)
	{
		___restTime_13 = value;
	}

	inline static int32_t get_offset_of_remindertimer_14() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___remindertimer_14)); }
	inline float get_remindertimer_14() const { return ___remindertimer_14; }
	inline float* get_address_of_remindertimer_14() { return &___remindertimer_14; }
	inline void set_remindertimer_14(float value)
	{
		___remindertimer_14 = value;
	}

	inline static int32_t get_offset_of_remindertime_15() { return static_cast<int32_t>(offsetof(ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D, ___remindertime_15)); }
	inline bool get_remindertime_15() const { return ___remindertime_15; }
	inline bool* get_address_of_remindertime_15() { return &___remindertime_15; }
	inline void set_remindertime_15(bool value)
	{
		___remindertime_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORESTMENU_T0B2C5BB757D1D339E4F3F19BD558962D3F2C211D_H
#ifndef FORESTQUIZCONTROLLER_T9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA_H
#define FORESTQUIZCONTROLLER_T9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForestQuizController
struct  ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Int32> ForestQuizController::questionQueueList
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___questionQueueList_4;
	// System.Collections.Generic.List`1<QuestionList> ForestQuizController::questionLists
	List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * ___questionLists_5;
	// System.Int32 ForestQuizController::limitTime
	int32_t ___limitTime_6;
	// System.Single ForestQuizController::time
	float ___time_7;
	// System.Int32 ForestQuizController::currentQuestionNum
	int32_t ___currentQuestionNum_8;
	// System.Int32 ForestQuizController::correctCount
	int32_t ___correctCount_9;
	// System.Single ForestQuizController::buttontimer
	float ___buttontimer_10;
	// System.Boolean ForestQuizController::buttontime
	bool ___buttontime_11;
	// System.Boolean ForestQuizController::isSelectA
	bool ___isSelectA_12;
	// System.Boolean ForestQuizController::isSelectB
	bool ___isSelectB_13;
	// System.Boolean ForestQuizController::isSelectC
	bool ___isSelectC_14;
	// System.Boolean ForestQuizController::isSelectD
	bool ___isSelectD_15;
	// System.Boolean ForestQuizController::isStart
	bool ___isStart_16;
	// System.Boolean ForestQuizController::isResumebutton
	bool ___isResumebutton_17;
	// System.Boolean ForestQuizController::isPausebutton
	bool ___isPausebutton_18;
	// System.Boolean ForestQuizController::isStartbutton
	bool ___isStartbutton_19;
	// System.Boolean ForestQuizController::isConfirmbutton
	bool ___isConfirmbutton_20;
	// System.Boolean ForestQuizController::isQuitbutton
	bool ___isQuitbutton_21;
	// System.Int32 ForestQuizController::animalsCount
	int32_t ___animalsCount_22;
	// System.Boolean ForestQuizController::canAnswer
	bool ___canAnswer_23;
	// UnityEngine.GameObject ForestQuizController::obj_Bear
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bear_24;
	// UnityEngine.GameObject ForestQuizController::obj_Cattle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Cattle_25;
	// UnityEngine.GameObject ForestQuizController::obj_Deer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Deer_26;
	// UnityEngine.GameObject ForestQuizController::obj_Ibex
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Ibex_27;
	// UnityEngine.GameObject ForestQuizController::obj_Wild_boar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Wild_boar_28;
	// UnityEngine.GameObject ForestQuizController::obj_Wolf
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Wolf_29;
	// UnityEngine.GameObject ForestQuizController::obj_Rabbit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Rabbit_30;
	// UnityEngine.GameObject ForestQuizController::obj_Crocodile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Crocodile_31;
	// UnityEngine.GameObject ForestQuizController::obj_Lion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Lion_32;
	// UnityEngine.GameObject ForestQuizController::obj_Tiger
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Tiger_33;
	// UnityEngine.GameObject ForestQuizController::obj_Elephant
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Elephant_34;
	// UnityEngine.UI.Text ForestQuizController::gui_time
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_time_35;
	// UnityEngine.UI.Text ForestQuizController::gui_round
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_round_36;
	// UnityEngine.UI.Image ForestQuizController::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_37;
	// UnityEngine.GameObject ForestQuizController::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_38;
	// UnityEngine.GameObject ForestQuizController::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_39;
	// UnityEngine.GameObject ForestQuizController::canvas_question
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_question_40;
	// UnityEngine.GameObject ForestQuizController::canvas_pause
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_pause_41;
	// UnityEngine.GameObject ForestQuizController::obj_btn_pause
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_btn_pause_42;
	// UnityEngine.GameObject ForestQuizController::obj_start_menu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_start_menu_43;
	// UnityEngine.UI.Text ForestQuizController::gui_mark
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_mark_44;
	// UnityEngine.GameObject ForestQuizController::canvas_finish
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_finish_45;
	// UnityEngine.UI.Text ForestQuizController::gui_choiceA
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceA_46;
	// UnityEngine.UI.Text ForestQuizController::gui_choiceB
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceB_47;
	// UnityEngine.UI.Text ForestQuizController::gui_choiceC
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceC_48;
	// UnityEngine.UI.Text ForestQuizController::gui_choiceD
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceD_49;
	// UnityEngine.GameObject ForestQuizController::btn_choiceA
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceA_50;
	// UnityEngine.GameObject ForestQuizController::btn_choiceB
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceB_51;
	// UnityEngine.GameObject ForestQuizController::btn_choiceC
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceC_52;
	// UnityEngine.GameObject ForestQuizController::btn_choiceD
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceD_53;

public:
	inline static int32_t get_offset_of_questionQueueList_4() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___questionQueueList_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_questionQueueList_4() const { return ___questionQueueList_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_questionQueueList_4() { return &___questionQueueList_4; }
	inline void set_questionQueueList_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___questionQueueList_4 = value;
		Il2CppCodeGenWriteBarrier((&___questionQueueList_4), value);
	}

	inline static int32_t get_offset_of_questionLists_5() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___questionLists_5)); }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * get_questionLists_5() const { return ___questionLists_5; }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D ** get_address_of_questionLists_5() { return &___questionLists_5; }
	inline void set_questionLists_5(List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * value)
	{
		___questionLists_5 = value;
		Il2CppCodeGenWriteBarrier((&___questionLists_5), value);
	}

	inline static int32_t get_offset_of_limitTime_6() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___limitTime_6)); }
	inline int32_t get_limitTime_6() const { return ___limitTime_6; }
	inline int32_t* get_address_of_limitTime_6() { return &___limitTime_6; }
	inline void set_limitTime_6(int32_t value)
	{
		___limitTime_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_currentQuestionNum_8() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___currentQuestionNum_8)); }
	inline int32_t get_currentQuestionNum_8() const { return ___currentQuestionNum_8; }
	inline int32_t* get_address_of_currentQuestionNum_8() { return &___currentQuestionNum_8; }
	inline void set_currentQuestionNum_8(int32_t value)
	{
		___currentQuestionNum_8 = value;
	}

	inline static int32_t get_offset_of_correctCount_9() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___correctCount_9)); }
	inline int32_t get_correctCount_9() const { return ___correctCount_9; }
	inline int32_t* get_address_of_correctCount_9() { return &___correctCount_9; }
	inline void set_correctCount_9(int32_t value)
	{
		___correctCount_9 = value;
	}

	inline static int32_t get_offset_of_buttontimer_10() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___buttontimer_10)); }
	inline float get_buttontimer_10() const { return ___buttontimer_10; }
	inline float* get_address_of_buttontimer_10() { return &___buttontimer_10; }
	inline void set_buttontimer_10(float value)
	{
		___buttontimer_10 = value;
	}

	inline static int32_t get_offset_of_buttontime_11() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___buttontime_11)); }
	inline bool get_buttontime_11() const { return ___buttontime_11; }
	inline bool* get_address_of_buttontime_11() { return &___buttontime_11; }
	inline void set_buttontime_11(bool value)
	{
		___buttontime_11 = value;
	}

	inline static int32_t get_offset_of_isSelectA_12() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isSelectA_12)); }
	inline bool get_isSelectA_12() const { return ___isSelectA_12; }
	inline bool* get_address_of_isSelectA_12() { return &___isSelectA_12; }
	inline void set_isSelectA_12(bool value)
	{
		___isSelectA_12 = value;
	}

	inline static int32_t get_offset_of_isSelectB_13() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isSelectB_13)); }
	inline bool get_isSelectB_13() const { return ___isSelectB_13; }
	inline bool* get_address_of_isSelectB_13() { return &___isSelectB_13; }
	inline void set_isSelectB_13(bool value)
	{
		___isSelectB_13 = value;
	}

	inline static int32_t get_offset_of_isSelectC_14() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isSelectC_14)); }
	inline bool get_isSelectC_14() const { return ___isSelectC_14; }
	inline bool* get_address_of_isSelectC_14() { return &___isSelectC_14; }
	inline void set_isSelectC_14(bool value)
	{
		___isSelectC_14 = value;
	}

	inline static int32_t get_offset_of_isSelectD_15() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isSelectD_15)); }
	inline bool get_isSelectD_15() const { return ___isSelectD_15; }
	inline bool* get_address_of_isSelectD_15() { return &___isSelectD_15; }
	inline void set_isSelectD_15(bool value)
	{
		___isSelectD_15 = value;
	}

	inline static int32_t get_offset_of_isStart_16() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isStart_16)); }
	inline bool get_isStart_16() const { return ___isStart_16; }
	inline bool* get_address_of_isStart_16() { return &___isStart_16; }
	inline void set_isStart_16(bool value)
	{
		___isStart_16 = value;
	}

	inline static int32_t get_offset_of_isResumebutton_17() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isResumebutton_17)); }
	inline bool get_isResumebutton_17() const { return ___isResumebutton_17; }
	inline bool* get_address_of_isResumebutton_17() { return &___isResumebutton_17; }
	inline void set_isResumebutton_17(bool value)
	{
		___isResumebutton_17 = value;
	}

	inline static int32_t get_offset_of_isPausebutton_18() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isPausebutton_18)); }
	inline bool get_isPausebutton_18() const { return ___isPausebutton_18; }
	inline bool* get_address_of_isPausebutton_18() { return &___isPausebutton_18; }
	inline void set_isPausebutton_18(bool value)
	{
		___isPausebutton_18 = value;
	}

	inline static int32_t get_offset_of_isStartbutton_19() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isStartbutton_19)); }
	inline bool get_isStartbutton_19() const { return ___isStartbutton_19; }
	inline bool* get_address_of_isStartbutton_19() { return &___isStartbutton_19; }
	inline void set_isStartbutton_19(bool value)
	{
		___isStartbutton_19 = value;
	}

	inline static int32_t get_offset_of_isConfirmbutton_20() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isConfirmbutton_20)); }
	inline bool get_isConfirmbutton_20() const { return ___isConfirmbutton_20; }
	inline bool* get_address_of_isConfirmbutton_20() { return &___isConfirmbutton_20; }
	inline void set_isConfirmbutton_20(bool value)
	{
		___isConfirmbutton_20 = value;
	}

	inline static int32_t get_offset_of_isQuitbutton_21() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___isQuitbutton_21)); }
	inline bool get_isQuitbutton_21() const { return ___isQuitbutton_21; }
	inline bool* get_address_of_isQuitbutton_21() { return &___isQuitbutton_21; }
	inline void set_isQuitbutton_21(bool value)
	{
		___isQuitbutton_21 = value;
	}

	inline static int32_t get_offset_of_animalsCount_22() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___animalsCount_22)); }
	inline int32_t get_animalsCount_22() const { return ___animalsCount_22; }
	inline int32_t* get_address_of_animalsCount_22() { return &___animalsCount_22; }
	inline void set_animalsCount_22(int32_t value)
	{
		___animalsCount_22 = value;
	}

	inline static int32_t get_offset_of_canAnswer_23() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___canAnswer_23)); }
	inline bool get_canAnswer_23() const { return ___canAnswer_23; }
	inline bool* get_address_of_canAnswer_23() { return &___canAnswer_23; }
	inline void set_canAnswer_23(bool value)
	{
		___canAnswer_23 = value;
	}

	inline static int32_t get_offset_of_obj_Bear_24() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Bear_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bear_24() const { return ___obj_Bear_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bear_24() { return &___obj_Bear_24; }
	inline void set_obj_Bear_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bear_24 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bear_24), value);
	}

	inline static int32_t get_offset_of_obj_Cattle_25() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Cattle_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Cattle_25() const { return ___obj_Cattle_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Cattle_25() { return &___obj_Cattle_25; }
	inline void set_obj_Cattle_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Cattle_25 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Cattle_25), value);
	}

	inline static int32_t get_offset_of_obj_Deer_26() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Deer_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Deer_26() const { return ___obj_Deer_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Deer_26() { return &___obj_Deer_26; }
	inline void set_obj_Deer_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Deer_26 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Deer_26), value);
	}

	inline static int32_t get_offset_of_obj_Ibex_27() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Ibex_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Ibex_27() const { return ___obj_Ibex_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Ibex_27() { return &___obj_Ibex_27; }
	inline void set_obj_Ibex_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Ibex_27 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Ibex_27), value);
	}

	inline static int32_t get_offset_of_obj_Wild_boar_28() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Wild_boar_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Wild_boar_28() const { return ___obj_Wild_boar_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Wild_boar_28() { return &___obj_Wild_boar_28; }
	inline void set_obj_Wild_boar_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Wild_boar_28 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Wild_boar_28), value);
	}

	inline static int32_t get_offset_of_obj_Wolf_29() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Wolf_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Wolf_29() const { return ___obj_Wolf_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Wolf_29() { return &___obj_Wolf_29; }
	inline void set_obj_Wolf_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Wolf_29 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Wolf_29), value);
	}

	inline static int32_t get_offset_of_obj_Rabbit_30() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Rabbit_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Rabbit_30() const { return ___obj_Rabbit_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Rabbit_30() { return &___obj_Rabbit_30; }
	inline void set_obj_Rabbit_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Rabbit_30 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Rabbit_30), value);
	}

	inline static int32_t get_offset_of_obj_Crocodile_31() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Crocodile_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Crocodile_31() const { return ___obj_Crocodile_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Crocodile_31() { return &___obj_Crocodile_31; }
	inline void set_obj_Crocodile_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Crocodile_31 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Crocodile_31), value);
	}

	inline static int32_t get_offset_of_obj_Lion_32() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Lion_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Lion_32() const { return ___obj_Lion_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Lion_32() { return &___obj_Lion_32; }
	inline void set_obj_Lion_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Lion_32 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Lion_32), value);
	}

	inline static int32_t get_offset_of_obj_Tiger_33() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Tiger_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Tiger_33() const { return ___obj_Tiger_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Tiger_33() { return &___obj_Tiger_33; }
	inline void set_obj_Tiger_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Tiger_33 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Tiger_33), value);
	}

	inline static int32_t get_offset_of_obj_Elephant_34() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Elephant_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Elephant_34() const { return ___obj_Elephant_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Elephant_34() { return &___obj_Elephant_34; }
	inline void set_obj_Elephant_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Elephant_34 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Elephant_34), value);
	}

	inline static int32_t get_offset_of_gui_time_35() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_time_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_time_35() const { return ___gui_time_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_time_35() { return &___gui_time_35; }
	inline void set_gui_time_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_time_35 = value;
		Il2CppCodeGenWriteBarrier((&___gui_time_35), value);
	}

	inline static int32_t get_offset_of_gui_round_36() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_round_36)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_round_36() const { return ___gui_round_36; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_round_36() { return &___gui_round_36; }
	inline void set_gui_round_36(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_round_36 = value;
		Il2CppCodeGenWriteBarrier((&___gui_round_36), value);
	}

	inline static int32_t get_offset_of_Bar_37() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___Bar_37)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_37() const { return ___Bar_37; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_37() { return &___Bar_37; }
	inline void set_Bar_37(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_37 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_37), value);
	}

	inline static int32_t get_offset_of_obj_Bar_38() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_Bar_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_38() const { return ___obj_Bar_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_38() { return &___obj_Bar_38; }
	inline void set_obj_Bar_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_38 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_38), value);
	}

	inline static int32_t get_offset_of_obj_shoot_39() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_shoot_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_39() const { return ___obj_shoot_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_39() { return &___obj_shoot_39; }
	inline void set_obj_shoot_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_39 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_39), value);
	}

	inline static int32_t get_offset_of_canvas_question_40() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___canvas_question_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_question_40() const { return ___canvas_question_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_question_40() { return &___canvas_question_40; }
	inline void set_canvas_question_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_question_40 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_question_40), value);
	}

	inline static int32_t get_offset_of_canvas_pause_41() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___canvas_pause_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_pause_41() const { return ___canvas_pause_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_pause_41() { return &___canvas_pause_41; }
	inline void set_canvas_pause_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_pause_41 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_pause_41), value);
	}

	inline static int32_t get_offset_of_obj_btn_pause_42() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_btn_pause_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_btn_pause_42() const { return ___obj_btn_pause_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_btn_pause_42() { return &___obj_btn_pause_42; }
	inline void set_obj_btn_pause_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_btn_pause_42 = value;
		Il2CppCodeGenWriteBarrier((&___obj_btn_pause_42), value);
	}

	inline static int32_t get_offset_of_obj_start_menu_43() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___obj_start_menu_43)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_start_menu_43() const { return ___obj_start_menu_43; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_start_menu_43() { return &___obj_start_menu_43; }
	inline void set_obj_start_menu_43(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_start_menu_43 = value;
		Il2CppCodeGenWriteBarrier((&___obj_start_menu_43), value);
	}

	inline static int32_t get_offset_of_gui_mark_44() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_mark_44)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_mark_44() const { return ___gui_mark_44; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_mark_44() { return &___gui_mark_44; }
	inline void set_gui_mark_44(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_mark_44 = value;
		Il2CppCodeGenWriteBarrier((&___gui_mark_44), value);
	}

	inline static int32_t get_offset_of_canvas_finish_45() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___canvas_finish_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_finish_45() const { return ___canvas_finish_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_finish_45() { return &___canvas_finish_45; }
	inline void set_canvas_finish_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_finish_45 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_finish_45), value);
	}

	inline static int32_t get_offset_of_gui_choiceA_46() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_choiceA_46)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceA_46() const { return ___gui_choiceA_46; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceA_46() { return &___gui_choiceA_46; }
	inline void set_gui_choiceA_46(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceA_46 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceA_46), value);
	}

	inline static int32_t get_offset_of_gui_choiceB_47() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_choiceB_47)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceB_47() const { return ___gui_choiceB_47; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceB_47() { return &___gui_choiceB_47; }
	inline void set_gui_choiceB_47(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceB_47 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceB_47), value);
	}

	inline static int32_t get_offset_of_gui_choiceC_48() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_choiceC_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceC_48() const { return ___gui_choiceC_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceC_48() { return &___gui_choiceC_48; }
	inline void set_gui_choiceC_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceC_48 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceC_48), value);
	}

	inline static int32_t get_offset_of_gui_choiceD_49() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___gui_choiceD_49)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceD_49() const { return ___gui_choiceD_49; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceD_49() { return &___gui_choiceD_49; }
	inline void set_gui_choiceD_49(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceD_49 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceD_49), value);
	}

	inline static int32_t get_offset_of_btn_choiceA_50() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___btn_choiceA_50)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceA_50() const { return ___btn_choiceA_50; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceA_50() { return &___btn_choiceA_50; }
	inline void set_btn_choiceA_50(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceA_50 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceA_50), value);
	}

	inline static int32_t get_offset_of_btn_choiceB_51() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___btn_choiceB_51)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceB_51() const { return ___btn_choiceB_51; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceB_51() { return &___btn_choiceB_51; }
	inline void set_btn_choiceB_51(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceB_51 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceB_51), value);
	}

	inline static int32_t get_offset_of_btn_choiceC_52() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___btn_choiceC_52)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceC_52() const { return ___btn_choiceC_52; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceC_52() { return &___btn_choiceC_52; }
	inline void set_btn_choiceC_52(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceC_52 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceC_52), value);
	}

	inline static int32_t get_offset_of_btn_choiceD_53() { return static_cast<int32_t>(offsetof(ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA, ___btn_choiceD_53)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceD_53() const { return ___btn_choiceD_53; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceD_53() { return &___btn_choiceD_53; }
	inline void set_btn_choiceD_53(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceD_53 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceD_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORESTQUIZCONTROLLER_T9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA_H
#ifndef FORESTQUIZMENU_TBC267C272E10BEBD0053CAFBED378C23724E3685_H
#define FORESTQUIZMENU_TBC267C272E10BEBD0053CAFBED378C23724E3685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForestQuizMenu
struct  ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ForestQuizMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean ForestQuizMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image ForestQuizMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject ForestQuizMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject ForestQuizMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean ForestQuizMenu::isTutorial
	bool ___isTutorial_9;
	// UnityEngine.GameObject ForestQuizMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_10;
	// System.Int32 ForestQuizMenu::restTime
	int32_t ___restTime_11;
	// System.Single ForestQuizMenu::remindertimer
	float ___remindertimer_12;
	// System.Boolean ForestQuizMenu::remindertime
	bool ___remindertime_13;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isTutorial_9() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___isTutorial_9)); }
	inline bool get_isTutorial_9() const { return ___isTutorial_9; }
	inline bool* get_address_of_isTutorial_9() { return &___isTutorial_9; }
	inline void set_isTutorial_9(bool value)
	{
		___isTutorial_9 = value;
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_10() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___obj_reminderCanvas_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_10() const { return ___obj_reminderCanvas_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_10() { return &___obj_reminderCanvas_10; }
	inline void set_obj_reminderCanvas_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_10), value);
	}

	inline static int32_t get_offset_of_restTime_11() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___restTime_11)); }
	inline int32_t get_restTime_11() const { return ___restTime_11; }
	inline int32_t* get_address_of_restTime_11() { return &___restTime_11; }
	inline void set_restTime_11(int32_t value)
	{
		___restTime_11 = value;
	}

	inline static int32_t get_offset_of_remindertimer_12() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___remindertimer_12)); }
	inline float get_remindertimer_12() const { return ___remindertimer_12; }
	inline float* get_address_of_remindertimer_12() { return &___remindertimer_12; }
	inline void set_remindertimer_12(float value)
	{
		___remindertimer_12 = value;
	}

	inline static int32_t get_offset_of_remindertime_13() { return static_cast<int32_t>(offsetof(ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685, ___remindertime_13)); }
	inline bool get_remindertime_13() const { return ___remindertime_13; }
	inline bool* get_address_of_remindertime_13() { return &___remindertime_13; }
	inline void set_remindertime_13(bool value)
	{
		___remindertime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORESTQUIZMENU_TBC267C272E10BEBD0053CAFBED378C23724E3685_H
#ifndef GVRALLEVENTSTRIGGER_TE19B85857831C7B0002AF79DD44739DA855B9272_H
#define GVRALLEVENTSTRIGGER_TE19B85857831C7B0002AF79DD44739DA855B9272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger
struct  GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnPointerClick
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnPointerClick_4;
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnPointerDown
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnPointerDown_5;
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnPointerUp
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnPointerUp_6;
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnPointerEnter
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnPointerEnter_7;
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnPointerExit
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnPointerExit_8;
	// GvrAllEventsTrigger_TriggerEvent GvrAllEventsTrigger::OnScroll
	TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * ___OnScroll_9;
	// System.Boolean GvrAllEventsTrigger::listenersAdded
	bool ___listenersAdded_10;

public:
	inline static int32_t get_offset_of_OnPointerClick_4() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnPointerClick_4)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnPointerClick_4() const { return ___OnPointerClick_4; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnPointerClick_4() { return &___OnPointerClick_4; }
	inline void set_OnPointerClick_4(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnPointerClick_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerClick_4), value);
	}

	inline static int32_t get_offset_of_OnPointerDown_5() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnPointerDown_5)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnPointerDown_5() const { return ___OnPointerDown_5; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnPointerDown_5() { return &___OnPointerDown_5; }
	inline void set_OnPointerDown_5(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnPointerDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerDown_5), value);
	}

	inline static int32_t get_offset_of_OnPointerUp_6() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnPointerUp_6)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnPointerUp_6() const { return ___OnPointerUp_6; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnPointerUp_6() { return &___OnPointerUp_6; }
	inline void set_OnPointerUp_6(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnPointerUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerUp_6), value);
	}

	inline static int32_t get_offset_of_OnPointerEnter_7() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnPointerEnter_7)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnPointerEnter_7() const { return ___OnPointerEnter_7; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnPointerEnter_7() { return &___OnPointerEnter_7; }
	inline void set_OnPointerEnter_7(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnPointerEnter_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_7), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_8() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnPointerExit_8)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnPointerExit_8() const { return ___OnPointerExit_8; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnPointerExit_8() { return &___OnPointerExit_8; }
	inline void set_OnPointerExit_8(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnPointerExit_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_8), value);
	}

	inline static int32_t get_offset_of_OnScroll_9() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___OnScroll_9)); }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * get_OnScroll_9() const { return ___OnScroll_9; }
	inline TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 ** get_address_of_OnScroll_9() { return &___OnScroll_9; }
	inline void set_OnScroll_9(TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6 * value)
	{
		___OnScroll_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnScroll_9), value);
	}

	inline static int32_t get_offset_of_listenersAdded_10() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272, ___listenersAdded_10)); }
	inline bool get_listenersAdded_10() const { return ___listenersAdded_10; }
	inline bool* get_address_of_listenersAdded_10() { return &___listenersAdded_10; }
	inline void set_listenersAdded_10(bool value)
	{
		___listenersAdded_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRALLEVENTSTRIGGER_TE19B85857831C7B0002AF79DD44739DA855B9272_H
#ifndef GVRAUDIOLISTENER_TA0668B4EEF081D281606E35E2F2B46CD816492E4_H
#define GVRAUDIOLISTENER_TA0668B4EEF081D281606E35E2F2B46CD816492E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioListener
struct  GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single GvrAudioListener::globalGainDb
	float ___globalGainDb_4;
	// UnityEngine.LayerMask GvrAudioListener::occlusionMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___occlusionMask_5;
	// GvrAudio_Quality GvrAudioListener::quality
	int32_t ___quality_6;

public:
	inline static int32_t get_offset_of_globalGainDb_4() { return static_cast<int32_t>(offsetof(GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4, ___globalGainDb_4)); }
	inline float get_globalGainDb_4() const { return ___globalGainDb_4; }
	inline float* get_address_of_globalGainDb_4() { return &___globalGainDb_4; }
	inline void set_globalGainDb_4(float value)
	{
		___globalGainDb_4 = value;
	}

	inline static int32_t get_offset_of_occlusionMask_5() { return static_cast<int32_t>(offsetof(GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4, ___occlusionMask_5)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_occlusionMask_5() const { return ___occlusionMask_5; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_occlusionMask_5() { return &___occlusionMask_5; }
	inline void set_occlusionMask_5(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___occlusionMask_5 = value;
	}

	inline static int32_t get_offset_of_quality_6() { return static_cast<int32_t>(offsetof(GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4, ___quality_6)); }
	inline int32_t get_quality_6() const { return ___quality_6; }
	inline int32_t* get_address_of_quality_6() { return &___quality_6; }
	inline void set_quality_6(int32_t value)
	{
		___quality_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOLISTENER_TA0668B4EEF081D281606E35E2F2B46CD816492E4_H
#ifndef GVRAUDIOROOM_T573BA65F08C7EF54ACAB0B45D2FF314498038268_H
#define GVRAUDIOROOM_T573BA65F08C7EF54ACAB0B45D2FF314498038268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioRoom
struct  GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::leftWall
	int32_t ___leftWall_4;
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::rightWall
	int32_t ___rightWall_5;
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::floor
	int32_t ___floor_6;
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::ceiling
	int32_t ___ceiling_7;
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::backWall
	int32_t ___backWall_8;
	// GvrAudioRoom_SurfaceMaterial GvrAudioRoom::frontWall
	int32_t ___frontWall_9;
	// System.Single GvrAudioRoom::reflectivity
	float ___reflectivity_10;
	// System.Single GvrAudioRoom::reverbGainDb
	float ___reverbGainDb_11;
	// System.Single GvrAudioRoom::reverbBrightness
	float ___reverbBrightness_12;
	// System.Single GvrAudioRoom::reverbTime
	float ___reverbTime_13;
	// UnityEngine.Vector3 GvrAudioRoom::size
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___size_14;

public:
	inline static int32_t get_offset_of_leftWall_4() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___leftWall_4)); }
	inline int32_t get_leftWall_4() const { return ___leftWall_4; }
	inline int32_t* get_address_of_leftWall_4() { return &___leftWall_4; }
	inline void set_leftWall_4(int32_t value)
	{
		___leftWall_4 = value;
	}

	inline static int32_t get_offset_of_rightWall_5() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___rightWall_5)); }
	inline int32_t get_rightWall_5() const { return ___rightWall_5; }
	inline int32_t* get_address_of_rightWall_5() { return &___rightWall_5; }
	inline void set_rightWall_5(int32_t value)
	{
		___rightWall_5 = value;
	}

	inline static int32_t get_offset_of_floor_6() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___floor_6)); }
	inline int32_t get_floor_6() const { return ___floor_6; }
	inline int32_t* get_address_of_floor_6() { return &___floor_6; }
	inline void set_floor_6(int32_t value)
	{
		___floor_6 = value;
	}

	inline static int32_t get_offset_of_ceiling_7() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___ceiling_7)); }
	inline int32_t get_ceiling_7() const { return ___ceiling_7; }
	inline int32_t* get_address_of_ceiling_7() { return &___ceiling_7; }
	inline void set_ceiling_7(int32_t value)
	{
		___ceiling_7 = value;
	}

	inline static int32_t get_offset_of_backWall_8() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___backWall_8)); }
	inline int32_t get_backWall_8() const { return ___backWall_8; }
	inline int32_t* get_address_of_backWall_8() { return &___backWall_8; }
	inline void set_backWall_8(int32_t value)
	{
		___backWall_8 = value;
	}

	inline static int32_t get_offset_of_frontWall_9() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___frontWall_9)); }
	inline int32_t get_frontWall_9() const { return ___frontWall_9; }
	inline int32_t* get_address_of_frontWall_9() { return &___frontWall_9; }
	inline void set_frontWall_9(int32_t value)
	{
		___frontWall_9 = value;
	}

	inline static int32_t get_offset_of_reflectivity_10() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___reflectivity_10)); }
	inline float get_reflectivity_10() const { return ___reflectivity_10; }
	inline float* get_address_of_reflectivity_10() { return &___reflectivity_10; }
	inline void set_reflectivity_10(float value)
	{
		___reflectivity_10 = value;
	}

	inline static int32_t get_offset_of_reverbGainDb_11() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___reverbGainDb_11)); }
	inline float get_reverbGainDb_11() const { return ___reverbGainDb_11; }
	inline float* get_address_of_reverbGainDb_11() { return &___reverbGainDb_11; }
	inline void set_reverbGainDb_11(float value)
	{
		___reverbGainDb_11 = value;
	}

	inline static int32_t get_offset_of_reverbBrightness_12() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___reverbBrightness_12)); }
	inline float get_reverbBrightness_12() const { return ___reverbBrightness_12; }
	inline float* get_address_of_reverbBrightness_12() { return &___reverbBrightness_12; }
	inline void set_reverbBrightness_12(float value)
	{
		___reverbBrightness_12 = value;
	}

	inline static int32_t get_offset_of_reverbTime_13() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___reverbTime_13)); }
	inline float get_reverbTime_13() const { return ___reverbTime_13; }
	inline float* get_address_of_reverbTime_13() { return &___reverbTime_13; }
	inline void set_reverbTime_13(float value)
	{
		___reverbTime_13 = value;
	}

	inline static int32_t get_offset_of_size_14() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268, ___size_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_size_14() const { return ___size_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_size_14() { return &___size_14; }
	inline void set_size_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___size_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOROOM_T573BA65F08C7EF54ACAB0B45D2FF314498038268_H
#ifndef GVRAUDIOSOUNDFIELD_T3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8_H
#define GVRAUDIOSOUNDFIELD_T3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioSoundfield
struct  GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GvrAudioSoundfield::bypassRoomEffects
	bool ___bypassRoomEffects_4;
	// System.Single GvrAudioSoundfield::gainDb
	float ___gainDb_5;
	// System.Boolean GvrAudioSoundfield::playOnAwake
	bool ___playOnAwake_6;
	// UnityEngine.AudioClip GvrAudioSoundfield::soundfieldClip0102
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___soundfieldClip0102_7;
	// UnityEngine.AudioClip GvrAudioSoundfield::soundfieldClip0304
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___soundfieldClip0304_8;
	// System.Boolean GvrAudioSoundfield::soundfieldLoop
	bool ___soundfieldLoop_9;
	// System.Boolean GvrAudioSoundfield::soundfieldMute
	bool ___soundfieldMute_10;
	// System.Single GvrAudioSoundfield::soundfieldPitch
	float ___soundfieldPitch_11;
	// System.Int32 GvrAudioSoundfield::soundfieldPriority
	int32_t ___soundfieldPriority_12;
	// System.Single GvrAudioSoundfield::soundfieldSpatialBlend
	float ___soundfieldSpatialBlend_13;
	// System.Single GvrAudioSoundfield::soundfieldDopplerLevel
	float ___soundfieldDopplerLevel_14;
	// System.Single GvrAudioSoundfield::soundfieldVolume
	float ___soundfieldVolume_15;
	// UnityEngine.AudioRolloffMode GvrAudioSoundfield::soundfieldRolloffMode
	int32_t ___soundfieldRolloffMode_16;
	// System.Single GvrAudioSoundfield::soundfieldMaxDistance
	float ___soundfieldMaxDistance_17;
	// System.Single GvrAudioSoundfield::soundfieldMinDistance
	float ___soundfieldMinDistance_18;
	// System.Int32 GvrAudioSoundfield::id
	int32_t ___id_19;
	// UnityEngine.AudioSource[] GvrAudioSoundfield::audioSources
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___audioSources_20;
	// System.Boolean GvrAudioSoundfield::isPaused
	bool ___isPaused_21;

public:
	inline static int32_t get_offset_of_bypassRoomEffects_4() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___bypassRoomEffects_4)); }
	inline bool get_bypassRoomEffects_4() const { return ___bypassRoomEffects_4; }
	inline bool* get_address_of_bypassRoomEffects_4() { return &___bypassRoomEffects_4; }
	inline void set_bypassRoomEffects_4(bool value)
	{
		___bypassRoomEffects_4 = value;
	}

	inline static int32_t get_offset_of_gainDb_5() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___gainDb_5)); }
	inline float get_gainDb_5() const { return ___gainDb_5; }
	inline float* get_address_of_gainDb_5() { return &___gainDb_5; }
	inline void set_gainDb_5(float value)
	{
		___gainDb_5 = value;
	}

	inline static int32_t get_offset_of_playOnAwake_6() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___playOnAwake_6)); }
	inline bool get_playOnAwake_6() const { return ___playOnAwake_6; }
	inline bool* get_address_of_playOnAwake_6() { return &___playOnAwake_6; }
	inline void set_playOnAwake_6(bool value)
	{
		___playOnAwake_6 = value;
	}

	inline static int32_t get_offset_of_soundfieldClip0102_7() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldClip0102_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_soundfieldClip0102_7() const { return ___soundfieldClip0102_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_soundfieldClip0102_7() { return &___soundfieldClip0102_7; }
	inline void set_soundfieldClip0102_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___soundfieldClip0102_7 = value;
		Il2CppCodeGenWriteBarrier((&___soundfieldClip0102_7), value);
	}

	inline static int32_t get_offset_of_soundfieldClip0304_8() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldClip0304_8)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_soundfieldClip0304_8() const { return ___soundfieldClip0304_8; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_soundfieldClip0304_8() { return &___soundfieldClip0304_8; }
	inline void set_soundfieldClip0304_8(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___soundfieldClip0304_8 = value;
		Il2CppCodeGenWriteBarrier((&___soundfieldClip0304_8), value);
	}

	inline static int32_t get_offset_of_soundfieldLoop_9() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldLoop_9)); }
	inline bool get_soundfieldLoop_9() const { return ___soundfieldLoop_9; }
	inline bool* get_address_of_soundfieldLoop_9() { return &___soundfieldLoop_9; }
	inline void set_soundfieldLoop_9(bool value)
	{
		___soundfieldLoop_9 = value;
	}

	inline static int32_t get_offset_of_soundfieldMute_10() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldMute_10)); }
	inline bool get_soundfieldMute_10() const { return ___soundfieldMute_10; }
	inline bool* get_address_of_soundfieldMute_10() { return &___soundfieldMute_10; }
	inline void set_soundfieldMute_10(bool value)
	{
		___soundfieldMute_10 = value;
	}

	inline static int32_t get_offset_of_soundfieldPitch_11() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldPitch_11)); }
	inline float get_soundfieldPitch_11() const { return ___soundfieldPitch_11; }
	inline float* get_address_of_soundfieldPitch_11() { return &___soundfieldPitch_11; }
	inline void set_soundfieldPitch_11(float value)
	{
		___soundfieldPitch_11 = value;
	}

	inline static int32_t get_offset_of_soundfieldPriority_12() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldPriority_12)); }
	inline int32_t get_soundfieldPriority_12() const { return ___soundfieldPriority_12; }
	inline int32_t* get_address_of_soundfieldPriority_12() { return &___soundfieldPriority_12; }
	inline void set_soundfieldPriority_12(int32_t value)
	{
		___soundfieldPriority_12 = value;
	}

	inline static int32_t get_offset_of_soundfieldSpatialBlend_13() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldSpatialBlend_13)); }
	inline float get_soundfieldSpatialBlend_13() const { return ___soundfieldSpatialBlend_13; }
	inline float* get_address_of_soundfieldSpatialBlend_13() { return &___soundfieldSpatialBlend_13; }
	inline void set_soundfieldSpatialBlend_13(float value)
	{
		___soundfieldSpatialBlend_13 = value;
	}

	inline static int32_t get_offset_of_soundfieldDopplerLevel_14() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldDopplerLevel_14)); }
	inline float get_soundfieldDopplerLevel_14() const { return ___soundfieldDopplerLevel_14; }
	inline float* get_address_of_soundfieldDopplerLevel_14() { return &___soundfieldDopplerLevel_14; }
	inline void set_soundfieldDopplerLevel_14(float value)
	{
		___soundfieldDopplerLevel_14 = value;
	}

	inline static int32_t get_offset_of_soundfieldVolume_15() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldVolume_15)); }
	inline float get_soundfieldVolume_15() const { return ___soundfieldVolume_15; }
	inline float* get_address_of_soundfieldVolume_15() { return &___soundfieldVolume_15; }
	inline void set_soundfieldVolume_15(float value)
	{
		___soundfieldVolume_15 = value;
	}

	inline static int32_t get_offset_of_soundfieldRolloffMode_16() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldRolloffMode_16)); }
	inline int32_t get_soundfieldRolloffMode_16() const { return ___soundfieldRolloffMode_16; }
	inline int32_t* get_address_of_soundfieldRolloffMode_16() { return &___soundfieldRolloffMode_16; }
	inline void set_soundfieldRolloffMode_16(int32_t value)
	{
		___soundfieldRolloffMode_16 = value;
	}

	inline static int32_t get_offset_of_soundfieldMaxDistance_17() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldMaxDistance_17)); }
	inline float get_soundfieldMaxDistance_17() const { return ___soundfieldMaxDistance_17; }
	inline float* get_address_of_soundfieldMaxDistance_17() { return &___soundfieldMaxDistance_17; }
	inline void set_soundfieldMaxDistance_17(float value)
	{
		___soundfieldMaxDistance_17 = value;
	}

	inline static int32_t get_offset_of_soundfieldMinDistance_18() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___soundfieldMinDistance_18)); }
	inline float get_soundfieldMinDistance_18() const { return ___soundfieldMinDistance_18; }
	inline float* get_address_of_soundfieldMinDistance_18() { return &___soundfieldMinDistance_18; }
	inline void set_soundfieldMinDistance_18(float value)
	{
		___soundfieldMinDistance_18 = value;
	}

	inline static int32_t get_offset_of_id_19() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___id_19)); }
	inline int32_t get_id_19() const { return ___id_19; }
	inline int32_t* get_address_of_id_19() { return &___id_19; }
	inline void set_id_19(int32_t value)
	{
		___id_19 = value;
	}

	inline static int32_t get_offset_of_audioSources_20() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___audioSources_20)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_audioSources_20() const { return ___audioSources_20; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_audioSources_20() { return &___audioSources_20; }
	inline void set_audioSources_20(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___audioSources_20 = value;
		Il2CppCodeGenWriteBarrier((&___audioSources_20), value);
	}

	inline static int32_t get_offset_of_isPaused_21() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8, ___isPaused_21)); }
	inline bool get_isPaused_21() const { return ___isPaused_21; }
	inline bool* get_address_of_isPaused_21() { return &___isPaused_21; }
	inline void set_isPaused_21(bool value)
	{
		___isPaused_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOSOUNDFIELD_T3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8_H
#ifndef GVRAUDIOSOURCE_TCBABB1F18C1786B2B220395E3019FB68BA6DE4F6_H
#define GVRAUDIOSOURCE_TCBABB1F18C1786B2B220395E3019FB68BA6DE4F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioSource
struct  GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GvrAudioSource::bypassRoomEffects
	bool ___bypassRoomEffects_4;
	// System.Single GvrAudioSource::directivityAlpha
	float ___directivityAlpha_5;
	// System.Single GvrAudioSource::directivitySharpness
	float ___directivitySharpness_6;
	// System.Single GvrAudioSource::listenerDirectivityAlpha
	float ___listenerDirectivityAlpha_7;
	// System.Single GvrAudioSource::listenerDirectivitySharpness
	float ___listenerDirectivitySharpness_8;
	// System.Single GvrAudioSource::gainDb
	float ___gainDb_9;
	// System.Boolean GvrAudioSource::occlusionEnabled
	bool ___occlusionEnabled_10;
	// System.Boolean GvrAudioSource::playOnAwake
	bool ___playOnAwake_11;
	// UnityEngine.AudioClip GvrAudioSource::sourceClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sourceClip_12;
	// System.Boolean GvrAudioSource::sourceLoop
	bool ___sourceLoop_13;
	// System.Boolean GvrAudioSource::sourceMute
	bool ___sourceMute_14;
	// System.Single GvrAudioSource::sourcePitch
	float ___sourcePitch_15;
	// System.Int32 GvrAudioSource::sourcePriority
	int32_t ___sourcePriority_16;
	// System.Single GvrAudioSource::sourceSpatialBlend
	float ___sourceSpatialBlend_17;
	// System.Single GvrAudioSource::sourceDopplerLevel
	float ___sourceDopplerLevel_18;
	// System.Single GvrAudioSource::sourceSpread
	float ___sourceSpread_19;
	// System.Single GvrAudioSource::sourceVolume
	float ___sourceVolume_20;
	// UnityEngine.AudioRolloffMode GvrAudioSource::sourceRolloffMode
	int32_t ___sourceRolloffMode_21;
	// System.Single GvrAudioSource::sourceMaxDistance
	float ___sourceMaxDistance_22;
	// System.Single GvrAudioSource::sourceMinDistance
	float ___sourceMinDistance_23;
	// System.Boolean GvrAudioSource::hrtfEnabled
	bool ___hrtfEnabled_24;
	// UnityEngine.AudioSource GvrAudioSource::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_25;
	// System.Int32 GvrAudioSource::id
	int32_t ___id_26;
	// System.Single GvrAudioSource::currentOcclusion
	float ___currentOcclusion_27;
	// System.Single GvrAudioSource::nextOcclusionUpdate
	float ___nextOcclusionUpdate_28;
	// System.Boolean GvrAudioSource::isPaused
	bool ___isPaused_29;

public:
	inline static int32_t get_offset_of_bypassRoomEffects_4() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___bypassRoomEffects_4)); }
	inline bool get_bypassRoomEffects_4() const { return ___bypassRoomEffects_4; }
	inline bool* get_address_of_bypassRoomEffects_4() { return &___bypassRoomEffects_4; }
	inline void set_bypassRoomEffects_4(bool value)
	{
		___bypassRoomEffects_4 = value;
	}

	inline static int32_t get_offset_of_directivityAlpha_5() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___directivityAlpha_5)); }
	inline float get_directivityAlpha_5() const { return ___directivityAlpha_5; }
	inline float* get_address_of_directivityAlpha_5() { return &___directivityAlpha_5; }
	inline void set_directivityAlpha_5(float value)
	{
		___directivityAlpha_5 = value;
	}

	inline static int32_t get_offset_of_directivitySharpness_6() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___directivitySharpness_6)); }
	inline float get_directivitySharpness_6() const { return ___directivitySharpness_6; }
	inline float* get_address_of_directivitySharpness_6() { return &___directivitySharpness_6; }
	inline void set_directivitySharpness_6(float value)
	{
		___directivitySharpness_6 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivityAlpha_7() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___listenerDirectivityAlpha_7)); }
	inline float get_listenerDirectivityAlpha_7() const { return ___listenerDirectivityAlpha_7; }
	inline float* get_address_of_listenerDirectivityAlpha_7() { return &___listenerDirectivityAlpha_7; }
	inline void set_listenerDirectivityAlpha_7(float value)
	{
		___listenerDirectivityAlpha_7 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivitySharpness_8() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___listenerDirectivitySharpness_8)); }
	inline float get_listenerDirectivitySharpness_8() const { return ___listenerDirectivitySharpness_8; }
	inline float* get_address_of_listenerDirectivitySharpness_8() { return &___listenerDirectivitySharpness_8; }
	inline void set_listenerDirectivitySharpness_8(float value)
	{
		___listenerDirectivitySharpness_8 = value;
	}

	inline static int32_t get_offset_of_gainDb_9() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___gainDb_9)); }
	inline float get_gainDb_9() const { return ___gainDb_9; }
	inline float* get_address_of_gainDb_9() { return &___gainDb_9; }
	inline void set_gainDb_9(float value)
	{
		___gainDb_9 = value;
	}

	inline static int32_t get_offset_of_occlusionEnabled_10() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___occlusionEnabled_10)); }
	inline bool get_occlusionEnabled_10() const { return ___occlusionEnabled_10; }
	inline bool* get_address_of_occlusionEnabled_10() { return &___occlusionEnabled_10; }
	inline void set_occlusionEnabled_10(bool value)
	{
		___occlusionEnabled_10 = value;
	}

	inline static int32_t get_offset_of_playOnAwake_11() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___playOnAwake_11)); }
	inline bool get_playOnAwake_11() const { return ___playOnAwake_11; }
	inline bool* get_address_of_playOnAwake_11() { return &___playOnAwake_11; }
	inline void set_playOnAwake_11(bool value)
	{
		___playOnAwake_11 = value;
	}

	inline static int32_t get_offset_of_sourceClip_12() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceClip_12)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sourceClip_12() const { return ___sourceClip_12; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sourceClip_12() { return &___sourceClip_12; }
	inline void set_sourceClip_12(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sourceClip_12 = value;
		Il2CppCodeGenWriteBarrier((&___sourceClip_12), value);
	}

	inline static int32_t get_offset_of_sourceLoop_13() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceLoop_13)); }
	inline bool get_sourceLoop_13() const { return ___sourceLoop_13; }
	inline bool* get_address_of_sourceLoop_13() { return &___sourceLoop_13; }
	inline void set_sourceLoop_13(bool value)
	{
		___sourceLoop_13 = value;
	}

	inline static int32_t get_offset_of_sourceMute_14() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceMute_14)); }
	inline bool get_sourceMute_14() const { return ___sourceMute_14; }
	inline bool* get_address_of_sourceMute_14() { return &___sourceMute_14; }
	inline void set_sourceMute_14(bool value)
	{
		___sourceMute_14 = value;
	}

	inline static int32_t get_offset_of_sourcePitch_15() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourcePitch_15)); }
	inline float get_sourcePitch_15() const { return ___sourcePitch_15; }
	inline float* get_address_of_sourcePitch_15() { return &___sourcePitch_15; }
	inline void set_sourcePitch_15(float value)
	{
		___sourcePitch_15 = value;
	}

	inline static int32_t get_offset_of_sourcePriority_16() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourcePriority_16)); }
	inline int32_t get_sourcePriority_16() const { return ___sourcePriority_16; }
	inline int32_t* get_address_of_sourcePriority_16() { return &___sourcePriority_16; }
	inline void set_sourcePriority_16(int32_t value)
	{
		___sourcePriority_16 = value;
	}

	inline static int32_t get_offset_of_sourceSpatialBlend_17() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceSpatialBlend_17)); }
	inline float get_sourceSpatialBlend_17() const { return ___sourceSpatialBlend_17; }
	inline float* get_address_of_sourceSpatialBlend_17() { return &___sourceSpatialBlend_17; }
	inline void set_sourceSpatialBlend_17(float value)
	{
		___sourceSpatialBlend_17 = value;
	}

	inline static int32_t get_offset_of_sourceDopplerLevel_18() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceDopplerLevel_18)); }
	inline float get_sourceDopplerLevel_18() const { return ___sourceDopplerLevel_18; }
	inline float* get_address_of_sourceDopplerLevel_18() { return &___sourceDopplerLevel_18; }
	inline void set_sourceDopplerLevel_18(float value)
	{
		___sourceDopplerLevel_18 = value;
	}

	inline static int32_t get_offset_of_sourceSpread_19() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceSpread_19)); }
	inline float get_sourceSpread_19() const { return ___sourceSpread_19; }
	inline float* get_address_of_sourceSpread_19() { return &___sourceSpread_19; }
	inline void set_sourceSpread_19(float value)
	{
		___sourceSpread_19 = value;
	}

	inline static int32_t get_offset_of_sourceVolume_20() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceVolume_20)); }
	inline float get_sourceVolume_20() const { return ___sourceVolume_20; }
	inline float* get_address_of_sourceVolume_20() { return &___sourceVolume_20; }
	inline void set_sourceVolume_20(float value)
	{
		___sourceVolume_20 = value;
	}

	inline static int32_t get_offset_of_sourceRolloffMode_21() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceRolloffMode_21)); }
	inline int32_t get_sourceRolloffMode_21() const { return ___sourceRolloffMode_21; }
	inline int32_t* get_address_of_sourceRolloffMode_21() { return &___sourceRolloffMode_21; }
	inline void set_sourceRolloffMode_21(int32_t value)
	{
		___sourceRolloffMode_21 = value;
	}

	inline static int32_t get_offset_of_sourceMaxDistance_22() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceMaxDistance_22)); }
	inline float get_sourceMaxDistance_22() const { return ___sourceMaxDistance_22; }
	inline float* get_address_of_sourceMaxDistance_22() { return &___sourceMaxDistance_22; }
	inline void set_sourceMaxDistance_22(float value)
	{
		___sourceMaxDistance_22 = value;
	}

	inline static int32_t get_offset_of_sourceMinDistance_23() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___sourceMinDistance_23)); }
	inline float get_sourceMinDistance_23() const { return ___sourceMinDistance_23; }
	inline float* get_address_of_sourceMinDistance_23() { return &___sourceMinDistance_23; }
	inline void set_sourceMinDistance_23(float value)
	{
		___sourceMinDistance_23 = value;
	}

	inline static int32_t get_offset_of_hrtfEnabled_24() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___hrtfEnabled_24)); }
	inline bool get_hrtfEnabled_24() const { return ___hrtfEnabled_24; }
	inline bool* get_address_of_hrtfEnabled_24() { return &___hrtfEnabled_24; }
	inline void set_hrtfEnabled_24(bool value)
	{
		___hrtfEnabled_24 = value;
	}

	inline static int32_t get_offset_of_audioSource_25() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___audioSource_25)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_25() const { return ___audioSource_25; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_25() { return &___audioSource_25; }
	inline void set_audioSource_25(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_25 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_25), value);
	}

	inline static int32_t get_offset_of_id_26() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___id_26)); }
	inline int32_t get_id_26() const { return ___id_26; }
	inline int32_t* get_address_of_id_26() { return &___id_26; }
	inline void set_id_26(int32_t value)
	{
		___id_26 = value;
	}

	inline static int32_t get_offset_of_currentOcclusion_27() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___currentOcclusion_27)); }
	inline float get_currentOcclusion_27() const { return ___currentOcclusion_27; }
	inline float* get_address_of_currentOcclusion_27() { return &___currentOcclusion_27; }
	inline void set_currentOcclusion_27(float value)
	{
		___currentOcclusion_27 = value;
	}

	inline static int32_t get_offset_of_nextOcclusionUpdate_28() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___nextOcclusionUpdate_28)); }
	inline float get_nextOcclusionUpdate_28() const { return ___nextOcclusionUpdate_28; }
	inline float* get_address_of_nextOcclusionUpdate_28() { return &___nextOcclusionUpdate_28; }
	inline void set_nextOcclusionUpdate_28(float value)
	{
		___nextOcclusionUpdate_28 = value;
	}

	inline static int32_t get_offset_of_isPaused_29() { return static_cast<int32_t>(offsetof(GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6, ___isPaused_29)); }
	inline bool get_isPaused_29() const { return ___isPaused_29; }
	inline bool* get_address_of_isPaused_29() { return &___isPaused_29; }
	inline void set_isPaused_29(bool value)
	{
		___isPaused_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOSOURCE_TCBABB1F18C1786B2B220395E3019FB68BA6DE4F6_H
#ifndef GVRBASEARMMODEL_T8D41D802A58F630557FAE6C739028B16B76240AE_H
#define GVRBASEARMMODEL_T8D41D802A58F630557FAE6C739028B16B76240AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBaseArmModel
struct  GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEARMMODEL_T8D41D802A58F630557FAE6C739028B16B76240AE_H
#ifndef GVRBASEPOINTER_T2E2389BAFCF1D39C6C6EE4997589B3879EBC855D_H
#define GVRBASEPOINTER_T2E2389BAFCF1D39C6C6EE4997589B3879EBC855D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer
struct  GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrBasePointer_RaycastMode GvrBasePointer::raycastMode
	int32_t ___raycastMode_4;
	// UnityEngine.Camera GvrBasePointer::overridePointerCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___overridePointerCamera_5;
	// System.Boolean GvrBasePointer::<ShouldUseExitRadiusForRaycast>k__BackingField
	bool ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6;
	// GvrControllerInputDevice GvrBasePointer::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * ___U3CControllerInputDeviceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_raycastMode_4() { return static_cast<int32_t>(offsetof(GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D, ___raycastMode_4)); }
	inline int32_t get_raycastMode_4() const { return ___raycastMode_4; }
	inline int32_t* get_address_of_raycastMode_4() { return &___raycastMode_4; }
	inline void set_raycastMode_4(int32_t value)
	{
		___raycastMode_4 = value;
	}

	inline static int32_t get_offset_of_overridePointerCamera_5() { return static_cast<int32_t>(offsetof(GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D, ___overridePointerCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_overridePointerCamera_5() const { return ___overridePointerCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_overridePointerCamera_5() { return &___overridePointerCamera_5; }
	inline void set_overridePointerCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___overridePointerCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___overridePointerCamera_5), value);
	}

	inline static int32_t get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D, ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6)); }
	inline bool get_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() const { return ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() { return &___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6; }
	inline void set_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6(bool value)
	{
		___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D, ___U3CControllerInputDeviceU3Ek__BackingField_7)); }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * get_U3CControllerInputDeviceU3Ek__BackingField_7() const { return ___U3CControllerInputDeviceU3Ek__BackingField_7; }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_7() { return &___U3CControllerInputDeviceU3Ek__BackingField_7; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_7(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTER_T2E2389BAFCF1D39C6C6EE4997589B3879EBC855D_H
#ifndef GVRCONTROLLERINPUT_T8DE5CC74523C7C84CA79CAC8399898A248FD5926_H
#define GVRCONTROLLERINPUT_T8DE5CC74523C7C84CA79CAC8399898A248FD5926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput
struct  GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrControllerInput_EmulatorConnectionMode GvrControllerInput::emulatorConnectionMode
	int32_t ___emulatorConnectionMode_10;

public:
	inline static int32_t get_offset_of_emulatorConnectionMode_10() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926, ___emulatorConnectionMode_10)); }
	inline int32_t get_emulatorConnectionMode_10() const { return ___emulatorConnectionMode_10; }
	inline int32_t* get_address_of_emulatorConnectionMode_10() { return &___emulatorConnectionMode_10; }
	inline void set_emulatorConnectionMode_10(int32_t value)
	{
		___emulatorConnectionMode_10 = value;
	}
};

struct GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields
{
public:
	// GvrControllerInputDevice[] GvrControllerInput::instances
	GvrControllerInputDeviceU5BU5D_t54B7B7395F76D730CAEF030BB0B0A283916EB480* ___instances_4;
	// Gvr.Internal.IControllerProvider GvrControllerInput::controllerProvider
	RuntimeObject* ___controllerProvider_5;
	// GvrSettings_UserPrefsHandedness GvrControllerInput::handedness
	int32_t ___handedness_6;
	// System.Action GvrControllerInput::onDevicesChangedInternal
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onDevicesChangedInternal_7;
	// System.Action GvrControllerInput::OnControllerInputUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnControllerInputUpdated_8;
	// System.Action GvrControllerInput::OnPostControllerInputUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnPostControllerInputUpdated_9;

public:
	inline static int32_t get_offset_of_instances_4() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___instances_4)); }
	inline GvrControllerInputDeviceU5BU5D_t54B7B7395F76D730CAEF030BB0B0A283916EB480* get_instances_4() const { return ___instances_4; }
	inline GvrControllerInputDeviceU5BU5D_t54B7B7395F76D730CAEF030BB0B0A283916EB480** get_address_of_instances_4() { return &___instances_4; }
	inline void set_instances_4(GvrControllerInputDeviceU5BU5D_t54B7B7395F76D730CAEF030BB0B0A283916EB480* value)
	{
		___instances_4 = value;
		Il2CppCodeGenWriteBarrier((&___instances_4), value);
	}

	inline static int32_t get_offset_of_controllerProvider_5() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___controllerProvider_5)); }
	inline RuntimeObject* get_controllerProvider_5() const { return ___controllerProvider_5; }
	inline RuntimeObject** get_address_of_controllerProvider_5() { return &___controllerProvider_5; }
	inline void set_controllerProvider_5(RuntimeObject* value)
	{
		___controllerProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___controllerProvider_5), value);
	}

	inline static int32_t get_offset_of_handedness_6() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___handedness_6)); }
	inline int32_t get_handedness_6() const { return ___handedness_6; }
	inline int32_t* get_address_of_handedness_6() { return &___handedness_6; }
	inline void set_handedness_6(int32_t value)
	{
		___handedness_6 = value;
	}

	inline static int32_t get_offset_of_onDevicesChangedInternal_7() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___onDevicesChangedInternal_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onDevicesChangedInternal_7() const { return ___onDevicesChangedInternal_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onDevicesChangedInternal_7() { return &___onDevicesChangedInternal_7; }
	inline void set_onDevicesChangedInternal_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onDevicesChangedInternal_7 = value;
		Il2CppCodeGenWriteBarrier((&___onDevicesChangedInternal_7), value);
	}

	inline static int32_t get_offset_of_OnControllerInputUpdated_8() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___OnControllerInputUpdated_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnControllerInputUpdated_8() const { return ___OnControllerInputUpdated_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnControllerInputUpdated_8() { return &___OnControllerInputUpdated_8; }
	inline void set_OnControllerInputUpdated_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnControllerInputUpdated_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnControllerInputUpdated_8), value);
	}

	inline static int32_t get_offset_of_OnPostControllerInputUpdated_9() { return static_cast<int32_t>(offsetof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields, ___OnPostControllerInputUpdated_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnPostControllerInputUpdated_9() const { return ___OnPostControllerInputUpdated_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnPostControllerInputUpdated_9() { return &___OnPostControllerInputUpdated_9; }
	inline void set_OnPostControllerInputUpdated_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnPostControllerInputUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostControllerInputUpdated_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERINPUT_T8DE5CC74523C7C84CA79CAC8399898A248FD5926_H
#ifndef GVRCONTROLLERRETICLEVISUAL_TDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6_H
#define GVRCONTROLLERRETICLEVISUAL_TDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerReticleVisual
struct  GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GvrControllerReticleVisual::isSizeBasedOnCameraDistance
	bool ___isSizeBasedOnCameraDistance_4;
	// System.Single GvrControllerReticleVisual::sizeMeters
	float ___sizeMeters_5;
	// GvrControllerReticleVisual_FaceCameraData GvrControllerReticleVisual::doesReticleFaceCamera
	FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428  ___doesReticleFaceCamera_6;
	// System.Int32 GvrControllerReticleVisual::sortingOrder
	int32_t ___sortingOrder_7;
	// System.Single GvrControllerReticleVisual::<ReticleMeshSizeMeters>k__BackingField
	float ___U3CReticleMeshSizeMetersU3Ek__BackingField_8;
	// System.Single GvrControllerReticleVisual::<ReticleMeshSizeRatio>k__BackingField
	float ___U3CReticleMeshSizeRatioU3Ek__BackingField_9;
	// UnityEngine.MeshRenderer GvrControllerReticleVisual::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_10;
	// UnityEngine.MeshFilter GvrControllerReticleVisual::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_11;
	// UnityEngine.Vector3 GvrControllerReticleVisual::preRenderLocalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___preRenderLocalScale_12;
	// UnityEngine.Quaternion GvrControllerReticleVisual::preRenderLocalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___preRenderLocalRotation_13;

public:
	inline static int32_t get_offset_of_isSizeBasedOnCameraDistance_4() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___isSizeBasedOnCameraDistance_4)); }
	inline bool get_isSizeBasedOnCameraDistance_4() const { return ___isSizeBasedOnCameraDistance_4; }
	inline bool* get_address_of_isSizeBasedOnCameraDistance_4() { return &___isSizeBasedOnCameraDistance_4; }
	inline void set_isSizeBasedOnCameraDistance_4(bool value)
	{
		___isSizeBasedOnCameraDistance_4 = value;
	}

	inline static int32_t get_offset_of_sizeMeters_5() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___sizeMeters_5)); }
	inline float get_sizeMeters_5() const { return ___sizeMeters_5; }
	inline float* get_address_of_sizeMeters_5() { return &___sizeMeters_5; }
	inline void set_sizeMeters_5(float value)
	{
		___sizeMeters_5 = value;
	}

	inline static int32_t get_offset_of_doesReticleFaceCamera_6() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___doesReticleFaceCamera_6)); }
	inline FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428  get_doesReticleFaceCamera_6() const { return ___doesReticleFaceCamera_6; }
	inline FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428 * get_address_of_doesReticleFaceCamera_6() { return &___doesReticleFaceCamera_6; }
	inline void set_doesReticleFaceCamera_6(FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428  value)
	{
		___doesReticleFaceCamera_6 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_7() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___sortingOrder_7)); }
	inline int32_t get_sortingOrder_7() const { return ___sortingOrder_7; }
	inline int32_t* get_address_of_sortingOrder_7() { return &___sortingOrder_7; }
	inline void set_sortingOrder_7(int32_t value)
	{
		___sortingOrder_7 = value;
	}

	inline static int32_t get_offset_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___U3CReticleMeshSizeMetersU3Ek__BackingField_8)); }
	inline float get_U3CReticleMeshSizeMetersU3Ek__BackingField_8() const { return ___U3CReticleMeshSizeMetersU3Ek__BackingField_8; }
	inline float* get_address_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8() { return &___U3CReticleMeshSizeMetersU3Ek__BackingField_8; }
	inline void set_U3CReticleMeshSizeMetersU3Ek__BackingField_8(float value)
	{
		___U3CReticleMeshSizeMetersU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___U3CReticleMeshSizeRatioU3Ek__BackingField_9)); }
	inline float get_U3CReticleMeshSizeRatioU3Ek__BackingField_9() const { return ___U3CReticleMeshSizeRatioU3Ek__BackingField_9; }
	inline float* get_address_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9() { return &___U3CReticleMeshSizeRatioU3Ek__BackingField_9; }
	inline void set_U3CReticleMeshSizeRatioU3Ek__BackingField_9(float value)
	{
		___U3CReticleMeshSizeRatioU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_meshRenderer_10() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___meshRenderer_10)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_10() const { return ___meshRenderer_10; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_10() { return &___meshRenderer_10; }
	inline void set_meshRenderer_10(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_10), value);
	}

	inline static int32_t get_offset_of_meshFilter_11() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___meshFilter_11)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_11() const { return ___meshFilter_11; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_11() { return &___meshFilter_11; }
	inline void set_meshFilter_11(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_11), value);
	}

	inline static int32_t get_offset_of_preRenderLocalScale_12() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___preRenderLocalScale_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_preRenderLocalScale_12() const { return ___preRenderLocalScale_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_preRenderLocalScale_12() { return &___preRenderLocalScale_12; }
	inline void set_preRenderLocalScale_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___preRenderLocalScale_12 = value;
	}

	inline static int32_t get_offset_of_preRenderLocalRotation_13() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6, ___preRenderLocalRotation_13)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_preRenderLocalRotation_13() const { return ___preRenderLocalRotation_13; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_preRenderLocalRotation_13() { return &___preRenderLocalRotation_13; }
	inline void set_preRenderLocalRotation_13(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___preRenderLocalRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERRETICLEVISUAL_TDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6_H
#ifndef GVRCONTROLLERTOOLTIPSSIMPLE_T0575897A0AB7289613E1556AD83E920CA4378C42_H
#define GVRCONTROLLERTOOLTIPSSIMPLE_T0575897A0AB7289613E1556AD83E920CA4378C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerTooltipsSimple
struct  GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.MeshRenderer GvrControllerTooltipsSimple::tooltipRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___tooltipRenderer_4;
	// GvrBaseArmModel GvrControllerTooltipsSimple::<ArmModel>k__BackingField
	GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * ___U3CArmModelU3Ek__BackingField_5;
	// UnityEngine.MaterialPropertyBlock GvrControllerTooltipsSimple::materialPropertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___materialPropertyBlock_6;
	// System.Int32 GvrControllerTooltipsSimple::colorId
	int32_t ___colorId_7;

public:
	inline static int32_t get_offset_of_tooltipRenderer_4() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42, ___tooltipRenderer_4)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_tooltipRenderer_4() const { return ___tooltipRenderer_4; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_tooltipRenderer_4() { return &___tooltipRenderer_4; }
	inline void set_tooltipRenderer_4(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___tooltipRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipRenderer_4), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42, ___U3CArmModelU3Ek__BackingField_5)); }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * get_U3CArmModelU3Ek__BackingField_5() const { return ___U3CArmModelU3Ek__BackingField_5; }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE ** get_address_of_U3CArmModelU3Ek__BackingField_5() { return &___U3CArmModelU3Ek__BackingField_5; }
	inline void set_U3CArmModelU3Ek__BackingField_5(GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * value)
	{
		___U3CArmModelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_6() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42, ___materialPropertyBlock_6)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_materialPropertyBlock_6() const { return ___materialPropertyBlock_6; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_materialPropertyBlock_6() { return &___materialPropertyBlock_6; }
	inline void set_materialPropertyBlock_6(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___materialPropertyBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_6), value);
	}

	inline static int32_t get_offset_of_colorId_7() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42, ___colorId_7)); }
	inline int32_t get_colorId_7() const { return ___colorId_7; }
	inline int32_t* get_address_of_colorId_7() { return &___colorId_7; }
	inline void set_colorId_7(int32_t value)
	{
		___colorId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERTOOLTIPSSIMPLE_T0575897A0AB7289613E1556AD83E920CA4378C42_H
#ifndef GVRCONTROLLERVISUAL_T59A828DA23AF5380628F2D5593A8913431CFE003_H
#define GVRCONTROLLERVISUAL_T59A828DA23AF5380628F2D5593A8913431CFE003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisual
struct  GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] GvrControllerVisual::attachmentPrefabs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___attachmentPrefabs_4;
	// UnityEngine.Color GvrControllerVisual::touchPadColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___touchPadColor_5;
	// UnityEngine.Color GvrControllerVisual::appButtonColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___appButtonColor_6;
	// UnityEngine.Color GvrControllerVisual::systemButtonColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___systemButtonColor_7;
	// System.Boolean GvrControllerVisual::readControllerState
	bool ___readControllerState_8;
	// GvrControllerVisual_ControllerDisplayState GvrControllerVisual::displayState
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F  ___displayState_9;
	// System.Single GvrControllerVisual::maximumAlpha
	float ___maximumAlpha_10;
	// GvrBaseArmModel GvrControllerVisual::<ArmModel>k__BackingField
	GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * ___U3CArmModelU3Ek__BackingField_11;
	// GvrControllerInputDevice GvrControllerVisual::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * ___U3CControllerInputDeviceU3Ek__BackingField_12;
	// UnityEngine.Renderer GvrControllerVisual::controllerRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___controllerRenderer_13;
	// UnityEngine.MaterialPropertyBlock GvrControllerVisual::materialPropertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___materialPropertyBlock_14;
	// System.Int32 GvrControllerVisual::alphaId
	int32_t ___alphaId_15;
	// System.Int32 GvrControllerVisual::touchId
	int32_t ___touchId_16;
	// System.Int32 GvrControllerVisual::touchPadId
	int32_t ___touchPadId_17;
	// System.Int32 GvrControllerVisual::appButtonId
	int32_t ___appButtonId_18;
	// System.Int32 GvrControllerVisual::systemButtonId
	int32_t ___systemButtonId_19;
	// System.Int32 GvrControllerVisual::batteryColorId
	int32_t ___batteryColorId_20;
	// System.Boolean GvrControllerVisual::wasTouching
	bool ___wasTouching_21;
	// System.Single GvrControllerVisual::touchTime
	float ___touchTime_22;
	// UnityEngine.Vector4 GvrControllerVisual::controllerShaderData
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___controllerShaderData_23;
	// UnityEngine.Vector4 GvrControllerVisual::controllerShaderData2
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___controllerShaderData2_24;
	// UnityEngine.Color GvrControllerVisual::currentBatteryColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___currentBatteryColor_25;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_CRITICAL_COLOR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___GVR_BATTERY_CRITICAL_COLOR_40;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_LOW_COLOR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___GVR_BATTERY_LOW_COLOR_41;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_MED_COLOR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___GVR_BATTERY_MED_COLOR_42;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_FULL_COLOR
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___GVR_BATTERY_FULL_COLOR_43;

public:
	inline static int32_t get_offset_of_attachmentPrefabs_4() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___attachmentPrefabs_4)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_attachmentPrefabs_4() const { return ___attachmentPrefabs_4; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_attachmentPrefabs_4() { return &___attachmentPrefabs_4; }
	inline void set_attachmentPrefabs_4(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___attachmentPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentPrefabs_4), value);
	}

	inline static int32_t get_offset_of_touchPadColor_5() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___touchPadColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_touchPadColor_5() const { return ___touchPadColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_touchPadColor_5() { return &___touchPadColor_5; }
	inline void set_touchPadColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___touchPadColor_5 = value;
	}

	inline static int32_t get_offset_of_appButtonColor_6() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___appButtonColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_appButtonColor_6() const { return ___appButtonColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_appButtonColor_6() { return &___appButtonColor_6; }
	inline void set_appButtonColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___appButtonColor_6 = value;
	}

	inline static int32_t get_offset_of_systemButtonColor_7() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___systemButtonColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_systemButtonColor_7() const { return ___systemButtonColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_systemButtonColor_7() { return &___systemButtonColor_7; }
	inline void set_systemButtonColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___systemButtonColor_7 = value;
	}

	inline static int32_t get_offset_of_readControllerState_8() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___readControllerState_8)); }
	inline bool get_readControllerState_8() const { return ___readControllerState_8; }
	inline bool* get_address_of_readControllerState_8() { return &___readControllerState_8; }
	inline void set_readControllerState_8(bool value)
	{
		___readControllerState_8 = value;
	}

	inline static int32_t get_offset_of_displayState_9() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___displayState_9)); }
	inline ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F  get_displayState_9() const { return ___displayState_9; }
	inline ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F * get_address_of_displayState_9() { return &___displayState_9; }
	inline void set_displayState_9(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F  value)
	{
		___displayState_9 = value;
	}

	inline static int32_t get_offset_of_maximumAlpha_10() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___maximumAlpha_10)); }
	inline float get_maximumAlpha_10() const { return ___maximumAlpha_10; }
	inline float* get_address_of_maximumAlpha_10() { return &___maximumAlpha_10; }
	inline void set_maximumAlpha_10(float value)
	{
		___maximumAlpha_10 = value;
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___U3CArmModelU3Ek__BackingField_11)); }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * get_U3CArmModelU3Ek__BackingField_11() const { return ___U3CArmModelU3Ek__BackingField_11; }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE ** get_address_of_U3CArmModelU3Ek__BackingField_11() { return &___U3CArmModelU3Ek__BackingField_11; }
	inline void set_U3CArmModelU3Ek__BackingField_11(GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * value)
	{
		___U3CArmModelU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___U3CControllerInputDeviceU3Ek__BackingField_12)); }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * get_U3CControllerInputDeviceU3Ek__BackingField_12() const { return ___U3CControllerInputDeviceU3Ek__BackingField_12; }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_12() { return &___U3CControllerInputDeviceU3Ek__BackingField_12; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_12(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_controllerRenderer_13() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___controllerRenderer_13)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_controllerRenderer_13() const { return ___controllerRenderer_13; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_controllerRenderer_13() { return &___controllerRenderer_13; }
	inline void set_controllerRenderer_13(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___controllerRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&___controllerRenderer_13), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_14() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___materialPropertyBlock_14)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_materialPropertyBlock_14() const { return ___materialPropertyBlock_14; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_materialPropertyBlock_14() { return &___materialPropertyBlock_14; }
	inline void set_materialPropertyBlock_14(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___materialPropertyBlock_14 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_14), value);
	}

	inline static int32_t get_offset_of_alphaId_15() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___alphaId_15)); }
	inline int32_t get_alphaId_15() const { return ___alphaId_15; }
	inline int32_t* get_address_of_alphaId_15() { return &___alphaId_15; }
	inline void set_alphaId_15(int32_t value)
	{
		___alphaId_15 = value;
	}

	inline static int32_t get_offset_of_touchId_16() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___touchId_16)); }
	inline int32_t get_touchId_16() const { return ___touchId_16; }
	inline int32_t* get_address_of_touchId_16() { return &___touchId_16; }
	inline void set_touchId_16(int32_t value)
	{
		___touchId_16 = value;
	}

	inline static int32_t get_offset_of_touchPadId_17() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___touchPadId_17)); }
	inline int32_t get_touchPadId_17() const { return ___touchPadId_17; }
	inline int32_t* get_address_of_touchPadId_17() { return &___touchPadId_17; }
	inline void set_touchPadId_17(int32_t value)
	{
		___touchPadId_17 = value;
	}

	inline static int32_t get_offset_of_appButtonId_18() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___appButtonId_18)); }
	inline int32_t get_appButtonId_18() const { return ___appButtonId_18; }
	inline int32_t* get_address_of_appButtonId_18() { return &___appButtonId_18; }
	inline void set_appButtonId_18(int32_t value)
	{
		___appButtonId_18 = value;
	}

	inline static int32_t get_offset_of_systemButtonId_19() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___systemButtonId_19)); }
	inline int32_t get_systemButtonId_19() const { return ___systemButtonId_19; }
	inline int32_t* get_address_of_systemButtonId_19() { return &___systemButtonId_19; }
	inline void set_systemButtonId_19(int32_t value)
	{
		___systemButtonId_19 = value;
	}

	inline static int32_t get_offset_of_batteryColorId_20() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___batteryColorId_20)); }
	inline int32_t get_batteryColorId_20() const { return ___batteryColorId_20; }
	inline int32_t* get_address_of_batteryColorId_20() { return &___batteryColorId_20; }
	inline void set_batteryColorId_20(int32_t value)
	{
		___batteryColorId_20 = value;
	}

	inline static int32_t get_offset_of_wasTouching_21() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___wasTouching_21)); }
	inline bool get_wasTouching_21() const { return ___wasTouching_21; }
	inline bool* get_address_of_wasTouching_21() { return &___wasTouching_21; }
	inline void set_wasTouching_21(bool value)
	{
		___wasTouching_21 = value;
	}

	inline static int32_t get_offset_of_touchTime_22() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___touchTime_22)); }
	inline float get_touchTime_22() const { return ___touchTime_22; }
	inline float* get_address_of_touchTime_22() { return &___touchTime_22; }
	inline void set_touchTime_22(float value)
	{
		___touchTime_22 = value;
	}

	inline static int32_t get_offset_of_controllerShaderData_23() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___controllerShaderData_23)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_controllerShaderData_23() const { return ___controllerShaderData_23; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_controllerShaderData_23() { return &___controllerShaderData_23; }
	inline void set_controllerShaderData_23(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___controllerShaderData_23 = value;
	}

	inline static int32_t get_offset_of_controllerShaderData2_24() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___controllerShaderData2_24)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_controllerShaderData2_24() const { return ___controllerShaderData2_24; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_controllerShaderData2_24() { return &___controllerShaderData2_24; }
	inline void set_controllerShaderData2_24(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___controllerShaderData2_24 = value;
	}

	inline static int32_t get_offset_of_currentBatteryColor_25() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___currentBatteryColor_25)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_currentBatteryColor_25() const { return ___currentBatteryColor_25; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_currentBatteryColor_25() { return &___currentBatteryColor_25; }
	inline void set_currentBatteryColor_25(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___currentBatteryColor_25 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_CRITICAL_COLOR_40() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___GVR_BATTERY_CRITICAL_COLOR_40)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_GVR_BATTERY_CRITICAL_COLOR_40() const { return ___GVR_BATTERY_CRITICAL_COLOR_40; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_GVR_BATTERY_CRITICAL_COLOR_40() { return &___GVR_BATTERY_CRITICAL_COLOR_40; }
	inline void set_GVR_BATTERY_CRITICAL_COLOR_40(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___GVR_BATTERY_CRITICAL_COLOR_40 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_LOW_COLOR_41() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___GVR_BATTERY_LOW_COLOR_41)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_GVR_BATTERY_LOW_COLOR_41() const { return ___GVR_BATTERY_LOW_COLOR_41; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_GVR_BATTERY_LOW_COLOR_41() { return &___GVR_BATTERY_LOW_COLOR_41; }
	inline void set_GVR_BATTERY_LOW_COLOR_41(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___GVR_BATTERY_LOW_COLOR_41 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_MED_COLOR_42() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___GVR_BATTERY_MED_COLOR_42)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_GVR_BATTERY_MED_COLOR_42() const { return ___GVR_BATTERY_MED_COLOR_42; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_GVR_BATTERY_MED_COLOR_42() { return &___GVR_BATTERY_MED_COLOR_42; }
	inline void set_GVR_BATTERY_MED_COLOR_42(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___GVR_BATTERY_MED_COLOR_42 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_FULL_COLOR_43() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003, ___GVR_BATTERY_FULL_COLOR_43)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_GVR_BATTERY_FULL_COLOR_43() const { return ___GVR_BATTERY_FULL_COLOR_43; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_GVR_BATTERY_FULL_COLOR_43() { return &___GVR_BATTERY_FULL_COLOR_43; }
	inline void set_GVR_BATTERY_FULL_COLOR_43(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___GVR_BATTERY_FULL_COLOR_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERVISUAL_T59A828DA23AF5380628F2D5593A8913431CFE003_H
#ifndef GVREDITOREMULATOR_TF953E2075E00E7B749B688060320DE25681966DD_H
#define GVREDITOREMULATOR_TF953E2075E00E7B749B688060320DE25681966DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEditorEmulator
struct  GvrEditorEmulator_tF953E2075E00E7B749B688060320DE25681966DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREDITOREMULATOR_TF953E2075E00E7B749B688060320DE25681966DD_H
#ifndef GVRLASERVISUAL_T7C90095C3AB0299526189657F7F0B599C89B3633_H
#define GVRLASERVISUAL_T7C90095C3AB0299526189657F7F0B599C89B3633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserVisual
struct  GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrControllerReticleVisual GvrLaserVisual::reticle
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6 * ___reticle_4;
	// UnityEngine.Transform GvrLaserVisual::controller
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___controller_5;
	// UnityEngine.Color GvrLaserVisual::laserColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___laserColor_6;
	// UnityEngine.Color GvrLaserVisual::laserColorEnd
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___laserColorEnd_7;
	// System.Single GvrLaserVisual::maxLaserDistance
	float ___maxLaserDistance_8;
	// System.Single GvrLaserVisual::lerpSpeed
	float ___lerpSpeed_9;
	// System.Single GvrLaserVisual::lerpThreshold
	float ___lerpThreshold_10;
	// System.Boolean GvrLaserVisual::shrinkLaser
	bool ___shrinkLaser_11;
	// System.Single GvrLaserVisual::shrunkScale
	float ___shrunkScale_12;
	// System.Single GvrLaserVisual::beginShrinkAngleDegrees
	float ___beginShrinkAngleDegrees_13;
	// System.Single GvrLaserVisual::endShrinkAngleDegrees
	float ___endShrinkAngleDegrees_14;
	// GvrBaseArmModel GvrLaserVisual::<ArmModel>k__BackingField
	GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * ___U3CArmModelU3Ek__BackingField_16;
	// UnityEngine.LineRenderer GvrLaserVisual::<Laser>k__BackingField
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___U3CLaserU3Ek__BackingField_17;
	// GvrLaserVisual_GetPointForDistanceDelegate GvrLaserVisual::<GetPointForDistanceFunction>k__BackingField
	GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869 * ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18;
	// System.Single GvrLaserVisual::shrinkRatio
	float ___shrinkRatio_19;
	// System.Single GvrLaserVisual::targetDistance
	float ___targetDistance_20;
	// System.Single GvrLaserVisual::currentDistance
	float ___currentDistance_21;
	// UnityEngine.Vector3 GvrLaserVisual::currentPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPosition_22;
	// UnityEngine.Vector3 GvrLaserVisual::currentLocalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentLocalPosition_23;
	// UnityEngine.Quaternion GvrLaserVisual::currentLocalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___currentLocalRotation_24;

public:
	inline static int32_t get_offset_of_reticle_4() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___reticle_4)); }
	inline GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6 * get_reticle_4() const { return ___reticle_4; }
	inline GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6 ** get_address_of_reticle_4() { return &___reticle_4; }
	inline void set_reticle_4(GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6 * value)
	{
		___reticle_4 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_4), value);
	}

	inline static int32_t get_offset_of_controller_5() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___controller_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_controller_5() const { return ___controller_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_controller_5() { return &___controller_5; }
	inline void set_controller_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___controller_5 = value;
		Il2CppCodeGenWriteBarrier((&___controller_5), value);
	}

	inline static int32_t get_offset_of_laserColor_6() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___laserColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_laserColor_6() const { return ___laserColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_laserColor_6() { return &___laserColor_6; }
	inline void set_laserColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___laserColor_6 = value;
	}

	inline static int32_t get_offset_of_laserColorEnd_7() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___laserColorEnd_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_laserColorEnd_7() const { return ___laserColorEnd_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_laserColorEnd_7() { return &___laserColorEnd_7; }
	inline void set_laserColorEnd_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___laserColorEnd_7 = value;
	}

	inline static int32_t get_offset_of_maxLaserDistance_8() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___maxLaserDistance_8)); }
	inline float get_maxLaserDistance_8() const { return ___maxLaserDistance_8; }
	inline float* get_address_of_maxLaserDistance_8() { return &___maxLaserDistance_8; }
	inline void set_maxLaserDistance_8(float value)
	{
		___maxLaserDistance_8 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_9() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___lerpSpeed_9)); }
	inline float get_lerpSpeed_9() const { return ___lerpSpeed_9; }
	inline float* get_address_of_lerpSpeed_9() { return &___lerpSpeed_9; }
	inline void set_lerpSpeed_9(float value)
	{
		___lerpSpeed_9 = value;
	}

	inline static int32_t get_offset_of_lerpThreshold_10() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___lerpThreshold_10)); }
	inline float get_lerpThreshold_10() const { return ___lerpThreshold_10; }
	inline float* get_address_of_lerpThreshold_10() { return &___lerpThreshold_10; }
	inline void set_lerpThreshold_10(float value)
	{
		___lerpThreshold_10 = value;
	}

	inline static int32_t get_offset_of_shrinkLaser_11() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___shrinkLaser_11)); }
	inline bool get_shrinkLaser_11() const { return ___shrinkLaser_11; }
	inline bool* get_address_of_shrinkLaser_11() { return &___shrinkLaser_11; }
	inline void set_shrinkLaser_11(bool value)
	{
		___shrinkLaser_11 = value;
	}

	inline static int32_t get_offset_of_shrunkScale_12() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___shrunkScale_12)); }
	inline float get_shrunkScale_12() const { return ___shrunkScale_12; }
	inline float* get_address_of_shrunkScale_12() { return &___shrunkScale_12; }
	inline void set_shrunkScale_12(float value)
	{
		___shrunkScale_12 = value;
	}

	inline static int32_t get_offset_of_beginShrinkAngleDegrees_13() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___beginShrinkAngleDegrees_13)); }
	inline float get_beginShrinkAngleDegrees_13() const { return ___beginShrinkAngleDegrees_13; }
	inline float* get_address_of_beginShrinkAngleDegrees_13() { return &___beginShrinkAngleDegrees_13; }
	inline void set_beginShrinkAngleDegrees_13(float value)
	{
		___beginShrinkAngleDegrees_13 = value;
	}

	inline static int32_t get_offset_of_endShrinkAngleDegrees_14() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___endShrinkAngleDegrees_14)); }
	inline float get_endShrinkAngleDegrees_14() const { return ___endShrinkAngleDegrees_14; }
	inline float* get_address_of_endShrinkAngleDegrees_14() { return &___endShrinkAngleDegrees_14; }
	inline void set_endShrinkAngleDegrees_14(float value)
	{
		___endShrinkAngleDegrees_14 = value;
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___U3CArmModelU3Ek__BackingField_16)); }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * get_U3CArmModelU3Ek__BackingField_16() const { return ___U3CArmModelU3Ek__BackingField_16; }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE ** get_address_of_U3CArmModelU3Ek__BackingField_16() { return &___U3CArmModelU3Ek__BackingField_16; }
	inline void set_U3CArmModelU3Ek__BackingField_16(GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * value)
	{
		___U3CArmModelU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CLaserU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___U3CLaserU3Ek__BackingField_17)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_U3CLaserU3Ek__BackingField_17() const { return ___U3CLaserU3Ek__BackingField_17; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_U3CLaserU3Ek__BackingField_17() { return &___U3CLaserU3Ek__BackingField_17; }
	inline void set_U3CLaserU3Ek__BackingField_17(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___U3CLaserU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLaserU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18)); }
	inline GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869 * get_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() const { return ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18; }
	inline GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869 ** get_address_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() { return &___U3CGetPointForDistanceFunctionU3Ek__BackingField_18; }
	inline void set_U3CGetPointForDistanceFunctionU3Ek__BackingField_18(GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869 * value)
	{
		___U3CGetPointForDistanceFunctionU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetPointForDistanceFunctionU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_shrinkRatio_19() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___shrinkRatio_19)); }
	inline float get_shrinkRatio_19() const { return ___shrinkRatio_19; }
	inline float* get_address_of_shrinkRatio_19() { return &___shrinkRatio_19; }
	inline void set_shrinkRatio_19(float value)
	{
		___shrinkRatio_19 = value;
	}

	inline static int32_t get_offset_of_targetDistance_20() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___targetDistance_20)); }
	inline float get_targetDistance_20() const { return ___targetDistance_20; }
	inline float* get_address_of_targetDistance_20() { return &___targetDistance_20; }
	inline void set_targetDistance_20(float value)
	{
		___targetDistance_20 = value;
	}

	inline static int32_t get_offset_of_currentDistance_21() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___currentDistance_21)); }
	inline float get_currentDistance_21() const { return ___currentDistance_21; }
	inline float* get_address_of_currentDistance_21() { return &___currentDistance_21; }
	inline void set_currentDistance_21(float value)
	{
		___currentDistance_21 = value;
	}

	inline static int32_t get_offset_of_currentPosition_22() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___currentPosition_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentPosition_22() const { return ___currentPosition_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentPosition_22() { return &___currentPosition_22; }
	inline void set_currentPosition_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentPosition_22 = value;
	}

	inline static int32_t get_offset_of_currentLocalPosition_23() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___currentLocalPosition_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentLocalPosition_23() const { return ___currentLocalPosition_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentLocalPosition_23() { return &___currentLocalPosition_23; }
	inline void set_currentLocalPosition_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentLocalPosition_23 = value;
	}

	inline static int32_t get_offset_of_currentLocalRotation_24() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633, ___currentLocalRotation_24)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_currentLocalRotation_24() const { return ___currentLocalRotation_24; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_currentLocalRotation_24() { return &___currentLocalRotation_24; }
	inline void set_currentLocalRotation_24(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___currentLocalRotation_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRLASERVISUAL_T7C90095C3AB0299526189657F7F0B599C89B3633_H
#ifndef GVRPOINTERMANAGER_TD4F8B06940E60BA591ADBFF80A78E08BCEBDB520_H
#define GVRPOINTERMANAGER_TD4F8B06940E60BA591ADBFF80A78E08BCEBDB520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerManager
struct  GvrPointerManager_tD4F8B06940E60BA591ADBFF80A78E08BCEBDB520  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERMANAGER_TD4F8B06940E60BA591ADBFF80A78E08BCEBDB520_H
#ifndef GVRRECENTERONLYCONTROLLER_T5A1A3C56508BC7EF0FC5292840919C0B3073C965_H
#define GVRRECENTERONLYCONTROLLER_T5A1A3C56508BC7EF0FC5292840919C0B3073C965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterOnlyController
struct  GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Quaternion GvrRecenterOnlyController::lastAppliedYawCorrection
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___lastAppliedYawCorrection_4;
	// UnityEngine.Quaternion GvrRecenterOnlyController::yawCorrection
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___yawCorrection_5;

public:
	inline static int32_t get_offset_of_lastAppliedYawCorrection_4() { return static_cast<int32_t>(offsetof(GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965, ___lastAppliedYawCorrection_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_lastAppliedYawCorrection_4() const { return ___lastAppliedYawCorrection_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_lastAppliedYawCorrection_4() { return &___lastAppliedYawCorrection_4; }
	inline void set_lastAppliedYawCorrection_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___lastAppliedYawCorrection_4 = value;
	}

	inline static int32_t get_offset_of_yawCorrection_5() { return static_cast<int32_t>(offsetof(GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965, ___yawCorrection_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_yawCorrection_5() const { return ___yawCorrection_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_yawCorrection_5() { return &___yawCorrection_5; }
	inline void set_yawCorrection_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___yawCorrection_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERONLYCONTROLLER_T5A1A3C56508BC7EF0FC5292840919C0B3073C965_H
#ifndef GVRSCROLLSETTINGS_T78386FFD18A2EBCFB97B8788386ECAD431C494BB_H
#define GVRSCROLLSETTINGS_T78386FFD18A2EBCFB97B8788386ECAD431C494BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrScrollSettings
struct  GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GvrScrollSettings::inertiaOverride
	bool ___inertiaOverride_4;
	// System.Single GvrScrollSettings::decelerationRateOverride
	float ___decelerationRateOverride_5;

public:
	inline static int32_t get_offset_of_inertiaOverride_4() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB, ___inertiaOverride_4)); }
	inline bool get_inertiaOverride_4() const { return ___inertiaOverride_4; }
	inline bool* get_address_of_inertiaOverride_4() { return &___inertiaOverride_4; }
	inline void set_inertiaOverride_4(bool value)
	{
		___inertiaOverride_4 = value;
	}

	inline static int32_t get_offset_of_decelerationRateOverride_5() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB, ___decelerationRateOverride_5)); }
	inline float get_decelerationRateOverride_5() const { return ___decelerationRateOverride_5; }
	inline float* get_address_of_decelerationRateOverride_5() { return &___decelerationRateOverride_5; }
	inline void set_decelerationRateOverride_5(float value)
	{
		___decelerationRateOverride_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSCROLLSETTINGS_T78386FFD18A2EBCFB97B8788386ECAD431C494BB_H
#ifndef GVRTOOLTIP_T4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_H
#define GVRTOOLTIP_T4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip
struct  GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrTooltip_Location GvrTooltip::location
	int32_t ___location_11;
	// UnityEngine.UI.Text GvrTooltip::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_12;
	// System.Boolean GvrTooltip::alwaysVisible
	bool ___alwaysVisible_13;
	// System.Boolean GvrTooltip::isOnLeft
	bool ___isOnLeft_14;
	// UnityEngine.RectTransform GvrTooltip::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_15;
	// UnityEngine.CanvasGroup GvrTooltip::canvasGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___canvasGroup_16;
	// GvrBaseArmModel GvrTooltip::<ArmModel>k__BackingField
	GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * ___U3CArmModelU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_location_11() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___location_11)); }
	inline int32_t get_location_11() const { return ___location_11; }
	inline int32_t* get_address_of_location_11() { return &___location_11; }
	inline void set_location_11(int32_t value)
	{
		___location_11 = value;
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___text_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_12() const { return ___text_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier((&___text_12), value);
	}

	inline static int32_t get_offset_of_alwaysVisible_13() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___alwaysVisible_13)); }
	inline bool get_alwaysVisible_13() const { return ___alwaysVisible_13; }
	inline bool* get_address_of_alwaysVisible_13() { return &___alwaysVisible_13; }
	inline void set_alwaysVisible_13(bool value)
	{
		___alwaysVisible_13 = value;
	}

	inline static int32_t get_offset_of_isOnLeft_14() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___isOnLeft_14)); }
	inline bool get_isOnLeft_14() const { return ___isOnLeft_14; }
	inline bool* get_address_of_isOnLeft_14() { return &___isOnLeft_14; }
	inline void set_isOnLeft_14(bool value)
	{
		___isOnLeft_14 = value;
	}

	inline static int32_t get_offset_of_rectTransform_15() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___rectTransform_15)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_15() const { return ___rectTransform_15; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_15() { return &___rectTransform_15; }
	inline void set_rectTransform_15(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_15), value);
	}

	inline static int32_t get_offset_of_canvasGroup_16() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___canvasGroup_16)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_canvasGroup_16() const { return ___canvasGroup_16; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_canvasGroup_16() { return &___canvasGroup_16; }
	inline void set_canvasGroup_16(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___canvasGroup_16 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_16), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42, ___U3CArmModelU3Ek__BackingField_17)); }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * get_U3CArmModelU3Ek__BackingField_17() const { return ___U3CArmModelU3Ek__BackingField_17; }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE ** get_address_of_U3CArmModelU3Ek__BackingField_17() { return &___U3CArmModelU3Ek__BackingField_17; }
	inline void set_U3CArmModelU3Ek__BackingField_17(GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * value)
	{
		___U3CArmModelU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_17), value);
	}
};

struct GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields
{
public:
	// UnityEngine.Quaternion GvrTooltip::RIGHT_SIDE_ROTATION
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___RIGHT_SIDE_ROTATION_4;
	// UnityEngine.Quaternion GvrTooltip::LEFT_SIDE_ROTATION
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___LEFT_SIDE_ROTATION_5;
	// UnityEngine.Vector2 GvrTooltip::SQUARE_CENTER
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___SQUARE_CENTER_6;
	// UnityEngine.Vector2 GvrTooltip::PIVOT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___PIVOT_7;

public:
	inline static int32_t get_offset_of_RIGHT_SIDE_ROTATION_4() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields, ___RIGHT_SIDE_ROTATION_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_RIGHT_SIDE_ROTATION_4() const { return ___RIGHT_SIDE_ROTATION_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_RIGHT_SIDE_ROTATION_4() { return &___RIGHT_SIDE_ROTATION_4; }
	inline void set_RIGHT_SIDE_ROTATION_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___RIGHT_SIDE_ROTATION_4 = value;
	}

	inline static int32_t get_offset_of_LEFT_SIDE_ROTATION_5() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields, ___LEFT_SIDE_ROTATION_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_LEFT_SIDE_ROTATION_5() const { return ___LEFT_SIDE_ROTATION_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_LEFT_SIDE_ROTATION_5() { return &___LEFT_SIDE_ROTATION_5; }
	inline void set_LEFT_SIDE_ROTATION_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___LEFT_SIDE_ROTATION_5 = value;
	}

	inline static int32_t get_offset_of_SQUARE_CENTER_6() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields, ___SQUARE_CENTER_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_SQUARE_CENTER_6() const { return ___SQUARE_CENTER_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_SQUARE_CENTER_6() { return &___SQUARE_CENTER_6; }
	inline void set_SQUARE_CENTER_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___SQUARE_CENTER_6 = value;
	}

	inline static int32_t get_offset_of_PIVOT_7() { return static_cast<int32_t>(offsetof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields, ___PIVOT_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_PIVOT_7() const { return ___PIVOT_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_PIVOT_7() { return &___PIVOT_7; }
	inline void set_PIVOT_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___PIVOT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRTOOLTIP_T4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_H
#ifndef GVRTRACKEDCONTROLLER_TD2C6093BA5D682467A5B478153581199982633E5_H
#define GVRTRACKEDCONTROLLER_TD2C6093BA5D682467A5B478153581199982633E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTrackedController
struct  GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GvrBaseArmModel GvrTrackedController::armModel
	GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * ___armModel_4;
	// GvrControllerInputDevice GvrTrackedController::controllerInputDevice
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * ___controllerInputDevice_5;
	// System.Boolean GvrTrackedController::isDeactivatedWhenDisconnected
	bool ___isDeactivatedWhenDisconnected_6;
	// GvrControllerHand GvrTrackedController::controllerHand
	int32_t ___controllerHand_7;

public:
	inline static int32_t get_offset_of_armModel_4() { return static_cast<int32_t>(offsetof(GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5, ___armModel_4)); }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * get_armModel_4() const { return ___armModel_4; }
	inline GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE ** get_address_of_armModel_4() { return &___armModel_4; }
	inline void set_armModel_4(GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE * value)
	{
		___armModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___armModel_4), value);
	}

	inline static int32_t get_offset_of_controllerInputDevice_5() { return static_cast<int32_t>(offsetof(GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5, ___controllerInputDevice_5)); }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * get_controllerInputDevice_5() const { return ___controllerInputDevice_5; }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 ** get_address_of_controllerInputDevice_5() { return &___controllerInputDevice_5; }
	inline void set_controllerInputDevice_5(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * value)
	{
		___controllerInputDevice_5 = value;
		Il2CppCodeGenWriteBarrier((&___controllerInputDevice_5), value);
	}

	inline static int32_t get_offset_of_isDeactivatedWhenDisconnected_6() { return static_cast<int32_t>(offsetof(GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5, ___isDeactivatedWhenDisconnected_6)); }
	inline bool get_isDeactivatedWhenDisconnected_6() const { return ___isDeactivatedWhenDisconnected_6; }
	inline bool* get_address_of_isDeactivatedWhenDisconnected_6() { return &___isDeactivatedWhenDisconnected_6; }
	inline void set_isDeactivatedWhenDisconnected_6(bool value)
	{
		___isDeactivatedWhenDisconnected_6 = value;
	}

	inline static int32_t get_offset_of_controllerHand_7() { return static_cast<int32_t>(offsetof(GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5, ___controllerHand_7)); }
	inline int32_t get_controllerHand_7() const { return ___controllerHand_7; }
	inline int32_t* get_address_of_controllerHand_7() { return &___controllerHand_7; }
	inline void set_controllerHand_7(int32_t value)
	{
		___controllerHand_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRTRACKEDCONTROLLER_TD2C6093BA5D682467A5B478153581199982633E5_H
#ifndef GVRXREVENTSSUBSCRIBER_TE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_H
#define GVRXREVENTSSUBSCRIBER_TE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrXREventsSubscriber
struct  GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String GvrXREventsSubscriber::_loadedDeviceName
	String_t* ____loadedDeviceName_5;

public:
	inline static int32_t get_offset_of__loadedDeviceName_5() { return static_cast<int32_t>(offsetof(GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A, ____loadedDeviceName_5)); }
	inline String_t* get__loadedDeviceName_5() const { return ____loadedDeviceName_5; }
	inline String_t** get_address_of__loadedDeviceName_5() { return &____loadedDeviceName_5; }
	inline void set__loadedDeviceName_5(String_t* value)
	{
		____loadedDeviceName_5 = value;
		Il2CppCodeGenWriteBarrier((&____loadedDeviceName_5), value);
	}
};

struct GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_StaticFields
{
public:
	// GvrXREventsSubscriber GvrXREventsSubscriber::instance
	GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_StaticFields, ___instance_4)); }
	inline GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A * get_instance_4() const { return ___instance_4; }
	inline GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRXREVENTSSUBSCRIBER_TE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_H
#ifndef MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#define MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MainMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean MainMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image MainMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject MainMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject MainMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean MainMenu::isLow
	bool ___isLow_9;
	// System.Boolean MainMenu::isMid
	bool ___isMid_10;
	// System.Boolean MainMenu::isHigh
	bool ___isHigh_11;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isLow_9() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___isLow_9)); }
	inline bool get_isLow_9() const { return ___isLow_9; }
	inline bool* get_address_of_isLow_9() { return &___isLow_9; }
	inline void set_isLow_9(bool value)
	{
		___isLow_9 = value;
	}

	inline static int32_t get_offset_of_isMid_10() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___isMid_10)); }
	inline bool get_isMid_10() const { return ___isMid_10; }
	inline bool* get_address_of_isMid_10() { return &___isMid_10; }
	inline void set_isMid_10(bool value)
	{
		___isMid_10 = value;
	}

	inline static int32_t get_offset_of_isHigh_11() { return static_cast<int32_t>(offsetof(MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105, ___isHigh_11)); }
	inline bool get_isHigh_11() const { return ___isHigh_11; }
	inline bool* get_address_of_isHigh_11() { return &___isHigh_11; }
	inline void set_isHigh_11(bool value)
	{
		___isHigh_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifndef QUIZANIMALSOUND_TF7945591B3819B9716312895356E8C3DAD69511D_H
#define QUIZANIMALSOUND_TF7945591B3819B9716312895356E8C3DAD69511D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuizAnimalSound
struct  QuizAnimalSound_tF7945591B3819B9716312895356E8C3DAD69511D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip QuizAnimalSound::sound_animal
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_animal_4;

public:
	inline static int32_t get_offset_of_sound_animal_4() { return static_cast<int32_t>(offsetof(QuizAnimalSound_tF7945591B3819B9716312895356E8C3DAD69511D, ___sound_animal_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_animal_4() const { return ___sound_animal_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_animal_4() { return &___sound_animal_4; }
	inline void set_sound_animal_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_animal_4 = value;
		Il2CppCodeGenWriteBarrier((&___sound_animal_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUIZANIMALSOUND_TF7945591B3819B9716312895356E8C3DAD69511D_H
#ifndef RESTTIMER_TBAF7BFB4237061BE276C78F7AE6B388D9345EB93_H
#define RESTTIMER_TBAF7BFB4237061BE276C78F7AE6B388D9345EB93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestTimer
struct  RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields
{
public:
	// System.Single RestTimer::timer
	float ___timer_4;
	// System.Boolean RestTimer::isStop
	bool ___isStop_5;

public:
	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}

	inline static int32_t get_offset_of_isStop_5() { return static_cast<int32_t>(offsetof(RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields, ___isStop_5)); }
	inline bool get_isStop_5() const { return ___isStop_5; }
	inline bool* get_address_of_isStop_5() { return &___isStop_5; }
	inline void set_isStop_5(bool value)
	{
		___isStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTTIMER_TBAF7BFB4237061BE276C78F7AE6B388D9345EB93_H
#ifndef SEAANIMALCONTROLLER_T89766BF927D6FEC862E9C7188AA865C42E362B51_H
#define SEAANIMALCONTROLLER_T89766BF927D6FEC862E9C7188AA865C42E362B51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaAnimalController
struct  SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform SeaAnimalController::player
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___player_4;
	// UnityEngine.Transform SeaAnimalController::originalLocation
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___originalLocation_5;
	// System.Single SeaAnimalController::buttontimer
	float ___buttontimer_6;
	// System.Boolean SeaAnimalController::buttontime
	bool ___buttontime_7;
	// System.Double SeaAnimalController::arrivedPosition
	double ___arrivedPosition_9;
	// System.Double SeaAnimalController::Runtimer
	double ___Runtimer_10;
	// System.Boolean SeaAnimalController::walk
	bool ___walk_11;
	// System.Double SeaAnimalController::backtimer
	double ___backtimer_12;
	// System.Boolean SeaAnimalController::isConfirmButton
	bool ___isConfirmButton_13;
	// System.Boolean SeaAnimalController::isSoundButton
	bool ___isSoundButton_14;
	// System.Boolean SeaAnimalController::isTranslateButton
	bool ___isTranslateButton_15;
	// System.Boolean SeaAnimalController::isBack
	bool ___isBack_16;
	// System.Boolean SeaAnimalController::isEng
	bool ___isEng_17;
	// UnityEngine.UI.Image SeaAnimalController::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_18;
	// UnityEngine.GameObject SeaAnimalController::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_19;
	// UnityEngine.GameObject SeaAnimalController::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_20;
	// UnityEngine.GameObject SeaAnimalController::obj_animals
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_animals_21;
	// UnityEngine.GameObject SeaAnimalController::obj_original
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_original_22;
	// UnityEngine.GameObject SeaAnimalController::obj_infocanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_infocanvas_23;
	// UnityEngine.UI.Text SeaAnimalController::gui_title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_title_24;
	// UnityEngine.UI.Text SeaAnimalController::gui_message
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_message_25;
	// System.String SeaAnimalController::str_title
	String_t* ___str_title_26;
	// System.String SeaAnimalController::str_title_chi
	String_t* ___str_title_chi_27;
	// System.String SeaAnimalController::str_message
	String_t* ___str_message_28;
	// System.String SeaAnimalController::str_message_chi
	String_t* ___str_message_chi_29;
	// UnityEngine.AudioClip SeaAnimalController::sound_pronunciation
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___sound_pronunciation_30;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___player_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_player_4() const { return ___player_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_originalLocation_5() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___originalLocation_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_originalLocation_5() const { return ___originalLocation_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_originalLocation_5() { return &___originalLocation_5; }
	inline void set_originalLocation_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___originalLocation_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalLocation_5), value);
	}

	inline static int32_t get_offset_of_buttontimer_6() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___buttontimer_6)); }
	inline float get_buttontimer_6() const { return ___buttontimer_6; }
	inline float* get_address_of_buttontimer_6() { return &___buttontimer_6; }
	inline void set_buttontimer_6(float value)
	{
		___buttontimer_6 = value;
	}

	inline static int32_t get_offset_of_buttontime_7() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___buttontime_7)); }
	inline bool get_buttontime_7() const { return ___buttontime_7; }
	inline bool* get_address_of_buttontime_7() { return &___buttontime_7; }
	inline void set_buttontime_7(bool value)
	{
		___buttontime_7 = value;
	}

	inline static int32_t get_offset_of_arrivedPosition_9() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___arrivedPosition_9)); }
	inline double get_arrivedPosition_9() const { return ___arrivedPosition_9; }
	inline double* get_address_of_arrivedPosition_9() { return &___arrivedPosition_9; }
	inline void set_arrivedPosition_9(double value)
	{
		___arrivedPosition_9 = value;
	}

	inline static int32_t get_offset_of_Runtimer_10() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___Runtimer_10)); }
	inline double get_Runtimer_10() const { return ___Runtimer_10; }
	inline double* get_address_of_Runtimer_10() { return &___Runtimer_10; }
	inline void set_Runtimer_10(double value)
	{
		___Runtimer_10 = value;
	}

	inline static int32_t get_offset_of_walk_11() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___walk_11)); }
	inline bool get_walk_11() const { return ___walk_11; }
	inline bool* get_address_of_walk_11() { return &___walk_11; }
	inline void set_walk_11(bool value)
	{
		___walk_11 = value;
	}

	inline static int32_t get_offset_of_backtimer_12() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___backtimer_12)); }
	inline double get_backtimer_12() const { return ___backtimer_12; }
	inline double* get_address_of_backtimer_12() { return &___backtimer_12; }
	inline void set_backtimer_12(double value)
	{
		___backtimer_12 = value;
	}

	inline static int32_t get_offset_of_isConfirmButton_13() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___isConfirmButton_13)); }
	inline bool get_isConfirmButton_13() const { return ___isConfirmButton_13; }
	inline bool* get_address_of_isConfirmButton_13() { return &___isConfirmButton_13; }
	inline void set_isConfirmButton_13(bool value)
	{
		___isConfirmButton_13 = value;
	}

	inline static int32_t get_offset_of_isSoundButton_14() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___isSoundButton_14)); }
	inline bool get_isSoundButton_14() const { return ___isSoundButton_14; }
	inline bool* get_address_of_isSoundButton_14() { return &___isSoundButton_14; }
	inline void set_isSoundButton_14(bool value)
	{
		___isSoundButton_14 = value;
	}

	inline static int32_t get_offset_of_isTranslateButton_15() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___isTranslateButton_15)); }
	inline bool get_isTranslateButton_15() const { return ___isTranslateButton_15; }
	inline bool* get_address_of_isTranslateButton_15() { return &___isTranslateButton_15; }
	inline void set_isTranslateButton_15(bool value)
	{
		___isTranslateButton_15 = value;
	}

	inline static int32_t get_offset_of_isBack_16() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___isBack_16)); }
	inline bool get_isBack_16() const { return ___isBack_16; }
	inline bool* get_address_of_isBack_16() { return &___isBack_16; }
	inline void set_isBack_16(bool value)
	{
		___isBack_16 = value;
	}

	inline static int32_t get_offset_of_isEng_17() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___isEng_17)); }
	inline bool get_isEng_17() const { return ___isEng_17; }
	inline bool* get_address_of_isEng_17() { return &___isEng_17; }
	inline void set_isEng_17(bool value)
	{
		___isEng_17 = value;
	}

	inline static int32_t get_offset_of_Bar_18() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___Bar_18)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_18() const { return ___Bar_18; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_18() { return &___Bar_18; }
	inline void set_Bar_18(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_18 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_18), value);
	}

	inline static int32_t get_offset_of_obj_Bar_19() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___obj_Bar_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_19() const { return ___obj_Bar_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_19() { return &___obj_Bar_19; }
	inline void set_obj_Bar_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_19 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_19), value);
	}

	inline static int32_t get_offset_of_obj_shoot_20() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___obj_shoot_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_20() const { return ___obj_shoot_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_20() { return &___obj_shoot_20; }
	inline void set_obj_shoot_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_20), value);
	}

	inline static int32_t get_offset_of_obj_animals_21() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___obj_animals_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_animals_21() const { return ___obj_animals_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_animals_21() { return &___obj_animals_21; }
	inline void set_obj_animals_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_animals_21 = value;
		Il2CppCodeGenWriteBarrier((&___obj_animals_21), value);
	}

	inline static int32_t get_offset_of_obj_original_22() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___obj_original_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_original_22() const { return ___obj_original_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_original_22() { return &___obj_original_22; }
	inline void set_obj_original_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_original_22 = value;
		Il2CppCodeGenWriteBarrier((&___obj_original_22), value);
	}

	inline static int32_t get_offset_of_obj_infocanvas_23() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___obj_infocanvas_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_infocanvas_23() const { return ___obj_infocanvas_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_infocanvas_23() { return &___obj_infocanvas_23; }
	inline void set_obj_infocanvas_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_infocanvas_23 = value;
		Il2CppCodeGenWriteBarrier((&___obj_infocanvas_23), value);
	}

	inline static int32_t get_offset_of_gui_title_24() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___gui_title_24)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_title_24() const { return ___gui_title_24; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_title_24() { return &___gui_title_24; }
	inline void set_gui_title_24(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_title_24 = value;
		Il2CppCodeGenWriteBarrier((&___gui_title_24), value);
	}

	inline static int32_t get_offset_of_gui_message_25() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___gui_message_25)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_message_25() const { return ___gui_message_25; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_message_25() { return &___gui_message_25; }
	inline void set_gui_message_25(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_message_25 = value;
		Il2CppCodeGenWriteBarrier((&___gui_message_25), value);
	}

	inline static int32_t get_offset_of_str_title_26() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___str_title_26)); }
	inline String_t* get_str_title_26() const { return ___str_title_26; }
	inline String_t** get_address_of_str_title_26() { return &___str_title_26; }
	inline void set_str_title_26(String_t* value)
	{
		___str_title_26 = value;
		Il2CppCodeGenWriteBarrier((&___str_title_26), value);
	}

	inline static int32_t get_offset_of_str_title_chi_27() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___str_title_chi_27)); }
	inline String_t* get_str_title_chi_27() const { return ___str_title_chi_27; }
	inline String_t** get_address_of_str_title_chi_27() { return &___str_title_chi_27; }
	inline void set_str_title_chi_27(String_t* value)
	{
		___str_title_chi_27 = value;
		Il2CppCodeGenWriteBarrier((&___str_title_chi_27), value);
	}

	inline static int32_t get_offset_of_str_message_28() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___str_message_28)); }
	inline String_t* get_str_message_28() const { return ___str_message_28; }
	inline String_t** get_address_of_str_message_28() { return &___str_message_28; }
	inline void set_str_message_28(String_t* value)
	{
		___str_message_28 = value;
		Il2CppCodeGenWriteBarrier((&___str_message_28), value);
	}

	inline static int32_t get_offset_of_str_message_chi_29() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___str_message_chi_29)); }
	inline String_t* get_str_message_chi_29() const { return ___str_message_chi_29; }
	inline String_t** get_address_of_str_message_chi_29() { return &___str_message_chi_29; }
	inline void set_str_message_chi_29(String_t* value)
	{
		___str_message_chi_29 = value;
		Il2CppCodeGenWriteBarrier((&___str_message_chi_29), value);
	}

	inline static int32_t get_offset_of_sound_pronunciation_30() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51, ___sound_pronunciation_30)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_sound_pronunciation_30() const { return ___sound_pronunciation_30; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_sound_pronunciation_30() { return &___sound_pronunciation_30; }
	inline void set_sound_pronunciation_30(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___sound_pronunciation_30 = value;
		Il2CppCodeGenWriteBarrier((&___sound_pronunciation_30), value);
	}
};

struct SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51_StaticFields
{
public:
	// System.Boolean SeaAnimalController::isEnable
	bool ___isEnable_8;

public:
	inline static int32_t get_offset_of_isEnable_8() { return static_cast<int32_t>(offsetof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51_StaticFields, ___isEnable_8)); }
	inline bool get_isEnable_8() const { return ___isEnable_8; }
	inline bool* get_address_of_isEnable_8() { return &___isEnable_8; }
	inline void set_isEnable_8(bool value)
	{
		___isEnable_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEAANIMALCONTROLLER_T89766BF927D6FEC862E9C7188AA865C42E362B51_H
#ifndef SEAHIDEANIMALCONTROLLER_T3711301C8F9B2A5D10AFB59E05B04173C1C10354_H
#define SEAHIDEANIMALCONTROLLER_T3711301C8F9B2A5D10AFB59E05B04173C1C10354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaHideAnimalController
struct  SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields
{
public:
	// UnityEngine.GameObject SeaHideAnimalController::obj_Octopus
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Octopus_4;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Crab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Crab_5;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Starfish
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Starfish_6;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Dolphin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Dolphin_7;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Sea_Urchin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Sea_Urchin_8;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Moray_Eel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Moray_Eel_9;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Long_Finned_Pilot_Whale
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Long_Finned_Pilot_Whale_10;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Killer_Whale
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Killer_Whale_11;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Basking_Shark
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Basking_Shark_12;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Great_White_Shark
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Great_White_Shark_13;
	// UnityEngine.GameObject SeaHideAnimalController::obj_Squid
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Squid_14;

public:
	inline static int32_t get_offset_of_obj_Octopus_4() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Octopus_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Octopus_4() const { return ___obj_Octopus_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Octopus_4() { return &___obj_Octopus_4; }
	inline void set_obj_Octopus_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Octopus_4 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Octopus_4), value);
	}

	inline static int32_t get_offset_of_obj_Crab_5() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Crab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Crab_5() const { return ___obj_Crab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Crab_5() { return &___obj_Crab_5; }
	inline void set_obj_Crab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Crab_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Crab_5), value);
	}

	inline static int32_t get_offset_of_obj_Starfish_6() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Starfish_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Starfish_6() const { return ___obj_Starfish_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Starfish_6() { return &___obj_Starfish_6; }
	inline void set_obj_Starfish_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Starfish_6 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Starfish_6), value);
	}

	inline static int32_t get_offset_of_obj_Dolphin_7() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Dolphin_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Dolphin_7() const { return ___obj_Dolphin_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Dolphin_7() { return &___obj_Dolphin_7; }
	inline void set_obj_Dolphin_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Dolphin_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Dolphin_7), value);
	}

	inline static int32_t get_offset_of_obj_Sea_Urchin_8() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Sea_Urchin_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Sea_Urchin_8() const { return ___obj_Sea_Urchin_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Sea_Urchin_8() { return &___obj_Sea_Urchin_8; }
	inline void set_obj_Sea_Urchin_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Sea_Urchin_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Sea_Urchin_8), value);
	}

	inline static int32_t get_offset_of_obj_Moray_Eel_9() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Moray_Eel_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Moray_Eel_9() const { return ___obj_Moray_Eel_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Moray_Eel_9() { return &___obj_Moray_Eel_9; }
	inline void set_obj_Moray_Eel_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Moray_Eel_9 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Moray_Eel_9), value);
	}

	inline static int32_t get_offset_of_obj_Long_Finned_Pilot_Whale_10() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Long_Finned_Pilot_Whale_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Long_Finned_Pilot_Whale_10() const { return ___obj_Long_Finned_Pilot_Whale_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Long_Finned_Pilot_Whale_10() { return &___obj_Long_Finned_Pilot_Whale_10; }
	inline void set_obj_Long_Finned_Pilot_Whale_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Long_Finned_Pilot_Whale_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Long_Finned_Pilot_Whale_10), value);
	}

	inline static int32_t get_offset_of_obj_Killer_Whale_11() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Killer_Whale_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Killer_Whale_11() const { return ___obj_Killer_Whale_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Killer_Whale_11() { return &___obj_Killer_Whale_11; }
	inline void set_obj_Killer_Whale_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Killer_Whale_11 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Killer_Whale_11), value);
	}

	inline static int32_t get_offset_of_obj_Basking_Shark_12() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Basking_Shark_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Basking_Shark_12() const { return ___obj_Basking_Shark_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Basking_Shark_12() { return &___obj_Basking_Shark_12; }
	inline void set_obj_Basking_Shark_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Basking_Shark_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Basking_Shark_12), value);
	}

	inline static int32_t get_offset_of_obj_Great_White_Shark_13() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Great_White_Shark_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Great_White_Shark_13() const { return ___obj_Great_White_Shark_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Great_White_Shark_13() { return &___obj_Great_White_Shark_13; }
	inline void set_obj_Great_White_Shark_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Great_White_Shark_13 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Great_White_Shark_13), value);
	}

	inline static int32_t get_offset_of_obj_Squid_14() { return static_cast<int32_t>(offsetof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields, ___obj_Squid_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Squid_14() const { return ___obj_Squid_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Squid_14() { return &___obj_Squid_14; }
	inline void set_obj_Squid_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Squid_14 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Squid_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEAHIDEANIMALCONTROLLER_T3711301C8F9B2A5D10AFB59E05B04173C1C10354_H
#ifndef SEAMENU_TF1E89779D3708A8D467C4B64D248A6A4D093C7C4_H
#define SEAMENU_TF1E89779D3708A8D467C4B64D248A6A4D093C7C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaMenu
struct  SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SeaMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean SeaMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image SeaMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject SeaMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject SeaMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// UnityEngine.GameObject SeaMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_9;
	// System.Boolean SeaMenu::isQuiz
	bool ___isQuiz_10;
	// System.Boolean SeaMenu::isLow
	bool ___isLow_11;
	// System.Boolean SeaMenu::isHigh
	bool ___isHigh_12;
	// System.Int32 SeaMenu::restTime
	int32_t ___restTime_13;
	// System.Single SeaMenu::remindertimer
	float ___remindertimer_14;
	// System.Boolean SeaMenu::remindertime
	bool ___remindertime_15;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_9() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___obj_reminderCanvas_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_9() const { return ___obj_reminderCanvas_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_9() { return &___obj_reminderCanvas_9; }
	inline void set_obj_reminderCanvas_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_9), value);
	}

	inline static int32_t get_offset_of_isQuiz_10() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___isQuiz_10)); }
	inline bool get_isQuiz_10() const { return ___isQuiz_10; }
	inline bool* get_address_of_isQuiz_10() { return &___isQuiz_10; }
	inline void set_isQuiz_10(bool value)
	{
		___isQuiz_10 = value;
	}

	inline static int32_t get_offset_of_isLow_11() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___isLow_11)); }
	inline bool get_isLow_11() const { return ___isLow_11; }
	inline bool* get_address_of_isLow_11() { return &___isLow_11; }
	inline void set_isLow_11(bool value)
	{
		___isLow_11 = value;
	}

	inline static int32_t get_offset_of_isHigh_12() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___isHigh_12)); }
	inline bool get_isHigh_12() const { return ___isHigh_12; }
	inline bool* get_address_of_isHigh_12() { return &___isHigh_12; }
	inline void set_isHigh_12(bool value)
	{
		___isHigh_12 = value;
	}

	inline static int32_t get_offset_of_restTime_13() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___restTime_13)); }
	inline int32_t get_restTime_13() const { return ___restTime_13; }
	inline int32_t* get_address_of_restTime_13() { return &___restTime_13; }
	inline void set_restTime_13(int32_t value)
	{
		___restTime_13 = value;
	}

	inline static int32_t get_offset_of_remindertimer_14() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___remindertimer_14)); }
	inline float get_remindertimer_14() const { return ___remindertimer_14; }
	inline float* get_address_of_remindertimer_14() { return &___remindertimer_14; }
	inline void set_remindertimer_14(float value)
	{
		___remindertimer_14 = value;
	}

	inline static int32_t get_offset_of_remindertime_15() { return static_cast<int32_t>(offsetof(SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4, ___remindertime_15)); }
	inline bool get_remindertime_15() const { return ___remindertime_15; }
	inline bool* get_address_of_remindertime_15() { return &___remindertime_15; }
	inline void set_remindertime_15(bool value)
	{
		___remindertime_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEAMENU_TF1E89779D3708A8D467C4B64D248A6A4D093C7C4_H
#ifndef SEAQUIZCONTROLLER_T546D83FD8AE42289E7EAB06A3758BA87A904CD75_H
#define SEAQUIZCONTROLLER_T546D83FD8AE42289E7EAB06A3758BA87A904CD75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaQuizController
struct  SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Int32> SeaQuizController::questionQueueList
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___questionQueueList_4;
	// System.Collections.Generic.List`1<QuestionList> SeaQuizController::questionLists
	List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * ___questionLists_5;
	// System.Int32 SeaQuizController::limitTime
	int32_t ___limitTime_6;
	// System.Single SeaQuizController::time
	float ___time_7;
	// System.Int32 SeaQuizController::currentQuestionNum
	int32_t ___currentQuestionNum_8;
	// System.Int32 SeaQuizController::correctCount
	int32_t ___correctCount_9;
	// System.Single SeaQuizController::buttontimer
	float ___buttontimer_10;
	// System.Boolean SeaQuizController::buttontime
	bool ___buttontime_11;
	// System.Boolean SeaQuizController::isSelectA
	bool ___isSelectA_12;
	// System.Boolean SeaQuizController::isSelectB
	bool ___isSelectB_13;
	// System.Boolean SeaQuizController::isSelectC
	bool ___isSelectC_14;
	// System.Boolean SeaQuizController::isSelectD
	bool ___isSelectD_15;
	// System.Boolean SeaQuizController::isStart
	bool ___isStart_16;
	// System.Boolean SeaQuizController::isResumebutton
	bool ___isResumebutton_17;
	// System.Boolean SeaQuizController::isPausebutton
	bool ___isPausebutton_18;
	// System.Boolean SeaQuizController::isStartbutton
	bool ___isStartbutton_19;
	// System.Boolean SeaQuizController::isConfirmbutton
	bool ___isConfirmbutton_20;
	// System.Boolean SeaQuizController::isQuitbutton
	bool ___isQuitbutton_21;
	// System.Int32 SeaQuizController::animalsCount
	int32_t ___animalsCount_22;
	// System.Boolean SeaQuizController::canAnswer
	bool ___canAnswer_23;
	// UnityEngine.GameObject SeaQuizController::obj_Octopus
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Octopus_24;
	// UnityEngine.GameObject SeaQuizController::obj_Crab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Crab_25;
	// UnityEngine.GameObject SeaQuizController::obj_Starfish
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Starfish_26;
	// UnityEngine.GameObject SeaQuizController::obj_Dolphin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Dolphin_27;
	// UnityEngine.GameObject SeaQuizController::obj_Sea_Urchin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Sea_Urchin_28;
	// UnityEngine.GameObject SeaQuizController::obj_Moray_Eel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Moray_Eel_29;
	// UnityEngine.GameObject SeaQuizController::obj_Long_Finned_Pilot_Whale
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Long_Finned_Pilot_Whale_30;
	// UnityEngine.GameObject SeaQuizController::obj_Killer_Whale
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Killer_Whale_31;
	// UnityEngine.GameObject SeaQuizController::obj_Basking_Shark
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Basking_Shark_32;
	// UnityEngine.GameObject SeaQuizController::obj_Great_White_Shark
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Great_White_Shark_33;
	// UnityEngine.GameObject SeaQuizController::obj_Squid
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Squid_34;
	// UnityEngine.UI.Text SeaQuizController::gui_time
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_time_35;
	// UnityEngine.UI.Text SeaQuizController::gui_round
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_round_36;
	// UnityEngine.UI.Image SeaQuizController::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_37;
	// UnityEngine.GameObject SeaQuizController::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_38;
	// UnityEngine.GameObject SeaQuizController::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_39;
	// UnityEngine.GameObject SeaQuizController::canvas_question
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_question_40;
	// UnityEngine.GameObject SeaQuizController::canvas_pause
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_pause_41;
	// UnityEngine.GameObject SeaQuizController::obj_start_menu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_start_menu_42;
	// UnityEngine.UI.Text SeaQuizController::gui_mark
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_mark_43;
	// UnityEngine.GameObject SeaQuizController::canvas_finish
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_finish_44;
	// UnityEngine.UI.Text SeaQuizController::gui_choiceA
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceA_45;
	// UnityEngine.UI.Text SeaQuizController::gui_choiceB
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceB_46;
	// UnityEngine.UI.Text SeaQuizController::gui_choiceC
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceC_47;
	// UnityEngine.UI.Text SeaQuizController::gui_choiceD
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___gui_choiceD_48;
	// UnityEngine.GameObject SeaQuizController::btn_choiceA
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceA_49;
	// UnityEngine.GameObject SeaQuizController::btn_choiceB
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceB_50;
	// UnityEngine.GameObject SeaQuizController::btn_choiceC
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceC_51;
	// UnityEngine.GameObject SeaQuizController::btn_choiceD
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___btn_choiceD_52;

public:
	inline static int32_t get_offset_of_questionQueueList_4() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___questionQueueList_4)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_questionQueueList_4() const { return ___questionQueueList_4; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_questionQueueList_4() { return &___questionQueueList_4; }
	inline void set_questionQueueList_4(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___questionQueueList_4 = value;
		Il2CppCodeGenWriteBarrier((&___questionQueueList_4), value);
	}

	inline static int32_t get_offset_of_questionLists_5() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___questionLists_5)); }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * get_questionLists_5() const { return ___questionLists_5; }
	inline List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D ** get_address_of_questionLists_5() { return &___questionLists_5; }
	inline void set_questionLists_5(List_1_t318CE05F7A7E4C4B6672CC888B121F24F51E573D * value)
	{
		___questionLists_5 = value;
		Il2CppCodeGenWriteBarrier((&___questionLists_5), value);
	}

	inline static int32_t get_offset_of_limitTime_6() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___limitTime_6)); }
	inline int32_t get_limitTime_6() const { return ___limitTime_6; }
	inline int32_t* get_address_of_limitTime_6() { return &___limitTime_6; }
	inline void set_limitTime_6(int32_t value)
	{
		___limitTime_6 = value;
	}

	inline static int32_t get_offset_of_time_7() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___time_7)); }
	inline float get_time_7() const { return ___time_7; }
	inline float* get_address_of_time_7() { return &___time_7; }
	inline void set_time_7(float value)
	{
		___time_7 = value;
	}

	inline static int32_t get_offset_of_currentQuestionNum_8() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___currentQuestionNum_8)); }
	inline int32_t get_currentQuestionNum_8() const { return ___currentQuestionNum_8; }
	inline int32_t* get_address_of_currentQuestionNum_8() { return &___currentQuestionNum_8; }
	inline void set_currentQuestionNum_8(int32_t value)
	{
		___currentQuestionNum_8 = value;
	}

	inline static int32_t get_offset_of_correctCount_9() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___correctCount_9)); }
	inline int32_t get_correctCount_9() const { return ___correctCount_9; }
	inline int32_t* get_address_of_correctCount_9() { return &___correctCount_9; }
	inline void set_correctCount_9(int32_t value)
	{
		___correctCount_9 = value;
	}

	inline static int32_t get_offset_of_buttontimer_10() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___buttontimer_10)); }
	inline float get_buttontimer_10() const { return ___buttontimer_10; }
	inline float* get_address_of_buttontimer_10() { return &___buttontimer_10; }
	inline void set_buttontimer_10(float value)
	{
		___buttontimer_10 = value;
	}

	inline static int32_t get_offset_of_buttontime_11() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___buttontime_11)); }
	inline bool get_buttontime_11() const { return ___buttontime_11; }
	inline bool* get_address_of_buttontime_11() { return &___buttontime_11; }
	inline void set_buttontime_11(bool value)
	{
		___buttontime_11 = value;
	}

	inline static int32_t get_offset_of_isSelectA_12() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isSelectA_12)); }
	inline bool get_isSelectA_12() const { return ___isSelectA_12; }
	inline bool* get_address_of_isSelectA_12() { return &___isSelectA_12; }
	inline void set_isSelectA_12(bool value)
	{
		___isSelectA_12 = value;
	}

	inline static int32_t get_offset_of_isSelectB_13() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isSelectB_13)); }
	inline bool get_isSelectB_13() const { return ___isSelectB_13; }
	inline bool* get_address_of_isSelectB_13() { return &___isSelectB_13; }
	inline void set_isSelectB_13(bool value)
	{
		___isSelectB_13 = value;
	}

	inline static int32_t get_offset_of_isSelectC_14() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isSelectC_14)); }
	inline bool get_isSelectC_14() const { return ___isSelectC_14; }
	inline bool* get_address_of_isSelectC_14() { return &___isSelectC_14; }
	inline void set_isSelectC_14(bool value)
	{
		___isSelectC_14 = value;
	}

	inline static int32_t get_offset_of_isSelectD_15() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isSelectD_15)); }
	inline bool get_isSelectD_15() const { return ___isSelectD_15; }
	inline bool* get_address_of_isSelectD_15() { return &___isSelectD_15; }
	inline void set_isSelectD_15(bool value)
	{
		___isSelectD_15 = value;
	}

	inline static int32_t get_offset_of_isStart_16() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isStart_16)); }
	inline bool get_isStart_16() const { return ___isStart_16; }
	inline bool* get_address_of_isStart_16() { return &___isStart_16; }
	inline void set_isStart_16(bool value)
	{
		___isStart_16 = value;
	}

	inline static int32_t get_offset_of_isResumebutton_17() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isResumebutton_17)); }
	inline bool get_isResumebutton_17() const { return ___isResumebutton_17; }
	inline bool* get_address_of_isResumebutton_17() { return &___isResumebutton_17; }
	inline void set_isResumebutton_17(bool value)
	{
		___isResumebutton_17 = value;
	}

	inline static int32_t get_offset_of_isPausebutton_18() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isPausebutton_18)); }
	inline bool get_isPausebutton_18() const { return ___isPausebutton_18; }
	inline bool* get_address_of_isPausebutton_18() { return &___isPausebutton_18; }
	inline void set_isPausebutton_18(bool value)
	{
		___isPausebutton_18 = value;
	}

	inline static int32_t get_offset_of_isStartbutton_19() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isStartbutton_19)); }
	inline bool get_isStartbutton_19() const { return ___isStartbutton_19; }
	inline bool* get_address_of_isStartbutton_19() { return &___isStartbutton_19; }
	inline void set_isStartbutton_19(bool value)
	{
		___isStartbutton_19 = value;
	}

	inline static int32_t get_offset_of_isConfirmbutton_20() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isConfirmbutton_20)); }
	inline bool get_isConfirmbutton_20() const { return ___isConfirmbutton_20; }
	inline bool* get_address_of_isConfirmbutton_20() { return &___isConfirmbutton_20; }
	inline void set_isConfirmbutton_20(bool value)
	{
		___isConfirmbutton_20 = value;
	}

	inline static int32_t get_offset_of_isQuitbutton_21() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___isQuitbutton_21)); }
	inline bool get_isQuitbutton_21() const { return ___isQuitbutton_21; }
	inline bool* get_address_of_isQuitbutton_21() { return &___isQuitbutton_21; }
	inline void set_isQuitbutton_21(bool value)
	{
		___isQuitbutton_21 = value;
	}

	inline static int32_t get_offset_of_animalsCount_22() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___animalsCount_22)); }
	inline int32_t get_animalsCount_22() const { return ___animalsCount_22; }
	inline int32_t* get_address_of_animalsCount_22() { return &___animalsCount_22; }
	inline void set_animalsCount_22(int32_t value)
	{
		___animalsCount_22 = value;
	}

	inline static int32_t get_offset_of_canAnswer_23() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___canAnswer_23)); }
	inline bool get_canAnswer_23() const { return ___canAnswer_23; }
	inline bool* get_address_of_canAnswer_23() { return &___canAnswer_23; }
	inline void set_canAnswer_23(bool value)
	{
		___canAnswer_23 = value;
	}

	inline static int32_t get_offset_of_obj_Octopus_24() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Octopus_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Octopus_24() const { return ___obj_Octopus_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Octopus_24() { return &___obj_Octopus_24; }
	inline void set_obj_Octopus_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Octopus_24 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Octopus_24), value);
	}

	inline static int32_t get_offset_of_obj_Crab_25() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Crab_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Crab_25() const { return ___obj_Crab_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Crab_25() { return &___obj_Crab_25; }
	inline void set_obj_Crab_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Crab_25 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Crab_25), value);
	}

	inline static int32_t get_offset_of_obj_Starfish_26() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Starfish_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Starfish_26() const { return ___obj_Starfish_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Starfish_26() { return &___obj_Starfish_26; }
	inline void set_obj_Starfish_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Starfish_26 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Starfish_26), value);
	}

	inline static int32_t get_offset_of_obj_Dolphin_27() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Dolphin_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Dolphin_27() const { return ___obj_Dolphin_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Dolphin_27() { return &___obj_Dolphin_27; }
	inline void set_obj_Dolphin_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Dolphin_27 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Dolphin_27), value);
	}

	inline static int32_t get_offset_of_obj_Sea_Urchin_28() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Sea_Urchin_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Sea_Urchin_28() const { return ___obj_Sea_Urchin_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Sea_Urchin_28() { return &___obj_Sea_Urchin_28; }
	inline void set_obj_Sea_Urchin_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Sea_Urchin_28 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Sea_Urchin_28), value);
	}

	inline static int32_t get_offset_of_obj_Moray_Eel_29() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Moray_Eel_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Moray_Eel_29() const { return ___obj_Moray_Eel_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Moray_Eel_29() { return &___obj_Moray_Eel_29; }
	inline void set_obj_Moray_Eel_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Moray_Eel_29 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Moray_Eel_29), value);
	}

	inline static int32_t get_offset_of_obj_Long_Finned_Pilot_Whale_30() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Long_Finned_Pilot_Whale_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Long_Finned_Pilot_Whale_30() const { return ___obj_Long_Finned_Pilot_Whale_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Long_Finned_Pilot_Whale_30() { return &___obj_Long_Finned_Pilot_Whale_30; }
	inline void set_obj_Long_Finned_Pilot_Whale_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Long_Finned_Pilot_Whale_30 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Long_Finned_Pilot_Whale_30), value);
	}

	inline static int32_t get_offset_of_obj_Killer_Whale_31() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Killer_Whale_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Killer_Whale_31() const { return ___obj_Killer_Whale_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Killer_Whale_31() { return &___obj_Killer_Whale_31; }
	inline void set_obj_Killer_Whale_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Killer_Whale_31 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Killer_Whale_31), value);
	}

	inline static int32_t get_offset_of_obj_Basking_Shark_32() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Basking_Shark_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Basking_Shark_32() const { return ___obj_Basking_Shark_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Basking_Shark_32() { return &___obj_Basking_Shark_32; }
	inline void set_obj_Basking_Shark_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Basking_Shark_32 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Basking_Shark_32), value);
	}

	inline static int32_t get_offset_of_obj_Great_White_Shark_33() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Great_White_Shark_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Great_White_Shark_33() const { return ___obj_Great_White_Shark_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Great_White_Shark_33() { return &___obj_Great_White_Shark_33; }
	inline void set_obj_Great_White_Shark_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Great_White_Shark_33 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Great_White_Shark_33), value);
	}

	inline static int32_t get_offset_of_obj_Squid_34() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Squid_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Squid_34() const { return ___obj_Squid_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Squid_34() { return &___obj_Squid_34; }
	inline void set_obj_Squid_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Squid_34 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Squid_34), value);
	}

	inline static int32_t get_offset_of_gui_time_35() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_time_35)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_time_35() const { return ___gui_time_35; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_time_35() { return &___gui_time_35; }
	inline void set_gui_time_35(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_time_35 = value;
		Il2CppCodeGenWriteBarrier((&___gui_time_35), value);
	}

	inline static int32_t get_offset_of_gui_round_36() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_round_36)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_round_36() const { return ___gui_round_36; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_round_36() { return &___gui_round_36; }
	inline void set_gui_round_36(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_round_36 = value;
		Il2CppCodeGenWriteBarrier((&___gui_round_36), value);
	}

	inline static int32_t get_offset_of_Bar_37() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___Bar_37)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_37() const { return ___Bar_37; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_37() { return &___Bar_37; }
	inline void set_Bar_37(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_37 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_37), value);
	}

	inline static int32_t get_offset_of_obj_Bar_38() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_Bar_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_38() const { return ___obj_Bar_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_38() { return &___obj_Bar_38; }
	inline void set_obj_Bar_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_38 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_38), value);
	}

	inline static int32_t get_offset_of_obj_shoot_39() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_shoot_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_39() const { return ___obj_shoot_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_39() { return &___obj_shoot_39; }
	inline void set_obj_shoot_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_39 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_39), value);
	}

	inline static int32_t get_offset_of_canvas_question_40() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___canvas_question_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_question_40() const { return ___canvas_question_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_question_40() { return &___canvas_question_40; }
	inline void set_canvas_question_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_question_40 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_question_40), value);
	}

	inline static int32_t get_offset_of_canvas_pause_41() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___canvas_pause_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_pause_41() const { return ___canvas_pause_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_pause_41() { return &___canvas_pause_41; }
	inline void set_canvas_pause_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_pause_41 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_pause_41), value);
	}

	inline static int32_t get_offset_of_obj_start_menu_42() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___obj_start_menu_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_start_menu_42() const { return ___obj_start_menu_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_start_menu_42() { return &___obj_start_menu_42; }
	inline void set_obj_start_menu_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_start_menu_42 = value;
		Il2CppCodeGenWriteBarrier((&___obj_start_menu_42), value);
	}

	inline static int32_t get_offset_of_gui_mark_43() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_mark_43)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_mark_43() const { return ___gui_mark_43; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_mark_43() { return &___gui_mark_43; }
	inline void set_gui_mark_43(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_mark_43 = value;
		Il2CppCodeGenWriteBarrier((&___gui_mark_43), value);
	}

	inline static int32_t get_offset_of_canvas_finish_44() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___canvas_finish_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_finish_44() const { return ___canvas_finish_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_finish_44() { return &___canvas_finish_44; }
	inline void set_canvas_finish_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_finish_44 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_finish_44), value);
	}

	inline static int32_t get_offset_of_gui_choiceA_45() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_choiceA_45)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceA_45() const { return ___gui_choiceA_45; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceA_45() { return &___gui_choiceA_45; }
	inline void set_gui_choiceA_45(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceA_45 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceA_45), value);
	}

	inline static int32_t get_offset_of_gui_choiceB_46() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_choiceB_46)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceB_46() const { return ___gui_choiceB_46; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceB_46() { return &___gui_choiceB_46; }
	inline void set_gui_choiceB_46(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceB_46 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceB_46), value);
	}

	inline static int32_t get_offset_of_gui_choiceC_47() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_choiceC_47)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceC_47() const { return ___gui_choiceC_47; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceC_47() { return &___gui_choiceC_47; }
	inline void set_gui_choiceC_47(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceC_47 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceC_47), value);
	}

	inline static int32_t get_offset_of_gui_choiceD_48() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___gui_choiceD_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_gui_choiceD_48() const { return ___gui_choiceD_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_gui_choiceD_48() { return &___gui_choiceD_48; }
	inline void set_gui_choiceD_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___gui_choiceD_48 = value;
		Il2CppCodeGenWriteBarrier((&___gui_choiceD_48), value);
	}

	inline static int32_t get_offset_of_btn_choiceA_49() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___btn_choiceA_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceA_49() const { return ___btn_choiceA_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceA_49() { return &___btn_choiceA_49; }
	inline void set_btn_choiceA_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceA_49 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceA_49), value);
	}

	inline static int32_t get_offset_of_btn_choiceB_50() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___btn_choiceB_50)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceB_50() const { return ___btn_choiceB_50; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceB_50() { return &___btn_choiceB_50; }
	inline void set_btn_choiceB_50(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceB_50 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceB_50), value);
	}

	inline static int32_t get_offset_of_btn_choiceC_51() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___btn_choiceC_51)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceC_51() const { return ___btn_choiceC_51; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceC_51() { return &___btn_choiceC_51; }
	inline void set_btn_choiceC_51(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceC_51 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceC_51), value);
	}

	inline static int32_t get_offset_of_btn_choiceD_52() { return static_cast<int32_t>(offsetof(SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75, ___btn_choiceD_52)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_btn_choiceD_52() const { return ___btn_choiceD_52; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_btn_choiceD_52() { return &___btn_choiceD_52; }
	inline void set_btn_choiceD_52(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___btn_choiceD_52 = value;
		Il2CppCodeGenWriteBarrier((&___btn_choiceD_52), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEAQUIZCONTROLLER_T546D83FD8AE42289E7EAB06A3758BA87A904CD75_H
#ifndef SEAQUIZMENU_TEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6_H
#define SEAQUIZMENU_TEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeaQuizMenu
struct  SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SeaQuizMenu::buttontimer
	float ___buttontimer_4;
	// System.Boolean SeaQuizMenu::buttontime
	bool ___buttontime_5;
	// UnityEngine.UI.Image SeaQuizMenu::Bar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___Bar_6;
	// UnityEngine.GameObject SeaQuizMenu::obj_Bar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_Bar_7;
	// UnityEngine.GameObject SeaQuizMenu::obj_shoot
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_shoot_8;
	// System.Boolean SeaQuizMenu::isTutorial
	bool ___isTutorial_9;
	// UnityEngine.GameObject SeaQuizMenu::obj_reminderCanvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_reminderCanvas_10;
	// System.Int32 SeaQuizMenu::restTime
	int32_t ___restTime_11;
	// System.Single SeaQuizMenu::remindertimer
	float ___remindertimer_12;
	// System.Boolean SeaQuizMenu::remindertime
	bool ___remindertime_13;

public:
	inline static int32_t get_offset_of_buttontimer_4() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___buttontimer_4)); }
	inline float get_buttontimer_4() const { return ___buttontimer_4; }
	inline float* get_address_of_buttontimer_4() { return &___buttontimer_4; }
	inline void set_buttontimer_4(float value)
	{
		___buttontimer_4 = value;
	}

	inline static int32_t get_offset_of_buttontime_5() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___buttontime_5)); }
	inline bool get_buttontime_5() const { return ___buttontime_5; }
	inline bool* get_address_of_buttontime_5() { return &___buttontime_5; }
	inline void set_buttontime_5(bool value)
	{
		___buttontime_5 = value;
	}

	inline static int32_t get_offset_of_Bar_6() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___Bar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_Bar_6() const { return ___Bar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_Bar_6() { return &___Bar_6; }
	inline void set_Bar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___Bar_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bar_6), value);
	}

	inline static int32_t get_offset_of_obj_Bar_7() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___obj_Bar_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_Bar_7() const { return ___obj_Bar_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_Bar_7() { return &___obj_Bar_7; }
	inline void set_obj_Bar_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_Bar_7 = value;
		Il2CppCodeGenWriteBarrier((&___obj_Bar_7), value);
	}

	inline static int32_t get_offset_of_obj_shoot_8() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___obj_shoot_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_shoot_8() const { return ___obj_shoot_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_shoot_8() { return &___obj_shoot_8; }
	inline void set_obj_shoot_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_shoot_8 = value;
		Il2CppCodeGenWriteBarrier((&___obj_shoot_8), value);
	}

	inline static int32_t get_offset_of_isTutorial_9() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___isTutorial_9)); }
	inline bool get_isTutorial_9() const { return ___isTutorial_9; }
	inline bool* get_address_of_isTutorial_9() { return &___isTutorial_9; }
	inline void set_isTutorial_9(bool value)
	{
		___isTutorial_9 = value;
	}

	inline static int32_t get_offset_of_obj_reminderCanvas_10() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___obj_reminderCanvas_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_reminderCanvas_10() const { return ___obj_reminderCanvas_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_reminderCanvas_10() { return &___obj_reminderCanvas_10; }
	inline void set_obj_reminderCanvas_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_reminderCanvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___obj_reminderCanvas_10), value);
	}

	inline static int32_t get_offset_of_restTime_11() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___restTime_11)); }
	inline int32_t get_restTime_11() const { return ___restTime_11; }
	inline int32_t* get_address_of_restTime_11() { return &___restTime_11; }
	inline void set_restTime_11(int32_t value)
	{
		___restTime_11 = value;
	}

	inline static int32_t get_offset_of_remindertimer_12() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___remindertimer_12)); }
	inline float get_remindertimer_12() const { return ___remindertimer_12; }
	inline float* get_address_of_remindertimer_12() { return &___remindertimer_12; }
	inline void set_remindertimer_12(float value)
	{
		___remindertimer_12 = value;
	}

	inline static int32_t get_offset_of_remindertime_13() { return static_cast<int32_t>(offsetof(SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6, ___remindertime_13)); }
	inline bool get_remindertime_13() const { return ___remindertime_13; }
	inline bool* get_address_of_remindertime_13() { return &___remindertime_13; }
	inline void set_remindertime_13(bool value)
	{
		___remindertime_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEAQUIZMENU_TEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef GVRARMMODEL_T3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_H
#define GVRARMMODEL_T3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrArmModel
struct  GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0  : public GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE
{
public:
	// UnityEngine.Vector3 GvrArmModel::elbowRestPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___elbowRestPosition_4;
	// UnityEngine.Vector3 GvrArmModel::wristRestPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___wristRestPosition_5;
	// UnityEngine.Vector3 GvrArmModel::controllerRestPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___controllerRestPosition_6;
	// UnityEngine.Vector3 GvrArmModel::armExtensionOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___armExtensionOffset_7;
	// System.Single GvrArmModel::elbowBendRatio
	float ___elbowBendRatio_8;
	// System.Single GvrArmModel::fadeControllerOffset
	float ___fadeControllerOffset_9;
	// System.Single GvrArmModel::fadeDistanceFromHeadForward
	float ___fadeDistanceFromHeadForward_10;
	// System.Single GvrArmModel::fadeDistanceFromHeadSide
	float ___fadeDistanceFromHeadSide_11;
	// System.Single GvrArmModel::tooltipMinDistanceFromFace
	float ___tooltipMinDistanceFromFace_12;
	// System.Int32 GvrArmModel::tooltipMaxAngleFromCamera
	int32_t ___tooltipMaxAngleFromCamera_13;
	// System.Boolean GvrArmModel::isLockedToNeck
	bool ___isLockedToNeck_14;
	// GvrControllerInputDevice GvrArmModel::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * ___U3CControllerInputDeviceU3Ek__BackingField_15;
	// UnityEngine.Vector3 GvrArmModel::neckPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___neckPosition_16;
	// UnityEngine.Vector3 GvrArmModel::elbowPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___elbowPosition_17;
	// UnityEngine.Quaternion GvrArmModel::elbowRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___elbowRotation_18;
	// UnityEngine.Vector3 GvrArmModel::wristPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___wristPosition_19;
	// UnityEngine.Quaternion GvrArmModel::wristRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___wristRotation_20;
	// UnityEngine.Vector3 GvrArmModel::controllerPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___controllerPosition_21;
	// UnityEngine.Quaternion GvrArmModel::controllerRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___controllerRotation_22;
	// System.Single GvrArmModel::preferredAlpha
	float ___preferredAlpha_23;
	// System.Single GvrArmModel::tooltipAlphaValue
	float ___tooltipAlphaValue_24;
	// UnityEngine.Vector3 GvrArmModel::handedMultiplier
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handedMultiplier_25;
	// UnityEngine.Vector3 GvrArmModel::torsoDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___torsoDirection_26;
	// UnityEngine.Quaternion GvrArmModel::torsoRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___torsoRotation_27;

public:
	inline static int32_t get_offset_of_elbowRestPosition_4() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___elbowRestPosition_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_elbowRestPosition_4() const { return ___elbowRestPosition_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_elbowRestPosition_4() { return &___elbowRestPosition_4; }
	inline void set_elbowRestPosition_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___elbowRestPosition_4 = value;
	}

	inline static int32_t get_offset_of_wristRestPosition_5() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___wristRestPosition_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_wristRestPosition_5() const { return ___wristRestPosition_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_wristRestPosition_5() { return &___wristRestPosition_5; }
	inline void set_wristRestPosition_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___wristRestPosition_5 = value;
	}

	inline static int32_t get_offset_of_controllerRestPosition_6() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___controllerRestPosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_controllerRestPosition_6() const { return ___controllerRestPosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_controllerRestPosition_6() { return &___controllerRestPosition_6; }
	inline void set_controllerRestPosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___controllerRestPosition_6 = value;
	}

	inline static int32_t get_offset_of_armExtensionOffset_7() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___armExtensionOffset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_armExtensionOffset_7() const { return ___armExtensionOffset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_armExtensionOffset_7() { return &___armExtensionOffset_7; }
	inline void set_armExtensionOffset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___armExtensionOffset_7 = value;
	}

	inline static int32_t get_offset_of_elbowBendRatio_8() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___elbowBendRatio_8)); }
	inline float get_elbowBendRatio_8() const { return ___elbowBendRatio_8; }
	inline float* get_address_of_elbowBendRatio_8() { return &___elbowBendRatio_8; }
	inline void set_elbowBendRatio_8(float value)
	{
		___elbowBendRatio_8 = value;
	}

	inline static int32_t get_offset_of_fadeControllerOffset_9() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___fadeControllerOffset_9)); }
	inline float get_fadeControllerOffset_9() const { return ___fadeControllerOffset_9; }
	inline float* get_address_of_fadeControllerOffset_9() { return &___fadeControllerOffset_9; }
	inline void set_fadeControllerOffset_9(float value)
	{
		___fadeControllerOffset_9 = value;
	}

	inline static int32_t get_offset_of_fadeDistanceFromHeadForward_10() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___fadeDistanceFromHeadForward_10)); }
	inline float get_fadeDistanceFromHeadForward_10() const { return ___fadeDistanceFromHeadForward_10; }
	inline float* get_address_of_fadeDistanceFromHeadForward_10() { return &___fadeDistanceFromHeadForward_10; }
	inline void set_fadeDistanceFromHeadForward_10(float value)
	{
		___fadeDistanceFromHeadForward_10 = value;
	}

	inline static int32_t get_offset_of_fadeDistanceFromHeadSide_11() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___fadeDistanceFromHeadSide_11)); }
	inline float get_fadeDistanceFromHeadSide_11() const { return ___fadeDistanceFromHeadSide_11; }
	inline float* get_address_of_fadeDistanceFromHeadSide_11() { return &___fadeDistanceFromHeadSide_11; }
	inline void set_fadeDistanceFromHeadSide_11(float value)
	{
		___fadeDistanceFromHeadSide_11 = value;
	}

	inline static int32_t get_offset_of_tooltipMinDistanceFromFace_12() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___tooltipMinDistanceFromFace_12)); }
	inline float get_tooltipMinDistanceFromFace_12() const { return ___tooltipMinDistanceFromFace_12; }
	inline float* get_address_of_tooltipMinDistanceFromFace_12() { return &___tooltipMinDistanceFromFace_12; }
	inline void set_tooltipMinDistanceFromFace_12(float value)
	{
		___tooltipMinDistanceFromFace_12 = value;
	}

	inline static int32_t get_offset_of_tooltipMaxAngleFromCamera_13() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___tooltipMaxAngleFromCamera_13)); }
	inline int32_t get_tooltipMaxAngleFromCamera_13() const { return ___tooltipMaxAngleFromCamera_13; }
	inline int32_t* get_address_of_tooltipMaxAngleFromCamera_13() { return &___tooltipMaxAngleFromCamera_13; }
	inline void set_tooltipMaxAngleFromCamera_13(int32_t value)
	{
		___tooltipMaxAngleFromCamera_13 = value;
	}

	inline static int32_t get_offset_of_isLockedToNeck_14() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___isLockedToNeck_14)); }
	inline bool get_isLockedToNeck_14() const { return ___isLockedToNeck_14; }
	inline bool* get_address_of_isLockedToNeck_14() { return &___isLockedToNeck_14; }
	inline void set_isLockedToNeck_14(bool value)
	{
		___isLockedToNeck_14 = value;
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___U3CControllerInputDeviceU3Ek__BackingField_15)); }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * get_U3CControllerInputDeviceU3Ek__BackingField_15() const { return ___U3CControllerInputDeviceU3Ek__BackingField_15; }
	inline GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_15() { return &___U3CControllerInputDeviceU3Ek__BackingField_15; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_15(GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_neckPosition_16() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___neckPosition_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_neckPosition_16() const { return ___neckPosition_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_neckPosition_16() { return &___neckPosition_16; }
	inline void set_neckPosition_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___neckPosition_16 = value;
	}

	inline static int32_t get_offset_of_elbowPosition_17() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___elbowPosition_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_elbowPosition_17() const { return ___elbowPosition_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_elbowPosition_17() { return &___elbowPosition_17; }
	inline void set_elbowPosition_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___elbowPosition_17 = value;
	}

	inline static int32_t get_offset_of_elbowRotation_18() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___elbowRotation_18)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_elbowRotation_18() const { return ___elbowRotation_18; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_elbowRotation_18() { return &___elbowRotation_18; }
	inline void set_elbowRotation_18(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___elbowRotation_18 = value;
	}

	inline static int32_t get_offset_of_wristPosition_19() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___wristPosition_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_wristPosition_19() const { return ___wristPosition_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_wristPosition_19() { return &___wristPosition_19; }
	inline void set_wristPosition_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___wristPosition_19 = value;
	}

	inline static int32_t get_offset_of_wristRotation_20() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___wristRotation_20)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_wristRotation_20() const { return ___wristRotation_20; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_wristRotation_20() { return &___wristRotation_20; }
	inline void set_wristRotation_20(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___wristRotation_20 = value;
	}

	inline static int32_t get_offset_of_controllerPosition_21() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___controllerPosition_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_controllerPosition_21() const { return ___controllerPosition_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_controllerPosition_21() { return &___controllerPosition_21; }
	inline void set_controllerPosition_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___controllerPosition_21 = value;
	}

	inline static int32_t get_offset_of_controllerRotation_22() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___controllerRotation_22)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_controllerRotation_22() const { return ___controllerRotation_22; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_controllerRotation_22() { return &___controllerRotation_22; }
	inline void set_controllerRotation_22(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___controllerRotation_22 = value;
	}

	inline static int32_t get_offset_of_preferredAlpha_23() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___preferredAlpha_23)); }
	inline float get_preferredAlpha_23() const { return ___preferredAlpha_23; }
	inline float* get_address_of_preferredAlpha_23() { return &___preferredAlpha_23; }
	inline void set_preferredAlpha_23(float value)
	{
		___preferredAlpha_23 = value;
	}

	inline static int32_t get_offset_of_tooltipAlphaValue_24() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___tooltipAlphaValue_24)); }
	inline float get_tooltipAlphaValue_24() const { return ___tooltipAlphaValue_24; }
	inline float* get_address_of_tooltipAlphaValue_24() { return &___tooltipAlphaValue_24; }
	inline void set_tooltipAlphaValue_24(float value)
	{
		___tooltipAlphaValue_24 = value;
	}

	inline static int32_t get_offset_of_handedMultiplier_25() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___handedMultiplier_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handedMultiplier_25() const { return ___handedMultiplier_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handedMultiplier_25() { return &___handedMultiplier_25; }
	inline void set_handedMultiplier_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handedMultiplier_25 = value;
	}

	inline static int32_t get_offset_of_torsoDirection_26() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___torsoDirection_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_torsoDirection_26() const { return ___torsoDirection_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_torsoDirection_26() { return &___torsoDirection_26; }
	inline void set_torsoDirection_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___torsoDirection_26 = value;
	}

	inline static int32_t get_offset_of_torsoRotation_27() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0, ___torsoRotation_27)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_torsoRotation_27() const { return ___torsoRotation_27; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_torsoRotation_27() { return &___torsoRotation_27; }
	inline void set_torsoRotation_27(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___torsoRotation_27 = value;
	}
};

struct GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields
{
public:
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_ELBOW_REST_POSITION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_ELBOW_REST_POSITION_28;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_WRIST_REST_POSITION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_WRIST_REST_POSITION_29;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_CONTROLLER_REST_POSITION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_CONTROLLER_REST_POSITION_30;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_ARM_EXTENSION_OFFSET
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DEFAULT_ARM_EXTENSION_OFFSET_31;
	// UnityEngine.Vector3 GvrArmModel::SHOULDER_POSITION
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___SHOULDER_POSITION_34;
	// UnityEngine.Vector3 GvrArmModel::NECK_OFFSET
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___NECK_OFFSET_35;

public:
	inline static int32_t get_offset_of_DEFAULT_ELBOW_REST_POSITION_28() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___DEFAULT_ELBOW_REST_POSITION_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_ELBOW_REST_POSITION_28() const { return ___DEFAULT_ELBOW_REST_POSITION_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_ELBOW_REST_POSITION_28() { return &___DEFAULT_ELBOW_REST_POSITION_28; }
	inline void set_DEFAULT_ELBOW_REST_POSITION_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_ELBOW_REST_POSITION_28 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_WRIST_REST_POSITION_29() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___DEFAULT_WRIST_REST_POSITION_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_WRIST_REST_POSITION_29() const { return ___DEFAULT_WRIST_REST_POSITION_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_WRIST_REST_POSITION_29() { return &___DEFAULT_WRIST_REST_POSITION_29; }
	inline void set_DEFAULT_WRIST_REST_POSITION_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_WRIST_REST_POSITION_29 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_CONTROLLER_REST_POSITION_30() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___DEFAULT_CONTROLLER_REST_POSITION_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_CONTROLLER_REST_POSITION_30() const { return ___DEFAULT_CONTROLLER_REST_POSITION_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_CONTROLLER_REST_POSITION_30() { return &___DEFAULT_CONTROLLER_REST_POSITION_30; }
	inline void set_DEFAULT_CONTROLLER_REST_POSITION_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_CONTROLLER_REST_POSITION_30 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_ARM_EXTENSION_OFFSET_31() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___DEFAULT_ARM_EXTENSION_OFFSET_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DEFAULT_ARM_EXTENSION_OFFSET_31() const { return ___DEFAULT_ARM_EXTENSION_OFFSET_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DEFAULT_ARM_EXTENSION_OFFSET_31() { return &___DEFAULT_ARM_EXTENSION_OFFSET_31; }
	inline void set_DEFAULT_ARM_EXTENSION_OFFSET_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DEFAULT_ARM_EXTENSION_OFFSET_31 = value;
	}

	inline static int32_t get_offset_of_SHOULDER_POSITION_34() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___SHOULDER_POSITION_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_SHOULDER_POSITION_34() const { return ___SHOULDER_POSITION_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_SHOULDER_POSITION_34() { return &___SHOULDER_POSITION_34; }
	inline void set_SHOULDER_POSITION_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___SHOULDER_POSITION_34 = value;
	}

	inline static int32_t get_offset_of_NECK_OFFSET_35() { return static_cast<int32_t>(offsetof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields, ___NECK_OFFSET_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_NECK_OFFSET_35() const { return ___NECK_OFFSET_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_NECK_OFFSET_35() { return &___NECK_OFFSET_35; }
	inline void set_NECK_OFFSET_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___NECK_OFFSET_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRARMMODEL_T3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_H
#ifndef GVRCONTROLLER_T49163591BDC2A15FF87EE7A9A42EC089CAC0A98F_H
#define GVRCONTROLLER_T49163591BDC2A15FF87EE7A9A42EC089CAC0A98F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrController
struct  GvrController_t49163591BDC2A15FF87EE7A9A42EC089CAC0A98F  : public GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLER_T49163591BDC2A15FF87EE7A9A42EC089CAC0A98F_H
#ifndef GVRCONTROLLERVISUALMANAGER_TA6692588113CD5C5FA19BDC09A8923BB490DDD14_H
#define GVRCONTROLLERVISUALMANAGER_TA6692588113CD5C5FA19BDC09A8923BB490DDD14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisualManager
struct  GvrControllerVisualManager_tA6692588113CD5C5FA19BDC09A8923BB490DDD14  : public GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERVISUALMANAGER_TA6692588113CD5C5FA19BDC09A8923BB490DDD14_H
#ifndef GVRLASERPOINTER_T36847BC18F5E926C47FA67B3B2FC59150E871CE0_H
#define GVRLASERPOINTER_T36847BC18F5E926C47FA67B3B2FC59150E871CE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserPointer
struct  GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0  : public GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D
{
public:
	// System.Single GvrLaserPointer::maxPointerDistance
	float ___maxPointerDistance_8;
	// System.Single GvrLaserPointer::defaultReticleDistance
	float ___defaultReticleDistance_9;
	// System.Single GvrLaserPointer::overrideCameraRayIntersectionDistance
	float ___overrideCameraRayIntersectionDistance_10;
	// GvrLaserVisual GvrLaserPointer::<LaserVisual>k__BackingField
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633 * ___U3CLaserVisualU3Ek__BackingField_12;
	// System.Boolean GvrLaserPointer::isHittingTarget
	bool ___isHittingTarget_13;

public:
	inline static int32_t get_offset_of_maxPointerDistance_8() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0, ___maxPointerDistance_8)); }
	inline float get_maxPointerDistance_8() const { return ___maxPointerDistance_8; }
	inline float* get_address_of_maxPointerDistance_8() { return &___maxPointerDistance_8; }
	inline void set_maxPointerDistance_8(float value)
	{
		___maxPointerDistance_8 = value;
	}

	inline static int32_t get_offset_of_defaultReticleDistance_9() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0, ___defaultReticleDistance_9)); }
	inline float get_defaultReticleDistance_9() const { return ___defaultReticleDistance_9; }
	inline float* get_address_of_defaultReticleDistance_9() { return &___defaultReticleDistance_9; }
	inline void set_defaultReticleDistance_9(float value)
	{
		___defaultReticleDistance_9 = value;
	}

	inline static int32_t get_offset_of_overrideCameraRayIntersectionDistance_10() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0, ___overrideCameraRayIntersectionDistance_10)); }
	inline float get_overrideCameraRayIntersectionDistance_10() const { return ___overrideCameraRayIntersectionDistance_10; }
	inline float* get_address_of_overrideCameraRayIntersectionDistance_10() { return &___overrideCameraRayIntersectionDistance_10; }
	inline void set_overrideCameraRayIntersectionDistance_10(float value)
	{
		___overrideCameraRayIntersectionDistance_10 = value;
	}

	inline static int32_t get_offset_of_U3CLaserVisualU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0, ___U3CLaserVisualU3Ek__BackingField_12)); }
	inline GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633 * get_U3CLaserVisualU3Ek__BackingField_12() const { return ___U3CLaserVisualU3Ek__BackingField_12; }
	inline GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633 ** get_address_of_U3CLaserVisualU3Ek__BackingField_12() { return &___U3CLaserVisualU3Ek__BackingField_12; }
	inline void set_U3CLaserVisualU3Ek__BackingField_12(GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633 * value)
	{
		___U3CLaserVisualU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLaserVisualU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_isHittingTarget_13() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0, ___isHittingTarget_13)); }
	inline bool get_isHittingTarget_13() const { return ___isHittingTarget_13; }
	inline bool* get_address_of_isHittingTarget_13() { return &___isHittingTarget_13; }
	inline void set_isHittingTarget_13(bool value)
	{
		___isHittingTarget_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRLASERPOINTER_T36847BC18F5E926C47FA67B3B2FC59150E871CE0_H
#ifndef GVRRETICLEPOINTER_T7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE_H
#define GVRRETICLEPOINTER_T7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrReticlePointer
struct  GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE  : public GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D
{
public:
	// System.Single GvrReticlePointer::maxReticleDistance
	float ___maxReticleDistance_12;
	// System.Int32 GvrReticlePointer::reticleSegments
	int32_t ___reticleSegments_13;
	// System.Single GvrReticlePointer::reticleGrowthSpeed
	float ___reticleGrowthSpeed_14;
	// System.Int32 GvrReticlePointer::reticleSortingOrder
	int32_t ___reticleSortingOrder_15;
	// UnityEngine.Material GvrReticlePointer::<MaterialComp>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CMaterialCompU3Ek__BackingField_16;
	// System.Single GvrReticlePointer::<ReticleInnerAngle>k__BackingField
	float ___U3CReticleInnerAngleU3Ek__BackingField_17;
	// System.Single GvrReticlePointer::<ReticleOuterAngle>k__BackingField
	float ___U3CReticleOuterAngleU3Ek__BackingField_18;
	// System.Single GvrReticlePointer::<ReticleDistanceInMeters>k__BackingField
	float ___U3CReticleDistanceInMetersU3Ek__BackingField_19;
	// System.Single GvrReticlePointer::<ReticleInnerDiameter>k__BackingField
	float ___U3CReticleInnerDiameterU3Ek__BackingField_20;
	// System.Single GvrReticlePointer::<ReticleOuterDiameter>k__BackingField
	float ___U3CReticleOuterDiameterU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_maxReticleDistance_12() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___maxReticleDistance_12)); }
	inline float get_maxReticleDistance_12() const { return ___maxReticleDistance_12; }
	inline float* get_address_of_maxReticleDistance_12() { return &___maxReticleDistance_12; }
	inline void set_maxReticleDistance_12(float value)
	{
		___maxReticleDistance_12 = value;
	}

	inline static int32_t get_offset_of_reticleSegments_13() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___reticleSegments_13)); }
	inline int32_t get_reticleSegments_13() const { return ___reticleSegments_13; }
	inline int32_t* get_address_of_reticleSegments_13() { return &___reticleSegments_13; }
	inline void set_reticleSegments_13(int32_t value)
	{
		___reticleSegments_13 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_14() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___reticleGrowthSpeed_14)); }
	inline float get_reticleGrowthSpeed_14() const { return ___reticleGrowthSpeed_14; }
	inline float* get_address_of_reticleGrowthSpeed_14() { return &___reticleGrowthSpeed_14; }
	inline void set_reticleGrowthSpeed_14(float value)
	{
		___reticleGrowthSpeed_14 = value;
	}

	inline static int32_t get_offset_of_reticleSortingOrder_15() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___reticleSortingOrder_15)); }
	inline int32_t get_reticleSortingOrder_15() const { return ___reticleSortingOrder_15; }
	inline int32_t* get_address_of_reticleSortingOrder_15() { return &___reticleSortingOrder_15; }
	inline void set_reticleSortingOrder_15(int32_t value)
	{
		___reticleSortingOrder_15 = value;
	}

	inline static int32_t get_offset_of_U3CMaterialCompU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CMaterialCompU3Ek__BackingField_16)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CMaterialCompU3Ek__BackingField_16() const { return ___U3CMaterialCompU3Ek__BackingField_16; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CMaterialCompU3Ek__BackingField_16() { return &___U3CMaterialCompU3Ek__BackingField_16; }
	inline void set_U3CMaterialCompU3Ek__BackingField_16(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CMaterialCompU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCompU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CReticleInnerAngleU3Ek__BackingField_17)); }
	inline float get_U3CReticleInnerAngleU3Ek__BackingField_17() const { return ___U3CReticleInnerAngleU3Ek__BackingField_17; }
	inline float* get_address_of_U3CReticleInnerAngleU3Ek__BackingField_17() { return &___U3CReticleInnerAngleU3Ek__BackingField_17; }
	inline void set_U3CReticleInnerAngleU3Ek__BackingField_17(float value)
	{
		___U3CReticleInnerAngleU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CReticleOuterAngleU3Ek__BackingField_18)); }
	inline float get_U3CReticleOuterAngleU3Ek__BackingField_18() const { return ___U3CReticleOuterAngleU3Ek__BackingField_18; }
	inline float* get_address_of_U3CReticleOuterAngleU3Ek__BackingField_18() { return &___U3CReticleOuterAngleU3Ek__BackingField_18; }
	inline void set_U3CReticleOuterAngleU3Ek__BackingField_18(float value)
	{
		___U3CReticleOuterAngleU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CReticleDistanceInMetersU3Ek__BackingField_19)); }
	inline float get_U3CReticleDistanceInMetersU3Ek__BackingField_19() const { return ___U3CReticleDistanceInMetersU3Ek__BackingField_19; }
	inline float* get_address_of_U3CReticleDistanceInMetersU3Ek__BackingField_19() { return &___U3CReticleDistanceInMetersU3Ek__BackingField_19; }
	inline void set_U3CReticleDistanceInMetersU3Ek__BackingField_19(float value)
	{
		___U3CReticleDistanceInMetersU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CReticleInnerDiameterU3Ek__BackingField_20)); }
	inline float get_U3CReticleInnerDiameterU3Ek__BackingField_20() const { return ___U3CReticleInnerDiameterU3Ek__BackingField_20; }
	inline float* get_address_of_U3CReticleInnerDiameterU3Ek__BackingField_20() { return &___U3CReticleInnerDiameterU3Ek__BackingField_20; }
	inline void set_U3CReticleInnerDiameterU3Ek__BackingField_20(float value)
	{
		___U3CReticleInnerDiameterU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE, ___U3CReticleOuterDiameterU3Ek__BackingField_21)); }
	inline float get_U3CReticleOuterDiameterU3Ek__BackingField_21() const { return ___U3CReticleOuterDiameterU3Ek__BackingField_21; }
	inline float* get_address_of_U3CReticleOuterDiameterU3Ek__BackingField_21() { return &___U3CReticleOuterDiameterU3Ek__BackingField_21; }
	inline void set_U3CReticleOuterDiameterU3Ek__BackingField_21(float value)
	{
		___U3CReticleOuterDiameterU3Ek__BackingField_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRETICLEPOINTER_T7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE_H
#ifndef BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#define BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_RaycastResultCache_4)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_AxisEventData_5)); }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_EventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_BaseEventData_7)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_InputOverride_8)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_DefaultInput_9)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifndef BASERAYCASTER_TC7F6105A89F54A38FBFC2659901855FDBB0E3966_H
#define BASERAYCASTER_TC7F6105A89F54A38FBFC2659901855FDBB0E3966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_TC7F6105A89F54A38FBFC2659901855FDBB0E3966_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef GVRBASEPOINTERRAYCASTER_TB09385BCEC9FE57CA7B90996CE27024095D08D8D_H
#define GVRBASEPOINTERRAYCASTER_TB09385BCEC9FE57CA7B90996CE27024095D08D8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointerRaycaster
struct  GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D  : public BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966
{
public:
	// GvrBasePointer_PointerRay GvrBasePointerRaycaster::lastRay
	PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED  ___lastRay_4;
	// GvrBasePointer_RaycastMode GvrBasePointerRaycaster::<CurrentRaycastModeForHybrid>k__BackingField
	int32_t ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_lastRay_4() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D, ___lastRay_4)); }
	inline PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED  get_lastRay_4() const { return ___lastRay_4; }
	inline PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED * get_address_of_lastRay_4() { return &___lastRay_4; }
	inline void set_lastRay_4(PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED  value)
	{
		___lastRay_4 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D, ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5)); }
	inline int32_t get_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() const { return ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() { return &___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5; }
	inline void set_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5(int32_t value)
	{
		___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTERRAYCASTER_TB09385BCEC9FE57CA7B90996CE27024095D08D8D_H
#ifndef GVRPOINTERINPUTMODULE_T74C62030DE3749C787FB9B93A65492FB50BA6DBA_H
#define GVRPOINTERINPUTMODULE_T74C62030DE3749C787FB9B93A65492FB50BA6DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModule
struct  GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA  : public BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939
{
public:
	// System.Boolean GvrPointerInputModule::vrModeOnly
	bool ___vrModeOnly_10;
	// GvrPointerScrollInput GvrPointerInputModule::scrollInput
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * ___scrollInput_11;
	// GvrPointerInputModuleImpl GvrPointerInputModule::<Impl>k__BackingField
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF * ___U3CImplU3Ek__BackingField_12;
	// GvrEventExecutor GvrPointerInputModule::<EventExecutor>k__BackingField
	GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3 * ___U3CEventExecutorU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_vrModeOnly_10() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA, ___vrModeOnly_10)); }
	inline bool get_vrModeOnly_10() const { return ___vrModeOnly_10; }
	inline bool* get_address_of_vrModeOnly_10() { return &___vrModeOnly_10; }
	inline void set_vrModeOnly_10(bool value)
	{
		___vrModeOnly_10 = value;
	}

	inline static int32_t get_offset_of_scrollInput_11() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA, ___scrollInput_11)); }
	inline GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * get_scrollInput_11() const { return ___scrollInput_11; }
	inline GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 ** get_address_of_scrollInput_11() { return &___scrollInput_11; }
	inline void set_scrollInput_11(GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485 * value)
	{
		___scrollInput_11 = value;
		Il2CppCodeGenWriteBarrier((&___scrollInput_11), value);
	}

	inline static int32_t get_offset_of_U3CImplU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA, ___U3CImplU3Ek__BackingField_12)); }
	inline GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF * get_U3CImplU3Ek__BackingField_12() const { return ___U3CImplU3Ek__BackingField_12; }
	inline GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF ** get_address_of_U3CImplU3Ek__BackingField_12() { return &___U3CImplU3Ek__BackingField_12; }
	inline void set_U3CImplU3Ek__BackingField_12(GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF * value)
	{
		___U3CImplU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImplU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA, ___U3CEventExecutorU3Ek__BackingField_13)); }
	inline GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3 * get_U3CEventExecutorU3Ek__BackingField_13() const { return ___U3CEventExecutorU3Ek__BackingField_13; }
	inline GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3 ** get_address_of_U3CEventExecutorU3Ek__BackingField_13() { return &___U3CEventExecutorU3Ek__BackingField_13; }
	inline void set_U3CEventExecutorU3Ek__BackingField_13(GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3 * value)
	{
		___U3CEventExecutorU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULE_T74C62030DE3749C787FB9B93A65492FB50BA6DBA_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef GVRPOINTERGRAPHICRAYCASTER_T29B1EAE457578A78A28FBF614169C6637DEE20BC_H
#define GVRPOINTERGRAPHICRAYCASTER_T29B1EAE457578A78A28FBF614169C6637DEE20BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster
struct  GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC  : public GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D
{
public:
	// System.Boolean GvrPointerGraphicRaycaster::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_7;
	// GvrPointerGraphicRaycaster_BlockingObjects GvrPointerGraphicRaycaster::blockingObjects
	int32_t ___blockingObjects_8;
	// UnityEngine.LayerMask GvrPointerGraphicRaycaster::blockingMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___blockingMask_9;
	// UnityEngine.Canvas GvrPointerGraphicRaycaster::targetCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___targetCanvas_10;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::raycastResults
	List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * ___raycastResults_11;
	// UnityEngine.Camera GvrPointerGraphicRaycaster::cachedPointerEventCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cachedPointerEventCamera_12;

public:
	inline static int32_t get_offset_of_ignoreReversedGraphics_7() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___ignoreReversedGraphics_7)); }
	inline bool get_ignoreReversedGraphics_7() const { return ___ignoreReversedGraphics_7; }
	inline bool* get_address_of_ignoreReversedGraphics_7() { return &___ignoreReversedGraphics_7; }
	inline void set_ignoreReversedGraphics_7(bool value)
	{
		___ignoreReversedGraphics_7 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_8() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___blockingObjects_8)); }
	inline int32_t get_blockingObjects_8() const { return ___blockingObjects_8; }
	inline int32_t* get_address_of_blockingObjects_8() { return &___blockingObjects_8; }
	inline void set_blockingObjects_8(int32_t value)
	{
		___blockingObjects_8 = value;
	}

	inline static int32_t get_offset_of_blockingMask_9() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___blockingMask_9)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_blockingMask_9() const { return ___blockingMask_9; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_blockingMask_9() { return &___blockingMask_9; }
	inline void set_blockingMask_9(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___blockingMask_9 = value;
	}

	inline static int32_t get_offset_of_targetCanvas_10() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___targetCanvas_10)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_targetCanvas_10() const { return ___targetCanvas_10; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_targetCanvas_10() { return &___targetCanvas_10; }
	inline void set_targetCanvas_10(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___targetCanvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetCanvas_10), value);
	}

	inline static int32_t get_offset_of_raycastResults_11() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___raycastResults_11)); }
	inline List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * get_raycastResults_11() const { return ___raycastResults_11; }
	inline List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 ** get_address_of_raycastResults_11() { return &___raycastResults_11; }
	inline void set_raycastResults_11(List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * value)
	{
		___raycastResults_11 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_11), value);
	}

	inline static int32_t get_offset_of_cachedPointerEventCamera_12() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC, ___cachedPointerEventCamera_12)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cachedPointerEventCamera_12() const { return ___cachedPointerEventCamera_12; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cachedPointerEventCamera_12() { return &___cachedPointerEventCamera_12; }
	inline void set_cachedPointerEventCamera_12(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cachedPointerEventCamera_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPointerEventCamera_12), value);
	}
};

struct GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::sortedGraphics
	List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * ___sortedGraphics_13;

public:
	inline static int32_t get_offset_of_sortedGraphics_13() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC_StaticFields, ___sortedGraphics_13)); }
	inline List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * get_sortedGraphics_13() const { return ___sortedGraphics_13; }
	inline List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 ** get_address_of_sortedGraphics_13() { return &___sortedGraphics_13; }
	inline void set_sortedGraphics_13(List_1_t5DB49737D499F93016BB3E3D19278B515B1272E6 * value)
	{
		___sortedGraphics_13 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERGRAPHICRAYCASTER_T29B1EAE457578A78A28FBF614169C6637DEE20BC_H
#ifndef GVRPOINTERPHYSICSRAYCASTER_TBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263_H
#define GVRPOINTERPHYSICSRAYCASTER_TBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster
struct  GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263  : public GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D
{
public:
	// UnityEngine.LayerMask GvrPointerPhysicsRaycaster::raycasterEventMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___raycasterEventMask_8;
	// System.Int32 GvrPointerPhysicsRaycaster::maxRaycastHits
	int32_t ___maxRaycastHits_9;
	// UnityEngine.RaycastHit[] GvrPointerPhysicsRaycaster::hits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___hits_10;
	// GvrPointerPhysicsRaycaster_HitComparer GvrPointerPhysicsRaycaster::hitComparer
	HitComparer_tFB834727888EDA249326245CA376445F484ADD2F * ___hitComparer_11;

public:
	inline static int32_t get_offset_of_raycasterEventMask_8() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263, ___raycasterEventMask_8)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_raycasterEventMask_8() const { return ___raycasterEventMask_8; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_raycasterEventMask_8() { return &___raycasterEventMask_8; }
	inline void set_raycasterEventMask_8(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___raycasterEventMask_8 = value;
	}

	inline static int32_t get_offset_of_maxRaycastHits_9() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263, ___maxRaycastHits_9)); }
	inline int32_t get_maxRaycastHits_9() const { return ___maxRaycastHits_9; }
	inline int32_t* get_address_of_maxRaycastHits_9() { return &___maxRaycastHits_9; }
	inline void set_maxRaycastHits_9(int32_t value)
	{
		___maxRaycastHits_9 = value;
	}

	inline static int32_t get_offset_of_hits_10() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263, ___hits_10)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_hits_10() const { return ___hits_10; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_hits_10() { return &___hits_10; }
	inline void set_hits_10(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___hits_10 = value;
		Il2CppCodeGenWriteBarrier((&___hits_10), value);
	}

	inline static int32_t get_offset_of_hitComparer_11() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263, ___hitComparer_11)); }
	inline HitComparer_tFB834727888EDA249326245CA376445F484ADD2F * get_hitComparer_11() const { return ___hitComparer_11; }
	inline HitComparer_tFB834727888EDA249326245CA376445F484ADD2F ** get_address_of_hitComparer_11() { return &___hitComparer_11; }
	inline void set_hitComparer_11(HitComparer_tFB834727888EDA249326245CA376445F484ADD2F * value)
	{
		___hitComparer_11 = value;
		Il2CppCodeGenWriteBarrier((&___hitComparer_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERPHYSICSRAYCASTER_TBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2602[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2610[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C), -1, sizeof(AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[34] = 
{
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_player_4(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_originalLocation_5(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_buttontimer_6(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_buttontime_7(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C_StaticFields::get_offset_of_isEnable_8(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_arrivedPosition_9(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_Runtimer_10(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_walk_11(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_backtimer_12(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_isConfirmButton_13(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_isSoundButton_14(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_isTranslateButton_15(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_isBack_16(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_isEng_17(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_Bar_18(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_obj_Bar_19(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_obj_shoot_20(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_obj_animals_21(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_obj_original_22(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_obj_infocanvas_23(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_gui_title_24(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_gui_message_25(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_title_26(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_title_chi_27(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_message_28(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_message_chi_29(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_sound_pronunciation_30(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_sound_animal_31(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_Anim_32(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_BS_33(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_anima_tion_34(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_animation_walk_35(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_str_animation_idle_36(),
	AnimalsController_t43F4FC976544E7B722CF2B1782037E4E22B5E16C::get_offset_of_sceneName_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[12] = 
{
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_buttontimer_4(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_buttontime_5(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_Bar_6(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_obj_Bar_7(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_obj_shoot_8(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_isQuiz_9(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_isMid_10(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_isLow_11(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_obj_reminderCanvas_12(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_restTime_13(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_remindertimer_14(),
	AridAreaMenu_t9B77C362683097C83FCD5F3E8DF516BD705CFC18::get_offset_of_remindertime_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[50] = 
{
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_questionQueueList_4(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_questionLists_5(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_limitTime_6(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_time_7(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_currentQuestionNum_8(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_correctCount_9(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_buttontimer_10(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_buttontime_11(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isSelectA_12(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isSelectB_13(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isSelectC_14(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isSelectD_15(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isStart_16(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isResumebutton_17(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isPausebutton_18(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isStartbutton_19(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isConfirmbutton_20(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_isQuitbutton_21(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_animalsCount_22(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_canAnswer_23(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Kangaroo_24(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_TasmanianDevil_25(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Koala_26(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Ornitorinco_27(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Ostrich_28(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_LizardCollar_29(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Scorpion_30(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Panther_31(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Giraffe_32(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Rhinoceros_33(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Zebra_34(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_time_35(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_round_36(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_Bar_37(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_Bar_38(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_shoot_39(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_canvas_question_40(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_canvas_pause_41(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_btn_pause_42(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_obj_start_menu_43(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_mark_44(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_canvas_finish_45(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_choiceA_46(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_choiceB_47(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_choiceC_48(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_gui_choiceD_49(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_btn_choiceA_50(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_btn_choiceB_51(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_btn_choiceC_52(),
	AridAreaQuizController_t51ABC34B4230DB477242891DCFF108B1BF431F5E::get_offset_of_btn_choiceD_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[3] = 
{
	U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502::get_offset_of_U3CU3E1__state_0(),
	U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502::get_offset_of_U3CU3E2__current_1(),
	U3CwaiterU3Ed__68_t79D74BE1A4B8ECB0DDA734C185BBD843CD2F2502::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[10] = 
{
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_buttontimer_4(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_buttontime_5(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_Bar_6(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_obj_Bar_7(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_obj_shoot_8(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_isTutorial_9(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_obj_reminderCanvas_10(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_restTime_11(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_remindertimer_12(),
	AridAreaQuizMenu_tB70D225F1023B865D11456144E86061C60216371::get_offset_of_remindertime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388), -1, sizeof(AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[11] = 
{
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Kangaroo_4(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_TasmanianDevil_5(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Koala_6(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Ornitorinco_7(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Ostrich_8(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_LizardCollar_9(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Scorpion_10(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Panther_11(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Giraffe_12(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Rhinoceros_13(),
	AridHideAnimalController_tAB9B160A5D66291A4549C57853FFE5F51BBB9388_StaticFields::get_offset_of_obj_Zebra_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5), -1, sizeof(ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2620[11] = 
{
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Bear_4(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Cattle_5(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Deer_6(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Ibex_7(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Wild_boar_8(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Wolf_9(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Rabbit_10(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Crocodile_11(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Lion_12(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Tiger_13(),
	ForestHideAnimalController_t3DFE138C1330EE03A9E18856F58B956605D436D5_StaticFields::get_offset_of_obj_Elephant_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[12] = 
{
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_buttontimer_4(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_buttontime_5(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_Bar_6(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_obj_Bar_7(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_obj_shoot_8(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_isQuiz_9(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_isMid_10(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_isHigh_11(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_obj_reminderCanvas_12(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_restTime_13(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_remindertimer_14(),
	ForestMenu_t0B2C5BB757D1D339E4F3F19BD558962D3F2C211D::get_offset_of_remindertime_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[50] = 
{
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_questionQueueList_4(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_questionLists_5(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_limitTime_6(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_time_7(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_currentQuestionNum_8(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_correctCount_9(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_buttontimer_10(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_buttontime_11(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isSelectA_12(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isSelectB_13(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isSelectC_14(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isSelectD_15(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isStart_16(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isResumebutton_17(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isPausebutton_18(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isStartbutton_19(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isConfirmbutton_20(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_isQuitbutton_21(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_animalsCount_22(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_canAnswer_23(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Bear_24(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Cattle_25(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Deer_26(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Ibex_27(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Wild_boar_28(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Wolf_29(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Rabbit_30(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Crocodile_31(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Lion_32(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Tiger_33(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Elephant_34(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_time_35(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_round_36(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_Bar_37(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_Bar_38(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_shoot_39(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_canvas_question_40(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_canvas_pause_41(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_btn_pause_42(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_obj_start_menu_43(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_mark_44(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_canvas_finish_45(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_choiceA_46(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_choiceB_47(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_choiceC_48(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_gui_choiceD_49(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_btn_choiceA_50(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_btn_choiceB_51(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_btn_choiceC_52(),
	ForestQuizController_t9C60CEEFF9BA1CD224E71231F4D3A4755B714BEA::get_offset_of_btn_choiceD_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[3] = 
{
	U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868::get_offset_of_U3CU3E1__state_0(),
	U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868::get_offset_of_U3CU3E2__current_1(),
	U3CwaiterU3Ed__68_t67EF5C9E90319BF27F94F060EE2FAE75B53D9868::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[10] = 
{
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_buttontimer_4(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_buttontime_5(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_Bar_6(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_obj_Bar_7(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_obj_shoot_8(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_isTutorial_9(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_obj_reminderCanvas_10(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_restTime_11(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_remindertimer_12(),
	ForestQuizMenu_tBC267C272E10BEBD0053CAFBED378C23724E3685::get_offset_of_remindertime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[8] = 
{
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_buttontimer_4(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_buttontime_5(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_Bar_6(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_obj_Bar_7(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_obj_shoot_8(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_isLow_9(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_isMid_10(),
	MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105::get_offset_of_isHigh_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[6] = 
{
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_question_0(),
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_choiceA_1(),
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_choiceB_2(),
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_choiceC_3(),
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_choiceD_4(),
	QuestionList_t55C63A912E36A8A65CAF13574DF965F8C28D9265::get_offset_of_ans_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (QuizAnimalSound_tF7945591B3819B9716312895356E8C3DAD69511D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[1] = 
{
	QuizAnimalSound_tF7945591B3819B9716312895356E8C3DAD69511D::get_offset_of_sound_animal_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93), -1, sizeof(RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2628[2] = 
{
	RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields::get_offset_of_timer_4(),
	RestTimer_tBAF7BFB4237061BE276C78F7AE6B388D9345EB93_StaticFields::get_offset_of_isStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51), -1, sizeof(SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2629[27] = 
{
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_player_4(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_originalLocation_5(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_buttontimer_6(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_buttontime_7(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51_StaticFields::get_offset_of_isEnable_8(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_arrivedPosition_9(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_Runtimer_10(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_walk_11(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_backtimer_12(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_isConfirmButton_13(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_isSoundButton_14(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_isTranslateButton_15(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_isBack_16(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_isEng_17(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_Bar_18(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_obj_Bar_19(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_obj_shoot_20(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_obj_animals_21(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_obj_original_22(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_obj_infocanvas_23(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_gui_title_24(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_gui_message_25(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_str_title_26(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_str_title_chi_27(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_str_message_28(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_str_message_chi_29(),
	SeaAnimalController_t89766BF927D6FEC862E9C7188AA865C42E362B51::get_offset_of_sound_pronunciation_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354), -1, sizeof(SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2630[11] = 
{
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Octopus_4(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Crab_5(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Starfish_6(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Dolphin_7(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Sea_Urchin_8(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Moray_Eel_9(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Long_Finned_Pilot_Whale_10(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Killer_Whale_11(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Basking_Shark_12(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Great_White_Shark_13(),
	SeaHideAnimalController_t3711301C8F9B2A5D10AFB59E05B04173C1C10354_StaticFields::get_offset_of_obj_Squid_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[12] = 
{
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_buttontimer_4(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_buttontime_5(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_Bar_6(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_obj_Bar_7(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_obj_shoot_8(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_obj_reminderCanvas_9(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_isQuiz_10(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_isLow_11(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_isHigh_12(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_restTime_13(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_remindertimer_14(),
	SeaMenu_tF1E89779D3708A8D467C4B64D248A6A4D093C7C4::get_offset_of_remindertime_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[49] = 
{
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_questionQueueList_4(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_questionLists_5(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_limitTime_6(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_time_7(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_currentQuestionNum_8(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_correctCount_9(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_buttontimer_10(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_buttontime_11(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isSelectA_12(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isSelectB_13(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isSelectC_14(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isSelectD_15(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isStart_16(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isResumebutton_17(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isPausebutton_18(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isStartbutton_19(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isConfirmbutton_20(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_isQuitbutton_21(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_animalsCount_22(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_canAnswer_23(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Octopus_24(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Crab_25(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Starfish_26(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Dolphin_27(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Sea_Urchin_28(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Moray_Eel_29(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Long_Finned_Pilot_Whale_30(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Killer_Whale_31(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Basking_Shark_32(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Great_White_Shark_33(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Squid_34(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_time_35(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_round_36(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_Bar_37(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_Bar_38(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_shoot_39(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_canvas_question_40(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_canvas_pause_41(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_obj_start_menu_42(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_mark_43(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_canvas_finish_44(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_choiceA_45(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_choiceB_46(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_choiceC_47(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_gui_choiceD_48(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_btn_choiceA_49(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_btn_choiceB_50(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_btn_choiceC_51(),
	SeaQuizController_t546D83FD8AE42289E7EAB06A3758BA87A904CD75::get_offset_of_btn_choiceD_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[3] = 
{
	U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828::get_offset_of_U3CU3E1__state_0(),
	U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828::get_offset_of_U3CU3E2__current_1(),
	U3CwaiterU3Ed__67_t699BB7F04DCCB6AFC4034EB61734765ED8A25828::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[10] = 
{
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_buttontimer_4(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_buttontime_5(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_Bar_6(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_obj_Bar_7(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_obj_shoot_8(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_isTutorial_9(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_obj_reminderCanvas_10(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_restTime_11(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_remindertimer_12(),
	SeaQuizMenu_tEA3A187CD8F1CCD11648725B5FEB6AEDA828D3D6::get_offset_of_remindertime_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96), -1, sizeof(GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2635[25] = 
{
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_sampleRate_0(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_numChannels_1(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_framesPerBuffer_2(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_listenerDirectivityColor_3(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_sourceDirectivityColor_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_bounds_17(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_enabledRooms_18(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_initialized_19(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_listenerTransform_20(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_occlusionHits_21(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_occlusionMaskValue_22(),
	GvrAudio_tA153F5F402364F6F8EF30AE98C56FCAB74669D96_StaticFields::get_offset_of_transformMatrix_23(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (Quality_t2A8FDC622752170001EBDA0DF3DCEFE60042982E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2636[4] = 
{
	Quality_t2A8FDC622752170001EBDA0DF3DCEFE60042982E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (SpatializerData_t2093C207741ACE666293262EF123349EB3B7F1C5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2637[9] = 
{
	SpatializerData_t2093C207741ACE666293262EF123349EB3B7F1C5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (SpatializerType_tD983F6F76C53B8E648DB5EAC2F18763BB309246F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2638[3] = 
{
	SpatializerType_tD983F6F76C53B8E648DB5EAC2F18763BB309246F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683)+ sizeof (RuntimeObject), sizeof(RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2639[20] = 
{
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_positionZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_rotationX_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_rotationY_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_rotationZ_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_rotationW_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_dimensionsX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_dimensionsY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_dimensionsZ_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialLeft_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialRight_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialBottom_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialTop_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialFront_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_materialBack_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_reflectionScalar_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_reverbGain_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_reverbTime_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t38CEF2E715BD4DC611ACB600288C87E43A59A683::get_offset_of_reverbBrightness_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[3] = 
{
	GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4::get_offset_of_globalGainDb_4(),
	GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4::get_offset_of_occlusionMask_5(),
	GvrAudioListener_tA0668B4EEF081D281606E35E2F2B46CD816492E4::get_offset_of_quality_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[11] = 
{
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_leftWall_4(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_rightWall_5(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_floor_6(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_ceiling_7(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_backWall_8(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_frontWall_9(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_reflectivity_10(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_reverbGainDb_11(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_reverbBrightness_12(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_reverbTime_13(),
	GvrAudioRoom_t573BA65F08C7EF54ACAB0B45D2FF314498038268::get_offset_of_size_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (SurfaceMaterial_t304A18E7AE00061A3E42922D9140E64184AC3573)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2642[24] = 
{
	SurfaceMaterial_t304A18E7AE00061A3E42922D9140E64184AC3573::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[18] = 
{
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_bypassRoomEffects_4(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_gainDb_5(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_playOnAwake_6(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldClip0102_7(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldClip0304_8(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldLoop_9(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldMute_10(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldPitch_11(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldPriority_12(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldSpatialBlend_13(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldDopplerLevel_14(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldVolume_15(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldRolloffMode_16(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldMaxDistance_17(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_soundfieldMinDistance_18(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_id_19(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_audioSources_20(),
	GvrAudioSoundfield_t3090B72F22C49ED8DCFF87FC95AD9ABB9EDFA4B8::get_offset_of_isPaused_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[26] = 
{
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_bypassRoomEffects_4(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_directivityAlpha_5(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_directivitySharpness_6(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_listenerDirectivityAlpha_7(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_listenerDirectivitySharpness_8(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_gainDb_9(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_occlusionEnabled_10(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_playOnAwake_11(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceClip_12(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceLoop_13(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceMute_14(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourcePitch_15(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourcePriority_16(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceSpatialBlend_17(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceDopplerLevel_18(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceSpread_19(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceVolume_20(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceRolloffMode_21(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceMaxDistance_22(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_sourceMinDistance_23(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_hrtfEnabled_24(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_audioSource_25(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_id_26(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_currentOcclusion_27(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_nextOcclusionUpdate_28(),
	GvrAudioSource_tCBABB1F18C1786B2B220395E3019FB68BA6DE4F6::get_offset_of_isPaused_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (GvrController_t49163591BDC2A15FF87EE7A9A42EC089CAC0A98F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (GvrControllerVisualManager_tA6692588113CD5C5FA19BDC09A8923BB490DDD14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (GvrPointerManager_tD4F8B06940E60BA591ADBFF80A78E08BCEBDB520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[14] = 
{
	0,
	0,
	0,
	0,
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_maxReticleDistance_12(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_reticleSegments_13(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_reticleGrowthSpeed_14(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_reticleSortingOrder_15(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CMaterialCompU3Ek__BackingField_16(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_17(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_18(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_19(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_20(),
	GvrReticlePointer_t7D7B8E72C40FF1E878E8536E4537B16A53DEE4AE::get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0), -1, sizeof(GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2649[35] = 
{
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_elbowRestPosition_4(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_wristRestPosition_5(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_controllerRestPosition_6(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_armExtensionOffset_7(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_elbowBendRatio_8(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_fadeControllerOffset_9(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_fadeDistanceFromHeadForward_10(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_fadeDistanceFromHeadSide_11(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_tooltipMinDistanceFromFace_12(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_tooltipMaxAngleFromCamera_13(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_isLockedToNeck_14(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_15(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_neckPosition_16(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_elbowPosition_17(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_elbowRotation_18(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_wristPosition_19(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_wristRotation_20(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_controllerPosition_21(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_controllerRotation_22(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_preferredAlpha_23(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_tooltipAlphaValue_24(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_handedMultiplier_25(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_torsoDirection_26(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0::get_offset_of_torsoRotation_27(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_DEFAULT_ELBOW_REST_POSITION_28(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_DEFAULT_WRIST_REST_POSITION_29(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_DEFAULT_CONTROLLER_REST_POSITION_30(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_DEFAULT_ARM_EXTENSION_OFFSET_31(),
	0,
	0,
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_SHOULDER_POSITION_34(),
	GvrArmModel_t3186FF0FBB1CBB9C61E604278BC1C806ED45C1E0_StaticFields::get_offset_of_NECK_OFFSET_35(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (GvrBaseArmModel_t8D41D802A58F630557FAE6C739028B16B76240AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2652[6] = 
{
	GvrConnectionState_tBEA19BB79DA64E9E956AB059FCA945ACC6803982::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2653[9] = 
{
	GvrControllerApiStatus_t039908D8922D8E98CBBFABB19FA503ACE3887A85::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2654[8] = 
{
	GvrControllerBatteryLevel_t48A4D5107F5C9B9CC137363AD21B7C0EBBC71904::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2655[8] = 
{
	GvrControllerButton_tE7A7A32A9D09E43D05C67221E70C1D44625EA645::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (GvrControllerHand_t378A8CB3F703F3F3BE381830AD8D36CE65C4B806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2656[5] = 
{
	GvrControllerHand_t378A8CB3F703F3F3BE381830AD8D36CE65C4B806::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926), -1, sizeof(GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2657[7] = 
{
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_instances_4(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_controllerProvider_5(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_handedness_6(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_onDevicesChangedInternal_7(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_OnControllerInputUpdated_8(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926_StaticFields::get_offset_of_OnPostControllerInputUpdated_9(),
	GvrControllerInput_t8DE5CC74523C7C84CA79CAC8399898A248FD5926::get_offset_of_emulatorConnectionMode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (OnStateChangedEvent_tA3E765C2974C2B6A1E9EE90009E5C437ADE3319F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (EmulatorConnectionMode_t2B86B91C3C252BF9BC47401216315ACE8FF6C199)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2659[4] = 
{
	EmulatorConnectionMode_t2B86B91C3C252BF9BC47401216315ACE8FF6C199::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[7] = 
{
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_controllerProvider_0(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_controllerId_1(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_controllerState_2(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_touchPosCentered_3(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_lastUpdatedFrameCount_4(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_valid_5(),
	GvrControllerInputDevice_t8271F0D8A91E8F069A0A2B36F4E19AB8C263DBF7::get_offset_of_OnStateChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[10] = 
{
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_isSizeBasedOnCameraDistance_4(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_sizeMeters_5(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_doesReticleFaceCamera_6(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_sortingOrder_7(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_meshRenderer_10(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_meshFilter_11(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_preRenderLocalScale_12(),
	GvrControllerReticleVisual_tDFC324DCB77D52ED105927BAA8E5DA09DD1B7FB6::get_offset_of_preRenderLocalRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428)+ sizeof (RuntimeObject), sizeof(FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2662[3] = 
{
	FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428::get_offset_of_alongXAxis_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428::get_offset_of_alongYAxis_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceCameraData_t5A7B2EF4CD3899BD7C26E5B5FE8EE104574C7428::get_offset_of_alongZAxis_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[41] = 
{
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_attachmentPrefabs_4(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_touchPadColor_5(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_appButtonColor_6(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_systemButtonColor_7(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_readControllerState_8(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_displayState_9(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_maximumAlpha_10(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_U3CArmModelU3Ek__BackingField_11(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_12(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_controllerRenderer_13(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_materialPropertyBlock_14(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_alphaId_15(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_touchId_16(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_touchPadId_17(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_appButtonId_18(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_systemButtonId_19(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_batteryColorId_20(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_wasTouching_21(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_touchTime_22(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_controllerShaderData_23(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_controllerShaderData2_24(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_currentBatteryColor_25(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_GVR_BATTERY_CRITICAL_COLOR_40(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_GVR_BATTERY_LOW_COLOR_41(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_GVR_BATTERY_MED_COLOR_42(),
	GvrControllerVisual_t59A828DA23AF5380628F2D5593A8913431CFE003::get_offset_of_GVR_BATTERY_FULL_COLOR_43(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F)+ sizeof (RuntimeObject), sizeof(ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2664[7] = 
{
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_batteryLevel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_batteryCharging_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_clickButton_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_appButton_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_homeButton_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_touching_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t656EBDB6918E1D539467855EEE9EF99312CCC00F::get_offset_of_touchPos_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[6] = 
{
	GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0::get_offset_of_maxPointerDistance_8(),
	GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0::get_offset_of_defaultReticleDistance_9(),
	GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0::get_offset_of_overrideCameraRayIntersectionDistance_10(),
	0,
	GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0::get_offset_of_U3CLaserVisualU3Ek__BackingField_12(),
	GvrLaserPointer_t36847BC18F5E926C47FA67B3B2FC59150E871CE0::get_offset_of_isHittingTarget_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[21] = 
{
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_reticle_4(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_controller_5(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_laserColor_6(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_laserColorEnd_7(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_maxLaserDistance_8(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_lerpSpeed_9(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_lerpThreshold_10(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_shrinkLaser_11(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_shrunkScale_12(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_beginShrinkAngleDegrees_13(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_endShrinkAngleDegrees_14(),
	0,
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_U3CArmModelU3Ek__BackingField_16(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_U3CLaserU3Ek__BackingField_17(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_shrinkRatio_19(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_targetDistance_20(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_currentDistance_21(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_currentPosition_22(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_currentLocalPosition_23(),
	GvrLaserVisual_t7C90095C3AB0299526189657F7F0B599C89B3633::get_offset_of_currentLocalRotation_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (GetPointForDistanceDelegate_t42D8A94340A612C14C21D3F9B20BE15A9306F869), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[2] = 
{
	GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965::get_offset_of_lastAppliedYawCorrection_4(),
	GvrRecenterOnlyController_t5A1A3C56508BC7EF0FC5292840919C0B3073C965::get_offset_of_yawCorrection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[4] = 
{
	GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5::get_offset_of_armModel_4(),
	GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5::get_offset_of_controllerInputDevice_5(),
	GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5::get_offset_of_isDeactivatedWhenDisconnected_6(),
	GvrTrackedController_tD2C6093BA5D682467A5B478153581199982633E5::get_offset_of_controllerHand_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[4] = 
{
	GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42::get_offset_of_tooltipRenderer_4(),
	GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42::get_offset_of_U3CArmModelU3Ek__BackingField_5(),
	GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42::get_offset_of_materialPropertyBlock_6(),
	GvrControllerTooltipsSimple_t0575897A0AB7289613E1556AD83E920CA4378C42::get_offset_of_colorId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42), -1, sizeof(GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2672[14] = 
{
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields::get_offset_of_RIGHT_SIDE_ROTATION_4(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields::get_offset_of_LEFT_SIDE_ROTATION_5(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields::get_offset_of_SQUARE_CENTER_6(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42_StaticFields::get_offset_of_PIVOT_7(),
	0,
	0,
	0,
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_location_11(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_text_12(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_alwaysVisible_13(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_isOnLeft_14(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_rectTransform_15(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_canvasGroup_16(),
	GvrTooltip_t4AD3DF6F4EDDFB6C0ADD8567EFB6252267BB6B42::get_offset_of_U3CArmModelU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (Location_t762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2673[6] = 
{
	Location_t762DE7406AF8AA2479F7EB6D23A14F4A5516EE5D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D::get_offset_of_raycastMode_4(),
	GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D::get_offset_of_overridePointerCamera_5(),
	GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6(),
	GvrBasePointer_t2E2389BAFCF1D39C6C6EE4997589B3879EBC855D::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (RaycastMode_tD7D5952B8515A9A9E4A0EA315926DBFDE3A24183)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	RaycastMode_tD7D5952B8515A9A9E4A0EA315926DBFDE3A24183::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED)+ sizeof (RuntimeObject), sizeof(PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED ), 0, 0 };
extern const int32_t g_FieldOffsetTable2676[3] = 
{
	PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED::get_offset_of_ray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED::get_offset_of_distanceFromStart_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t8B4442A8D4822E7C61B34FB22F27B0EBFDA903ED::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[2] = 
{
	GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D::get_offset_of_lastRay_4(),
	GvrBasePointerRaycaster_tB09385BCEC9FE57CA7B90996CE27024095D08D8D::get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790), -1, sizeof(GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	GvrExecuteEventsExtension_tAB53FCA3FA539177EFFE0E1C63F007F1E6B17790_StaticFields::get_offset_of_s_HoverHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC), -1, sizeof(GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2680[8] = 
{
	0,
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_ignoreReversedGraphics_7(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_blockingObjects_8(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_blockingMask_9(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_targetCanvas_10(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_raycastResults_11(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC::get_offset_of_cachedPointerEventCamera_12(),
	GvrPointerGraphicRaycaster_t29B1EAE457578A78A28FBF614169C6637DEE20BC_StaticFields::get_offset_of_sortedGraphics_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (BlockingObjects_tFBC510CA4D09993D8A31624F4DAD506EBCC60B69)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2681[5] = 
{
	BlockingObjects_tFBC510CA4D09993D8A31624F4DAD506EBCC60B69::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF), -1, sizeof(U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2682[2] = 
{
	U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t35C5BBC339F1F8766F5E7262AC8FA1A5BFBA41DF_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[6] = 
{
	0,
	0,
	GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263::get_offset_of_raycasterEventMask_8(),
	GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263::get_offset_of_maxRaycastHits_9(),
	GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263::get_offset_of_hits_10(),
	GvrPointerPhysicsRaycaster_tBC59C8BC77DE8327CFD52BE954D6FEFFA4E77263::get_offset_of_hitComparer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (HitComparer_tFB834727888EDA249326245CA376445F484ADD2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[17] = 
{
	0,
	0,
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485::get_offset_of_inertia_2(),
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485::get_offset_of_decelerationRate_3(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485::get_offset_of_scrollHandlers_15(),
	GvrPointerScrollInput_tDA3CEE0132364FDF0C4E5A1707327F7075C77485::get_offset_of_scrollingObjects_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[6] = 
{
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_isScrollingX_0(),
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_isScrollingY_1(),
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_initScroll_2(),
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_lastScroll_3(),
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_scrollVelocity_4(),
	ScrollInfo_t0B9CDE332B831B35646C30041820D159435C3E1C::get_offset_of_scrollSettings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[2] = 
{
	GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB::get_offset_of_inertiaOverride_4(),
	GvrScrollSettings_t78386FFD18A2EBCFB97B8788386ECAD431C494BB::get_offset_of_decelerationRateOverride_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A), -1, sizeof(GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2688[2] = 
{
	GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A_StaticFields::get_offset_of_instance_4(),
	GvrXREventsSubscriber_tE78DCE95D68B7DEDEC3BFCE1CD6DE1B84B07867A::get_offset_of__loadedDeviceName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[7] = 
{
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnPointerClick_4(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnPointerDown_5(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnPointerUp_6(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnPointerEnter_7(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnPointerExit_8(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_OnScroll_9(),
	GvrAllEventsTrigger_tE19B85857831C7B0002AF79DD44739DA855B9272::get_offset_of_listenersAdded_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (TriggerEvent_t65ED04BBD7DF3C12C56C7884EB8FB18AF0D01BA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[1] = 
{
	GvrEventExecutor_t898582E56497D9854114651E438F61B403245CD3::get_offset_of_eventTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (EventDelegate_tA7EE228FD3A939EF5CCB9EE55F0A66907AB18995), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[4] = 
{
	GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA::get_offset_of_vrModeOnly_10(),
	GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA::get_offset_of_scrollInput_11(),
	GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA::get_offset_of_U3CImplU3Ek__BackingField_12(),
	GvrPointerInputModule_t74C62030DE3749C787FB9B93A65492FB50BA6DBA::get_offset_of_U3CEventExecutorU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[9] = 
{
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_U3CModuleControllerU3Ek__BackingField_0(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_U3CEventExecutorU3Ek__BackingField_1(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_U3CScrollInputU3Ek__BackingField_3(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_pointer_5(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_lastPose_6(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_isPointerHovering_7(),
	GvrPointerInputModuleImpl_tAA775CFC6E299454B1CF55E6B80423281A5287AF::get_offset_of_isActive_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (GvrCardboardHelpers_tF9D758761742FA1F97490A6E7AE979CA2CBFC500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GvrEditorEmulator_tF953E2075E00E7B749B688060320DE25681966DD), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
