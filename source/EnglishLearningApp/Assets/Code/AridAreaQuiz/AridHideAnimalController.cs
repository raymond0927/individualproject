﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AridHideAnimalController : MonoBehaviour
{
    public static GameObject obj_Kangaroo;
    public static GameObject obj_TasmanianDevil;
    public static GameObject obj_Koala;
    public static GameObject obj_Ornitorinco;
    public static GameObject obj_Ostrich;
    public static GameObject obj_LizardCollar;
    public static GameObject obj_Scorpion;
    public static GameObject obj_Panther;
    public static GameObject obj_Giraffe;
    public static GameObject obj_Rhinoceros;
    public static GameObject obj_Zebra;

    // Start is called before the first frame update
    void Start()
    {
       obj_Kangaroo = GameObject.Find("KangarooLocation");
       obj_TasmanianDevil = GameObject.Find("TasmanianDevilLocation");
       obj_Koala = GameObject.Find("KoalaLocation");
       obj_Ornitorinco = GameObject.Find("PlatypusLocation");
       obj_Ostrich = GameObject.Find("OstrichLocation");
       obj_LizardCollar = GameObject.Find("LizardCollarLocation");
       obj_Scorpion = GameObject.Find("ScorpionLocation");
       obj_Panther = GameObject.Find("BlackPantherLocation");
       obj_Giraffe = GameObject.Find("GiraffeLocation");
       obj_Rhinoceros = GameObject.Find("RhinocerosLocation");
       obj_Zebra = GameObject.Find("ZebraLocation");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void hideAnimals(String name)
    {
        Debug.Log("name "+ name);
        if (name.Length > 0)
        {
        obj_Kangaroo.SetActive(false);
        obj_TasmanianDevil.SetActive(false);
        obj_Koala.SetActive(false);
        obj_Ornitorinco.SetActive(false);
        obj_Ostrich.SetActive(false);
        obj_LizardCollar.SetActive(false);
        obj_Scorpion.SetActive(false);
        obj_Panther.SetActive(false);
        obj_Giraffe.SetActive(false);
        obj_Rhinoceros.SetActive(false);
        obj_Zebra.SetActive(false);
        }
        else
        {
            obj_Kangaroo.SetActive(true);
            obj_TasmanianDevil.SetActive(true);
            obj_Koala.SetActive(true);
            obj_Ornitorinco.SetActive(true);
            obj_Ostrich.SetActive(true);
            obj_LizardCollar.SetActive(true);
            obj_Scorpion.SetActive(true);
            obj_Panther.SetActive(true);
            obj_Giraffe.SetActive(true);
            obj_Rhinoceros.SetActive(true);
            obj_Zebra.SetActive(true);
        }


        switch (name)
        {
            case "Kangaroo":
                obj_Kangaroo.SetActive(true);
                break;
            case "Tasmanian Devil":
                obj_TasmanianDevil.SetActive(true);
                break;
            case "Koala":
                obj_Koala.SetActive(true);
                break;
            case "Ornitorinco (platypus)":
                obj_Ornitorinco.SetActive(true);
                break;
            case "Ostrich":
                obj_Ostrich.SetActive(true);
                break;
            case "Lizard Collar":
                obj_LizardCollar.SetActive(true);
                break;
            case "Scorpion":
                obj_Scorpion.SetActive(true);
                break;
            case "Black Panther":
                obj_Panther.SetActive(true);
                break;
            case "Giraffe":
                obj_Giraffe.SetActive(true);
                break;
            case "Rhinoceros":
                obj_Rhinoceros.SetActive(true);
                break;
            case "Zebra":
                obj_Zebra.SetActive(true);
                break;
        }
    }

    
}

